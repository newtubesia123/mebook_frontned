import axios from "axios"
import { SERVER } from "../server/server"
import { toast } from "react-toastify";
import Cookies from 'universal-cookie';


export const getAuth = () => JSON.parse(localStorage.getItem('token'))
const cookies = new Cookies();
export const getUserId = async () => {

    let result = await axios.get(`${SERVER}/getUserId`,
        {
            headers: { Authorization: `Bearer ${getAuth()}` }
        })
    // console.log("action", result.data)
    return await result.data
}

export const getUserData = (token) => {
    return async (dispatch, getState) => {
        if (getAuth() || token) {
            dispatch({ type: "CALLING__PROGRESS", payload: 10 })
            axios.get(`${SERVER}/getuserById`,
                {
                    headers: { Authorization: `Bearer ${token ? token : getAuth()}` }
                }).then((response) => {
                    dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                    dispatch({ type: 'NO_ERROR' })
                    dispatch({ type: 'UPDATED_USER_DATA', payload: response.data })
                }).catch((error) => {
                    dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                    dispatch({ type: 'GOT_ERROR' })
                    console.log(error)
                })
        }

    }
}
// update user detail
export const updateUserDetail = (data, id, res) => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        axios.post(`${SERVER}/userDetail`, { userData: data, userId: id })
            .then((response) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'NO_ERROR' })
                dispatch(userProfile(id))
                toast.success(response.data.message, {
                    position: 'top-center',
                    autoClose: 2000,
                    pauseOnHover: false
                })
                res.send({ code: 200 })
                dispatch({ type: 'UPDATED_USER_DETAIL', payload: response.data })
            }).catch((error) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                res.send({ code: 400 })
            })
    }
}


export const writerWork = (process, UserData, res) => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        axios.post(`${SERVER}/work_1/author_work1/${await getUserId()}/?process=${process}`, UserData)
            .then((response) => {
                dispatch({ type: 'NO_ERROR' })
                dispatch(userWorkDetail())
                toast.success(response.data.message, {
                    position: 'top-center',
                    autoClose: 2000,
                    pauseOnHover: false
                });
                res.send({ code: 200 })
                dispatch({ type: 'WRITER_ID', payload: response.data.workId })
            }).catch((error) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'GOT_ERROR' })
                res.send({ code: 400 })
                console.log(error)
            })
    }
}





// path:/CreatorNew1 api integration 

export const getCreatorData = () => {

    return async (dispatch, getState) => {
        if (getAuth()) {
            dispatch({ type: "CALLING__PROGRESS", payload: 10 })
            axios.get(`${SERVER}/admin/getCategory`,
                {
                    headers: { Authorization: `Bearer ${getAuth()}` }
                }).then((response) => {
                    dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                    dispatch({ type: 'NO_ERROR' })
                    dispatch({ type: 'GET_CREATOR_DATA', payload: response.data })
                }).catch((error) => {
                    dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                    dispatch({ type: 'GOT_ERROR' })
                    console.log(error);
                })
        }

    }
}

// get creator sub catergory
export const getSubCategory = (id) => {
    return async (dispatch, getState) => {
        if (id !== 'Music') {
            dispatch({ type: "CALLING__PROGRESS", payload: 10 })
            dispatch({ type: "CALLING_SUBCATEGORY" })
            axios.get(`${SERVER}/admin/getSubCategory?id=${id}`)
                .then((response) => {
                    dispatch({ type: 'NO_ERROR' })
                    dispatch({ type: "GET_SUBCATEGORY", payload: response.data.dummy })
                    setTimeout(() => {
                        dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                    }, 500)
                }).catch((error) => {
                    dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                    dispatch({ type: 'GOT_ERROR' })
                    console.log(error);
                })
        }

    }
}
// get subcategory for music
export const getMusicSubCategory = (id) => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        dispatch({ type: "CALLING_SUBCATEGORY" })
        axios.get(`${SERVER}/admin/getSubCategory?id=${id}`)
            .then((response) => {
                localStorage.setItem("Music", JSON.stringify(response.data.dummy))
                dispatch({ type: 'NO_ERROR' })

                dispatch({ type: "COMPLETE_SUBCATEGORY" })
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            }).catch((error) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'GOT_ERROR' })
                console.log(error);
            })
    }

}

// metaProfile setUp
export const mebookProfile = (UserData) => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        dispatch({ type: 'CALLING_METAPROFILE' })
        axios.post(`${SERVER}/mebookProfile`, UserData,
            {
                headers: { Authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'GET_SUBCATEGORY', payload: false })
                if (response.data.errorCode === 200) {
                    dispatch({ type: 'NO_ERROR' })
                    toast.success(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                    dispatch({ type: 'GETTING_METAPROFILE', payload: response.data })
                } else if (response.data.errorCode === 400) {

                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                    dispatch({ type: 'GETTING_METAPROFILE', payload: response.data })
                }
                else if (response.data.errorCode === 300) {

                    toast.success(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })

                    dispatch({ type: 'GETTING_METAPROFILE', payload: response.data })
                }
                else {
                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}
// Identity SetUp
export const userIdentity = (UserData, callBack) => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        dispatch({ type: 'CALLING_IDENTITY' })
        axios.post(`${SERVER}/userIdentity`, UserData,
            {
                headers: { Authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                dispatch(getUserData())
                callBack()
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                if (response.data.errorCode === 200) {
                    dispatch({ type: 'NO_ERROR' })
                    toast.success(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                    dispatch({ type: 'GETTING_IDENTITY', payload: response.data })
                } else if (response.data.errorCode === 400) {
                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                    dispatch({ type: 'GETTING_IDENTITY', payload: response.data })
                } else {
                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}
// getuserProfile
export const userProfile = (userId) => {
    return async (dispatch, getState) => {
        dispatch({ type: 'CALLING_USERPROFILE' })
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        axios.get(`${SERVER}/users/getProfileDetails/${userId ? userId : 0}`,
            {
                headers: { authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                if (response.data.errorCode === 200) {
                    localStorage.setItem("subCategory", JSON.stringify(response.data.meProfile.subCategory))
                    dispatch({ type: 'NO_ERROR' })
                    dispatch({ type: 'GET_USERPROFILE', payload: response.data })
                } else if (response.data.errorCode === 400) {
                    dispatch({ type: 'GOT_ERROR' })
                    dispatch({ type: 'GET_USERPROFILE', payload: response.data })
                } else {
                    dispatch({ type: 'GOT_ERROR' })
                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log("error", error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}

// getting followers
// user/getFollowers/:id
export const getFollowers = (userId) => {
    return async (dispatch, getState) => {
        axios.get(`${SERVER}/users/getFollowers/${userId ? userId : 0}`,
            {
                headers: { authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                if (response.data.errorCode === 200) {
                    dispatch({ type: 'NO_ERROR' })
                    dispatch({ type: 'GET_USERFOLLOWER', payload: response.data })
                    // res.send(response.data.allFollower)
                } else {
                    dispatch({ type: 'GOT_ERROR' })
                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log("error", error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}

// handle follow request
// user/handleFollow/:id

export const handleFollow = (userId,myId) => {
    // myId recieved if I want to update my followings detail just after follow or unfollow anyone by me. I will pass my userId to getFollowers so that i can update my data after follow or unfollow anyone
    // console.log('hey',myId)
    return async (dispatch, getState) => {
        axios.get(`${SERVER}/users/handleFollow/${userId}`,
            {
                headers: { authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                if (response.data.errorCode === 200) {  // follower added
                    // dispatch(getFollowers(userId))
                    dispatch(getFollowers(myId?myId:userId))
                    //      {
                    //     send: (currentFollower) => {
                    //         // cookies.set('currentFollowState', currentFollower, { path: "/" })
                    //     }
                    // }))
                    // console.log("res",response.data)
                    // res.send(response.data.follow)
                } else if (response.data.errorCode === 300) { // follower removed
                    // dispatch(getFollowers(userId))
                    dispatch(getFollowers(myId?myId:userId))
                    //  {
                    //     send: (currentFollower) => {
                    //         // cookies.set('currentFollowState', currentFollower, { path: "/" })
                    //     }
                    // }))
                    // console.log("res",response.data)
                    // res.send(response.data.follow)
                } else {
                    dispatch({ type: 'GOT_ERROR' })
                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log("error", error)
            })
    }
}


// user work detail
export const userWorkDetail = (id) => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        dispatch({ type: 'CALLING_USER_WORK_DETAIL' })
        axios.get(`${SERVER}/work_1/getIndividualWork/${id ? id : await getUserId()}`,
            {
                headers: { Authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'NO_ERROR' })
                if (response.data.errorCode === 200) {
                    dispatch({ type: 'GETTING_USER_WORK_DETAIL', payload: response.data })
                } else {
                    toast.success(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}


export const delUserWork = (id) => {


    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        axios.post(`${SERVER}/work_1/delIndividualWork/${id}`, { body: "body" },
            {
                headers: { Authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'NO_ERROR' })
                if (response.data.errorCode === 200) {
                    dispatch(userWorkDetail())
                    toast.success(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                } else {
                    toast.error("token expire", {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}


// get pitch
export const getPitch = (process, id, call, body, vdo) => {
    // console.log("getPitch",process,id,call,body,vdo)
    return async (dispatch, getState) => {
        if (getAuth() || id) {
            dispatch({ type: "CALLING__PROGRESS", payload: 10 })
            dispatch({ type: "CALLING_PITCH" })
            axios.post(`${SERVER}/users/user_pitch/${id ? id : await getUserId()}/?process=${process}`, { pitch: body, pitchVdo: vdo }
                ,
                {
                    headers: { Authorization: `Bearer ${getAuth()}` }
                }
            ).then((response) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'NO_ERROR' })
                switch (response.data.errorCode) {
                    case 200:
                        toast.success(response.data.message, {
                            position: 'top-center',
                            autoClose: 2000,
                            pauseOnHover: false
                        })
                        dispatch(getPitch('get', false, () => console.log("getting...")))
                        break;
                    case 600:
                        toast.success(response.data.message, {
                            position: 'top-center',
                            autoClose: 2000,
                            pauseOnHover: false
                        })
                        dispatch(getPitch('get', false, () => console.log("getting...")))
                        break;
                    case 300:
                        dispatch({ type: "GET_CREATOR_PITCH", payload: response.data })
                        // call()
                        break;
                    case 404:
                        dispatch({ type: 'NO_PITCH' })
                        // toast.error("no pitch yet")
                        break;
                    default:
                        break;
                }
            }).catch((error) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'GOT_ERROR' })
                console.log(error);
            })
        }

    }
}

// Post Pitch
export const postPitch = (UserData, res) => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        axios.post(`${SERVER}/${await getUserId()}`, UserData)
            .then((response) => {
                dispatch({ type: 'NO_ERROR' })
                alert(response.data.message);
                dispatch({ type: 'WRITTING_PITCH', payload: response.data.workId })
                res.send({ code: 200 })
            }).catch((error) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'GOT_ERROR' })
                res.send({ code: 400 })
                console.log(error)
            })
    }
}

// Dashboard card

export const userDashboardDetail = (token) => {
    // console.log("userDashboardDetail",getAuth())
    // console.log("dashboard")
    return async (dispatch, getState) => {
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        dispatch({ type: 'CALLING_USER_DASHBOARD_DETAIL' })
        axios.get(`${SERVER}/dashboard/mainDashboard`,
            {
                headers: { Authorization: `Bearer ${token ? token : getAuth()}` }
            }).then((response) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
                dispatch({ type: 'NO_ERROR' })
                if (response.data.errorCode === 200) {
                    dispatch({ type: 'GETTING_USER_DASHBOARD_DETAIL', payload: response.data })
                    dispatch({ type: "GETTING_NOTIFICATION_REQ", payload: response.data.notificationData })
                } else {
                    toast.error(response.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                }

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}

// handle work like

export const handleWorkLikes = (userId, workId, likeType, res) => {
    // console.log("dashboard")
    return async (dispatch, getState) => {
        axios.get(`${SERVER}/dashboard/mainDashboard/handleWorkLikes/${userId}?workId=${workId}&type=${likeType}`,
            {
                headers: { authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                if (response.data.errorCode === 200) res.send(200)
                if (response.data.errorCode === 300) res.send(200)

            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}

//  notificationRequest

export const notificationRequest = (debaunceStage, res) => {
    // console.log("dashboard")
    return async (dispatch, getState) => {
        axios.get(`${SERVER}/dashboard/notificationRequest?debaunceStage=${debaunceStage}`,
            {
                headers: { authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                if (!response.data.message.length) res.send(404)
                if (response.data.errorCode === 200) dispatch({ type: "GETTING_NOTIFICATION_REQ", payload: response.data })
                if (response.data.errorCode === 404) dispatch(notificationRequest(0, { send: () => false }))
            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}

//handleNotification

export const handleNotification = (notificationId) => {
    // console.log("dashboard")
    return async (dispatch, getState) => {
        axios.get(`${SERVER}/dashboard/handleNotification/${notificationId}`,
            {
                headers: { authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                if (response.data.errorCode === 200) dispatch(notificationRequest(0, { send: () => false }))
            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}

// global popUp 

export const callPopUp = (PopUPName) => {
    // console.log(signup);
    let obj = { type: "CALL_POPUP", payload: PopUPName }
    return obj
}
// getMessage of chatting
export const getMessage = (senderId, recieverId) => {
    console.log("id", senderId, recieverId)
    return async (dispatch, getState) => {
        axios.post(`${SERVER}/messages/getMsg`,
            {
                from: senderId ? senderId : await getUserId(),
                to: recieverId,
            }).then((response) => {
                console.log("message data", response)
                dispatch({ type: "GET_MESSAGE", payload: response.data })
            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}


// getUserList of chatting
export const getUserList = () => {
    return async (dispatch, getState) => {
        dispatch({ type: "CALL_GET_USER_LIST"})
        axios.get(`${SERVER}/messages/getUserList`,
            {
                headers: { authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                if (response.data.errorCode === 200) {
                    global.USER_LIST = response.data?.data?.getAllUserList
                    dispatch({ type: "GET_USER_LIST", payload: response.data?.data?.getAllUserList })
                }
            }).catch((error) => {
                dispatch({ type: 'GOT_ERROR' })
                console.log(error)
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            })
    }
}