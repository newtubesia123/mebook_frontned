import axios from "axios";
import { SERVER } from "../../server/server";


function getRecognization (user) {
    return (dispatch, getState) => {
        // const {getProfileData:{userData}} = getState()
        dispatch({type:"GET_USER_RECOGNIZATION",payload:user})
    }
}    

function getUserMsg ({ from,to},callback) {
    return async (dispatch, getState) => {
        const {messageSection:
            {userMessage:
                {loading,data}
            }} = getState()
            if(data.length===0)dispatch({type:"CALL_GET_USER_MSG"})
          axios.post(`${SERVER}/messages/getMsg`, {from: from,to: to})
          .then((response) => {
              if(response.data){
                if(response.data.find((individualMsg)=>!individualMsg.fromSelf && !individualMsg.isSeen)){
                    callback()
                }
                dispatch({type:"GET_USER_MSG",payload:response.data})
            }
          }).catch((err)=>{
            console.log(err)
          })
          
    }
}    
   

export {getRecognization,getUserMsg}


