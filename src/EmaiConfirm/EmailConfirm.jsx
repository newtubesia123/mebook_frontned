import { Box, Button, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import emailpic from '../png/email.png'
import mepic from '../mepic.png'
import { useNavigate, useParams } from 'react-router-dom'
import axios from 'axios'
import { SERVER } from '../server/server'
import TaskAltIcon from '@mui/icons-material/TaskAlt';
import { useDispatch } from 'react-redux'
import MeLogo from '../assets/MeLogoMain'
// import React from 'react'

export default function EmailConfirm() {
  const [isVerifiend, setisVerifiend] = useState('none')
  const [email, setEmail] = useState(false)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const param = useParams()
  const handleConfirmation = async () => {
    await axios
      .post(`${SERVER}/verification`, { userId: param.id })
      .then((res) => {
        if (res.data.errorCode == 200) {
          setisVerifiend('block')
          setTimeout(() => {
            sessionStorage.setItem("verification", true);
            navigate("/SignIn");
          }, 2000)

        }
      })
      .catch((err) => console.log(err));
  }
  useEffect(() => {
    if (sessionStorage.getItem("verification")) {
      navigate('/SignIn')
    }
    axios.get(`${SERVER}/getUserEmail/${param.id}`).then((response) => {
      dispatch({ type: 'NO_ERROR' })
      if (response.data?.errorCode === 200) setEmail(response.data?.email?.email)
      if (response.data?.email?.isVerifiend) setisVerifiend('block')
      if (response.data?.errorCode === 404) navigate('/SignIn')
    }).catch((error) => {
      dispatch({ type: 'GOT_ERROR' })
      console.log(error)
      // dispatch({ type: 'ERROR_PERCENTAGE', error }
      // )
    })
  }, [])
  return (
    <Box gap={2} sx={{
      background: 'rgb(0,0,0)',
      background: 'linear-gradient(245deg, rgba(0,0,0,1) 69%, rgba(255,0,0,1) 100%)', color: 'white', display: 'flex', flexDirection: 'column', justifyContent: 'start', paddingTop: '40px', alignItems: 'center', height: '100vh'
    }}>

      {/* <img src={mepic} alt="email" width="10%" /> */}
      <MeLogo/>
      <img src={emailpic} alt="email" width="8%" />

      <Box sx={{ textAlign: 'center', display: isVerifiend }}>
        <Typography variant='h5' style={{ textAlign: 'center' }}>
          <TaskAltIcon sx={{ color: "green", fontSize: '75%' }} /> <br />
          Verified

        </Typography>
        {/* changes */}

      </Box>


      <Typography variant='h4' style={{ textAlign: 'center', fontSize: '75%' }}>
        Verify Your Email Address
      </Typography>
      <Typography variant='h5'>
        You have entered {email ? email : "Incorrect crredential"} as the email address for your account.
      </Typography>
      <Typography variant='h6'>
        please verify this email by clicking the button bellow
      </Typography>
      <Button
        onClick={() => handleConfirmation()}
        variant="contained" sx={{
          padding: '.4rem 3rem', fontSize: '75%', background: 'rgb(14,0,255)',
          background: 'linear-gradient(90deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)', color: 'whitesmoke', animation: 'ease .1s',
          '&:hover': {
            background: 'rgb(14,0,255)',
            background: 'linear-gradient(245deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)', animation: 'ease .1s'
          }
        }}

      >
        Verify
      </Button>
    </Box>
  )
}
