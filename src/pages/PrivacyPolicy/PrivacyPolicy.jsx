import {React, useState} from 'react'
import './PrivacyPolicy.scss'
import Navbar from './Navbar'
import Body from './Body'
// import MeLogo from '../../assets/MeLogoMain'
import { newButton } from "../../Component new/background";
import { useNavigate } from 'react-router-dom'
import Button from "@mui/material/Button";


export default function PrivacyPolicy() {
    const navigate= useNavigate();
    const [index,setIndex]=useState();
  return (
    <div className='main-div'>
      <Button className=' mx-3 my-2' sx={{ ...newButton, fontSize: "75%" }} onClick={() => navigate(-1)} variant="contained">Go Back</Button>
      
      <div className='container-fluid'>
        <div className="row d-flex justify-content-center">
          <div className="col-12 col-sm-12 col-md-11">
             <div className='body'>
               <div className='row'>
                 <div className="col-12 col-sm-12 col-md-12 col-lg-3 relative"><Navbar setIndex={setIndex} index={index}/></div>
                 <div className="col-12 col-sm-12 col-md-12 col-lg-9"><Body index={index}/></div>
               </div>
             </div>
          </div>
        </div>
      </div>
      
    </div>
  )
}
