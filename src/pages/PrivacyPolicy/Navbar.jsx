import React, { useEffect } from 'react'
import './PrivacyPolicy.scss'
import { NavLink } from 'react-router-dom'
import image from "../../mepic.png";
import { colors } from '@mui/material';

export default function Navbar({setIndex,index}) {
    useEffect(()=>{
        setIndex(0)
    },[])
   
  return (
    <div classNameName='Navbar ' style={{width:'100%'}}>
      
      <nav className="navbar navbar-expand-lg navbar-light">
  
  <button className="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

<div className="collapse navbar-collapse " id="navbarSupportedContent">
  <span className='d-flex justify-content-center'>
    <img src={image} alt="/" style={{ width: "40%", height: "40%", margin: "10px 0px" }}  />
    </span>
    <div className='underline'></div>
    <div className='navbody'>
    <ul className="navbar-nav text-start ">
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(0)} className={`${index===0?'actives':'tab'} `}>Terms and conditions</NavLink>
      </li>
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(1)} className={`${index===1?'actives':'tab'}`}>Accounts and membership</NavLink>
      </li>
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(2)} className={`${index===2?'actives':'tab'}`}>User content</NavLink>
      </li>
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(3)} className={`${index===3?'actives':'tab'}`}>Adult content</NavLink>
      </li>
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(4)} className={`${index===4?'actives':'tab'}`}>Billing and payments</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(5)} className={`${index===5?'actives':'tab'}`}>Accuracy of information</NavLink>
      </li>   
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(6)} className={`${index===6?'actives':'tab'}`}>Third party services</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(7)} className={`${index===7?'actives':'tab'}`}>Uptime guarantee</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(8)} className={`${index===8?'actives':'tab'}`}>Backups</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(9)} className={`${index===9?'actives':'tab'}`}>Advertisements</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(10)} className={`${index===10?'actives':'tab'}`}>Links to other resources</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(11)} className={`${index===11?'actives':'tab'}`}>Prohibited uses</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(12)} className={`${index===12?'actives':'tab'}`}>Intellectual property rights</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(13)} className={`${index===13?'actives':'tab'}`}>Disclaimer of warranty</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(14)} className={`${index===14?'actives':'tab'}`}>Limitation of liability</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(15)} className={`${index===15?'actives':'tab'}`}>Indemnification</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(16)} className={`${index===16?'actives':'tab'}`}>Severability</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(17)} className={`${index===17?'actives':'tab'}`}>Dispute resolution</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(18)} className={`${index===18?'actives':'tab'}`}>Changes and amendments</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(19)} className={`${index===19?'actives':'tab'}`}>Acceptance of these terms</NavLink>
      </li> 
      <li className="nav-item">
      <NavLink onClick={()=>setIndex(20)} className={`${index===20?'actives':'tab'}`}>Contacting us</NavLink>
      </li>

    </ul>
    </div>
  </div>
</nav>

    </div>
  )
}
