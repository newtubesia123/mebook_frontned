import React, { useEffect, useState } from 'react'
import './Opportunities.css'
import MyAskList from '../../Component new/MyAskList/MyAskList'
import axios from 'axios'
import { SERVER } from '../../server/server'
import { useParams } from 'react-router-dom'
import pic from '../../mepic.png'
import MeLogo from '../../assets/MeLogoMain'
import BackBtn from '../../components/componentsB/btn/backBtn/BackBtn'

export default function Opportunities() {
    const [data, setData] = useState([])
    const params = useParams()
    // console.log(params)

    const getDetails = () => {
        axios.get(`${SERVER}/my_ask/getUserAllAsk/${params?.userId}`)
            .then((res) => {
                // console.log('response get myAsk', res?.data?.message)
                setData(res?.data?.message)
            })
    }
    useEffect(() => {
        getDetails()
    }, [])

    return (
        <div className='container-fluid bgCss' style={{ minHeight: "100vh" }}>
            <div className='mt-4' style={{ position: "absolute", right: 0, }}>
                <BackBtn style={{ margin: '1rem' }} />
            </div>
            <div className="row d-flex justify-content-center text-white">
                <div className="col-12 col-sm-8 col-md-7 text-center bg-light py-4 my-4 bgchange">
                    {/* <img src={pic} alt="mepic" style={{ width: '30%', height: '40%' }} /> */}
                    <MeLogo />
                    <h3>OPPORTUNITY</h3>
                    <h3>WITH</h3>
                    <h3>{params?.name?.toUpperCase()}</h3>
                </div>
                <div className="col-12 col-sm-10 text-center my-5 ">
                    <MyAskList data={data} userId={params?.userId} noData={"NO OPPORTUNITIES FROM " + params?.name.toUpperCase()} />
                </div>
            </div>
        </div>
    )
}