import { Box, Typography } from '@mui/material'
import React, { useState, useEffect, } from 'react'
import pic from '../../mepic.png'
import Button from "@mui/material/Button";
import { newButton, BackGround } from '../../Component new/background';
import { toast } from 'react-toastify';
import axios from 'axios';
import { SERVER } from '../../server/server';
import { useSelector } from 'react-redux';
import MyAskList from '../../Component new/MyAskList/MyAskList'
import { getUserId } from '../../Action';
import ImageIcon from '@mui/icons-material/Image';
import VideoIcon from '@mui/icons-material/VideoFile';
import VideoFileRoundedIcon from '@mui/icons-material/VideoFileRounded';
import { getImgURL } from '../../assets/getImage';
import TextField from '@mui/material/TextField';
import { useNavigate } from 'react-router-dom';
import LoadingButton from '@mui/lab/LoadingButton';
import MuiAccordian from '../../components/componentsA/muiAccordian/MuiAccordian';
import BackBtn from '../../components/componentsB/btn/backBtn/BackBtn';
import UplProgBar from '../../components/componentsC/uplProgBar/UplProgBar';
import { onUplProg } from '../../assets/common/onUplProg';


const AccordianBackground = {
  ...BackGround,
  color: "white",
  margin: "10px 0 10px 0",
  border: "1px solid white",
};

const MyAsk = () => {
  const [progress, setProgress] = useState(false)
  const [items, setItems] = useState([]);
  const [media, setMedia] = useState({
    file: "",
    filepreview: null,
  });
  const [depend, setDepend] = useState(false)
  const [data, setData] = useState([])
  const [edit, setEdit] = useState(false)
  const [mediaLink, setMediaLink] = useState("")
  const [isEdit, setIsEdit] = useState(false)
  const [currMediaLink, setCurrMediaLink] = useState('');
  const [askValue, setAskValue] = useState({
    askTitle: "",
    askDescription: "",
    askFile: "",
    id: ''
  });
  const saveBtn = { ...newButton, width: "10rem", margin: "20px", fontSize: "80%" };
  const STORE = useSelector((state) => state);
  const userId = STORE.getProfileData?.userData?._id
  const navigate = useNavigate()
  const onChangeHandler = (e) => {
    setAskValue({
      ...askValue,
      [e.target.name]: e.target.value
    })
  }
  // for media upload
  const handleInputChange = (event) => {
    setMedia({
      ...media,
      file: event.target.files[0],
      filepreview: URL.createObjectURL(event.target.files[0]),
    });

  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const askData = {
      askTitle: askValue?.askTitle,
      askDescription: askValue?.askDescription,
      askFile: mediaLink
    }

    if (askValue.askTitle == '' || askValue.askDescription == '') {
      toast.error('Please Provide Ask Tittle And Ask Description')
    }
    else if (askValue?.askDescription?.split(' ')?.length < 15 || askValue?.askDescription?.split(' ')?.length > 250) {
      toast.error('Ask Description Must Be Between 15 To 100 Words')

    } else {
      axios.post(`${SERVER}/my_ask/my_ask/${userId}/?process=Add`, askData)
        .then((response) => {
          setDepend(true)
          toast.success(response.data.message, {
            position: 'top-center',
            autoClose: 2000,
            pauseOnHover: false
          });
          setAskValue({
            askTitle: "",
            askDescription: "",
            askFile: "",
            id: '',
          })
          setMediaLink('')

          setMedia({
            file: "",
            filepreview: null,
          })
        })
        .catch((err) => console.log(err))
    }
  }

  // to update the ask
  const handleUpdate = (e) => {
    e.preventDefault()
    const askData = {
      askTitle: askValue?.askTitle,
      askDescription: askValue?.askDescription,
      askFile: mediaLink,
      id: askValue?.id
    }
    if (askValue.askTitle == '' || askValue.askDescription == '') {
      toast.error('Please Provide Ask Tittle And Ask Description')
    }
    // else if (askValue?.askDescription?.split(' ')?.length < 15 || askValue?.askDescription?.split(' ')?.length > 100) {
      else if (askValue?.askDescription?.split(' ')?.length < 15 ) {
      toast.error('Ask Description Must Be Between 15 To 100 Words')

    }
    else {
      axios.post(`${SERVER}/my_ask/my_ask/${userId}/?process=Edit`, askData)
        .then((response) => {
          setDepend(true)
          toast.success(response.data.message, {
            position: 'top-center',
            autoClose: 2000,
            pauseOnHover: false
          });
          setAskValue({
            askTitle: "",
            askDescription: "",
            askFile: "",
            id: '',
          })
          setIsEdit(false)
          setMedia({
            file: "",
            filepreview: null,
          })
        })
        .catch((err) => console.log(err))
    }

  }
  // myAsk list 
  const getDetails = async () => {
    axios.get(`${SERVER}/my_ask/getUserAllAsk/${await getUserId()}`)
      .then((res) => {
        setData(res?.data?.message)
      })
  }
  useEffect(() => {
    getDetails()
    setDepend(false)
    window.scrollTo(0, 0)
  }, [depend])




  const handleImage = () => {
    setProgress(true)
    const formdata = new FormData();
    formdata.append("profiles", media.file);



    axios
      .post(
        `${SERVER}/coverPicture`, formdata, { onUploadProgress: onUplProg }
      )
      .then((res) => {
        setProgress(false)
        toast.success(res?.data?.success, { autoClose: 2000 })
        setMediaLink(res?.data?.file)

      })
      .catch((error) => {
        console.log('error', error)
        setProgress(false)
        toast.error('Something Error', { autoClose: 2000 })

      })

  }




  //  accordian objects
  const myask = [
    {
      question: "Why Create “An Ask”?",
      answer: "First and foremost, you have to determine for yourself what you want. If you want success, then you must focus on what success means to you, because it does not mean the same thing to every person, and many have realized that success is not always specifically about money. As a creator, sometimes success is more related to recognition, or more simply, appreciation, which lives beyond our mortal lives. Yet sometimes success is the financial reward for all the hard work and lonely hours we put in to take an idea from a sketch to a fully completed work.",
    },
    {
      question: "Ask the Universe... And You Shall Receive",
      answer: " You cannot expect specific results without taking the time and effort to create a specific ask, which is more than saying, “I want to be rich [or famous].” If you are sharing your work with the universe, have a clear intent about how you would like it to be received, and by whom. Be reasonable in your request, put it out there and let it go.Then you will have to give it time and make adjustments as might be necessary Listen to feedback from your audience, but above all, trust your intuition, your vision. Your “Pitch” tells the universe who you are, what you do and what makes you unique, why you do what you do and where to find your work, but your “Ask” will specifically share how your friends, collaborators and audience can support you to become an integral part of your success. If you have created a campaign for sales, you can ask followers to buy your product from a specific distribution channel within a specific time range, you can ask for online reviews or you can ask your audience to share your specific message out to fifty reliable friends. As creators, be creative about your “Ask,” and share your success stories to pay your success forward."
    }
  ]

  return (

    <div className='row justify-content-center d-flex flex-column align-items-center' style={{ background: '#000000', opacity: progress ? 0.8 : 1, minHeight: '100vh', margin: '0', padding: '0', gap: { xs: '20rem', sm: '10rem', md: '1rem' } }}>
      {
        progress ?
          <UplProgBar variant="determinate" />
          : ''
      }

      <div className='ml-auto mt-5'>

        <BackBtn style={{ margin: '1rem' }} />
      </div>
      <Box sx={{ width: { xs: '10rem', sm: '30rem', md: '15%' }, height: { xs: '20rem', sm: '30rem', md: '' }, my: 5 }}>
        <img src={pic} alt="mepic" style={{ width: '10rem', height: '10rem' }} />
      </Box>


      <Box className="col-md-10 col-sm-12 justify-content-center align-items-center d-flex flex-column" sx={{ gap: { xs: '10rem', sm: '10rem', md: '1rem' } }}>
        <Box className="mainRow" sx={{ height: '18rem', width: '100%', gap: '5rem', display: 'flex', flexDirection: { xs: 'column', sm: 'column', md: 'row' }, justifyContent: { xs: 'center', sm: 'center', md: 'flex-start' }, alignItems: { xs: 'center', sm: 'center', md: 'flex-start' } }}>







          <Box className="video" sx={{
            width: { xs: '100%', sm: '100%', md: '60%' },
            height: '100%',
            boxShadow: "black 5px 5px 10px 5px",
            border: "none",
            borderRadius: "10px",
            outline: "none",
            background: '#fff'
          }}>
            <input
              id="file-input"
              accept="video/*"
              type="file"
              style={{ display: "none" }}
              onChange={handleInputChange}
              multiple
            />
            {
              askValue?.askFile == "" ?
                <>
                  {
                    media?.filepreview == null ?
                      <>
                        {/* <label for="file-input" style={{
                          width: '100%',
                          height: '100%', display: "flex", justifyContent: "center", alignItems: "center"
                        }}>
                          <VideoFileRoundedIcon color="action" sx={{ fontSize: '70px', backgroundColor: '#eee', margin: '150px 90px' }} />
                          <Typography sx={{ margin: '-150px auto' }} color="text.secondary">Add a Video</Typography>
                        </label> */}
                        <label for="file-input" style={{
                          width: '100%',
                          height: '100%', display: "flex", justifyContent:"space-evenly", alignItems: "center",cursor:'pointer'
                        }}>
                          <VideoFileRoundedIcon color="action" sx={{ fontSize: '4rem', margin: '' }} />
                          <Typography sx={{ margin: '' }} color="text.secondary">Add a Video</Typography>
                        </label>
                      </>
                      :
                      <>
                        <div style={{ width: '100%', height: '100%', position: 'relative', }}>
                          <embed scrolling="no" src={media?.filepreview} style={{ height: "100%", width: "100%", objectFit: '', }} />
                          <label style={{}}>

                            {/* <Button onClick={handleImage} variant='contained' sx={{ ...newButton, padding: '10px 53px', color: 'white' }}>
                            {uploaded ? 'uploading' : 'Click to Upload'}
                            <ImageIcon color='white' /></Button> */}

                            <LoadingButton
                              sx={{ ...newButton, padding: '10px 53px', color: 'white' }}
                              size="small"
                              onClick={handleImage}
                              loading={progress}
                              loadingIndicator="Uploading…"
                              variant="outlined"

                            >
                              <span>Click To Upload</span>
                            </LoadingButton>
                          </label>

                          <label for="file-input" style={{ zIndex: 2, position: '', color: 'white', height: '100%', width: '100%', background: 'none', position: 'absolute', right: 20 }}>
                            {/* <ImageIcon sx={{ fontSize: '30px', margin: '8px auto' }} color='white' /> */}
                          </label>

                        </div>



                      </>
                  }
                </>
                : <div style={{ width: '100%', height: '100%', position: 'relative', }}>
                  <embed scrolling="no"

                    src={
                      media?.filepreview == null ?
                        currMediaLink
                        :
                        media?.filepreview
                    }

                    style={{ height: "100%", width: "100%", objectFit: '', }} />
                  <label style={{}}>
                    <Button onClick={handleImage} variant='contained' sx={{ ...newButton, padding: '10px 53px', color: 'white' }}>
                      {progress ? 'uploading' : 'Click to Update'}
                      <ImageIcon color='white' /></Button>


                  </label>

                  <label for="file-input" style={{ zIndex: 2, position: '', color: 'white', height: '100%', width: '100%', background: 'none', position: 'absolute', right: 20 }}>
                    {/* <ImageIcon sx={{ fontSize: '30px', margin: '8px auto' }} color='white' /> */}
                  </label>
                </div>
            }

          </Box>
          <Box className="input" sx={{
            width: { xs: '100%', sm: '100%', md: '40%' },
            height: '100%',
            boxShadow: "black 5px 5px 10px 5px",
            border: "none",
            borderRadius: "10px",
            outline: "none",
            background: '#fff'
          }}>

            <div className='d-flex flex-column justify-content-evenly  ' style={{ height: '100%', width: '100%', gap: '1rem', background: 'black' }}>

              {/* <input
                style={{
                  width: '100%', height:{xs:'100%',sm:'100%',md:'40%'},
                  padding: "5px",
                  border: "none",
                  borderRadius: "10px",
                  outline: "none",
                  fontSize: "100%"
                }}
                onChange={onChangeHandler}
                name="askTitle" type="text"
                value={askValue.askTitle}
                placeholder="Enter Your Ask Title" /> */}
              <TextField id="outlined-basic"
                placeholder="Enter Your Ask Title" variant="outlined"
                sx={{
                  width: '100%', height: { xs: '50%', sm: '50%', md: '20%' },
                  // padding: "5px",
                  border: "none",
                  borderRadius: "10px",
                  outline: "none",
                  fontSize: "100%", background: 'white'
                }}
                onChange={onChangeHandler}
                name="askTitle" type="text"
                value={askValue.askTitle} />
              <TextField
                // id="outlined-basic"
                placeholder="Enter Your Ask Description"
                id="outlined-multiline-static"
                multiline
                rows={8}
                sx={{
                  width: '100%',
                  height: '80%',
                  // padding: "5px",
                  border: "none",
                  borderRadius: "10px",
                  outline: "none",
                  fontSize: "100%",
                  background: 'white',


                }}
                type='text'
                name="askDescription"
                value={askValue.askDescription}
                onChange={onChangeHandler}
              />
            </div>
          </Box>
        </Box>

        <Box className="btnDiv " sx={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: { xs: 'center', sm: 'center', md: 'flex-end' } }}>
          <div className="" style={{ width: 'auto' }}>
            {
              !isEdit ?
                <Button
                  variant="contained"
                  size="large"
                  sx={saveBtn}
                  onClick={handleSubmit}
                >
                  Submit Ask
                </Button>
                :
                <Button
                  variant="contained"
                  size="large"
                  sx={saveBtn}
                  onClick={handleUpdate}
                >
                  Update Ask
                </Button>
            }


          </div>
        </Box>
        <div>

          {
            myask?.map((item, index) => (

              <MuiAccordian key={index} accBg={AccordianBackground} iconColor={{ color: 'white' }} accSummary={item?.question} accDetails={item?.answer} />




              // <Accordion key={index} sx={AccordianBackground}>
              //   <AccordionSummary
              //     expandIcon={<ExpandMoreIcon sx={{ color: "#fff" }} />}
              //     aria-controls="panel1a-content"
              //     id="panel1a-header"
              //   >
              //     <Typography>{item?.question}</Typography>
              //   </AccordionSummary>
              //   <AccordionDetails>
              //     <Typography>{item?.answer}</Typography>
              //   </AccordionDetails>
              // </Accordion>
            ))}
        </div>
        {/* <embed src={getImgURL({currMediaLink})}/> */}
        <MyAskList data={data} setDepend={setDepend} setEdit={setEdit} setAskValue={setAskValue} askValue={askValue} noData={"ADD OPPORTUTINIES"} setIsEdit={setIsEdit} isEdit={isEdit} setCurrMediaLink={setCurrMediaLink} />
      </Box>

    </div>


  )
}
export default MyAsk

