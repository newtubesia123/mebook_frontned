import React, { useEffect, useState } from 'react'
import './AboutMe.css'
import CircularProgress from "@mui/material/CircularProgress";
import { circularProgressbarStyle } from "../../assets/common/theme";
import { Box, Button, Typography } from "@mui/material";
import { useDispatch, useSelector } from 'react-redux';
import { BackGround, newButton } from '../../Component new/background';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import FingerprintIcon from '@mui/icons-material/Fingerprint';
import BookmarkAddedIcon from '@mui/icons-material/BookmarkAdded';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import ChromeReaderModeIcon from '@mui/icons-material/ChromeReaderMode';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import ShareIcon from '@mui/icons-material/Share';
import WorkIcon from '@mui/icons-material/Work';
import Fingerprint from '../../dashboardComponent/Fingerprint';
import Badge from '@mui/material/Badge';
import Following from '../../dashboardComponent/Following';
import Followers from '../../dashboardComponent/Followers';
import WorkLikes from '../../dashboardComponent/WorkLikes';
import PropTypes from 'prop-types';
import { getAuth, userProfile, userWorkDetail } from '../../Action';
import axios from 'axios';
import { SERVER } from '../../server/server';
import { useNavigate, useParams } from 'react-router-dom';
// import { ActivePlan, NoPlan } from '../../dashboardComponent/ActivePlan';

import { ActivePlan, } from '../../dashboardComponent/ActivePlan';
import ShareId from '../../dashboardComponent/ShareId';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const AboutMe = () => {
  const STORE = useSelector((state) => state);
  const navigate = useNavigate()
  const [value, setValue] = useState(0);
  const [followersData, setFollowersData] = useState([]);
  const [followingData, setFollowingData] = useState([]);
  const params = useParams();
  const dispatch = useDispatch();
  const fPBatch = STORE.userProfile.userProfile?.meProfile?.subCategory &&
    STORE.userProfile.userProfile?.meProfile?.subCategory
      .length > 0 ? STORE.userProfile.userProfile?.meProfile?.subCategory.length : "0"

  const activePlan = STORE?.userDashboardDetail?.userWorkDetail?.subscriptionType?.isSubscribed
  // console.log("active plan: " + activePlan);


  const handleChange = (event, newValue) => {
    // console.log(newValue)
    setValue(newValue);
  };
  const AccordianBackground = {
    ...BackGround,
    color: "white",
    margin: "10px 0 10px 0",
    border: "1px solid white",
  };
  const singleTabStyle = { fontSize: '1.2rem', color: "#1C6DD0" }
  const iconStyle = { fontSize: '1.2rem' }


  useEffect(() => {
    window.scroll(0, 0)
    dispatch(userWorkDetail());
    dispatch(userProfile());

    axios.get(`${SERVER}/users/handleFollowers`,
      {
        headers: { authorization: `Bearer ${getAuth()}` }
      }).then((res) => {
        // console.log("followers data", res.data);
        // console.log("followers list data", res?.data?.allFollowers);
        // console.log("following data", res.data.allFollowing);
        setFollowersData(res?.data?.allFollowers);
        setFollowingData(res?.data?.allFollowing);
      })

      .catch((err) =>
        console.log(err)
      )
  }, [params]);

  // console.log('followersData', followersData)
  // console.log('followingData', followingData)




  return (
    <>
      <Box sx={{
        display: "flex", justifyContent: "center", flexDirection: { xs: "column", sm: "column" }, alignItems: "center",
        background: 'black', minHeight: '100vh', margin: '0', minWidth: "100wh"
      }}>
        <div className='ml-auto mt-5'>
          <Button sx={{ m: 2, ...newButton, fontSize: "75%" }} onClick={() => navigate(-1)} variant="contained">Go Back</Button>
        </div>
        <div className='my-4 col-md-10 col-sm-10 d-flex flex-column align-items-center' style={{ background: '#181823' }}>
          {/* <Box sx={{ bgcolor: 'background.paper', display: 'flex', flexDirection:{xs:"column", sm:"column"}, height:450, minWidth:"100wh"  }}> */}
          <Tabs
            // orientation="vertical"
            aria-label="scrollable force tabs example"
            value={value}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons
            indicatorColor="primary"
            allowScrollButtonsMobile
            // aria-label="vertical force tabs example"
            selectionFollowsFocus
            sx={{ background: '#181823', padding: ".5rem 0rem", color: '#00E7FF', width: { xs: '350px', sm: '500px', md: 'auto' } }}

          >
            <Tab className='DashboardTab' label="FingerPrint" sx={singleTabStyle}
              icon={
                <Badge color="secondary" badgeContent={fPBatch} max={100}>
                  <FingerprintIcon />
                </Badge>
              }
              {...a11yProps(0)}
            />
            <Tab className='DashboardTab' label="Following" sx={singleTabStyle}
              icon={
                <Badge color="secondary" badgeContent={followingData?.length > 0 ? followingData?.length : '0'} max={50}>
                  <BookmarkAddedIcon sx={iconStyle} />
                </Badge>
              }
              {...a11yProps(1)}
            />
            <Tab className='DashboardTab' label="Followers" sx={singleTabStyle}
              icon={
                <Badge color="secondary" badgeContent={followersData?.length > 0 ? followersData?.length : '0'} max={50}>

                  <BookmarkIcon sx={iconStyle} />
                </Badge>
              }
              {...a11yProps(2)}
            />
            <Tab className='DashboardTab' label="Work Samples" sx={singleTabStyle}
              icon={
                <Badge color="secondary" badgeContent={STORE.userWorkDetail?.userWorkDetail?.message?.length > 0 ? STORE.userWorkDetail?.userWorkDetail?.message?.length : '0'} max={50}>
                  <ChromeReaderModeIcon sx={iconStyle} />
                </Badge>
              } />
            <Tab className='DashboardTab' label="My Account" sx={singleTabStyle} icon={<MonetizationOnIcon sx={iconStyle} />} />
            <Tab className='DashboardTab' label="Share MeBookMeta" sx={singleTabStyle} icon={<ShareIcon sx={iconStyle} />} />
            {/* <Tab className='DashboardTab' label="Hire Me" sx={singleTabStyle} icon={<WorkIcon sx={iconStyle} />} /> */}




            {/* <Tab label="Opportunities" sx={singleTabStyle} icon={<CorporateFareIcon sx={iconStyle} />} />
              <Tab label="MeBookMeta" sx={singleTabStyle} icon={<MePic sx={iconStyle} />} /> */}
          </Tabs>

          {/* </Box> */}
          <>
            <TabPanel value={value} index={0} sx={{ padding: 0, }}>
              <Fingerprint />
            </TabPanel>

            <TabPanel value={value} index={1}>

              <Following data={followingData} />

            </TabPanel>

            <TabPanel value={value} index={2}>
              <Followers data={followersData} />
            </TabPanel>

            <TabPanel value={value} index={3}>
              <WorkLikes />
            </TabPanel>

            <TabPanel value={value} index={4}>
              {activePlan === true ? <ActivePlan /> : "No Plan"}

            </TabPanel>
            <TabPanel value={value} index={5}>
              <ShareId/>
            </TabPanel>
          </>
        </div>
      </Box>

    </>
  )
}

export default AboutMe