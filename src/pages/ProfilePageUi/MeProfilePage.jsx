import React, { useEffect, useState } from 'react'
import './MeProfilePage.css'
import MuiAccordian from '../../components/componentsA/muiAccordian/MuiAccordian'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { callPopUp, getAuth, getFollowers, getPitch, handleFollow, userProfile, userWorkDetail } from '../../Action'
import { CLIENTSERVER, SERVER } from '../../server/server'
import { QRCodeCanvas } from 'qrcode.react'
import logo from '../../mepic.png'
import FacebookIcon from "@mui/icons-material/Facebook";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import InstagramIcon from "@mui/icons-material/Instagram";
import PinterestIcon from "@mui/icons-material/Pinterest";
import TwitterIcon from "@mui/icons-material/Twitter";
import YouTubeIcon from "@mui/icons-material/YouTube";
import RedditIcon from "@mui/icons-material/Reddit";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import TelegramIcon from "@mui/icons-material/Telegram";
import SettingsIcon from '@mui/icons-material/Settings';
import MeCircularProgress from '../../components/componentsC/meCircularProgress/MeCircularProgress'
import SignInModal from '../../SignInModal'
import EditIcon from '@mui/icons-material/Edit';
import axios from 'axios'
import { onUplProg } from '../../assets/common/onUplProg'
import UplProgBar from '../../components/componentsC/uplProgBar/UplProgBar'
import { toast } from 'react-toastify'
import Tooltip from '@mui/material/Tooltip';
import IconButton from "@mui/material/IconButton";
import NewBtn from '../../components/componentsB/btn/newBtn/NewBtn'
import { getRecognization } from '../../Action/messages/actions'
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
const MeProfilePage = () => {
  const dispatch = useDispatch()
  const STORE = useSelector((state) => state)
 




  const params = useParams();
  const navigate = useNavigate();
  const { username, userId } = params;
  const [profData, setProfData] = useState({})
  const [followersData, setFollowersData] = useState('')
  const [ModalSignin, setModalSignin] = useState(false);
  const [progress, setProgress] = useState(false);

  const [qrUrl, setQrUrl] = useState('')

  function handleMessage () {
    dispatch(getRecognization({_id:userId,email:"dummy@gmail.com",name:username,pdf:profData.profPic}))
    global.ACTIVE_USER = userId
    navigate('/MainDashboard/Chat')
  }
  function getFolData() {
    if (getAuth()) {
      axios.get(`${SERVER}/users/handleFollowers`,
        {
          headers: { authorization: `Bearer ${getAuth()}` }
        }).then((res) => {
          // console.log("res for fol and folw", res?.data)
          // setProfData({...profData,allFollowers:res?.data?.allFollowers,allFollowings:res?.data?.allFollowing})
          // console.log("followers resposne", res?.data?.allFollowers)
          // console.log("followings response", res?.data?.allFollowing)
          /// api res 
          setFollowersData(res?.data);
          // setFollowingData(res?.data?.allFollowing);
          // for search res
          // setSearchFollowData(res?.data?.allFollowers);
          // setSearchFollowingData(res?.data?.allFollowing);
        })
        .catch((err) =>
          console.log(err)
        )
    }
  }
  useEffect(() => {
    dispatch(userProfile(userId));
    dispatch(getPitch("get", userId));
    dispatch(userWorkDetail(userId));
    dispatch(getFollowers(userId))
    setQrUrl(`${CLIENTSERVER}/Profile/${username}/${userId}`)
    getFolData()
  }, [userId])
  useEffect(() => {
    getFolData()

   
  },[profData?.following])
  useEffect(() => {
    setProfData({
      ...profData,
      profPic: STORE?.userProfile?.userProfile?.meProfile?.userId?.pdf,
      backPic: STORE?.userProfile?.userProfile?.meProfile?.userId?.backGroundImage,
      name: STORE?.userProfile?.userProfile?.meProfile?.userId?.name,
      position: STORE?.userProfile?.userProfile?.meIdentity?.position,
      status: STORE?.userProfile?.userProfile?.meIdentity?.status,
      category: STORE?.userProfile?.userProfile?.meProfile?.category,
      followers: STORE?.userProfile?.userFollower?.allFollower,
      following: STORE?.userProfile?.userFollower?.allFollowing,
      allFollowers: followersData?.allFollowers,
      allFollowings: followersData?.allFollowing,
      likes:STORE.userProfile.userFollower?.totalLikes ,
      fpTags: STORE?.userProfile?.userProfile?.meProfile?.subCategory,
      birthPlace: STORE?.userProfile?.userProfile?.meProfile?.userId?.birthPlace,
      cAdd: STORE?.userProfile?.userProfile?.meProfile?.userId?.currentAddress,
      homeTown: STORE?.userProfile?.userProfile?.meProfile?.userId?.homeTown,
      college: STORE?.userProfile?.userProfile?.meProfile?.userId?.college,
      schooling: STORE?.userProfile?.userProfile?.meProfile?.userId?.schooling,
      workSamples: '',
      link1: STORE?.userProfile?.userProfile?.meProfile?.userId?.link1,
      link2: STORE?.userProfile?.userProfile?.meProfile?.userId?.link2,
      link3: STORE?.userProfile?.userProfile?.meProfile?.userId?.link3,
      link4: STORE?.userProfile?.userProfile?.meProfile?.userId?.link4,
      link5: STORE?.userProfile?.userProfile?.meProfile?.userId?.link5,
    })
  }, [STORE, followersData])
  const bgUpload = async (e) => {
    setProgress(true)
    let token = JSON.parse(localStorage.getItem("token"));
    const formData = new FormData();
    formData.append("profiles", e.target.files[0]);
    await axios
      .post(`${SERVER}/imageupload/?picType=userBackgroundImage`, formData, {
        headers: { Authorization: `Bearer ${token}` }, onUploadProgress: onUplProg
      })
      .then((response) => {
        setProgress(false)
        if (response.data.isSuccess) {
          dispatch(userProfile(userId));
          dispatch(userWorkDetail(userId));
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "colored",
          });
        }
      })
      .catch((error) => {
        setProgress(false)
        console.log('error', error)
        toast.error('Something Error', { autoClose: 2000, position: 'top-right' })
      })
  };
  const handleWork = () => {

    localStorage.getItem('token') ? (
      userId === STORE.getProfileData?.userData?._id
        ? navigate(`/MainDashboard/WriterStoryDetails`)
        : navigate(`/MainDashboard/WriterStoryDetails/${userId}`)
    )
      : navigate(`/WriterStoryDetails/${userId}`)

  }
  const handleFollowBtn = () => {
    localStorage.getItem('token') ?
      dispatch(handleFollow(userId)) :
      setModalSignin(true)
  }
  const handleMyAsk = () => {
    localStorage.getItem('token') ?
      (
        userId == STORE?.getProfileData?.userData?._id ?
          navigate(`/MainDashboard/MyAsk`)
          :
          navigate(`/MainDashboard/opportunities/${username}/${userId}`)
      )
      :
      setModalSignin(true)
  }
  const handleMyPitch = () => {
    dispatch(callPopUp({ type: 'ShowPitch' }))
  }
  const ShowFollowers = (query) => {
    // allFollowers & allFollowing is not in use because in callpopup we already get these data through api so no need to replace it or worried about it. But remember query is in use.
    if (localStorage.getItem('token') && userId === STORE?.getProfileData?.userData?._id) {
      dispatch(callPopUp({ type: 'ShowFollowers', data: { allFollowers: profData?.allFollowers, allFollowings: profData?.allFollowings, query: query } }))
    }
  }
  const showFPTags = () => {
    dispatch(callPopUp({ type: 'ShowFPTags', data: { fptags: profData?.fpTags } }))
  }
  const qrCode = (
    <QRCodeCanvas
      id="qrCode"
      value={qrUrl}
      size={220}
      bgColor={"#ffff"}
      level={"H"}
      imageSettings={{
        src: logo,
        x: undefined,
        y: undefined,
        height: 40,
        width: 40,
        excavate: true,
      }}
    />
  );
  const socialHandler = {
    Facebook: <FacebookIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    Twitter: <TwitterIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    Instagram: <InstagramIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    Pinterest: <PinterestIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    LinkedIn: <LinkedInIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    Youtube: <YouTubeIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    Reddit: <RedditIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    WhatsApp: <WhatsAppIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    Telegram: <TelegramIcon className='prof_social' style={{ fontSize: '2rem' }} />,
    0: "link1",
    1: "link2",
    2: "link3",
  };
  const getLink = (a) => {
    let b = {
      0: `//${profData?.link1}`,
      1: `//${profData?.link2}`,
      2: `//${profData?.link3}`,
      3: `//${profData?.link4}`,
      4: `//${profData?.link5}`,
    };
    return b[a];
  };
  // function isMyProfile() {
  //   if (userId === STORE?.getProfileData?.userData?._id) {
  //     return true
  //   } else return false
  // }

  return (
    <>

      {STORE.userProfile.loading ?
        <div className='col-lg-12 col-md-12 p-0 d-flex align-items-center justify-content-center bg-dark ' style={{height:'100vh' }}>
          <MeCircularProgress />
        </div>
        :

        <div className='col-md-12 d-flex flex-column prof_main ' style={{ backgroundImage: `url(${profData?.backPic})`, }}>


          <div className='col-lg-11 col-md-11 p-0 profile_card' style={{}}>

            <div className='col-md-12 image_container m-0 p-0' style={{}}>
              <img style={{}} src={profData?.backPic} />


              {userId === STORE?.getProfileData?.userData?._id ?
                <div className='d-flex align-items-center prof_dashboard_btn '>

                  <NewBtn onClick={() => navigate(`/MainDashboard/AboutMe`)} text={'DASHBOARD'} className={'new_prof_btn dash_btn'} comp={<SettingsIcon sx={{ marginLeft: '5px', fontSize: '.8rem' }} />} />


                  {/* <button className="new_prof_btn" style={{ }}
                  onClick={() => navigate(`/MainDashboard/AboutMe`)}
                >
                  DASHBOARD
                  <SettingsIcon sx={{ marginLeft: '5px', fontSize: '.8rem' }} />
                </button> */}
                </div>
                : ""
              }


              {userId === STORE?.getProfileData?.userData?._id ?
                <div className='bg_edit_icon_div' style={{}}>
                  <Tooltip title='Edit Background'>

                    <IconButton
                      onChange={bgUpload}
                      // onClick={() => setType("userBackgroundImage")}
                      color="primary"
                      aria-label="upload picture"
                      component="label"
                    >
                      <input hidden accept="image/*" type="file" />
                      <EditIcon className='bg_edit_icon' sx={{}} />
                    </IconButton>
                  </Tooltip>
                </div>
                : ""
              }


              <div className='d-flex align-items-center col-lg-5 col-md-5 col-sm-12 p-0 fol_msg_work' style={{}}>
                {userId !== STORE?.getProfileData?.userData?._id ?
                  <>
                    {/* As Message work is in progress so below button is commented */}

                    {/* <NewBtn onClick={handleMessage} text={'Message'} className={'new_prof_btn prof_msg_btn'} /> */}




                    {/* <button className="col-4 new_prof_btn" style={{}}>
                    Message
                  </button> */}


                    <NewBtn onClick={handleFollowBtn} text={STORE?.userProfile?.userFollower?.isFollow ? 'Following' : 'Follow'} className={'new_prof_btn prof_msg_btn'} comp={STORE?.userProfile?.userFollower?.isFollow ?<CheckCircleIcon sx={{ marginLeft: '5px', fontSize: '1rem' }} />:''} style={{ background: STORE?.userProfile?.userFollower?.isFollow ? 'blue' : 'white', color: STORE?.userProfile?.userFollower?.isFollow ? 'white' : 'black' }} />

                    {/* <button className="new_prof_btn" style={{ background: STORE?.userProfile?.userFollower?.isFollow ? 'blue' : 'white', color: STORE?.userProfile?.userFollower?.isFollow ? 'white' : 'black' }}
                    onClick={handleFollowBtn}
                  >
                    {STORE?.userProfile?.userFollower?.isFollow ? 'Following' : 'Follow'}
                  </button> */}

                  </>
                  : ""
                }

                <NewBtn onClick={handleWork} text={'Work Sample'} className={'new_prof_btn prof_msg_btn'} />

                {/* <button className="new_prof_btn" style={{}}
                onClick={handleWork}
              >
                Work Sample
              </button> */}
              </div>
            </div>


            <div className='col-md-12 d-flex flex-wrap m-0 p-0 data_container ' style={{ height: 'auto', minWidth: '100%', borderBottomRightRadius: '25px', borderBottomLeftRadius: '25px', }}>


              <div className='col-lg-4 col-md-4 col-sm-12 p-0 prof_container_left' style={{}}>
                <img style={{}} src={profData?.profPic ? profData?.profPic : 'https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png'} />


                <div className='d-flex flex-column align-items-center' style={{
                  height: '100%', boxShadow: '8px 8px 20px 0px rgba(0, 0, 0, 0.25)'
                  , justifyContent: 'flex-end',
                }}>
                  <div className='d-flex flex-column align-items-center' style={{ height: '30rem', width: '100%', justifyContent: 'center' }}>
                    <div style={{ width: '100%', height: '9rem' }}></div>
                    <div className='d-flex flex-column align-items-center' style={{ height: '6rem', width: '100%', justifyContent: 'space-evenly' }}>
                      <h6 className='prof_name' style={{}}>{profData?.name}</h6>
                      <h6 className='prof_fp'>MEBOOKMETA FINGERPRINT</h6>
                      <h6 className='prof_cat'>{profData?.position}, {profData?.status}, {profData?.category}</h6>
                    </div>

                    <div className='d-flex align-items-center' style={{ height: '4rem', width: '100%', justifyContent: 'center', gap: '2rem' }}>

                      <NewBtn onClick={handleMyAsk} text={'My Ask'} className={'new_prof_btn prof_msg_btn'} />
                      <NewBtn onClick={handleMyPitch} text={'My Pitch'} className={'new_prof_btn prof_msg_btn'} />
                      {/* <button className="new_prof_btn" style={{ background: 'white', borderRadius: '20px', width: '30%', padding: '5px 15px', outline: 'none', border: 'none' }}
                      onClick={handleMyAsk}
                    >
                      My Ask
                    </button> */}


                      {/* <button className="new_prof_btn" style={{ background: 'white', borderRadius: '20px', width: '30%', padding: '5px 15px', outline: 'none', border: 'none' }}
                      onClick={handleMyPitch}
                    >
                      My Pitch
                    </button> */}
                    </div>
                    <div className="d-flex align-items-center  " style={{ height: '4rem', width: '90%', flexWrap: 'wrap', justifyContent: 'space-evenly' }}>
                      {STORE.userProfile?.userProfile?.meProfile?.userId?.socialMedia?.map(
                        (icon_name, index) => (
                          // <a href="https://www.instagram.com/zeniii.02/" className="profile-card-social__item instagram" target="_blank">

                          // </a> 
                          <a
                            key={"link" + index}
                            href={getLink(index)}
                            target="_blank"
                            className=""
                            style={{ padding: '5px', color: 'white', }}
                          >
                            {socialHandler[icon_name]}
                          </a>
                        )
                      )}
                    </div>
                    <div className='d-flex align-items-center justify-content-center ' style={{ height: '7rem', width: '100%', borderTop: '2px solid white' }}>
                      <div className='btn_like d-flex align-items-center justify-content-center' style={{ minWidth: '34%', minHeight: '7rem', borderRight: '3px solid white', textAlign: 'center', cursor: 'pointer' }}
                        onClick={() => ShowFollowers(1)}
                      >
                        <h6 style={{ lineHeight: '1.5' }}> Following<br />{profData?.following}</h6>

                      </div>
                      <div className='btn_like d-flex align-items-center justify-content-center' style={{ minWidth: '34%', minHeight: '7rem', borderRight: '3px solid white', textAlign: 'center', cursor: 'pointer' }}
                        onClick={() => ShowFollowers(2)}
                      >
                        
                        <h6 style={{ lineHeight: '1.5' }}> {profData?.followers > 1 ? 'Followers' : 'Follower'}<br />{profData?.followers}</h6>

                      </div>
                      <div className='btn_like d-flex align-items-center justify-content-center' style={{ minWidth: '34%', minHeight: '7rem', textAlign: 'center', cursor: 'pointer' }}
                      onClick={handleWork}
                      >
                        <h6 style={{ lineHeight: '1.5' }}>{profData?.likes>1?"Likes":"Like"} <br />{profData?.likes}</h6>
                      </div>


                    </div>
                  </div>
                </div>
              </div>



              <div className='col-lg-8 col-md-8 col-sm-12 p-3 d-flex flex-wrap  prof_container_right' style={{}}>


                <div className='col-lg-8 col-md-8 col-sm-12 p-0 div_fptag'>
                  <div className='div_fptag_main'>
                    <h6 className='profLable'>MEBOOKMETA FINGERPRINT TAGS:</h6>
                    <span style={{ display: "flex", flexWrap: "wrap" }}>
                      {profData?.fpTags && profData?.fpTags?.length ? (
                        profData?.fpTags?.slice(0, 12)?.map((item, index) => (
                          <span
                            key={index}
                            style={{
                              color: "rgb(252,252,252)",
                              background: "#2B3467",
                              margin: ".2rem .2rem",
                              padding: "4px 8px 4px 8px",
                              borderRadius: "10px",
                              fontSize: "75%",
                            }}
                          >
                            <span>{item?.name}</span>
                          </span>
                        ))

                      ) : <span style={{ display: "" }}>This Profile has not Completed yet</span>
                      }
                      {profData?.fpTags?.length > 12 ?
                        (
                          <span
                            style={{
                              color: "rgb(252,252,252)",
                              background: "blue",
                              margin: ".2rem .2rem",
                              padding: "4px 8px 4px 8px",
                              borderRadius: "10px",
                              fontSize: "75%",
                            }}
                          >
                            <span
                              onClick={showFPTags}
                              style={{ cursor: 'pointer' }}
                            >+{profData?.fpTags?.length - 12} see all</span>
                          </span>
                        )
                        : ""
                      }
                    </span>
                  </div>
                  <div className='div_prof_details' style={{}}>
                    {profData?.birthPlace ?
                      <h6 className='profLable'>PLACE OF BIRTH: <span className='labelSpan'>{profData?.birthPlace}</span></h6>
                      : ""
                    }
                    {profData?.cAdd ?
                      <h6 className='profLable'>CURRENT ADDRESS: <span className='labelSpan'>{profData?.cAdd}</span></h6>
                      : ""
                    }
                    {profData?.homeTown ?
                      <h6 className='profLable'>HOME TOWN: <span className='labelSpan'>{profData?.homeTown}</span></h6>
                      : ""
                    }
                    {profData?.college ?
                      <h6 className='profLable'>COLLEGE: <span className='labelSpan'>{profData?.college}</span></h6>
                      : ""
                    }
                    {profData?.schooling ?
                      <h6 className='profLable'>SCHOOLING: <span className='labelSpan'>{profData?.schooling}</span></h6>
                      : ""
                    }

                  </div>
                </div>
                <div className='col-lg-4 col-md-4 col-sm-12 d-flex flex-column align-items-center div_qr '>
                  <div>
                    {qrCode}
                  </div>
                  <h6 style={{ fontSize: '.7rem' }}>SCAN QR TO KNOW ABOUT MY PROFILE</h6>
                  {/* <button className="new_prof_btn" style={{ background: 'black', color: 'white', borderRadius: '20px', padding: '5px 20px', outline: 'none', border: 'none', fontSize: '.8rem', letterSpacing: '1px' }}
                    >
                      COLLAB

                    </button> */}
                </div>




              </div>


              {ModalSignin ? <SignInModal setModalSignin={setModalSignin} username={username} userId={userId} path={"UserProfile"} /> : ''}
              {progress ?
                <UplProgBar variant="determinate" />
                : ''}
            </div>
          </div>

        </div>
      }
    </>
  )
}

export default MeProfilePage