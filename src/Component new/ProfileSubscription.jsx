import React, { useState, useEffect } from "react";
import axios from "axios";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';

import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link, useLocation } from "react-router-dom";
import Checkbox from "@mui/material/Checkbox";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "./Profile.css";
import img from "../Screenshot_2.png";
import Crousel from "./CrouselPage";
import WriterNav from "./WriterNav";
import { BackGround, newButton } from "./background";
export default function ProfileSubscription() {
    let location = useLocation();
    let option = sessionStorage.getItem('options')

    let userName = sessionStorage.getItem('userName')
    // console.log("first",JSON.parse(user).name)
  


    return (
        <>
            <div className="container-fluid" style={BackGround}>
                <WriterNav />
                <div className="img text-center">
                    <img
                        src={image}
                        alt=""
                        style={{
                            width: "232.5px",
                            height: "131px",
                            margin: "20px 0px",
                        }}
                    />
                </div>
                <div className="row">
                    <div className="col-md-8 text-center offset-md-2" style={{color:"white"}}>
                        <h2>  Welcome, {userName}</h2>
                        <h2>     Manage Profile Subscription My <span style={{ color: 'rgb(187 11 11)', fontFamily: "Times New Roman", }}>M</span><span style={{ color: '#0a0a8a' }}>eBook</span><span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#0a0a8a' }}>eta</span> Account</h2>

                    </div>
                </div>

                <div className="row">
                    <Crousel />
                </div>

            </div>
        </>
    )
}
