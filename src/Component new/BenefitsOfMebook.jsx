import React from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import { CardActionArea } from "@mui/material";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import picture1 from "../Picture1.jpg";
import picture3 from "../Picture3.jpg";
import picture4 from "../Picture4.jpg";
import picture5 from "../dollar.jpg";
import Picture2 from "../Picture2.jpg";
import PropTypes from "prop-types";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import Slide from "@mui/material/Slide";
import { Link, useNavigate } from "react-router-dom";
import { newButton } from "./background";
import MEE from '../mepic.png'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          className=""
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 20,
            top: 20,
            
          }}
        >
          <CloseIcon
            sx={{
              fontSize: "50px",
              color: "white",
              borderRadius: "50%",
              backgroundColor: "red",
            }}
          />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default function BenefitsOfMebook() {
  // Dialog1
  const [dialog1, setDialog1] = React.useState(false);
const navigate=useNavigate();
  const handleClickDialog1 = () => {
    setDialog1(true);
  };
  const handleCloseDialog1 = () => {
    setDialog1(false);
  };
  // Dialog 2
  const [dialog2, setDialog2] = React.useState(false);

  const handleClickDialog2 = () => {
    setDialog2(true);
  };
  const handleCloseDialog2 = () => {
    setDialog2(false);
  };
  // Dialog 3
  const [dialog3, setDialog3] = React.useState(false);

  const handleClickDialog3 = () => {
    setDialog3(true);
  };
  const handleCloseDialog3 = () => {
    setDialog3(false);
  };
  // Dialog 4
  const [dialog4, setDialog4] = React.useState(false);

  const handleClickDialog4 = () => {
    setDialog4(true);
  };
  const handleCloseDialog4 = () => {
    setDialog4(false);
  };
  return (
      <div
      className="container-fluid SubscriptionPic1 p-2"
      style={{ minHeight: "100vh",alignSelf:'center' }}
    >
      {/* <div className='mr-auto '>
        <Button sx={{...newButton, fontSize: "75%" }} onClick={() => navigate(-1)} variant="contained">Go Back</Button>
    </div> */}
      <div className="text-center" style={{paddingTop:''}}>
          <div
            className="head"
            style={{ fontSize: "40px", marginTop: "", fontWeight: "bold", fontSize:"175%" }}
          >
            {" "}
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eBook</span>
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eta</span> and You!
          </div>

          <div className="head" style={{ margin: "10px auto" }}>
            <h5>
              {" "}
              <b style={{fontSize:"80%"}}>MeBookMeta creates a sustainable community of creators,</b>{" "}
            </h5>
            <h5>
              {" "}
              <b style={{fontSize:"80%"}}>
                collaborators, mentors, influencers, providers, educators,{" "}
              </b>{" "}
            </h5>
            <h5 style={{fontSize:"80%"}}>
              {" "}
              <b>Consumers, Readers, Viewers, Listeners and Customers – </b>{" "}
            </h5>
          </div>
          <h1
            className="head"
            style={{ fontSize: "200%", fontWeight: "bolder", marginBottom: "10px" }}
          >
            {" "}
            The Global Media Marketplace!
          </h1>

          <div className=" ">
            <div className="row ">
              <div className="col-md-8 mx-auto">
                <button
                  type="button"
                  id="sub"
                  class="px-1"
                  style={{
                    width: "50%",
                    fontSize: "80%",
                    color: "black",
                    margin: "20px 10% 2px ",
                    borderRadius: "10px",
                    padding: "8px 0px",
                    fontWeight: "bold",
                    fontFamily: "Times New Roman",

                  }}
                  onClick={handleClickDialog1}
                >
                  The Greatest Challenge for Creators?
                </button>
                <BootstrapDialog
                  className=""
                  fullScreen
                  TransitionComponent={Transition}
                  onClose={handleCloseDialog1}
                  aria-labelledby="customized-dialog-title"
                  open={dialog1}
                >
                  <DialogContent
                    className="SubscriptionPic3"
                    sx={{
                      fontWeight: "bolder",
                      color: "black",
                      textAlign: "center",
                    }}
                    dividers
                  >
                    <div><img src={MEE} alt="/" style={{width:'200px'}}/></div>
                    <BootstrapDialogTitle
                      id="customized-dialog-title"
                      onClose={handleCloseDialog1}
                    >
                      <h1>
                        <b className="text-dark"> The Greatest Challenge for Creators?</b>
                      </h1>
                      
                    </BootstrapDialogTitle>
                    <div className="row">
                      <div className="col-md-8 mx-auto ">
                      <div style={{margin:'20px auto'}}> <img src={picture3} alt="" width='100%' /></div>
                        <Typography variant="h5" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            {" "}
                            It’s not content creation. Creating is literally what
                            creators do. No, the biggest challenge for creators is
                            Sharing Work, and specifically, sharing work with ideal
                            audiences, creating a following (a unique brand) and
                            becoming profitable.
                          </b>
                        </Typography>
                        <Typography variant="h5" gutterBottom></Typography>
                        <Typography variant="h5" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            {" "}
                            The key to success is connectivity and networking, or
                            market reach and influence. Success will come to those
                            who are able to sell work products, skills, concepts and
                            ideas with people they do not already know by packaging
                            and offering interesting and exciting products and
                            making them discoverable to a global market on an
                            ingenious platform, where the laser focus is to create
                            connections between content creators, collaborators,
                            industry and other professionals with global consumers
                            of content.
                          </b>
                        </Typography>
                      </div>
                    </div>
                  </DialogContent>
                </BootstrapDialog>
              </div>

              <div className="col-md-8 mx-auto ">
                <button
                  type="button"
                  id="sub"
                  class="px-1"
                  style={{
                    width: "50%",
                    fontSize: "80%",
                    color: "black",
                    margin: "15px 10% 2px ",
                    borderRadius: "10px",
                    padding: "8px 0px",
                    fontFamily: "Times New Roman",
                    fontWeight: "bold",
                  }}
                  onClick={handleClickDialog2}
                >
                  How Does It Work?
                </button>
                <BootstrapDialog
                  fullScreen
                  TransitionComponent={Transition}
                  onClose={handleCloseDialog2}
                  aria-labelledby="customized-dialog-title"
                  open={dialog2}
                >
                  <DialogContent
                    className=""
                    sx={{ fontWeight: "bolder", textAlign: "center" }}
                    dividers
                  >
                    <div><img src={MEE} alt="/" style={{width:'200px'}}/></div>
                    <BootstrapDialogTitle
                      id="customized-dialog-title"
                      onClose={handleCloseDialog2}
                    >
                      <h1>
                        <b className="text-dark">How Does It Work?</b>
                      </h1>
                    </BootstrapDialogTitle>
                    <div className="row">
                      <div className="col-md-10 mx-auto">
                        <div className="mb-3"> <img src={picture4} width='60%' height='250px' alt="" /></div>
                        <Typography variant="h6" gutterBottom style={{fontSize:"100%"}} >
                          <b>
                            {" "}
                            MeBookMeta is the only place on Earth where you will
                            have access to so many other serious, professional and
                            experienced content creators. Sell your work directly to
                            your audience, connect with new friends and
                            collaborators, and easily find the answers to all your
                            larger success-related questions. When you share your
                            work and ideas, you will become part of the change that
                            will revolutionize the global media industry!{" "}
                          </b>
                        </Typography>
                        <Typography variant="h6" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            Begin by sharing your up-to-date status on the
                            MeBookMeta GMM® Wall, or Instant Message with
                            like-minded creators, or share a link to your website,
                            book, blog, project, group, author page or book page. Or
                            share interesting articles, information or media
                            content. Share your needs, request a review, update your
                            friends and followers on your goals, or the status of
                            your next project.
                          </b>
                        </Typography>
                        <Typography variant="h6" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            Once you have shared your work, you will be instantly
                            connected to a network of consumers, collaborators,
                            production professionals and providers who can help you
                            reach that next level, to endless possibilities and to
                            places you never dreamed possible.
                          </b>
                        </Typography>
                      </div>
                      {/* <div className="col-md-4">
                                            <img src={picture3} width={400} height={400} alt="" />
                                        </div> */}
                    </div>
                  </DialogContent>
                </BootstrapDialog>
              </div>
              <div className="col-md-8 mx-auto ">
                <button
                  type="button"
                  id="sub"
                  class="px-1"
                  style={{
                    width: "50%",
                    fontSize: "80%",
                    color: "black",
                    margin: "15px 10% 2px ",
                    borderRadius: "10px",
                    padding: "8px 0px",
                    fontFamily: "Times New Roman",
                    fontWeight: "bold",
                  }}
                  onClick={handleClickDialog3}
                >
                  Boost Your Connectivity
                </button>
                <BootstrapDialog
                  fullScreen
                  TransitionComponent={Transition}
                  onClose={handleCloseDialog3}
                  aria-labelledby="customized-dialog-title"
                  open={dialog3}
                >
                  <DialogContent className="text-center">
                  <div><img src={MEE} alt="/" style={{width:'200px'}}/></div>
                    <BootstrapDialogTitle
                      id="customized-dialog-title"
                      onClose={handleCloseDialog3}
                    >
                      
                        <h1>
                          <b className="text-dark"> Boost Your Connectivity </b>
                        </h1>
                    </BootstrapDialogTitle>
                    <div className="row text-center ">
                      <div className="col-md-10 mx-auto">
                        <div style={{margin:'15px auto'}}> <img src={Picture2} width='100%'height='280px' alt="" /> </div>
                        <Typography variant="h5" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            {" "}
                            Upon enrollment, users will provide the background for
                            “Your Story,” and together with users created “Work
                            Profile,” users will provide the marketplace with a
                            unique perspective, which will include a video or
                            written “Me Pitch,” describing “who you are,” “what you
                            do,” and “why you do it.” It will also include a “Work
                            and Identity Pitch,” where you will pitch your work,
                            projects, concepts and/or ideas.
                          </b>
                        </Typography>
                        <Typography variant="h5" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            {" "}
                            Users will also create an “Identity Profile,” where
                            users will work to identify their ideal audience, based
                            on their work, pitch and appeal. “Your Story,” “Your
                            Work Profile” and “Your Identity” will be combined to
                            create your MeBookMeta User Public Profile (MePP), which
                            can best be compared to an onboard website.
                          </b>
                        </Typography>
                        <Typography variant="h5" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            The greatest challenge for websites is driving traffic
                            or attracting visitors and warranting repeat visits. So
                            rather than being a solitary website, twinkling out
                            there in a vast, desolate cyber universe,{" "}
                            <span style={{ color: "red" }}>
                              {" "}
                              your MePP will be part of a larger platform,{" "}
                            </span>{" "}
                            a Global Media Marketplace. Millions of visitors will
                            easily be able to find you, based on what you offer and
                            what you do.
                          </b>
                        </Typography>
                      </div>
                    </div>
                  </DialogContent>
                </BootstrapDialog>
              </div>
              <div className="col-md-8  mx-auto">
                <button
                  type="button"
                  id="sub"
                  class="px-1"
                  style={{
                    width: "50%",
                    fontSize: "80%",
                    color: "black",
                    margin: "15px 10% 2px ",
                    borderRadius: "10px",
                    padding: "8px 0px",
                    fontFamily: "Times New Roman",
                    fontWeight: "bold",
                  }}
                  onClick={handleClickDialog4}
                >
                  Save Your Money/Best Bang for the Buck
                </button>
                <BootstrapDialog
                  fullScreen
                  TransitionComponent={Transition}
                  onClose={handleCloseDialog4}
                  aria-labelledby="customized-dialog-title"
                  open={dialog4}
                >
                  <DialogContent className="" dividers>
                  <div className="d-flex justify-content-center"><img src={MEE} alt="/" style={{width:'200px'}}/></div>
                    <BootstrapDialogTitle
                      id="customized-dialog-title"
                      onClose={handleCloseDialog4}
                    ></BootstrapDialogTitle>
                    <div
                      className="container-fluid"
                    
                    >
                      <h1 style={{ textAlign: "center" }}>
                        <b>Save Your Money/Best Bang for the Buck</b>
                      </h1>
                        <div style={{textAlign:'center',margin:'20px auto'}}><img src={picture5} width='300px' height='100px' alt="" /></div>
                        

  <div className="row d-flex justify-content-center">
  <div className="col-12 col-sm-12 col-md-10 col-lg-8">
  <table class="table table-borderless">
  <tbody>
  <tr>
      <th scope="col">Initial Cost for Creating an Effective (well-trafficked) Website:</th>
      <th scope="col">$1,000 - $1,500</th>
    </tr>
  <tr>
      <th scope="col">Annual Cost for Maintaining an Effective (well-trafficked) Website:</th>
      <th scope="col">$100 - $500/year</th>
    </tr>
    <tr>
      <th scope="col">Annual Cost for Updating an Effective (well-trafficked) Website:</th>
      <th scope="col">$450/year</th>
    </tr>
    <tr>
      <th scope="col">Cost for Promoting an Effective (well-trafficked) Website:</th>
      <th scope="col">$1,500/week (4-weeks)</th>
    </tr>
    
    <tr>
      <th scope="col"></th>
      <th scope="col" style={{borderTop:'2px solid black'}}>$6,550/year</th>
    </tr>
  </tbody>
</table>
  </div>
  </div>

                        
                       



                      <div className="row ">
                        <div className="col-md-2"></div>
                        <div className="col-md-6">
                          {/* <Typography variant="h5" gutterBottom style={{fontSize:"100%"}}>
                            <b>
                              {" "}
                              <p>
                                {" "}
                                Initial Cost for Creating an Effective
                                (well-trafficked) Website:
                              </p>
                              <p>
                                Annual Cost for Maintaining an Effective
                                (well-trafficked) Website:
                              </p>
                              <p>
                                Annual Cost for Updating an Effective
                                (well-trafficked) Website:
                              </p>
                              <p>
                                Cost for Promoting an Effective (well-trafficked)
                                Website:
                              </p>
                            </b>
                          </Typography> */}
                        </div>
                        <div className="col-md-2">
                          {/* <Typography variant="h5" gutterBottom style={{fontSize:"100%"}}>
                          <b>
                            <p>$1,000 - $1,500</p>
                            <p>$100 - $500/year</p>
                            <p>$450/year</p>
                            <p>$1,500/week (4-weeks)</p>
                            <hr style={{ backgroundColor: "black" }} />
                          
                              {" "}
                              <p>$6,550/year</p>
                            </b>
                          </Typography> */}
                        </div>
                        <div className="col-md-2">
                        </div>
                        <div className="col-md-2"></div>
                        <div className="col-md-6">
                          <Typography
                            variant="h5"
                            gutterBottom
                            sx={{ ml: 5, fontWeight: "bolder" }}
                          >
                            <b> MeBookMeta Profile</b>
                            <ul>
                              <li>Includes On-Board Website</li>
                              <li>Simple User-Maintained Dashboard</li>
                              <li>Easy Real-Time Profile Updates</li>
                              <li>User-Focused Global Traffic</li>
                              <li>Collaboration and Networking Opportunities</li>
                              <li>Algorithmic On-Site Recommendations</li>
                              <li>
                                Add-Ons, including Payment Portal, Vetting, Deep
                                Searches
                              </li>
                            </ul>
                          </Typography>
                        </div>
                        <div className="col-md-4"></div>
                        <div className="col-md-2"></div>

                        <div className="col-md-6">
                          <Typography  variant="h5" gutterBottom>
                            <b>Projected Cost for MeBookMeta Website/Mobile App </b>
                          </Typography>
                        </div>
                        <div className="col-md-2">
                          {" "}
                          <h3>
                            {" "}
                            <b> $35/month - $350/year </b>{" "}
                          </h3>
                        </div>
                        <div className="col-md-2"></div>
                      </div>
                    </div>
                  </DialogContent>
                </BootstrapDialog>
              </div>
            </div>
          </div>

          <div className="row mt-4" >
            <div className="col-md-4 my-2">
              <Link to="/" style={{textDecoration:'none'}}>
                {" "}
                <Button variant="contained" size="large" sx={{ ...newButton, py: "10px", width:{xs:'80%',sm:'50%',md:'8rem'}, fontSize:"100%"}}>
                  Go Back
                </Button>
              </Link>
            </div>
            <div className="col-md-4 my-2">
            <Link to="/Signup"  style={{textDecoration:'none'}}>
                {" "}
                <Button variant="contained" size="large" sx={{ ...newButton, py: "10px",  width:{xs:'80%',sm:'50%',md:'8rem'}, fontSize:"100%"}}>
                  Sign Up
                </Button>
              </Link>
            </div>
            <div className="col-md-4 my-2">
              <Link to="/SignIn"  style={{textDecoration:'none'}}>
                {" "}
                <Button variant="contained" size="large" sx={{ ...newButton, py: "10px", width:{xs:'80%',sm:'50%',md:'8rem'}, fontSize:"100%"}}>
                  Sign In
                </Button>
              </Link>
            </div>
          </div>
        </div>
    </div>
    
  );
}
