import React, { useState, useEffect } from "react";
import axios from "axios";
import CommonAppbar from './CommonAppbar'
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import GradeIcon from '@mui/icons-material/Grade';
import Fab from '@mui/material/Fab';
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight';
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft';
import Divider from '@mui/material/Divider';
import { Link, useNavigate,useLocation, Navigate } from "react-router-dom";
import { BackGround } from "./background";

export default function ViewPublishedStory() {
    const location=useLocation();
    let userId= location.state.userid
    console.log("location",userId)
    const navigate = useNavigate();

    const [video, setVideo] = useState(false)
    const handleChange = () => {
        setVideo(true)
    }
    let user = sessionStorage.getItem('userName')

    const [story, setStory] = useState('')

    const [coverPic,setCoverPic]=useState({
        file: "",
        filepreview: null,
        pdfpreview: null,
    })
    useEffect(() => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios
            .get(`http://54.246.61.54:3002/getWorkbyId/${userId}`)

            .then((res) => {
                // console.log("token--->", id);
                console.log("res.data---> ", res.data);
                setStory(res.data.writerDetail[0])
                setCoverPic({
                    ...coverPic,
                    file: `http://54.246.61.54:3002/uploads/${res.data.writerDetail[0].coverPicture}`,

                    // file: `localhost:3002/uploads/${res.data.userData.pdf}`,
                    filepreview: `http://54.246.61.54:3002/uploads/${res.data.writerDetail[0].coverPicture}`,
                    pdfpreview: `http://54.246.61.54:3002/uploads/${res.data.writerDetail[0].pdfFile}`,
                });
            })
            .catch((err) => console.log(err));
    }, []);
    const handleEdit=()=>{
        navigate('/UpdatePublishedStory',{state:{userid:userId}})
    }
    return (
        <div className='container-fluid' style={BackGround}>
            <CommonAppbar />
            <div className="row">
                <div className="col-md-12 mt-3 d-flex">
                    <img src={coverPic.filepreview} style={{ height: '50px', width: '30px' }} alt="" />
                    <b style={{ marginLeft: '10px' }}>title<br />by {user}</b>
                    <Fab size="small" sx={{ ml: 'auto' }} color="primary" aria-label="add">
                        <EditIcon />
                    </Fab>

                    <Button sx={{ ml: '' }} onClick={handleEdit} variant="text">Edit Work</Button>
                    <Button sx={{ mx: 3 }} startIcon={<GradeIcon sx={{ color: '' }} />} variant="text">Vote</Button>
                <Link to='/MultipleOptions'> <Button  variant="contained">Next page</Button></Link> 
                </div>
                <div className="row text-center" style={{ backgroundColor: '#eee', margin: '10px 0px', width: '100%' }}>
                    <div className="col-md-12 text-center" style={{ backgroundColor: '#eee', width: '100%' }} >
                        {video == false ? (<> <iframe style={{ width: '500px', height: '300px' }} src={`https://www.youtube.com/embed/${story.youtubeUrl}`} frameborder='0' allowfullscreentitle='video' ></iframe> <KeyboardDoubleArrowRightIcon sx={{ fontSize: '50px', mb: '', mr: '10px' }} onClick={handleChange} /> </>)
                            : (<> <KeyboardDoubleArrowLeftIcon sx={{ fontSize: '50px', pb: '', mr: '10px' }} onClick={() => setVideo(!video)} /> <iframe src= {coverPic.pdfpreview} style={{ width: '500px', height: '300px' }} frameborder="0"></iframe></>)}
                    </div>
                </div>
                
            </div>
            <div className="row my-5">
                    <div className="col-md-12 text-center">
                        <h1>
                         {story.titleStory} 
                        </h1>
                        <Divider sx={{ width: '50%', mx: "auto",mb:5 }} orientation="" />
                        <p> {story.textStory} </p>
                    </div>


                </div>

        </div>
    )
}
