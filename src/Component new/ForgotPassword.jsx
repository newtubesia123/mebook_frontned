import React, { useState, useEffect } from "react";
import axios from "axios";
import Grid from "@mui/material/Grid";
import image from "../mepic.png";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import { useNavigate, useLocation } from "react-router-dom";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";
import { SERVER } from "../server/server";
import { BackGround, newButton } from "./background";
import { toast } from "react-toastify";
import MeLogo from "../assets/MeLogoMain";

// import "./Edit.css";
export default function ForgotPassword() {
  const [success, setSuccess] = React.useState(false);

  const navigate = useNavigate();
  const [values, setValues] = useState({
    email: "",
  });

  const bg = { ...newButton, marginBottom: "190px", width: "10rem" };

  const handleChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    localStorage.setItem('forgotPasswordEmail', values.email)
    const email = {
      email: values.email,
    };

    await axios
      .post(`${SERVER}/SendOtp`, email)
      .then((res) => {
        // console.log("data->", emailData);
        if (res.data.isSuccess) {
          toast.success(res.data.message);
          navigate("/CheckOtp");
          // sessionStorage.setItem("key", id);
        } else {
          toast.error(res.data.Error);
        }
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className="MebookMetaimage" style={BackGround}>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
      // sx={{ padding: "25px 5px" }}
      >
        <Grid item xs={2} md={2} sm={2}></Grid>
        <Grid
          item
          xs={8}
          md={8}
          sm={8}
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
            margin: "20px auto",
          }}
        >
          {/* <img
            src={image}
            alt=""
            style={{
              width: "10rem",
              height: "8rem",
            }}
          />{" "} */}
          <MeLogo />
          <p className=" my-4" style={{ fontSize: "125%", color: "white" }}>
            <b> Forgot Password! </b>
          </p>
          <p style={{ fontSize: "100%", color: "white" }}>
            <b>
              {" "}
              Please confirm your email address. We will send you a 6 digit
              One-time Password in your account
            </b>
          </p>
          <form style={{ margin: "50px 0px 0px" }} onSubmit={handleSubmit}>
            <div class="form-group">
              <input
                type="email"
                class="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                placeholder="Enter Your Email"
                name="email"
                value={values.email}
                onChange={handleChange}
              />
            </div>
            <div className="btn m-auto">
              <Button type="submit"
                sx={newButton} varient="contained" size="large"
              >
                Submit
              </Button>
            </div>
          </form>
        </Grid>
        <Grid item xs={2} md={2} sm={2}></Grid>
      </Grid>
    </div>
  );
}
