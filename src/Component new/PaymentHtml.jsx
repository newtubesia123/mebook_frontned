import React from 'react';
import './Pay.css';

function PaymentHtml() {
  return (
    <div className=" main1 container-fluid row d-flex justify-content-center">
    <div className="col-10 col-sm-10 col-md-8 col-lg-8 col-xl-8 col-xxl-8 text-center ">


        <div className="row">

            <button type="button"
                className="button-top mt-1 mb-1 btn btn-info rounded-pill col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                <i className="fa-solid fa-credit-card me-2"></i> Debit Card</button>

            <button type="button"
                className="button-top mt-1 mb-1 btn btn-info rounded-pill col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                <i className="fa-brands fa-paypal me-2"></i>
                Credit Card</button>


            <button type="button"
                className="button-top mt-1 mb-1 btn btn-info rounded-pill col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 col-xxl-4">
                <i className="fa-solid fa-building-columns me-2"></i> Bank
                Transfer</button>

        </div>
<div className="row d-flex justify-content-center">
    <div className="col-md-10">
<form >


  <div class="mb-3 mt-3">
    <label for="email" class="form-label">Email:</label>
    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" />
  </div>
  </form>
</div>
</div>
    </div>
</div>
  )
}

export default PaymentHtml