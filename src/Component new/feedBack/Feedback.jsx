import React, { useEffect, useState } from "react";
import axios from "axios";
import "./feedback.css";
import { BackGround, newButton } from "../background.jsx";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import { SERVER } from "../../server/server";
import { getUserId } from "../../Action/index";
import pic from "../../mepic.png";
import Rating from "@mui/material/Rating";
import Box from "@mui/material/Box";
import { useSelector } from "react-redux";

// import { BackGround } from "../../background";

import StarBorderIcon from "@mui/icons-material/StarBorder";
import { useNavigate } from "react-router-dom";
import MeLogo from "../../assets/MeLogoMain";
import ForwardBtn from "../../components/componentsB/btn/forwardBtn/ForwardBtn";
// VALUE OF RATING

const labels = {
  1: "Needs Emprovement",
  2: "Okay",
  3: " Excellent",
};

function getLabelText(value) {
  return `${value} Star${value !== 1 ? "s" : ""}, ${labels[value]}`;
}

const Feedback = () => {
  const [value, setValue] = React.useState(0);
  const [hover, setHover] = React.useState(-1);
  const STORE = useSelector((state) => state);
  const navigate = useNavigate()
  const [data, setData] = useState({
    fullname: STORE.userProfile.userProfile?.meProfile?.userId.name,
    email: STORE.userProfile.userProfile?.meProfile?.userId.email,
    message: "",
  });
  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const feedbackData = {
      fullname: data.fullname,
      email: data.email,
      message: data.message.split(/(.{20})/).filter((o) => o),
      rating: value,
    };
    if (
      data.fullname === "" ||
      data.email === "" ||
      data.message === "" ||
      value === 0
    ) {
      toast.error("Please fill all details ");
      return false;
    }

    await axios
      .post(
        `${SERVER}/dashboard/user_feedback/${await getUserId()}`,
        feedbackData
      )
      .then((response) => {
        if (response.data.errorCode === 200) {
          toast.success(response.data.message, { position: "top-center" });
          setData({
            fullname: "",
            email: "",
            message: "",
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const bg = {
    background: 'black',
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  };
  return (
    <div>
      <div className="container-fluid" style={bg}>
        <div className='ml-auto mt-5'>
          <Button sx={{ ...newButton, fontSize: "75%" }} onClick={() => navigate(-1)} variant="contained">Go Back</Button>
        </div>
        <div className="FeedbackMe" id="mepic" style={{ paddingTop: "1.5rem" }}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: "0",
            }}
          >
            {/* <img src={pic} width={100} height={75} alt="" style={{ width: '30%', height: '20%' }} /> */}
            <MeLogo/>
          </div>
        </div>
        <div className="col-md-8 left-form">
          <h3 style={{ fontSize: "125%" }} className="ct-section-head text-center">
            Write us feedback to improve our application
          </h3>
          <form>
            <div className="form-group">
              <input
                className="required form-control"
                id="fullname"
                name="fullname"
                placeholder="Your Name&nbsp;*"
                type="text"
                required
                disabled
                value={data.fullname}
                style={{ height: "1.8rem", fontSize: "75%" }}
              // value={STORE.userProfile.userProfile?.meProfile?.userId.name}
              />
            </div>
            <div className="form-group">
              <input
                className="required form-control h5-email"
                id="email"
                name="email"
                placeholder="Email&nbsp;*"
                type="text"
                required
                disabled
                value={data.email}
                style={{ height: "1.8rem", fontSize: "75%" }}
              // value={STORE.userProfile.userProfile?.meProfile?.userId.email}
              />
            </div>
            <div className="form-group">
              <label className="sr-only" for="message">
                Type your message here
              </label>
              <textarea
                className="required form-control"
                id="message"
                name="message"
                placeholder="Type your message here&nbsp;*"
                rows="4"
                required
                value={data.message}
                onChange={handleChange}
                style={{ fontSize: "75%" }}
              ></textarea>
            </div>
          </form>

          <p className="Feedbacktext" style={{ color: "lightgray", fontWeight: "bold", fontSize: "80%" }}>
            Please, take a few moments to rate our services to helps us grow and
            improve
          </p>
          <div className="RatingStar"
            style={
              {
                display: "flex",
                alignItems: "center",
                height: "100%",
                gap: "2rem"
              }
            }
          >
            {/* RATING PART */}
            <Rating
              name="hover-feedback"
              value={value}
              size="large"
              max={3}
              precision={1}
              getLabelText={getLabelText}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
              emptyIcon={
                <StarBorderIcon
                  style={{
                    opacity: "1",
                    fontSize: "inherit",
                    color: "#ffa500",
                  }}
                />
              }
            />
            {value !== null && (
              <Box
                sx={{
                  color: "#ffa500",
                }}
              >
                {labels[value]}
              </Box>
            )}
          </div>
          <div className="RatingStar RatingStarbtn">
            {/* <Button
              id="fdbtn"
              type="submit"
              onClick={handleSubmit}
              varient="contained"
              size="small"
              sx={{ ...newButton, marginTop: "4px", width: { xs: "100%", sm: "100%", lg: "25%", xl: "25%" } }}
            >
              Submit
            </Button> */}
            <ForwardBtn text={'SUBMIT'} style={{padding:'0.3rem 0.5rem',fontSize:'1rem'}} onClick={handleSubmit}/>
          </div>
        </div>


        
      </div>
     
    </div>
  );
};

export default Feedback;
