import React from 'react'
import './OfferCard.css'

const OfferCard = () => {
    return (
        <>
            <div class="popupBox">
                <div class="popupBox__content">
                    <div class="popupBox__img">
                        <img src="https://cdn.pixabay.com/photo/2021/09/21/02/48/gift-6642306__480.png" alt="" />
                    </div>
                    <div class="popupBox__contentTwo">
                        <div>
                            <h3 class="popupBox__title">Great Offer</h3>
                            <h2 class="popupBox__titleTwo">60 <sup>%</sup><span> Off</span></h2>
                            <p class="popupBox__description">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            </p>
                            <a href="#" class="popupBox__btn">Get The Deal</a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default OfferCard