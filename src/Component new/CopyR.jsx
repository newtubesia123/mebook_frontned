import React, { useEffect } from 'react'
import './Copyrights.css'
import { BackGround, newButton } from './background';
import { shadows } from '@mui/system';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getUserData } from '../Action';


export default function Copyrights() {
    const STORE = useSelector((state) => state)
    const dispatch = useDispatch()
    const navigate = useNavigate();
    useEffect(() => {
        dispatch(getUserData())


    }, [])
    return (

        <div class="container-fluid px-1 py-5 mx-auto" style={{ ...BackGround, minHeight: '100vh', color: 'white' }}>
            <div class="row d-flex justify-content-center ">
                <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center BgCopyright py-5 shadow-lg" style={{ borderRadius: '30px' }}>
                    <h3 style={{ fontSize: "125%" }}>COPYRIGHT RELEASE FORM</h3>

                    {/* <div class="card mx-auto" >
                        
                        <form class="form-card">
                            <div class="row  text-left">
                                <div class=" col-md-12 flex-column d-flex"> <label class=" px-3">Full name</label> <input value={STORE.getProfileData?.userData?.name} class='sss' type="text" id="fname" name="fname" placeholder="Enter your first name" onblur="validate(1)" /> </div>
                                
                            </div>
                            <div class="row justify-content-between text-left">
                                <div class="form-group col-sm-12 flex-column d-flex"> <label class="form-control-label px-3">Email Id </label> <input value={STORE.getProfileData?.userData?.email} class='sss' type="text" id="email" name="email" placeholder="" onblur="validate(3)" /> </div>
                                
                            </div>
                            <div class="row justify-content-between text-left">
                                <div class="form-group col-12 flex-column d-flex"> <label class="form-control-label px-3">Address</label> <input value={STORE.getProfileData?.userData?.currentAddress} class='sss' type="text" id="ans" name="ans" placeholder="" onblur="validate(6)" /> </div>
                            </div>
                            <div class="row justify-content-between text-left">

                                <div class="form-group col-12 flex-column d-flex"> <label class="form-control-label px-3">Copyright</label>
                                    <select class='sss' name="ans" id="ans">
                                        <option value="volvo">All Rights Reserved</option>
                                        <option value="saab">Public Domain</option>
                                        <option value="opel">Creative Commons(cc) Attribution</option>
                                        <option value="audi">(CC)Attribution NonCommercial</option>
                                    </select> </div>
                            </div>
                        </form>
                        <div className='mx-5' style={{textAlign:'start'}}>
                            <p> <b style={{ fontSize: '20px' }}>
                                   For the Purpose of Sharing Out my Work/Content, as to the Corresponding Work/Content
                                   Shared on this Site, I (the MeBookMeta User on record) Warrant that:</b> <br />
                               <ol type='a' style={{marginLeft:20}}>
                                <li className='my-1'>The Work/Content submitted on this site is my/our original Work/Content;</li>
                                <li className='my-1'>All acknowledged users who participated to the Work/Content in a substantive way are
                                    prepared to take public responsibility for the Work/Content;</li>
                                <li className='my-1'>I am authorizing MeBookMeta to share on its platform the Work/Content samples that I
                                    have uploaded to the platform;</li>
                                <li className='my-1'>I was authorized by all other contributors (if any) to share the following Work/Content
                                    with the MeBookMeta team for the purpose of sharing a Work/Content sample with the
                                    platform audience, and I will be responsible in the event of all disputes that have occurred
                                    and that may occur;</li>
                                <li className='my-1'>All relevant contributors to the Work/Content have seen and approved of the
                                    Work/Content sample as submitted;</li>
                                <li className='my-1'>The text copy, photos, illustrations and any other materials in the Work/Content
                                    submission do not infringe upon any existing copyright or other rights of anyone;</li>
                                <li className='my-1'>While I am authorizing MeBookMeta to share the Work/Content I have submitted, I
                                    retain all my original rights to the Work/Content.</li>
                               </ol>
                        
                            </p>
                            <div className=" ">
                                <div class="form-group d-flex"> 
                                   <input type="checkbox" style={{ height: '30px', width: '30px',marginRight:'15px' }} /> 
                                   <p>I acknowledge and authorize the inherent warrants, waivers and permissions contained above.</p>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="form-group col-sm-6"> <button  class="btn-block btn-primary btn-lg" onClick={()=>navigate('/MainDashboard/WriterStoryDetails')}>Submit</button> </div>
                            </div>
                        </div>
                    </div> */}
                    {/* ---------------------------------------------------- */}

                    <div className="row d-flex justify-content-center pt-5" >
                        <div className="col-12 col-sm-10 col-md-10">

                            {/* <h5 className="text-center mb-4">Powering world-className companies</h5> */}
                            <form className="form-card">

                                <label className=" px-3">Full name</label>
                                <input value={STORE.getProfileData?.userData?.name} className='sss' type="text" id="fname" name="fname" placeholder="Enter your first name" onblur="validate(1)" style={{ height: "2rem", fontSize: "100%" }} />
                                {/* <div className="form-group col-sm-6 flex-column d-flex"> <label className="form-control-label px-3">Last name</label> <input className='sss' type="text" id="lname" name="lname" placeholder="Enter your last name" onblur="validate(2)" /> </div> */}



                                <label className="form-control-label px-3 mt-2">Email Id </label>
                                <input value={STORE.getProfileData?.userData?.email} className='sss' type="text" id="email" name="email" placeholder="" onblur="validate(3)" style={{ height: "2rem", fontSize: "100%" }} />
                                {/* <div className="form-group col-sm-6 flex-column d-flex"> <label className="form-control-label px-3">Phone number</label> <input className='sss' type="text" id="mob" name="mob" placeholder="" onblur="validate(4)" /> </div> */}


                                <label className="form-control-label px-3 mt-2">Address</label>
                                <input value={STORE.getProfileData?.userData?.currentAddress} className='sss' type="text" id="ans" name="ans" placeholder="" onblur="validate(6)" style={{ height: "2rem", }} />



                                <label className="form-control-label px-3 mt-2">Copyright</label>
                                <select className='sss' name="ans" id="ans" >
                                    <option value="volvo">All Rights Reserved</option>
                                    <option value="saab">Public Domain</option>
                                    <option value="opel">Creative Commons(cc) Attribution</option>
                                    <option value="audi">(CC)Attribution NonCommercial</option>
                                </select>

                            </form>
                            <div className='my-2' style={{ textAlign: 'start', }}>
                                <p> <b style={{ fontSize: '100%' }}>
                                    For the Purpose of Sharing Out my Work/Content, as to the Corresponding Work/Content
                                    Shared on this Site, I (the MeBookMeta User on record) Warrant that:</b> <br />
                                    <ol type='a' style={{ marginLeft: 20, fontSize: "75%" }}>
                                        <li className='my-1'>The Work/Content submitted on this site is my/our original Work/Content;</li>
                                        <li className='my-1'>All acknowledged users who participated to the Work/Content in a substantive way are
                                            prepared to take public responsibility for the Work/Content;</li>
                                        <li className='my-1'>I am authorizing MeBookMeta to share on its platform the Work/Content samples that I
                                            have uploaded to the platform;</li>
                                        <li className='my-1'>I was authorized by all other contributors (if any) to share the following Work/Content
                                            with the MeBookMeta team for the purpose of sharing a Work/Content sample with the
                                            platform audience, and I will be responsible in the event of all disputes that have occurred
                                            and that may occur;</li>
                                        <li className='my-1'>All relevant contributors to the Work/Content have seen and approved of the
                                            Work/Content sample as submitted;</li>
                                        <li className='my-1'>The text copy, photos, illustrations and any other materials in the Work/Content
                                            submission do not infringe upon any existing copyright or other rights of anyone;</li>
                                        <li className='my-1'>While I am authorizing MeBookMeta to share the Work/Content I have submitted, I
                                            retain all my original rights to the Work/Content.</li>
                                    </ol>

                                </p>
                                <div className=" ">
                                    <div class="form-group d-flex">
                                        <input type="checkbox " style={{ height: '20px', width: '20px', marginRight: '15px' }} />
                                        <p style={{ fontSize: "90%" }}>I acknowledge and authorize the inherent warrants, waivers and permissions contained above.</p>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="form-group col-sm-6"> <button class="btn-block btn-primary btn-sm" onClick={() => navigate('/MainDashboard/WriterStoryDetails')}>Submit</button> </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {/* -------------------------------------------------------- */}
                </div>
            </div>
        </div>

    );
}


