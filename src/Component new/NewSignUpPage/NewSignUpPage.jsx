// import React, { useState } from 'react'
// import axios from "axios";
// import { toast } from "react-toastify";
// import './NewSignUpPage.css'
// import img from '../../Picture3.jpg'
// import MeLogo from '../../assets/MeLogoMain';
// import { newButton } from '../background';
// import { Button } from '@mui/material'
// import { useNavigate } from "react-router-dom";
// import { SERVER } from '../../server/server';
// import BackBtn from '../../components/componentsB/btn/backBtn/BackBtn';

// export default function NewSignUpPage() {
//     const navigate = useNavigate();
//     const [error, setError] = useState(false);
//     const [confirmpassword, setConfirmpassword] = useState("");
//     const [email, setEmail] = useState("");
//     const [username, setUsername] = useState("");
//     const [password, setPassword] = useState("");
//     const [passwordValue, setPasswordValue] = useState("");
//     const [values, setValues] = useState({
//         name: "",
//         email: "",
//         password: "",
//     });
//     const handleEmailChange = (e) => {
//         let item = e.target.value;

//         if (item.includes("@")) {
//             setEmail(false);
//         } else {
//             setEmail(true);
//         }
//         // setSubmitted(true);
//         const { name, value } = e.target;
//         setValues((prevState) => ({
//             ...prevState,
//             [name]: value,
//         }));
//     };
//     const handlePasswordChange = (e) => {
//         let item = e.target.value;
//         setPasswordValue(item);

//         if (item.length < 8) {
//             setPassword(true);
//         } else {
//             setPassword(false);
//         }
//         // setSubmitted(true);
//         const { name, value } = e.target;
//         setValues((prevState) => ({
//             ...prevState,
//             [name]: value,
//         }));
//     };
//     const handleChange = (e) => {
//         setValues({
//             ...values,
//             [e.target.name]: e.target.value,
//         });
//         setUsername(false);
//     };
//     const handleSubmit = async (e) => {
//         e.preventDefault();

//         const registerData = {
//             name: values.name,
//             email: values.email,
//             password: values.password,
//             confirmpassword: values.confirmpassword,
//         };
//         if (email === "" && password === "" && username === "") {
//             setEmail(true);
//             setError(true);
//             setPassword(true);
//             setUsername(true);
//             // setOpen(true);
//             toast.error('please enter all details', {
//                 position: 'top-center'
//             })
//             return false;
//         } else if (email === true || email === "") {
//             setEmail(true);
//             return false;
//         } else if (error === true || password === "" || password === true) {
//             setError(true);
//             setPassword(true);
//             return false;
//         } else if (values.name === "") {
//             setUsername(true);
//             return false;
//         }
//         await axios
//             .post(`${SERVER}/signup`, { registerData: registerData })
//             .then((res) => {
//                 console.log("res.data", res.data);
//                 if (res.data.success) {
//                     // sessionStorage.setItem("id", res.data.data._id);
//                     navigate(`/ConfirmMail/${registerData.email}`);
//                 } else {
//                     toast.error("Email Already Exist", {
//                         position: "top-center",
//                         autoClose: 2000,
//                         pauseOnHover: false,
//                         theme: "light",
//                     });
//                     return;
//                 }
//             })
//             .catch((err) => console.log(err));
//     };

//     const checkValidation = (e) => {
//         setConfirmpassword(e.target.value);
//         if (passwordValue !== e.target.value) {
//             setError(true);
//         } else {
//             setError(false);
//         }
//         // setSubmitted(true);
//         const { name, value } = e.target;
//         setValues((prevState) => ({
//             ...prevState,
//             [name]: value,
//         }));
//     };

//     return (
//         <>
//             <div className="main-login">
//                 <div className="left-login">
//                     <MeLogo />
//                     <div
//                         className="mt-3 tagname"
//                         style={{ fontFamily: "Times New Roman", fontSize: "3rem" }}
//                     >
//                         <span style={{ color: "rgb(187 11 11)" }}>M</span>
//                         <span style={{ color: "#0a0a8a" }}>eBook</span>
//                         <span style={{ color: "rgb(187 11 11)" }}>M</span>
//                         <span style={{ color: "#0a0a8a" }}>eta</span>
//                     </div>
//                     <h5
//                         style={{
//                             fontFamily: "Times New Roman",
//                             fontWeight: "bold",
//                             color: "white",
//                             fontSize: "1.5rem", textAlign: "center"
//                         }}
//                     >
//                         Welcome to the Global Media Marketplace
//                     </h5>
//                     <h1
//                         className="head"
//                         style={{ textAlign: "center", fontSize: "150%", fontWeight: "bolder", marginBottom: '1rem' }}
//                     >
//                         {" "}
//                         The Global Media Marketplace!
//                     </h1>
//                     <img src={img} className="left-login-image" alt={img} />
//                 </div>
//                 <div className="right-login">
//                     <form className="card-signup" onSubmit={handleSubmit}>
//                         <div className='ml-auto'>
//                             <BackBtn style={{ margin: '1rem' }} />
//                         </div>
//                         {/* <MeLogo /> */}
//                         <div className="textfield">
//                             <label htmlFor="name">Name</label>
//                             <input type="text" placeholder="Enter your Full Name"
//                                 name="name"
//                                 value={values.name}
//                                 onChange={handleChange} />
//                             {username ? (
//                                 <p style={{ color: "red", float: "left" }}>
//                                     Please Enter a valid Name
//                                 </p>
//                             ) : (
//                                 ""
//                             )}
//                         </div>
//                         <div className="textfield">
//                             <label htmlFor="usuario">Email</label>
//                             <input type="email" name="email"
//                                 placeholder='Enter your Email Address'
//                                 autoComplete="off"
//                                 value={values.email}
//                                 onChange={handleEmailChange} />
//                             {email ? (
//                                 <p style={{ color: "red", float: "left" }}>
//                                     Please Enter a valid Email address
//                                 </p>
//                             ) : (
//                                 ""
//                             )}
//                         </div>
//                         <div className="textfield">
//                             <label htmlFor="password">Password</label>
//                             <input type="password" placeholder="Enter your Password"
//                                 name="password"
//                                 autoComplete="off"
//                                 value={values.password}
//                                 onChange={handlePasswordChange} />
//                             {password ? (
//                                 <p className="" style={{ color: "red", float: "left" }}>
//                                     password must be 8 characters{" "}
//                                 </p>
//                             ) : (
//                                 ""
//                             )}
//                         </div>
//                         <div className="textfield">
//                             <label htmlFor="password">Confirm Password</label>
//                             <input type="password" placeholder="Confirm Password "
//                                 name="confirmpassword"
//                                 autoComplete="off"
//                                 onChange={(e) => checkValidation(e)}
//                                 value={confirmpassword} />
//                             {error ? (
//                                 <p className="" style={{ color: "red", float: "left" }}>
//                                     Password does not match
//                                 </p>
//                             ) : (
//                                 ""
//                             )}
//                         </div>
//                         <Button type="submit" className="btn-signup" style={{ ...newButton }} onClick={handleSubmit}>MeBook Meta Account Verification</Button>
//                         <span>OR</span>
//                         {/* <button type="button" className="login-with-google-btn" >
//                             Sign in with Google
//                         </button> */}
//                         <p id="fp">Already a user ? <span onClick={() => {
//                             navigate(`/SignIn`)
//                         }} style={{ color: 'lightblue', marginLeft: '0.2rem' }}>SignIn</span></p>
//                     </form>
//                 </div>
//             </div>
//         </>
//     )
// }

import React, { useState } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import "./NewSignUpPage.css";
import img from "../../Picture3.jpg";
import MeLogo from "../../assets/MeLogoMain";
import { newButton } from "../background";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { SERVER } from "../../server/server";
import BackBtn from "../../components/componentsB/btn/backBtn/BackBtn";

// google auth

import jwt_decode from "jwt-decode";
import { GoogleOAuthProvider, GoogleLogin } from "@react-oauth/google";
import { useDispatch } from "react-redux";
import { getMusicSubCategory } from "../../Action";

export default function NewSignUpPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [authUser, setAuthUser] = useState({
    name: "",
    email: "",
    email_verified: false,
  });

  // console.log("authuser", authUser);

  const [authForm, setAuthForm] = useState(true);

  const [formValueObject, setFormValueObject] = useState({
    usernameValue: "",
    emailValue: "",
    refferalCode:null,
    passwordValue: "",
    confirmpassword: "",
  });

  const [validatorObject, setValidatorObject] = useState({
    email: false,
    username: false,
    password: false,
    error: false,
  });

  // console.log("validatorObject", validatorObject);

  const clientId =
    "1084042994316-hhruvb92ilnt4ejr0s28u70ehtu1ibv0.apps.googleusercontent.com";

  const [values, setValues] = useState({
    name: "",
    email: "",
    refferalCode:null,
    password: "",
  });
  // console.log("values", values);

  // email Handle
  const handleEmailChange = (e) => {
    let item = e.target.value;
    // setEmailValue(item);

    setFormValueObject((preVal) => ({
      ...preVal,
      emailValue: item,
    }));

    if (item.includes("@")) {
      // setEmail(false);
      setValidatorObject((prevState) => ({
        ...prevState,
        email: false,
      }));
      const { name, value } = e.target;
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
      setAuthUser({
        ...values,
        [e.target.name]: e.target.value,
      });
    } else {
      // setEmail(true);
      setValidatorObject((prevState) => ({
        ...prevState,
        email: true,
      }));
    }
  };

// refferal handle
const handlerefferalChange = (e) => {
  let item = e.target.value;
  // setEmailValue(item);

  setFormValueObject((preVal) => ({
    ...preVal,
    refferalCode: item,
  }));
  setValues({
    ...values,
    [e.target.name]: e.target.value,
  });

  
};


  // password Handle

  const handlePasswordChange = (e) => {
    let item = e.target.value;
    // setPasswordValue(item);
    setFormValueObject((preVal) => ({
      ...preVal,
      passwordValue: item,
    }));

    if (item.length < 8) {
      // setPassword(true);

      setValidatorObject((prevState) => ({
        ...prevState,
        password: true,
      }));
    } else {
      // setPassword(false);
      setValidatorObject((prevState) => ({
        ...prevState,
        password: false,
      }));

      const { name, value } = e.target;
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
      setAuthUser({
        ...values,
        [e.target.name]: e.target.value,
      });
    }

    if (item != formValueObject.confirmpassword) {
      //  setError(true)
      setValidatorObject((prevState) => ({
        ...prevState,
        error: true,
      }));
    } else {
      // setError(false)
      setValidatorObject((prevState) => ({
        ...prevState,
        error: false,
      }));
    }
  };

  // name handle
  const handleChange = (e) => {
    // console.log(e.target.value);
    let item = e.target.value;
    // setUsernameValue(item)
    setFormValueObject((preVal) => ({
      ...preVal,
      usernameValue: item,
    }));
    if (item.length < 5) {
      // setUsername(true)
      setValidatorObject((prevState) => ({
        ...prevState,
        username: true,
      }));
    } else {
      setValues({
        ...values,
        [e.target.name]: e.target.value,
      });
      // setUsername(false);
      setValidatorObject((prevState) => ({
        ...prevState,
        username: false,
      }));

      setAuthUser({
        ...values,
        [e.target.name]: e.target.value,
      });
      // setAuthUser({
      //   ...values,
      //   [e.target.name]: e.target.value,
      // });
    }

    // console.log("input check", authUser);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const registerData = {
      name: authForm ? values.name : authUser.name,
      email: authForm ? values.email : authUser.email,
      refferalId:values?.refferalCode,
      password: values.password,
      confirmpassword: values.confirmpassword,
      googleVerified : !authForm ? true : false
    };
    // console.log("btn check", registerData);

    if (
      registerData.name &&
      registerData.email &&
      registerData.password &&
      registerData.confirmpassword &&
      validatorObject.email == false &&
      validatorObject.username == false &&
      validatorObject.password == false &&
      validatorObject.error == false
    )
    
     {
      await axios
        .post(`${SERVER}/signup`, { registerData: registerData })
        .then((res) => {
         switch (res.data.errorCode) {
          case 200:
            navigate(`/ConfirmMail/${registerData.email}`);
            break;
          case 300:
            localStorage.setItem("token", JSON.stringify(res.data.token));
            dispatch(getMusicSubCategory("Music"));
            navigate("/ProfileDetails");
            break
          case 404:
            setUserExist(true);
            toast.error(res.data.Error, {
              position: "top-center",
              autoClose: 2000,
              pauseOnHover: false,
              theme: "light",
            });
          break;
          default:
            break;
         }
        })
        .catch((err) => console.log(err));
    }
     else {
      if (
        formValueObject.emailValue === "" &&
        formValueObject.passwordValue === "" &&
        formValueObject.usernameValue === ""
      ) {
        // setEmail(true);
        // setError(true);
        // setPassword(true);
        // setUsername(true);

        setValidatorObject((prevState) => ({
          ...prevState,
          email: true,
          password: true,
          username: true,
          error: true,
        }));

        setOpen(true);
        toast.error("please enter all details", {
          position: "top-center",
        });
        return false;
      }

      if (
        formValueObject.emailValue === "" &&
        formValueObject.passwordValue === ""
      ) {
        // setEmail(true);
        // setPassword(true);
        setValidatorObject((prevState) => ({
          ...prevState,
          email: true,
          password: true,
          error: true,
        }));
      }

      // name
      if (formValueObject.usernameValue == "") {
        setUser(true);
        // setUsername(true);
        setValidatorObject((prevState) => ({
          ...prevState,
          username: true,
        }));
        setTimeout(() => {
          setUser(false);
        }, 5000);
        return false;
      }

      // email
      if (formValueObject.emailValue == "") {
        // setEmail(true);
        setValidatorObject((prevState) => ({
          ...prevState,
          email: true,
        }));
        setMail(true);
        return false;
      }

      if (formValueObject.passwordValue == "") {
        setPwd(true);
        // setError(true);
        // setPassword(true);
        setValidatorObject((prevState) => ({
          ...prevState,
          error: true,
          password: true,
        }));

        setTimeout(() => {
          setPwd(false);
        }, 5000);
        return false;
      }

      if (formValueObject.confirmpassword === "") {
        //  setError(true)
        setValidatorObject((prevState) => ({
          ...prevState,
          error: true,
        }));
      }
    }
  };

  // confirm password handle
  const checkValidation = (e) => {
    // setConfirmpassword(e.target.value);
    setFormValueObject((preVal) => ({
      ...preVal,
      confirmpassword: e.target.value,
    }));
    if (formValueObject.passwordValue !== e.target.value) {
      // setError(true);
      setValidatorObject((prevState) => ({
        ...prevState,
        error: true,
      }));
    } else {
      // setError(false);
      setValidatorObject((prevState) => ({
        ...prevState,
        error: false,
      }));
      const { name, value } = e.target;
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    }
  };
  // Alert hooks
  const [open, setOpen] = React.useState(false);

  const [userExist, setUserExist] = React.useState(false);
  const [mail, setMail] = React.useState(false);
  const [pwd, setPwd] = React.useState(false);
  const [user, setUser] = React.useState(false);

  const responseGoogle = (response) => {
    // console.log("response", response);
    setAuthForm(false);
    const userObject = jwt_decode(response.credential);
    // console.log("userObj", userObject);
    sessionStorage.setItem("user", JSON.stringify(userObject));
    const { name, email, email_verified } = userObject;
    // setEmailValue(email);
    // setUsernameValue(name);
    setFormValueObject((preVal) => ({
      ...preVal,
      usernameValue: name,
      emailValue: email,
    }));

    // setPasswordValue("");
    // setConfirmpassword("");

    setFormValueObject((preVal) => ({
      ...preVal,
      passwordValue: "",
      confirmpassword: "",
    }));

    const doc = {
      name: name,
      email: email,
      email_verified: email_verified,
    };
    // console.log(doc, "doc");
    setAuthUser(doc, "doc");
    setValues(doc);
    // setEmail(false);
    setValidatorObject((prevState) => ({
      ...prevState,
      email: false,
    }));
    // setError(false);
    setValidatorObject((prevState) => ({
      ...prevState,
      error: false,
    }));
    // setPassword(false);
    setValidatorObject((prevState) => ({
      ...prevState,
      password: false,
    }));
    // setUsername(false);
    setValidatorObject((prevState) => ({
      ...prevState,
      username: false,
    }));
  };
  // console.log(authUser, "authuser")

  return (
    <>
      <div className="main-login">
        <div className="left-login">
          <MeLogo />
          <div
            className="mt-3 tagname"
            style={{ fontFamily: "Times New Roman", fontSize: "3rem" }}
          >
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eBook</span>
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eta</span>
          </div>
          <h5
            style={{
              fontFamily: "Times New Roman",
              fontWeight: "bold",
              color: "white",
              fontSize: "1.5rem",
              textAlign: "center",
            }}
          >
            Welcome to the Global Media Marketplace
          </h5>
          <h1
            className="head"
            style={{
              textAlign: "center",
              fontSize: "150%",
              fontWeight: "bolder",
              marginBottom: "1rem",
            }}
          >
            {" "}
            The Global Media Marketplace!
          </h1>
          <img src={img} className="left-login-image" alt={img} />
        </div>
        <div className="right-login">
          <div className="card-signup">
            <div className="ml-auto">
              <BackBtn style={{ margin: "1rem" }} />
            </div>

            {authForm ? (
              <form>
                <div className="textfield">
                  <label htmlFor="name">Name</label>
                  <input
                    type="text"
                    placeholder="Enter your Full Name"
                    name="name"
                    value={formValueObject.usernameValue}
                    onChange={handleChange}
                  />
                  {validatorObject.username ? (
                    <p style={{ color: "red", float: "left", marginBottom:"0px" }}>
                      Please Enter a valid Name
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div className="textfield">
                  <label htmlFor="usuario">Email</label>
                  <input
                    type="email"
                    name="email"
                    placeholder="Enter your Email Address"
                    // autoComplete="off"
                    value={formValueObject.emailValue}
                    onChange={handleEmailChange}
                  />
                  {validatorObject.email ? (
                    <p style={{ color: "red", float: "left",marginBottom:"0px" }}>
                      Please Enter a valid Email address
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div className="textfield">
                  <label htmlFor="usuario">Refferal Code (if any)</label>
                  <input
                    type="text"
                    name="refferalCode"
                    placeholder="Enter Refferal Code"
                    // autoComplete="off"
                    value={formValueObject.refferalCode}
                    onChange={handlerefferalChange}
                  />
                  
                </div>
                <div className="textfield">
                  <label htmlFor="password">Password</label>
                  <input
                    type="password"
                    placeholder="Enter your Password"
                    name="password"
                    autoComplete="off"
                    value={formValueObject.passwordValue}
                    onChange={handlePasswordChange}
                  />
                  {validatorObject.password ? (
                    <p className="" style={{ color: "red", float: "left",marginBottom:"0px" }}>
                      password must be 8 characters{" "}
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div className="textfield">
                  <label htmlFor="password">Confirm Password</label>
                  <input
                    type="password"
                    placeholder="Confirm Password "
                    name="confirmpassword"
                    autoComplete="off"
                    onChange={(e) => checkValidation(e)}
                    value={formValueObject.confirmpassword}
                  />
                  {validatorObject.error ? (
                    <p className="" style={{ color: "red", float: "left", marginBottom:"0px" }}>
                      Password does not match
                    </p>
                  ) : (
                    ""
                  )}
                </div>
              </form>
            ) : (
              <form>
                <div className="textfield">
                  <label htmlFor="name">Name</label>
                  <input
                    type="text"
                    placeholder="Enter your Full Name"
                    name="name"
                    disabled
                    value={authUser.name}
                    onChange={handleChange}
                  />
                  {validatorObject.username ? (
                    <p style={{ color: "red", float: "left", marginBottom:"0px" }}>
                      Please Enter a valid Name
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div className="textfield">
                  <label htmlFor="usuario">Email</label>
                  <input
                    type="email"
                    name="email"
                    placeholder="Enter your Email Address"
                    autoComplete="off"
                    disabled
                    value={authUser.email}
                    onChange={handleEmailChange}
                  />
                  {validatorObject.email ? (
                    <p style={{ color: "red", float: "left", marginBottom:"0px" }}>
                      Please Enter a valid Email address
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div className="textfield">
                  <label htmlFor="password">Password</label>
                  <input
                    type="password"
                    placeholder="Enter your Password"
                    name="password"
                    autoComplete="off"
                    value={formValueObject.passwordValue}
                    onChange={handlePasswordChange}
                  />
                  {validatorObject.password ? (
                    <p className="" style={{ color: "red", float: "left", marginBottom:"0px" }}>
                      password must be 8 characters{" "}
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div className="textfield">
                  <label htmlFor="password">Confirm Password</label>
                  <input
                    type="password"
                    placeholder="Confirm Password "
                    name="confirmpassword"
                    autoComplete="off"
                    onChange={(e) => checkValidation(e)}
                    value={formValueObject.confirmpassword}
                  />
                  {validatorObject.error ? (
                    <p className="" style={{ color: "red", float: "left", marginBottom:"0px" }}>
                      Password does not match
                    </p>
                  ) : (
                    ""
                  )}
                </div>
              </form>
            )}

            <div className="text-center " style={{ marginTop: "15px" }}>
              {authForm ? (
                <Button
                  sx={newButton}
                  style={{ fontSize: "75%" }}
                  onClick={handleSubmit}
                >
                  MebookMeta Account Verification
                </Button>
              ) : (
                <Button
                  sx={newButton}
                  style={{ fontSize: "75%", width: "100%" }}
                  onClick={handleSubmit}
                >
                  Sign Up
                </Button>
              )}

              {authForm ? (
                <div className="d-flex justify-content-center align-items-center my-2">
                  <GoogleOAuthProvider clientId="1084042994316-hhruvb92ilnt4ejr0s28u70ehtu1ibv0.apps.googleusercontent.com">
                    <GoogleLogin
                      onSuccess={responseGoogle}
                      onFailure={responseGoogle}
                    />
                  </GoogleOAuthProvider>
                </div>
              ) : (
                ""
              )}
              <p
                style={{
                  marginBottom: "5px",
                  fontFamily: "Times New Roman",
                  fontSize: "1rem",
                  color: "black",
                }}
              >
                Already have an account?
                <div
                  className="nav-link"
                  onClick={() => {
                    navigate(`/SignIn`);
                  }}
                  to={"/SignIn"}
                  style={{
                    display: "inline-block",
                    color: "blue",
                    cursor: "pointer",
                    fontSize: "1rem",
                  }}
                >
                  Sign in
                </div>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
