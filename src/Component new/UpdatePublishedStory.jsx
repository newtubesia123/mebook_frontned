import React, { useState, useEffect } from "react";
import axios from "axios";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ButtonGroup from '@mui/material/ButtonGroup';
import ImageIcon from '@mui/icons-material/Image';
import Divider from '@mui/material/Divider';
import VideocamIcon from '@mui/icons-material/Videocam';
import Input from '@mui/material/Input';
import TextField from '@mui/material/TextField';
import InputBase from '@mui/material/InputBase';
import { Link, useNavigate, useLocation } from "react-router-dom";
import { BackGround } from "./background";


export default function UpdatePublishedStory() {

    const bg = {...BackGround, minHeight:"100vh" , minWidth:"100wh"}
    const location = useLocation();
    let userId = location.state.userid
    console.log("location", userId)
    const navigate = useNavigate();
    const [text, setText] = useState({
        textStory: "",
        titleStory: "",
        pdfFile: '',
        youtubeUrl: '',
    });
    const [inputBox, setInputBox] = useState(true)
    const handleConvertInput = () => {
        setInputBox(false)
        setEditFile(true)
    }
    const [userImage, setUserImage] = useState('')
    console.log("image", userImage)
    const [userInfo, setuserInfo] = useState({
        file: "",
        filepreview: null,
    });
    const handleInputChange = (event) => {
        setuserInfo({
            ...userInfo,
            file: event.target.files[0],
            filepreview: URL.createObjectURL(event.target.files[0]),
        });
        setEditFile(true)
    };
    useEffect(() => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios
            .get(`http://54.246.61.54:3002/getWorkbyId/${userId}`)

            .then((res) => {
                // console.log("token--->", id);
                console.log("res.data---> ", res.data);
                setText(res.data.writerDetail[0])
            })
            .catch((err) => console.log(err));
    }, []);
    const submitFiles = async () => {
        let token = JSON.parse(localStorage.getItem("token"));
        const formdata = new FormData();
        formdata.append("profiles", userInfo.file);

        await axios
            .post(
                "http://54.246.61.54:3002/pdfFile", formdata)

            .then((res) => {
                console.log("res-->", res.data)
                if (res.data.success) {
                    setUserImage(res.data.file[0].filename)
                    alert(res.data.success);
                }
            });
    };
    let user = localStorage.getItem('userData')

    const handleChange = (e) => {
        setText({
            ...text,
            [e.target.name]: e.target.value,
        });
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const userData = {
            textStory: text.textStory,
            titleStory: text.titleStory,
            pdfFile: userImage,
            youtubeUrl: baseUrl,
        };
        await axios
            .post("http://54.246.61.54:3002/updateWork", { userData: userData, userId: userId })
            .then((res) => {
                console.log("data->", res.data);
                if (res.data.isSuccess) {
                    alert(res.data.message);
                    navigate("/WriterStory");
                } else {
                    alert("something went wrong")
                }
            })
            .catch((err) => console.log(err));

    };
    const [url, setUrl] = useState("")
    const fun = (e) => {
        setUrl(e.target.value)
    }
    let baseUrl = url.substring(32, 43)
    if (url.length == 28) {
        baseUrl = url.substring(17)
        console.log("url", baseUrl)
    }
    const [editFile, setEditFile] = useState(false)

    return (
        <div className="container-fluid" style={bg}>
            <Box sx={{ flexGrow: 1, mb: 10 }}>
                <AppBar elevation position="fixed" sx={{ backgroundColor: 'white', borderBottom: " 1px solid lightgray" }}>
                    <Toolbar>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: "black" }}>
                            Untitled Part 1
                        </Typography>
                        <button type='button' class='btn btn-info' onClick={handleSubmit} color="">Publish</button>
                        <button onClick={submitFiles} style={{ margin: '0px 10px' }} type='button' class='btn btn-primary' color="">Save</button>
                    </Toolbar>
                </AppBar>
            </Box>
            <div className="container-fluid text-center" style={{ height: '125px', backgroundColor: '#eee' }}>
                <input type="file" id="pdf" style={{ display: 'none' }} onChange={handleInputChange} />
                {inputBox ? <><button style={{ backgroundColor: 'white', margin: '30px 0px', width: '120px' }} type='button' class="btn btn-light btn-sm"> <label for="pdf"> <ImageIcon color="action" sx={{ fontSize: '38px', margin: 'auto 30px' }} /></label>  </button>
                    <button style={{ backgroundColor: 'white', margin: '30px 0px', width: '120px' }} type='button' class="btn btn-light btn-sm">  <VideocamIcon onClick={handleConvertInput} color="action" sx={{ fontSize: '45px', margin: '' }} /> </button></>
                    : (<form class="row g-3 justify-content-center"> <div className='col-auto' style={{ padding: '35px 0px' }}> <input value={url} onChange={fun} style={{ margin: " ", width: '250px', padding: '25px 0px', fontSize: '20px' }} class="form-control" placeholder='Paste Your YouTube URL' /></div>
                        <div class="col-auto my-auto ">
                            <button type='button' onClick={() => setInputBox(!inputBox)} class="btn btn-light">Cancel</button>
                        </div></form>)}
            </div>


            <div className="container-fluid d-flex justify-content-center" style={{ backgroundColor: '#eee', margin: '10px 0px' }} >
                {editFile == false ? <><iframe style={{ width: '500px', height: '300px', margin: '5px 10px', border: '5px dotted gray' }} src={`https://www.youtube.com/embed/${text.youtubeUrl}`} frameborder='0'
                    allow='autoplay'
                    allowfullscreen
                    title='video' ></iframe>
                    <iframe style={{ height: '300px', width: '500px', border: '5px dotted gray' }} src={`http://54.246.61.54:3002/uploads/${text.pdfFile}`}></iframe>
                </> : <><iframe style={{ width: '500px', height: '300px', margin: '5px 10px' }} src={`https://www.youtube.com/embed/${baseUrl}`} frameborder='0'
                    allow='autoplay'
                    allowfullscreen
                    title='video' ></iframe>
                    <iframe style={{ height: '300px', width: '500px' }} src={userInfo.filepreview}></iframe>
                </>}
            </div>
            <div className='text-center' style={{}}>
                <InputBase
                    sx={{ mt: 8, fontWeight: 'bolder', flex: 1, fontSize: '35px', border: '2px dotted gray' }}
                    placeholder="Untitled part1"
                    name="titleStory"
                    onChange={handleChange}
                    value={text.titleStory}
                />
                <Divider sx={{ width: '50%', mx: "auto" }} orientation="" />

            </div>
            <div className='col-md-6 mx-auto'>
                <InputBase
                    multiline
                    sx={{ flex: 1, mt: '5px', mb: '100px', fontSize: '20px', width: '100%', border: '2px dotted gray' }}
                    placeholder="Type your text"
                    name="textStory"
                    onChange={handleChange}
                    value={text.textStory}
                />
            </div>
        </div >
    )
}
