import React from "react";
import image from "../mepic.png";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import { BackGround, newButton } from "./background";

import "./Profile.css";
import MeLogo from "../assets/MeLogoMain";

export default function New() {

  const navigate = useNavigate();

  const bg = { ...BackGround, minHeight: "100vh" }

  return (
    <div className="container-fluid d-flex justify-content-center flex-column text-center"
      style={bg}
    >
      <div className="row ">
        <div className="col-md-6 col-sm-6 mx-auto pt-3 ">
          <div
            className=" tagname"
            style={{ fontFamily: "Times New Roman", fontSize: "175%" }}
          >
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eBook</span>
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eta</span>
          </div>
          <div className="">
            {/* <img
              src={image}
              alt=""
              style={{ width: "20%", height: "15%", margin: "5px 0px" }}
            /> */}
            <MeLogo/>
          </div>
          <h5
            style={{
              color: "white",
              fontFamily: "Times New Roman",
              fontSize: "110%",
            }}
          >
            The MeBook About You <br />
            and What You Do
          </h5>
          <div className="">
            <h4
              style={{
                fontFamily: "Times New Roman",
                fontWeight: "bold",
                color: "white", fontSize: "110%"
              }}
            >
              Share Your Work <br /> Build Your Following <br /> Discover Art,
              Books and Projects
            </h4>
          </div>
          <div
            className=" "
            style={{
              paddingBottom: "1px",
              fontWeight: "bold",
              fontSize: "0.9rem",
              textAlign: "justify",
              color: "white",
            }}
          >
            <p className="ptag">
              Finally, a content-based platform, a tech revolution, a Global
              Media Marketplace exclusively linking creators of content and
              consumers across the Earth. This digital platform provides a
              marketplace where creators can connect with useful contacts, other
              creators, collaborators, experts, influencers, publishers,
              producers, project developers, providers, media, promoters,
              agents, distributors, libraries, retail sellers and consumers in
              ways never imagined before.
            </p>
          </div>
          <div >
            {/* <Link className="nav-link" to={"/BenefitsOfMebook"}> */}
            {/* {" "} */}
            <Button
              variant="contained"
              sx={{ ...newButton, fontSize: "70%", width:"90%" }}
              onClick={() => { navigate(`/BenefitsOfMebook`) }}
            >
              Get Started
            </Button>
            {/* </Link> */}
          </div>
        </div>
      </div>
    </div>
  );
}
