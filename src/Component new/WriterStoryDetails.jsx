import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import PropTypes from 'prop-types';
import { useLocation, useNavigate, useParams } from "react-router-dom";
import ImageIcon from '@mui/icons-material/Image';
import EditIcon from '@mui/icons-material/Edit';
import axios from "axios";
import PublishIcon from '@mui/icons-material/Publish';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import { useDispatch, useSelector } from 'react-redux';
import { callPopUp, getUserId, getUserWorkById, userProfile, userWorkDetail, writerWork } from '../Action';
import { BackGround, newButton } from './background';
import { SERVER } from '../server/server';
import { toast } from "react-toastify";
import Checkbox from '@mui/material/Checkbox';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
// import { getImgURL } from '../../assets/common/getImgUrl'
import './WriterStoryDetails.css'
import { Cascader } from 'antd';
import { getImgURL } from '../util/getImgUrl';
import { onUplProg } from '../assets/common/onUplProg';
import UplProgBar from '../components/componentsC/uplProgBar/UplProgBar';

// for modal
import Backdrop from '@mui/material/Backdrop';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Copyrights from './Copyrights';
import { descriptionFunction } from '../assets/descriptionFun';
const { SHOW_CHILD } = Cascader;
// let array = []
// {
//     response.data.message.map((i) => {
//         const data = { label: i.name, value: i._id }
//         array.push(data)
//     })
// }
const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}
// changes

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function WriterStoryDetails() {
    // for modal start
    const [open, setOpen] = useState(false);
    // for modal end

    const params = useParams()
    const STORE = useSelector((state) => state)
    const [editWork, setEditWork] = useState(null)
    const [subCat, setSubCat] = useState([])

    const dispatch = useDispatch()
    const getImgURLPdf = (name) => `${SERVER}/uploads/${name}`
    const [progress, setProgress] = useState(false);
    const [loading, setLoading] = useState(false)
    const [array, setArray] = useState([])

    const [showCategory, setShowcategory] = useState('')
    const [edit, setEdit] = useState(params.process === 'Edit' ? true : false)
    const [filePreview, setFilePreview] = useState(false)

    const navigate = useNavigate();
    const location = useLocation();

    // setInputValue( params.process==='Edit'?{...inputValue,title:editWork?.title,description:'',subCategory:editWork?.subCategory,audience:editWork?.audience,coverPicture:editWork?.coverPicture,userWork:editWork?.userWork,youtubeUrl:editWork?.youtubeUrl,id: editWork?._id}:{...inputValue})


    const [inputValue, setInputValue] = useState({
        title: '',
        description: '',
        subCategory: '',
        audience: '',
        coverPicture: '',
        userWork: '',
        youtubeUrl: '',
        id: ''
    });
    const [tick, setTick] = useState({ copyright: params.process === 'Add' ? false : true, tnc: params.process === 'Add' ? false : true })
    // cover picture upload 
    const [userImage, setUserImage] = useState('')
    const [confirm, setConfirm] = useState(params.process === 'Add' ? false : true)
    const [formValue, setFormValue] = useState({
        name: STORE.getProfileData?.userData?.name,
        email: STORE.getProfileData?.userData?.email,
        address: STORE.getProfileData?.userData?.currentAddress,
        copyright: 'Select'
    })

    const handleOpen = () => {
        setOpen(true)
    }
    useEffect(() => {
        setEditWork(STORE?.userWorkDetail?.userWorkDetail?.message?.filter((item) => item._id === location?.state?.workId)[0])


        setInputValue(params.process === 'Edit' ? { ...inputValue, title: editWork?.title, description: editWork?.description?.join(' '), subCategory: editWork?.subCategory?.map((item, index) => item?._id), audience: editWork?.audience, coverPicture: editWork?.coverPicture, userWork: editWork?.userWork, youtubeUrl: editWork?.youtubeUrl, id: editWork?._id } : { ...inputValue })
        setFormValue(params.process === 'Edit' ? { ...formValue, copyright: editWork?.copyrightId?.copyright } : { ...formValue })


    }, [STORE])

    useEffect(() => {

        window.scroll(0, 0)

        // axios.get(`${SERVER}/work_1/getUserWorkById/${location.state.id}`).then((res) => {
        //     setShowcategory(res.data.data[0].subCategory.map((e) => e.name))
        //     let { title, description, subCategory, audience, copyright, coverPicture, userWork, youtubeUrl } = res.data.data[0]
        //     setInputValue({ ...inputValue, title: title, description: description.join(' '), subCategory: subCategory.map((e) => e._id), audience: audience, copyright: copyright, coverPicture: coverPicture, userWork: userWork, youtubeUrl: youtubeUrl })
        //     setEdit(true)
        // })

        setTimeout(async () => {
            dispatch(userProfile(await getUserId()))
        }, 1000)

        STORE?.userProfile?.userProfile?.meProfile?.subCategory.map((i) => setArray((array) => [...array, array.filter((e) => e?.value == i._id)[0] ? "" : { label: i.name, value: i._id }]))
    }, [])

    let subCategory = JSON.parse(localStorage.getItem('subCategory'))

    const bg = { backgroundColor: 'black', minHeight: "100vh" }

    const [userInfo, setuserInfo] = useState({
        file: "",
        filepreview: null,
    });

    const handleTakeImage = (event) => {
        setuserInfo({
            ...userInfo,
            file: event.target.files[0],
            filepreview: URL.createObjectURL(event.target.files[0]),
        });
    };

    let userCategory = STORE?.userProfile?.userProfile?.meProfile?.category
    console.log("userCategory", userCategory)

    // for pdf
    const pdfUpload = (e) => {
        const file = e.target.files[0];
        const fileSize = file.size;
        if ((userCategory == "Author" && fileSize > 100 * 1024 * 1024) || (userCategory == "Writer" && fileSize > 100 * 1024 * 1024) || (userCategory == "Music" && fileSize > 100 * 1024 * 1024) || (userCategory == "Sound" && fileSize > 100 * 1024 * 1024) || (userCategory == "Music Video" && fileSize > 100 * 1024 * 1024) || (userCategory == "Film" && fileSize > 100 * 1024 * 1024) || (userCategory == "Television" && fileSize > 100 * 1024 * 1024) || (userCategory == "Print or Media" && fileSize > 100 * 1024 * 1024) || (userCategory == "Visual Arts" && fileSize > 100 * 1024 * 1024) || (userCategory == "Audio" && fileSize > 100 * 1024 * 1024)) {
            setFilePreview(false)
            toast.error('File must not exceed 2MB', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
        } else {
            setProgress(true)
            setFilePreview(true)
            const formData = new FormData();
            formData.append('profiles', e.target.files[0], e.target.files[0].name)

            setLoading(true)
            axios.post(`${SERVER}/pdfFile`, formData, { onUploadProgress: onUplProg })
                .then((response) => {
                    setProgress(false)
                    setInputValue({ ...inputValue, userWork: response.data.file })  // that is why i again set the name of the image which i got from backend
                    toast.success(response.data.success, {
                        position: "top-right",
                        autoClose: 2000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme: "colored",
                    });

                    setLoading(false)
                }).catch((err) => {
                    setProgress(false)
                    toast.error('upload failed go back and try again', {
                        position: "top-right",
                        autoClose: 2000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme: "colored",
                    });
                })
        }

        // setProgress(true)
        // const formData = new FormData();
        // formData.append('profiles', e.target.files[0], e.target.files[0].name)

        // setLoading(true)
        // axios.post(`${SERVER}/pdfFile`, formData, { onUploadProgress: onUplProg })
        //     .then((response) => {
        //         setProgress(false)
        //         setInputValue({ ...inputValue, userWork: response.data.file })  // that is why i again set the name of the image which i got from backend
        //         toast.success(response.data.success, {
        //             position: "top-right",
        //             autoClose: 2000,
        //             hideProgressBar: false,
        //             closeOnClick: true,
        //             pauseOnHover: true,
        //             draggable: true,
        //             progress: undefined,
        //             theme: "colored",
        //         });

        //         setLoading(false)
        //     }).catch((err) => {
        //         setProgress(false)
        //         toast.error('upload failed go back and try again', {
        //             position: "top-right",
        //             autoClose: 2000,
        //             hideProgressBar: false,
        //             closeOnClick: true,
        //             pauseOnHover: true,
        //             draggable: true,
        //             progress: undefined,
        //             theme: "colored",
        //         });
        //     })
    }

    const [userPdf, setUserPdf] = useState({
        pdfFile: "",
        pdfFilepreview: null,
    });
    const handlePdf = (event) => {
        if (filePreview == true) {
            setUserPdf({
                ...userPdf,
                pdfFile: event.target.files[0],
                pdfFilepreview: URL.createObjectURL(event.target.files[0]),
            });
        }
    };


    const handleImage = () => {
        setProgress(true)
        const formdata = new FormData();
        formdata.append("profiles", userInfo.file);
        axios
            .post(`${SERVER}/CoverPicture`, formdata, { onUploadProgress: onUplProg })
            .then((res) => {
                setProgress(false)
                toast.success(res.data.success, { autoClose: 2000 })
                // setUserImage(res.data?.file)
                setInputValue({ ...inputValue, coverPicture: res?.data?.file })
            })
            .catch((error) => {
                console.log('error', error);
                setProgress(false)
                toast.success('Something Went Wrong', { autoClose: 2000 })
            })
    }


    const handleChangeInput = (e) => {
        setInputValue({
            ...inputValue,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (inputValue?.title == '' || inputValue?.description == '' || inputValue?.subCategory == '' || inputValue?.audience == '') {
            toast.error('Please Fill All Details')
            return false
        }
        else if (params.process === 'Add' && (inputValue?.description?.split(' ')?.length < 30 || inputValue?.description?.split(' ')?.length > 250)) {
            toast.error('Description must be between 30 words to 250 words ', { autoClose: 2000 })
            return false

        }
        // else if(params.process==='Edit' &&){}
        else if (inputValue?.coverPicture == '') {
            toast.error('Please Upload Cover Picture', { autoClose: 2000 })
            return false

        }
        else if (!uploadYtube && inputValue?.userWork == '' && params.process === 'Add') {
            toast.error('Please Upload Your Work', { autoClose: 2000 })
            return false
        } else if (uploadYtube && inputValue?.youtubeUrl == '') {
            toast.error('Please Fill Youtube Url', { autoClose: 2000 })
            return false
        }
        else if (params.process === 'Add' && formValue?.copyright == 'Select') {
            toast.error('Please Fill Copyright Details', { autoClose: 1000 })
            setOpen(true)
        }


        else {
            if (params?.process === 'Add') {
                dispatch(writerWork("Add", { work: inputValue, copyrightform: formValue }
                    , {
                        send: (e) => {
                            e.code === 200 ? navigate("/MainDashboard/WriterStoryDetails") : alert('something went wrong')
                        }
                    }
                ))

            }
            else if (params?.process === 'Edit') {

                dispatch(writerWork("Edit", { work: inputValue, copyrightform: formValue },
                    {
                        send: (e) => {
                            e.code === 200 ? navigate("/MainDashboard/WriterStoryDetails") : alert('something went wrong in edit')
                        }
                    }
                ))
            }
        }


        // else{alert('all ok')}

        // const UserData = {
        //     title: inputValue.title,
        //     description: inputValue.description,
        //     mainCharacter: inputValue.mainCharacter,
        //     subCategory: inputValue.subCategory,
        //     audience: inputValue.audience,
        //     // copyright: inputValue.copyright,
        //     status: inputValue.status,
        //     coverPicture: userImage,
        //     userWork: inputValue.userWork,
        //     youtubeUrl: ytubeUrl,
        //     id: location.state.id

        // };




        // if (inputValue.description === '' || inputValue.description.split(' ').length < 30 || inputValue.description.split(' ').length > 100) {
        //     toast.error('Description must be between 30 words to 100 words', { autoClose: 2000 })
        //     return false
        // }
        // if (inputValue.description === '' && inputValue.description.split(' ').filter((e) => e).length < 30 && inputValue.description.split(' ').filter((e) => e).length > 250) {
        //     toast.error('Please Enter Valid Work description ', { autoClose: 2000 })
        //     return false
        // }


        // if (inputValue.description === '' || inputValue.description.split(' ').filter((e) => e).length < 20 || inputValue.description.split(' ').filter((e) => e).length > 250) {
        //     toast.error('Description must be between 30 words to 250 words ', { autoClose: 2000 })
        //     return false
        // }

        // if (inputValue.subCategory === '') {
        //     toast.error('Please Choose work Category', { autoClose: 2000 })
        //     return false
        // }
        // if (inputValue.audience === '') {
        //     toast.error('Please Choose projected audience', { autoClose: 2000 })
        //     return false
        // }
        // if (inputValue.copyright === '') {
        //     toast.error('Please choose right options', { autoClose: 2000 })
        //     return false
        // }

        // if (location.state.id) {
        //     dispatch(writerWork("Edit", UserData,
        //         {
        //             send: (e) => {
        //                 e.code === 200 ? navigate(-1) : alert('something went wrong')
        //             }
        //         }
        //     ))
        // }
        // else {
        //     if (UserData.coverPicture === '') {
        //         toast.error('Please upload Cover picture', { autoClose: 2000 })
        //         return false
        //     }
        //     if (inputValue.userWork === null && ytubeUrl === null) {
        //         toast.error('Please upload valid Work', { autoClose: 2000 })
        //         return false
        //     }
        //     if (!confirm) {
        //         toast.error('Please fill copyright details', { autoClose: 2000 })

        //     }
        //     else {
        //         dispatch(writerWork("Add", { work: UserData, copyrightform: formValue }
        //             , {
        //                 send: (e) => {
        //                     e.code === 200 ? navigate("/MainDashboard/WriterStoryDetails") : alert('something went wrong')
        //                 }
        //             }
        //         ))
        //     }
        // }

    };

    if (STORE.errorPage) {
        navigate('/ErrorPage')
    }


    //upload file and youtube
    const [uploadFile, setUploadFile] = useState(false)
    const [uploadYtube, setUploadYtube] = useState(false)
    const [url, setUrl] = useState('')

    const handleUrl = (e) => {
        // setUrl(e.target.value)
        const willSplit = e.target.value?.split('/')
        setInputValue({ ...inputValue, 'youtubeUrl': willSplit[willSplit?.length - 1] })
    }
    var ytubeUrl = url.substring(32, 43)
    if (url.length == 28) {
        ytubeUrl = url.substring(17)
    }
    if (ytubeUrl === '') {
        ytubeUrl = null
    }
    const onChangefun = (val) => {
        setSubCat(val.map((e) => e[e.length - 1]))

    };
    useEffect(() => {
        // Array.from(new Set([...array1, ...array2]))
        setInputValue({ ...inputValue, subCategory: Array.from(new Set([...inputValue?.subCategory, ...subCat])) })
    }, [subCat])
    const userCat = STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase();
    const decideTxt = () => {
        if (params.process === 'Add') {
            if (formValue?.copyright === 'Select') {
                return { text: 'Click To Fill Copyright Details', bg: 'red' }
            } else return { text: 'Click To Update Copyright Details', bg: 'green' }
        } else if (params.process === 'Edit') {
            return { text: 'You Already Filled Copyright Details. Click To View', bg: 'green' }
        }
    }
    const fileAccept = (userCategory) => {
        if (userCategory === 'author' || userCategory === 'writer') {
            return '.pdf';
        }
        else if (userCategory === 'print or media') {
            return '.pdf,video/*';

        } else if (userCat === 'visual arts' || userCat === 'performing arts') {
            return 'image/*,video/*';
        }
        
        else { return 'audio/*,video/*' }

    }

    return (
        <div className='container-fluid' style={bg}>
            {
                progress ?
                    <UplProgBar variant="determinate" />
                    : ''
            }
            <div className="row d-flex justify-content-between ">
                <div className="col-md-6 col-sm-6 col-6">
                    <Button className='workbtn' variant='contained' sx={{ ...newButton, float: '', padding: '5px 10px', mt: 4, fontSize: "75%" }} onClick={() => navigate(-1)} >Go Back</Button>
                </div>
                <div className={"col-md-6 col-sm-6 col-6 text-right"} >
                    <Button className='workbtn' variant='contained' sx={{ ...newButton, float: '', padding: '5px 10px', mt: 4, fontSize: "75%" }} onClick={handleSubmit} > {location.state.id ? 'Update' : 'Submit'}  </Button>

                </div>
            </div>
            <div className="row">
                <div className="col-md-5 col-sm-12 col-12 text-center">
                    <input
                        id="file-input"
                        accept="image/*"
                        type="file"
                        style={{ display: "none" }}
                        onChange={handleTakeImage}
                        multiple
                    />
                    {inputValue.coverPicture === '' ?
                        <>
                            {userInfo.filepreview == null ? (<Box sx={{ width: '256px', height: '400px', backgroundColor: '#eee', marginTop: "1.5rem", mx: 'auto' }}>
                                <label for="file-input">
                                    <ImageIcon color="action" sx={{ fontSize: '70px', backgroundColor: '#eee', margin: '150px 90px' }} />
                                    <Typography sx={{ margin: '-150px auto' }} color="text.secondary">Add a cover</Typography>
                                </label>
                            </Box>) :
                                <>

                                    <Box sx={{ width: '256px', height: '400px', backgroundColor: '#eee', marginTop: "1.5rem", position: 'relative', mx: 'auto' }}>

                                        <img src={userInfo.filepreview} style={{ width: '256px', height: '400px', backgroundColor: '#eee' }} alt="" />

                                        <label style={{}}><Button onClick={handleImage} variant='contained' sx={{ ...newButton, padding: '10px 53px', color: 'white' }}>Click to Upload<FileUploadIcon color='white' /></Button></label>
                                        <label for="file-input" style={{ zIndex: 2, position: 'absolute', color: 'white', top: 0, right: 0, ...BackGround, height: '50px', width: '50px', borderRadius: '50%' }}><EditIcon sx={{ fontSize: '30px', margin: '8px auto' }} color='white' /></label>
                                    </Box>
                                </>
                            }
                        </>
                        :

                        <>
                            {userInfo.filepreview == null ? <>
                                <Box sx={{ width: '256px', height: '400px', backgroundColor: '#eee', marginTop: "1.5rem", position: 'relative', mx: 'auto' }}>
                                    <img src={getImgURL(inputValue.coverPicture)} style={{ width: '256px', height: '400px', backgroundColor: '#eee' }} alt="" />
                                    <label style={{}}><Button onClick={handleImage} variant='contained' sx={{ ...newButton, padding: '10px 53px', color: 'white' }}>Click to Upload<FileUploadIcon color='white' /></Button></label>
                                    <label for="file-input" style={{ zIndex: 2, position: 'absolute', color: 'white', top: 0, right: 0, ...BackGround, height: '50px', width: '50px', borderRadius: '50%' }}><EditIcon sx={{ fontSize: '30px', margin: '8px auto' }} color='white' /></label>
                                </Box>

                            </> : <>
                                <Box sx={{ width: '256px', height: '400px', backgroundColor: '#eee', marginTop: "1.5rem", position: 'relative', mx: 'auto' }}>
                                    <img src={userInfo.filepreview} style={{ width: '256px', height: '400px', backgroundColor: '#eee' }} alt="" />
                                    <label style={{}}><Button onClick={handleImage} variant='contained' sx={{ ...newButton, padding: '10px 53px', color: 'white' }}>Click to Upload<FileUploadIcon color='white' /></Button></label>
                                    <label for="file-input" style={{ zIndex: 2, position: 'absolute', color: 'white', top: 0, right: 0, ...BackGround, height: '50px', width: '50px', borderRadius: '50%' }}><EditIcon sx={{ fontSize: '30px', margin: '8px auto' }} color='white' /></label>
                                </Box>

                            </>
                            }
                        </>
                    }


                </div>
                <div className="col-md-7 col-sm-12 col-12">

                    <form className="col-md-12 col-sm-10 col-10 mx-auto" style={{ color: 'white' }} action="">
                        <div className='form-group'>
                            <h5 style={{ fontSize: "85%" }}>
                                <b>Title </b>
                            </h5>
                            <input className='workinput' style={{ width: '83%', padding: '10px', color: "black", height: "2rem", fontSize: "80%" }} name="title" value={inputValue.title} onChange={handleChangeInput} type="text" placeholder="Enter Your Work Title" />

                        </div>
                        <div className='form-group'>
                            <h5 style={{ fontSize: "85%" }}>
                                <b>Work Description </b>

                            </h5>
                            <textarea className='workinput' name="description" value={inputValue.description} onChange={handleChangeInput} style={{ width: '83%', padding: '5px', fontSize: "80%" }} rows="6"></textarea>
                        </div>
                        <div className='form-group'>
                            <div className="row">

                                <div className="col-md-10 col-12">
                                    <h5 style={{ fontSize: "85%" }}>
                                        <b>MeBookMeta Work Genre</b>
                                    </h5>
                                    <div className='d-flex flex-wrap'>
                                        {
                                            editWork?.subCategory && editWork?.subCategory?.length && editWork?.subCategory?.map((e, i) => (
                                                <span
                                                    key={"profileSubCat" + i}
                                                    style={{
                                                        color: "rgb(252,252,252)",
                                                        background: "#2B3467",
                                                        margin: ".2rem .2rem",
                                                        padding: "1px 8px 4px 8px",
                                                        borderRadius: "10px",
                                                        fontSize: "75%",
                                                    }}
                                                >
                                                    <span>{e?.name}</span>
                                                </span>
                                            ))
                                        }
                                    </div>
                                    <Cascader
                                        style={{
                                            width: '100%',
                                        }}
                                        size="small"
                                        options={array}
                                        onChange={onChangefun}
                                        multiple
                                        expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}
                                        maxTagCount="responsive"
                                        showCheckedStrategy={SHOW_CHILD}
                                        placeholder={<span style={{ fontSize: "22px", color: 'black', height: "20px" }}>{params?.process === 'Edit' ? 'Add Category' : 'Choose Category'}
                                        </span>}
                                    // && editWork?.subCategory && editWork?.subCategory?.length ? editWork?.subCategory?.map((e) => e.name)
                                    />
                                </div>
                                <div className="col-md-2"></div>
                            </div>
                        </div>
                        <div className='form-group'>
                            <div className="row">
                                <div className="col-md-10">
                                    <h5 style={{ fontSize: "85%" }}>
                                        <b>Projected Audience</b>
                                    </h5>
                                    <select style={{ padding: '10px', width: '100%', height: "2.5rem", fontSize: "75%" }} name="audience" onChange={handleChangeInput} id="audience">
                                        <option style={{ display: 'none' }} value=""> {inputValue.audience != '' ? inputValue.audience : "Who is your primary audience?"}  </option>
                                        <option value="Adult Education">Adult Education</option>
                                        <option value="College Audience">College Audience</option>
                                        <option value="Elementary/Secondary School">Elementary/Secondary School</option>
                                        <option value="Family">Family</option>
                                        <option value="Juvenile Audience">Juvenile Audience</option>
                                        <option value="Scholarly & Professional">Scholarly & Professional</option>
                                        <option value="PTrade">Trade</option>
                                        <option value="Young Adult Audience">Young Adult Audience</option>
                                    </select>

                                </div>
                                <div className="col-md-2"></div>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="row">

                                <div className="col-md-10" style={{ displa: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                    <Button variant='contained' sx={{ background: decideTxt()?.bg, padding: '5px 20px', fontSize: "75%", width: '100%' }}
                                        onClick={handleOpen}
                                    // onClick={()=>dispatch(callPopUp({type:"WorkCopyRight"}))}
                                    >{decideTxt()?.text} </Button>
                                </div>
                                <div className="col-md-2"></div>
                            </div>
                        </div>
                        <h5 style={{ fontSize: "85%" }}><b> Choose Options</b> </h5>
                        <div className="row mb-2">
                            <div className="col-md-5  d-flex justify-content-between">

                                <div>
                                    <p style={{ fontSize: '100%', fontWeight: '600', marginTop: 8 }} htmlFor="">
                                        Upload From Device
                                        <span>
                                            {

                                                STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'writer' || STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'author' ? ` (pdf only)`
                                                    :

                                                    STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'music' ||
                                                        STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'audio' ||
                                                        STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'sound' ? `(mp3, mp4 only)`
                                                        :
                                                        STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'music video' ? ` (mp4 only)`
                                                            :
                                                            STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'print or media' ? ` (pdf or mp4 only)` :
                                                                (STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'visual arts' ||STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'performing arts') ? ` (jpg or jpeg or mp4 only)`
                                                                    :
                                                                    ` (mp3 or mp4 only)`

                                            }
                                        </span>
                                    </p>
                                </div>
                                {edit == true && inputValue.userWork != null && (
                                    <div> <Checkbox onClick={() => setUploadFile(!uploadFile)} {...label} sx={{ color: 'white', ...newButton, fontSize: "50%" }} color="default" /></div>

                                )}
                                {edit === false && (<div> <Checkbox onClick={() => setUploadFile(!uploadFile)} disabled={uploadYtube || inputValue.userWork ? true : false} {...label} sx={{ color: 'white', ...newButton, fontSize: "50%" }} color="default" /></div>
                                )}

                            </div>


                            {
                                STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'writer' || STORE?.userProfile?.userProfile?.meProfile?.category?.toLowerCase() === 'author' ?

                                    ""
                                    :
                                    <div className="col-md-5 mt-1 d-flex justify-content-between">

                                        <div>
                                            <p style={{ fontSize: '100%', fontWeight: '600', marginTop: 8 }} htmlFor="">
                                                Upload Youtube Video Link
                                            </p>
                                        </div>
                                        {edit === true && inputValue.youtubeUrl != null && (< div > <Checkbox onClick={() => setUploadYtube(!uploadYtube)}{...label} sx={{ color: 'white', ...newButton, fontSize: "60%" }} color='default' /></div>)}
                                        {edit === false && (

                                            < div > <Checkbox onClick={() => setUploadYtube(!uploadYtube)} disabled={uploadFile || ytubeUrl ? true : false} {...label} sx={{ color: 'white', ...newButton, fontSize: "60%" }} color='default' /></div>
                                        )}
                                    </div>
                            }

                            <div className="col-md-2"></div>

                        </div>
                        <div className="row mb-3">
                            {uploadFile === true && (<div className="col-md-10 text-center">
                                <div>
                                    {/* {loading == true ? <Loading /> : null} */}
                                    <Button sx={{ ...newButton, textAlign: 'center', width: '100%' }} component="label" variant='contained' size="small"> Click here to Upload Work<PublishIcon color="white" sx={{ fontSize: '100%', margin: '' }} />
                                        <input
                                            type="file" max-file-size="1024" max="1"
                                            accept={fileAccept(userCat)}  // accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf"
                                            style={{ display: 'none' }} onChange={(e) => {
                                                pdfUpload(e)
                                                handlePdf(e)
                                            }} />
                                    </Button>
                                </div>

                            </div>
                            )}
                            {uploadYtube === true && (

                                <div className="col-md-10 text-center">
                                    <input onChange={handleUrl} value={inputValue?.youtubeUrl} type="text" style={{ width: '100%', padding: '8px' }} placeholder='Paste Your YouTube URL' />
                                </div>
                            )}
                        </div>
                    </form>
                </div>


                {
                    userPdf.pdfFilepreview != null ?
                        <>
                            <div className='mx-auto mt-2'>
                                <embed src={userPdf.pdfFilepreview} style={{ height: '800px', width: '800px' }} frameBorder="1" />
                                <div className='d-flex justify-content-end'>
                                </div>
                            </div>
                        </> :
                        inputValue.userWork != null ?
                            <>
                                <div className='mx-auto mt-2'>
                                    <embed src={inputValue.userWork !== '' ? getImgURL(inputValue.userWork) : ''} style={{ height: '800px', width: '800px' }} frameBorder="1" />
                                    <div className='d-flex justify-content-end'>
                                    </div>
                                </div>
                            </> : ''
                }
                {inputValue.youtubeUrl != '' && ytubeUrl === null ? <>
                    <div className="container-fluid text-center" style={{ backgroundColor: '', margin: '10px 0px' }} >
                        <iframe style={{ width: '1000px', height: '500px' }} src={`https://www.youtube.com/embed/${inputValue.youtubeUrl}`} frameBorder='0'
                            allow='autoplay'
                            allowFullScreen
                            title='video' ></iframe>
                    </div>
                </> : <>
                    {
                        url.includes("https://www.youtube.com") || url.includes("https://youtu.be") && (
                            <div className="container-fluid text-center" style={{ backgroundColor: '', margin: '10px 0px' }} ><iframe style={{ width: '1000px', height: '500px' }} src={`https://www.youtube.com/embed/${ytubeUrl}`} frameBorder='0'
                                allow='autoplay'
                                allowFullScreen
                                title='video' ></iframe>
                            </div>
                        )
                    }
                </>}

            </div >
            <TransitionsModal open={open} setOpen={setOpen} formValue={formValue} setFormValue={setFormValue} confirm={confirm} setConfirm={setConfirm} tick={tick} setTick={setTick} />
        </div >
    )
}



const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: '',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    overflowY: 'scroll'
};

function TransitionsModal({ open, setOpen, formValue, setFormValue, confirm, setConfirm, tick, setTick }) {

    const handleClose = () => setOpen(false);

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                className='BgCopyright'
                onClose={handleClose}
                closeAfterTransition
                slots={{ backdrop: Backdrop }}
                slotProps={{
                    backdrop: {
                        timeout: 500,
                    },
                }}
            >
                <Fade in={open}>
                    <Box sx={style}>
                        <Copyrights formValue={formValue} setFormValue={setFormValue} confirm={confirm} setConfirm={setConfirm} handleClose={handleClose} tick={tick} setTick={setTick} />
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
}