import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { Link, useLocation, useNavigate } from "react-router-dom";
import DoneIcon from "@mui/icons-material/Done";
import Button from '@mui/material/Button';

import "./CrouselPage.css";
import { NoEncryption } from "@mui/icons-material";
export default function Crousel() {
  const navigate = useNavigate()
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  return (
    <div className="container mx-auto text-center  my-5">
      <Carousel

        responsive={responsive}
      >
        <div className="pt-4">
          <Card
            id="workCard"
            elevation={15}
            sx={{
              width: "85%",
              height: "50%",
              backgroundColor: "rgb(253, 252, 251)",
              marginLeft: "25px",
              borderRadius: '10px',
              zIndex: 9999,
              fontFamily: "'Source Serif Pro', serif !important"
            }}
          >
            <CardContent id="Card" sx={{ fontWeight: "" }}>
              <p id='tag'
                style={{
                  // fontWeight: "bold",
                  fontSize: "30px",
                  color: "rgb(187 11 11)",

                }}
              >
                Standard Profile
              </p>

              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Site profile</p>
              </div>
              <div className="d-flex font1 ">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>standard site website</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>standard search</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>messaging</p>
              </div>
              <div className="row ">
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $10/mo
                  </Button>
                  {/* </Link> */}
                </div>
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $40/6mo
                  </Button>
                  {/* </Link> */}
                </div>
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $60/yr
                  </Button>
                  {/* </Link> */}
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
        <div className="pt-4">
          <Card
            id="workCard"
            elevation={15}
            sx={{
              width: "85%",
              height: "50%",
              backgroundColor: "rgb(253, 252, 251)",
              marginLeft: "25px",
              borderRadius: '10px',
              zIndex: 9999,
              fontFamily: "'Source Serif Pro', serif !important"
            }}
          >
            <CardContent id="Card" sx={{ fontWeight: "" }}>
              <p id='tag'
                style={{
                  // fontWeight: "bold",
                  fontSize: "30px",
                  color: "rgb(187 11 11)",

                }}
              >
                Enhanced Profile
              </p>

              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Enhanced site profile</p>
              </div>
              <div className="d-flex font1 ">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Enhanced site website</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Enhanced search</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>messaging</p>
              </div>
              <div className="row ">
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $20/mo
                  </Button>
                  {/* </Link> */}
                </div>
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $80/6mo
                  </Button>

                </div>
                <div className="col-md-10 mx-auto my-2">

                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $120/yr
                  </Button>
                  {/* </Link> */}
                </div>
              </div>

            </CardContent>
          </Card>
        </div>
        <div className="pt-4">
          <Card
            id="workCard"
            elevation={15}
            sx={{
              width: "85%",
              height: "50%",
              backgroundColor: "rgb(253, 252, 251)",
              marginLeft: "25px",
              borderRadius: '10px',
              zIndex: 9999,
              fontFamily: "'Source Serif Pro', serif !important"
            }}
          >
            <CardContent id="Card" sx={{ fontWeight: "" }}>
              <p id='tag'
                style={{
                  // fontWeight: "bold",
                  fontSize: "30px",
                  color: "rgb(187 11 11)",

                }}
              >
                Vetted profile
              </p>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p> Vetted profile</p>
              </div>

              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>vetted site website</p>
              </div>
              <div className="d-flex font1 ">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>enhanced search</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>messaging</p>
              </div>
              <div className="row ">
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $35/mo
                  </Button>
                  {/* </Link> */}
                </div>
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $140/6mo
                  </Button>

                </div>
                <div className="col-md-10 mx-auto my-2">

                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $210/yr
                  </Button>
                  {/* </Link> */}
                </div>
              </div>

            </CardContent>
          </Card>
        </div>
        <div className="pt-4">
          <Card
            id="workCard"
            elevation={15}
            sx={{
              width: "85%",
              height: "50%",
              backgroundColor: "rgb(253, 252, 251)",
              marginLeft: "25px",
              borderRadius: '10px',
              zIndex: 9999,
              fontFamily: "'Source Serif Pro', serif !important"
            }}
          >
            <CardContent id="Card" sx={{ fontWeight: "" }}>
              <p id='tag'
                style={{
                  // fontWeight: "bold",
                  fontSize: "30px",
                  color: "rgb(187 11 11)",

                }}
              >
                Premium Profile
              </p>

              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Premium site profile</p>
              </div>
              <div className="d-flex font1 ">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>vetted site website</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>expanded search</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>expanded messaging</p>
              </div>
              <div className="row ">
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $35/mo
                  </Button>
                  {/* </Link> */}
                </div>
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $140/6mo
                  </Button>
                  {/* </Link> */}
                </div>
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $210/yr
                  </Button>
                  {/* </Link> */}
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
        <div className="pt-4">
          <Card
            id="workCard"
            elevation={15}
            sx={{
              width: "85%",
              height: "485px",
              backgroundColor: "rgb(253, 252, 251)",
              marginLeft: "25px",
              borderRadius: '10px',
              zIndex: 9999,
              fontFamily: "'Source Serif Pro', serif !important"
            }}
          >
            <CardContent id="Card" sx={{ fontWeight: "" }}>
              <p id='tag'
                style={{
                  // fontWeight: "bold",
                  fontSize: "30px",
                  color: "rgb(187 11 11)",

                }}
              >
                Profile Payment Portal
              </p>

              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Profile payment</p>
              </div>
              <div className="d-flex font1 ">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Profile fulfillment</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>add-on upcoming</p>
              </div>
              {/* <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>messaging</p>
              </div> */}
              <div className="row mt-5">
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $15/mo
                  </Button>
                  {/* </Link> */}
                </div>

                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $150/yr
                  </Button>
                  {/* </Link> */}
                </div>
              </div>

            </CardContent>
          </Card>
        </div>
        <div className="pt-4">
          <Card
            id="workCard"
            elevation={15}
            sx={{
              width: "85%",
              height: "485px",
              backgroundColor: "rgb(253, 252, 251)",
              marginLeft: "25px",
              borderRadius: '10px',
              zIndex: 9999,
              fontFamily: "'Source Serif Pro', serif !important",

            }}
          >
            <CardContent id="Card" sx={{ fontWeight: "" }}>
              <p id='tag'
                style={{
                  // fontWeight: "bold",
                  fontSize: "30px",
                  color: "rgb(187 11 11)",

                }}
              >
                Expanded Search
              </p>

              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>Expanded site</p>
              </div>
              <div className="d-flex font1 ">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>search</p>
              </div>
              <div className="d-flex font1">
                {" "}
                <DoneIcon sx={{ mx: 2, color: "rgb(187 11 11)" }} />{" "}
                <p>and research</p>
              </div>

              <div className="row mt-5">
                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $15/mo
                  </Button>
                  {/* </Link> */}
                </div>

                <div className="col-md-10 mx-auto my-2">
                  {/* <Link to={"/PaymentPage"}> */}
                  <Button variant='contained' id='paymentbtn' onClick={() => navigate('/paymentpage')}
                    sx={{
                      borderRadius: "8px",
                      width: '100%',
                      fontSize: '1.4rem',
                      letterSpacing: '2px',
                      '&:hover': {
                        backgroundColor: '#100892',

                      },
                    }}
                  >
                    {" "}
                    $150/yr
                  </Button>
                  {/* </Link> */}
                </div>
              </div>
            </CardContent>
          </Card>
        </div>

      </Carousel>
    </div>
  );
}
