import React, { useState, useEffect } from "react";
import axios from "axios";

import image from '../../mepic.png'
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../profilepic.png";
import { Link, useNavigate, useLocation } from "react-router-dom";
import EditIcon from "@mui/icons-material/Edit";
import Badge from "@mui/material/Badge";

import PageviewIcon from '@mui/icons-material/Pageview';
// import '../creatornew.css'
export default function ProjectDeveloper() {
  const navigate = useNavigate();
  let user = sessionStorage.getItem('userName')
  let token = JSON.parse(localStorage.getItem("token"));
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {         
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div className="container-fluid MebookMetaimage">
      <div className="row">
        <div className="col-md-3"></div>
        <div
          className="col-md-6"
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />
          <Card
            id="card"
            sx={{
              width: "75%",
              height: "200px",
              backgroundColor: "#100892",
              borderRadius: "30px",
              m: "auto",
            }}
          >
            <CardContent>
              <div>
              <img
                  alt="Remy Sharp"
                  src={userInfo.filepreview}
                  style={{
                    width: "150px",
                    height: "130px",
                    margin: "auto",
                    borderRadius:'50%'
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "15px 0px", fontFamily: "Times New Roman" }}
              >
                 {user}, Project Developer
                  {/* {workProfile} */}
              </Typography>
            </CardContent>
          </Card>
          <Typography className="head"
            gutterBottom
            component="div"
            sx={{
              margin: "10px auto 20px ",
              width:'75%',
              fontSize: "2.1vw",
              fontWeight: "bold",
              color: "#100892",
            }}
          >
            Set Up Your  <span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#100892' }}>eBook</span><span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#100892' }}>eta</span> Reader Profile: Project Developer
          </Typography>
          <Card
            sx={{
              width: "75%",
              minHeight: "420px",
              maxHeight: "2500px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              <div
                style={{
                  backgroundColor: "white",
                  margin: "25px 100px 20px ",
                  borderRadius: "10px",
                }}
              >
                <h3
                  style={{
                    padding: "8px 0px",
                    fontFamily: "Times New Roman"
                  }}
                >
                  Project Developer
                </h3>
              </div>
              <div
                style={{
                  backgroundColor: "white",
                  margin: "25px 40px 10px ",
                  borderRadius: "50px",
                }}
              >
                <p
                  style={{
                    padding: "20px 20px",
                    textAlign: "justify",
                    fontFamily: "Times New Roman"
                  }}
                >
                  <b> This profile is reserved for MeBookMeta users who are readers and/or researchers involved in the acquisition and development of projects that derive from original entertaining or educational material, as expressed through any medium or channel. In the Global Media Marketplace, Project Developers include public or private program developers, documentary makers, innovators and acquisition partners who use all forms or writing, speech, performance, graphic and visual arts, music, sound and audio, dance and choreography, design, all aspects of media, television, film, and other original material.</b>
                </p>
              </div>
            </CardContent>
          </Card>

          <div className="d-flex justify-content-around my-5">
            <div>
              <Link to={'/ReaderProfile'}><button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
              >
                Go Back
              </button></Link>
            </div>
            
            <div>
              <Link to={""}><button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
                id="creator"
              >
                Choose
              </button></Link>
            </div>
            <div>
              <Link to={''}><button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
              >
                Continue
              </button></Link>
            </div>
          </div>
        </div>
        <div className="col-md-3"></div>
      </div>
    </div>
  );
}
