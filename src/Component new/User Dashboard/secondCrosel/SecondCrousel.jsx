import React from "react";
import { useEffect } from "react";
import Carousel from "react-multi-carousel";
import axios from "axios";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { useState } from "react";
import { Paper } from "@mui/material";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import { Box, Container } from "@mui/system";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function SecondCrousel() {
  const [allData, setAllData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [message, setMessage] = useState(false);
  const [getUserById, setGetUserById] = useState([]);
  const getUserDetails = () => {
    axios
      .get("http://54.246.61.54:3002/getWork")
      .then((e) => {
        setAllData(e.data.writerDetail.splice(5, 5));
        // console.log(e.data.writerDetail);
      })
      .catch((err) => console.log("err", err));
    // console.log("-->", allData.data.writerDetail);`
  };
  // console.log("render");
  const handleClose = () => {
    setModalOpen(false);
  };
  const modelOpen = (i, j) => {
    setGetUserById(
      allData && allData.length ? allData.filter((e) => e._id == i)[0] : false
    );
    setModalOpen(true);
  };
  useEffect(() => {
    // console.log("useEffect render");
    getUserDetails();
  }, []);

  return (
    <Container sx={{ padding: "20px" }}>
      <Box sx={{ paddingLeft: "30px" }}>
        <Typography variant="h3">Because You Like Fanfiction</Typography>
        <Typography variant="subtitle1" component="h5" sx={{ color: "gray" }}>
          Based on your favorite genres
        </Typography>
      </Box>

      <Container style={{ display: "flex", padding: "20px" }}>
        {allData && allData.length ? (
          allData.map((i) => (
            <Paper
              elevation={10}
              key={i._id}
              sx={{ height: 350, width: "100%", mx: 1 }}
              onClick={() => modelOpen(i._id, i.coverPicture)}
            >
              <CardContent>
                <img
                  src={`http://54.246.61.54:3002/uploads/${i.coverPicture}`}
                  style={{ height: 250, width: "100%" }}
                  alt="image"
                />
                <Typography variant="subtitle1">
                  Catagory: {i.catagory}
                </Typography>
              </CardContent>
            </Paper>
          ))
        ) : (
          <h1>No Data</h1>
        )}

        {/* <Modal open={modalOpen} onClose={handleClose}>
        <div style={{ width: "200px", height: "200px" }}>
          <h1> Hi i am image</h1>
        </div>
      </Modal> */}
      </Container>
      <Modal
        open={modalOpen}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Card sx={style}>
          <CardContent>
            <div className="row">
              <div className="col-md-6">
                <img
                  src={`http://54.246.61.54:3002/uploads/${getUserById.coverPicture}`}
                  style={{ height: 250, width: "100%" }}
                  alt=""
                />
              </div>
              <div className="col-md-6">
                <h3>
                  {" "}
                  <b>{getUserById.title}</b>
                </h3>
                <p> {getUserById.titleStory}</p>
              </div>
            </div>
          </CardContent>
          <div className="row justify-content-between mt-3">
            {message === false ? (
              <button
                type="button"
                class="btn btn-primary"
                style={{ background: "#100892" }}
                // onClick={() => setMessage(true)}
              >
                Message
              </button>
            ) : (
              <button
                type="button"
                class="btn btn-primary"
                style={{ background: "#100892" }}
                // onClick={() => setMessage(!message)}
              >
                Send
              </button>
            )}

            <button
              type="button"
              class="btn btn-primary"
              style={{ background: "#100892" }}
              // onClick={handleView}
            >
              Continue Reading
            </button>
          </div>
          {message === true ? (
            <div className="mt-3">
              <textarea name="" id="" cols="75" rows="5"></textarea>
            </div>
          ) : null}
        </Card>
      </Modal>
    </Container>
  );
}

export default SecondCrousel;
