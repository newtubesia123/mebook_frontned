import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MailIcon from '@mui/icons-material/Mail';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Dashboard from './Dashboard';
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { sidebarItem } from './sideBarItem/SideBar';
import UserProfile from './UserProfile';
import MyPitch from './MyPitch';
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import DashboardMain from "./DashboardMain/DashboardMain";
import FirstCrousel from "./firstCrosel/FirstCrousel";
import CommonAppbar from '../CommonAppbar';

import UserAppBar from './UserAppBar';
import { useDispatch, useSelector } from 'react-redux';
import WritePitch from '../WritePitch';
import WriterStory from '../WriterStory';
const drawerWidth = 180;


function UserDashboard(props) {
  const { username } = useParams()
  const dispatch = useDispatch()
  const liveState = useSelector((state) => state.sideBar)
  console.log("liveState", liveState)
  const obj = {
    1: <DashboardMain />,
    2: <UserProfile name={username} />,
    3: <WritePitch />,
    4: <WriterStory />
  }

  const { window } = props;
  const navigate = useNavigate()
  const location = useLocation()
  console.log('location', location.state)
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [click, setClick] = React.useState('Home')
  const [id, setId] = React.useState(1)
  const [open, setOpen] = React.useState(false)
  // if(location.state == 'PaySuccess'){
  //   setOpen(!open)
  // }
  // {location.state== 'PaySuccess' ? setOpen(false):setOpen(true) }
  const handleSubmit = () => {
    navigate('/ProfileSubscription')
  }
  const handleClose = () => {
    navigate(-1)

  }

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container = window !== undefined ? () => window().document.body : undefined;
  // const useStyles = makeStyles({

  //   paper: {
  //     background: "blue"
  //   }
  // })

  return (
    <>
      <UserAppBar />
      <Box className='sidebar' sx={{ display: 'flex', mt: 3, }}>
        {/* new */}
        <CssBaseline />
        {/* <AppBar
        position="fixed"
        sx={{
          width: { sm: `calc(100% - ${drawerWidth}px)` },
          ml: { sm: `${drawerWidth}px` },
          backgroundColor:  '#100892'
        }}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div" sx={{ml:'auto'}}>
          Help
          </Typography>
        </Toolbar>
      </AppBar> */}
        {/* <Dialog
        open={location.state == 'PaySuccess'? open : !open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Please Click on Pay button for further process 
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose}>Back</Button>
          <Button onClick={handleSubmit} autoFocus>
            Pay
          </Button>
        </DialogActions>
      </Dialog> */}
        <Box className=''
          component="nav"
          sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
          aria-label="mailbox folders"
        >
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}

          {/* <Drawer
          
            variant="permanent"
            sx={{
              display: { xs: 'none', sm: 'block' }, backgroundColor: 'red',
              transition: 'ease 2s',
              opacity: '0.8',
              '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
            }}
            open
          > */}
          <div className='' >
            <Toolbar />
            <List sx={{ textAlign: 'center', fontSize: '30px', position: 'fixed' }} >
              {
                sidebarItem.map((i) => (
                  <ListItem sx={{ '&:hover': { letterSpacing: '2px', textAlign: 'center' } }} disablePadding>

                    <p onClick={() => {
                      dispatch({ type: "increament", payload: i.id })
                      // navigate(`${i.link}${i.listItem==="Profile"?`/${username}`:""}`)
                      setClick(i.listItem)
                    }} className={click === i.listItem ? "m-auto hoverBtnPAfter hoverBtnPBefore" : "m-auto hoverBtnPBefore"} >{i.icon} {i.listItem} <span></span></p>
                  </ListItem>
                ))
              }

            </List>

          </div>
          {/* </Drawer> */}
        </Box>
        <Box
          component="main"
          sx={{ flexGrow: 1, width: { sm: `calc(100% - ${drawerWidth}px)` } }}
        >
          {/* <Toolbar /> */}
          {
            obj[liveState]
          }

        </Box>
      </Box>
    </>
  );
}

UserDashboard.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default UserDashboard;
// import { Typography } from "@mui/material";
// import React from "react";
// import CommonAppbar from "../CommonAppbar";
// import CrouselUser from "./CrouselUser";
// import DashboardCard from "./DashboardCard";
// import DashboardMain from "./DashboardMain/DashboardMain";
// import FirstCrousel from "./firstCrosel/FirstCrousel";
// import SecondCrousel from "./secondCrosel/SecondCrousel";
// import UserImageCrousel from "./UserImageCrousel";
// export default function UserDashboard() {
//   let userName = sessionStorage.getItem("userName");
//   let val = JSON.parse(localStorage.getItem("MultiOptions"));
//   console.log("val", val);
//   var identity = sessionStorage.getItem("identity Option");
//   let workProfile = localStorage.getItem("Work profile");
//   return (
//     <>
//       <div className="">
//         <CommonAppbar />
//       </div>
//       <div>
//         <DashboardMain />
//       </div>
//       <div
//         className="container-fluid "
//       >
//         <FirstCrousel />
//         <SecondCrousel />
//       </div>
//     </>
//   );
// }
