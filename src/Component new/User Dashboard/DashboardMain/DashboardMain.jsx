import React, { useEffect, useState } from "react";
import { Box, Grid, Typography } from "@mui/material";
// import { QRCodeCanvas } from "qrcode.react";
// import FirstCrousel from "../firstCrosel/FirstCrousel";

import "./DashboardMain.css";
// import UsersFeed from "../UsersFeed";
// import { SERVER } from "../../../server/server";
import { useDispatch, useSelector } from "react-redux";
// import DashboardCard from "../userDashboardItem/DashboardCard";
import {
  callPopUp,
  // getPitch,
  // getUserId,
  userDashboardDetail,
  userProfile,
} from "../../../Action";
// import { useParams } from "react-router-dom";
// import Skeleton from "@mui/material/Skeleton";
import CircularProgress from "@mui/material/CircularProgress";
import pic from "../../../mepic.png";
import { circularProgressbarStyle } from "../../../assets/common/theme";
import DashCrousel from "../firstCrosel/DashCrousel";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Accordion from "@mui/material/Accordion";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { BackGround } from "../../background";
import ProfileCrousel from "../firstCrosel/ProfileCrousel";
import DebounceCard from "../DebounceCard";
import DashboardSlider from "../userDashboardItem/DashboardSlider";
import MeCircularProgress from "../../../components/componentsC/meCircularProgress/MeCircularProgress";
import { useParams } from "react-router-dom";



function DashboardMain({ name }) {
  const [CircularProgress, setCircularProgress] = useState(false)
  const STORE = useSelector((state) => state);
  const params = useParams()
  const dispatch = useDispatch();
  useEffect(() => {
    if (window?.location?.search?.includes('?success=true')) {
      if (!STORE?.userDashboardDetail?.userWorkDetail?.subscriptionType?.isSubscribed) {
        dispatch(userDashboardDetail());
        setCircularProgress(true)
      } else {
        dispatch(callPopUp({ type: "SuccessPayment" }))
        setCircularProgress(false)
      }
    }
  }, [STORE?.userDashboardDetail?.userWorkDetail?.subscriptionType?.isSubscribed])
  useEffect(() => {
    window.scrollTo(0, 0)
    dispatch(userDashboardDetail());
    dispatch(userProfile());

    // dispatch(userProfile(userId))
  }, [dispatch]);

  let keys;
  if (STORE.userDashboardDetail.userWorkDetail) {
    keys = Object.keys(STORE.userDashboardDetail.userWorkDetail?.top5_work);
  }

  // logined user id
  const loginUserId = STORE?.getData?.userData?.data?._id;
  // console.log(loginUserId,"loginUserId")
  return (
    <>
      <div className=" container-fluid dashBody" style={{paddingRight:"1.5rem", paddingLeft:"1.5rem", overflowY:"hidden"}} >
        {CircularProgress ? <MeCircularProgress /> : ''}
        <DashboardSlider />
        <Box>
          {

            STORE.userDashboardDetail.userWorkDetail?.top5_work ? (
              keys && keys.length ? (
                keys.map((e, index) =>
                  STORE.userDashboardDetail.userWorkDetail?.top5_work[e]
                    .length ? (
                    <Box key={e} >
                      <DashCrousel name={e} data={STORE.userDashboardDetail.userWorkDetail?.top5_work[e]} />
                    </Box>
                  ) : (
                    <h5
                      key={e}
                      style={
                        keys.indexOf(e) === 0
                          ? {
                            color: "white",
                            display: "none",
                            textAlign: "center",
                            marginTop: "100px",
                          }
                          : { display: "none" }
                      }
                    >
                      No Work
                    </h5>
                  )
                )
              ) : (
                <h5 style={{ color: "white", display: "none" }}>Not Work</h5>
              )
            ) : (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  width: "100%",
                  minHeight: "100vh",
                }}
              >
                {/* <CircularProgress sx={circularProgressbarStyle} /> */}
                <MeCircularProgress />
              </Box>
            )
          }
          <Box>
            <ProfileCrousel data={STORE.userDashboardDetail.userWorkDetail} loginUserId={loginUserId} />
          </Box>
          <div>
            <DebounceCard />
          </div>
        </Box>

      </div>
    </>
  );
}

export default DashboardMain;
