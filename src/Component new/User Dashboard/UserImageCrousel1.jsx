import React, { useState, useEffect } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Carousel from "react-multi-carousel";
import { Link, useNavigate, useLocation } from "react-router-dom";

import "react-multi-carousel/lib/styles.css";
import pic from "../../book.jpg";
import { deepOrange, deepPurple, green, pink } from "@mui/material/colors";
import { allData, getUser } from "./userData";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function UserImageCrousel1() {
  const navigate = useNavigate();

  const [user, setUser] = useState([]);
  const [userId, setUserId] = useState("");
  const [getUserById, setGetUserById] = useState([]);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [open, setOpen] = React.useState(false);

  useEffect(() => {
    getUser().then((e) => setUser(e));
  }, []);
  const showModal = (userId) => {
    axios
      .get(`http://54.246.61.54:3002/getWorkbyId/${userId}`)

      .then((res) => {
        console.log("res.data by id---> ", res.data.writerDetail[0].title);
        setGetUserById(res.data.writerDetail[0]);
      });
    getUser().then((e) => setUser(e));
    setOpen(true);
    // console.log("kfjgasklgf", userId);
    // console.log(user);
  };
  const handleClose = () => {
    getUser().then((e) => setUser(e));
    setOpen(false);
  };
  useEffect(() => {
    console.log(getUserById);
  }, [getUserById]);
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  let Array = [];
  for (let i = 0; i < user.length; i++) {
    Array.push(user[i].name);
  }

  console.log("userId", userId);
  const handleView = () => {
    navigate("/ViewPublishedStory", { state: { userid: userId } });
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    // axios

    //   .get("http://34.244.150.71:3002/getWork")

    //   .then((res) => {
    //     console.log("res.data---> ", res.data.writerDetail);
    //     setUser(res.data.writerDetail);
    //   })
    //   .catch((err) => console.log(err));
  };
  const [message, setMessage] = useState(false);
  const [next, setNext] = useState(0);
  return (
    <div className="container-fluid">
      {user && user.length ? (
        <Carousel showDots={true} responsive={responsive}>
          {user.splice(0, 2).map(() => (
            // yaha hamko jitna scroll chahye utna splice me pass kr denge/ user.lenght v daal skte hai
            <div className="text-center d-flex ">
              {user.splice(0, 3).map((i) => {
                return (
                  <>
                    <Card
                      key={i._id}
                      sx={{ height: 350, width: "100%", mx: 1 }}
                    >
                      <CardContent onClick={() => showModal(i._id)}>
                        <img
                          src={`http://54.246.61.54:3002/uploads/${i.coverPicture}`}
                          style={{ height: 350, width: "100%" }}
                          alt=""
                        />
                      </CardContent>
                    </Card>
                  </>
                );
              })}
            </div>
          ))}
        </Carousel>
      ) : (
        <Carousel showDots={true} responsive={responsive}>
          <div className="text-center d-flex ">
            <Card sx={{ height: 350, width: "100%", mx: 1 }}>
              <CardContent>
                <h2>Image unAvailable</h2>
              </CardContent>
            </Card>
          </div>
        </Carousel>
      )}
      {/* <Modal
        footer={null}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        
      </Modal> */}
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Card sx={style}>
          <CardContent>
            <div className="row">
              <div className="col-md-6">
                <img
                  src={`http://54.246.61.54:3002/uploads/${getUserById.coverPicture}`}
                  style={{ height: 350, width: "100%" }}
                  alt=""
                />
              </div>
              <div className="col-md-6">
                <h3>
                  {" "}
                  <b>{getUserById.title}</b>
                </h3>
                <p> {getUserById.titleStory}</p>
              </div>
            </div>
          </CardContent>
          <div className="row justify-content-between mt-3">
            {message === false ? (
              <button
                type="button"
                class="btn btn-primary"
                style={{ background: "#100892" }}
                onClick={() => setMessage(true)}
              >
                Message
              </button>
            ) : (
              <button
                type="button"
                class="btn btn-primary"
                style={{ background: "#100892" }}
                onClick={() => setMessage(!message)}
              >
                Send
              </button>
            )}

            <button
              type="button"
              class="btn btn-primary"
              style={{ background: "#100892" }}
              onClick={handleView}
            >
              Continue Reading
            </button>
          </div>
          {message === true ? (
            <div className="mt-3">
              <textarea name="" id="" cols="75" rows="5"></textarea>
            </div>
          ) : null}
        </Card>

        {/* <div className="row justify-content-between mt-3">
          {message === false ? (
            <button
              type="button"
              class="btn btn-primary"
              style={{ background: "#100892" }}
              onClick={() => setMessage(true)}
            >
              Message
            </button>
          ) : (
            <button
              type="button"
              class="btn btn-primary"
              style={{ background: "#100892" }}
              onClick={() => setMessage(!message)}
            >
              Send
            </button>
          )}

          <button
            type="button"
            class="btn btn-primary"
            style={{ background: "#100892" }}
            onClick={handleView}
          >
            Continue Reading
          </button>
        </div>
        {message === true ? (
          <div className="mt-3">
            <textarea name="" id="" cols="75" rows="5"></textarea>
          </div>
        ) : null} */}
      </Modal>
         
    </div>
  );
}
