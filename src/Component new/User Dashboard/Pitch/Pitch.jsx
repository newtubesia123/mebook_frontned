// import React,{useEffect} from 'react'
// import './pitch.css'
// import { useParams } from 'react-router-dom';
// import { useDispatch, useSelector } from 'react-redux';
// import { getPitch } from '../../../Action';

// const Pitch = ({id}) => {

//     const dispatch = useDispatch();
//     const STORE = useSelector((state) => state.getPitch)
//     const params = useParams()

//     useEffect(() => {
//         dispatch(getPitch('get',id?id:false));
//       }, []);

//     return (
//         <div className='container'>
//             <div id="pitchfield">
//                 {
//                     STORE.loading?
//                     <h5>
//                         loading.....
//                     </h5>
//                     :
//                     STORE.userPitch?.message?.pitch && STORE.userPitch?.message?.pitch.length && !STORE.loading?
//                     <h6>{STORE.userPitch?.message?.pitch.join(' ')}</h6>
//                     :
//                     <h6>no pitch yet</h6>

//                 }
//                 </div>
//         </div>
//     )
// }

// export default Pitch


import React, { useEffect } from 'react'
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
// import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getPitch } from '../../../Action';

const Pitch = ({ id }) => {

  const dispatch = useDispatch();
  const STORE = useSelector((state) => state)
  // const params = useParams()
  // const [userId, setUserId] = useState("")
  useEffect(() => {
    if (id) dispatch(getPitch('get', id,()=>console.log("getting...")));
  }, [id,dispatch]);
  return (
    <div
      style={{
        boxShadow: "0px 5px 10px 0",
        borderRadius: "15px",
        marginTop: "2rem",
      }}
    >
      {/* <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>
            {
              id === STORE.getProfileData?.userData?._id ? "Click here to see my personal pitch to you and the global media market place" : `Click here to see my personal pitch to you and global media market place ${STORE.userProfile.userProfile?.meProfile?.userId.name.split(" ")[0]}`
            }

          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            {
              STORE.getPitch.loading ?
                <h5>
                  loading...
                </h5>
                :
                STORE.getPitch.userPitch?.message?.pitch && STORE.getPitch.userPitch?.message?.pitch.length && !STORE.getPitch.loading ?
                  <h6>{STORE.getPitch.userPitch?.message?.pitch.join(' ')}</h6>
                  :
                  <h6>No Pitch Uploaded Yet</h6>

            }
          </Typography>
        </AccordionDetails>
      </Accordion> */}
      {STORE.getPitch.userPitch?.message?.pitch &&
        STORE.getPitch.userPitch?.message?.pitch.length ? (
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography sx={{color:"black"}}>
              {id === STORE.getProfileData?.userData?._id
                ? "Click here to see my personal pitch to you and the global media market place"
                : `Click here to see my personal pitch to you and global media market place ${STORE.userProfile.userProfile?.meProfile?.userId.name.split(
                  " "
                )[0]
                }`}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
          <Typography variant='body1' sx={{color:"black"}}>
              {STORE.getPitch.loading ? (
                "loading..."
              ) : (
                STORE.getPitch.userPitch?.message?.pitch.join(" ")
              )}
            </Typography>
          </AccordionDetails>
        </Accordion>
      ) : null}
    </div>
  );
}







export default Pitch