import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContentText from "@mui/material/DialogContentText";
import pic from "../../../mepic.png";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Slide from "@mui/material/Slide";
import { useNavigate } from "react-router-dom";
import { BackGround, newButton } from "../../background";
import MeLogo from "../../../assets/MeLogoMain";
import { Hidden } from "@mui/material";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});




export default function PitchInfo() {
    const navigate = useNavigate()
    // Dialog
    const [open, setOpen] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);
    const [open3, setOpen3] = React.useState(false);
    const [open4, setOpen4] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpen2 = () => {
        setOpen2(true);
    };

    const handleClose2 = () => {
        setOpen2(false);
    };
    const handleClickOpen3 = () => {
        setOpen3(true);
    };

    const handleClose3 = () => {
        setOpen3(false);
    };
    const handleClickOpen4 = () => {
        setOpen4(true);
    };
    return (
        <div
            className="container-fluid  text-center"
            style={{ minHeight: "100vh", alignSelf: 'center', ...BackGround, overflowY:"hidden"
            //  margin:"4rem auto" 
                 
            }}
        >
            <div style={{marginTop:"5rem"}}>
                {/* <img src={pic} alt="" style={{ width: '8rem', height: '9rem', margin: '25px auto' }} /> */}
                <MeLogo/>

            </div>

            <div className="container1" style={{marginTop:"2rem", padding:"0rem 1rem"}}>
                <div className="row ">
                    <div className="col-md-6 mx-auto">
                        <Button
                            variant="outlined" size="small"
                            sx={{ ...newButton, width: "100%", height: "2.5rem" }}
                            onClick={handleClickOpen}
                        >
                            What is a Pitch ?
                        </Button>
                        <Dialog
                            open={open}
                            TransitionComponent={Transition}
                            keepMounted
                            onClose={handleClose}
                            aria-describedby="alert-dialog-slide-description"
                        >
                            <DialogContent>
                                <DialogContentText id="alert-dialog-slide-description">
                                    <h4 style={{ fontSize: "100%" }}>
                                        <span style={{ color: "red", fontSize: "100%" }}>M</span>ePitch
                                    </h4>
                                    <h5 style={{ fontSize: "100%" }}>
                                        {" "}
                                        Your “pitch” is a short written or video presentation
                                        where you describe who you are, what you do, how and why
                                        you do it, your background and experience, what makes you
                                        and your work unique, the audience you seek and why that
                                        audience should pay attention to you.{" "}
                                    </h5>
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose}>Close</Button>
                            </DialogActions>
                        </Dialog>
                    </div>
                </div>
                <div className="row">


                    <div className="col-md-6 my-4 mx-auto">
                        <Button
                            variant="outlined" size="small"
                            sx={{ ...newButton, width: "100%", height: "2.5rem" }}
                            onClick={handleClickOpen2}
                        >
                            What is the Importance of Pitching ?
                        </Button>
                        <Dialog
                            open={open2}
                            TransitionComponent={Transition}
                            keepMounted
                            onClose={handleClose2}
                            aria-describedby="alert-dialog-slide-description"
                        >
                            <DialogContent>
                                <DialogContentText id="alert-dialog-slide-description">
                                    <h5 style={{ fontSize: "100%" }}>The Importance of Pitching</h5>
                                    <h5 style={{ fontSize: "100%" }}>
                                        In 1920, an aspiring young illustrator was fired from his
                                        job at a newspaper. 'You lack any form of creativity!' his
                                        boss told him. Over the next seven years, the artist took
                                        on odd jobs to pay the bills, all while continuing to
                                        pursue his dream of becoming an animator. During this
                                        time, he pitched, or presented, his cartoons countless
                                        times to banks, motion picture companies, and other
                                        potential supporters. Despite delivering over 300 pitches,
                                        he was rejected every single time.
                                    </h5>
                                    <h5 style={{ fontSize: "100%" }}>
                                        {" "}
                                        Instead of giving up, the young artist took every
                                        rejection to heart and poured over the reasons his pitches
                                        were not securing investments. Each time, he adjusted his
                                        pitch and tried again. In 1928, he finally made a
                                        successful pitch to a motion picture company who agreed to
                                        use his animations in a film synchronized to music{" "}
                                    </h5>
                                    <h5 style={{ fontSize: "100%" }}>
                                        The film was a success and jump-started the career of the
                                        young artist, who was none other than Walt Disney. Through
                                        practice and persistence, Disney was able to turn his 300
                                        rejections into what would ultimately become one of the
                                        most valuable brands in the world. That is the power of a
                                        great pitch.
                                    </h5>
                                    <h5 style={{ fontSize: "100%" }}>
                                        On <span style={{ color: "red" }}>M</span>
                                        <span style={{ color: "blue" }}>eBook</span>
                                        <span style={{ color: "red" }}>M</span>
                                        <span style={{ color: "blue" }}>eta</span>, , users will
                                        create two pitches: the first is a personal pitch related
                                        to your Identity profile, and the second is a business
                                        pitch related to your Work profile. Your personal pitch
                                        introduces you to the Global Media Marketplace, while your
                                        business pitch is purposed to present your work, skills,
                                        abilities, experience or status to potential consumers or
                                        collaborators.
                                    </h5>
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose2}>Close</Button>
                            </DialogActions>
                        </Dialog>
                    </div>
                </div>
                <div className="row">


                    <div className="col-md-6 mx-auto">
                        <Button
                            variant="outlined" size="small"
                            sx={{ ...newButton, width: "100%", height: "2.5rem" }}
                            onClick={handleClickOpen3}
                        >
                            How do I create a Pitch ?
                        </Button>
                        <Dialog
                            open={open3}
                            TransitionComponent={Transition}
                            keepMounted
                            onClose={handleClose3}
                            aria-describedby="alert-dialog-slide-description"
                        >
                            <DialogContent>
                                <DialogContentText id="alert-dialog-slide-description">
                                    <p>How to Create a Pitch</p>
                                    <p>To write a great pitch, follow these steps:</p>
                                    <p>1. Identify your target audience/consumers;</p>
                                    <p>
                                        2. Show that you understand your audience’s/consumers’
                                        needs and challenges;{" "}
                                    </p>
                                    <p>
                                        3. Explain the value of meeting needs and challenges
                                        versus not meeting those needs;
                                    </p>
                                    <p>
                                        4. Identify how your product or service can help them meet
                                        their needs and solve problems;
                                    </p>
                                    <p>5. Back up your claims with facts and data;</p>
                                    <p>6. Ask them directly for their business</p>
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose3}>Close</Button>
                            </DialogActions>
                        </Dialog>
                    </div>
                </div>
                <div className="row">


                    <div className="col-md-6 my-4 mx-auto">
                        <Button
                            variant="outlined" size="small"
                            sx={{ ...newButton, width: "100%", height: "2.5rem" }}
                            href="https://www.bing.com/videos/search?q=How+to+creat+a+pitch&view=detail&mid=2AACE894B01B332382DB2AACE894B01B332382DB&FORM=VIRE"
                        // onClick={handleClickOpen4}
                        >
                            {" "}
                            I need help!
                        </Button>

                    </div>
                </div>

            </div>

            <div className="row">
                <div className="col-md-4 my-2">
                    <Button onClick={() => navigate(-1)} variant="contained" size="small" sx={{ ...newButton, py: "10px", width: "10rem" }}>
                        Go Back
                    </Button>
                </div>
                <div className="col-md-4"></div>
                <div className="col-md-4 my-2">
                    <Button onClick={() => navigate('/MainDashboard/MyPitch')} variant="contained" size="small" sx={{ ...newButton, py: "10px", width: "10rem" }}>
                        Write Pitch
                    </Button>

                </div>
            </div>

        </div>

    );
}
