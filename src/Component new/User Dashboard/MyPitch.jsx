 import React, { useState,useEffect } from "react";
import axios from "axios";
import image from '../../mepic.png'
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link} from "react-router-dom";
import pic from "../../Screenshot_2.png";
import { Toolbar } from "@mui/material";
import { BackGround } from "../background";


export default function MyPitch() {

  const bg={...BackGround, minHeight:"100vh"}

  const [pitch,setPitch]=useState("")
  let userPitch = localStorage.getItem('userPitch')
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        setPitch(res.data.userData)
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="container-fluid" style={bg}>
      <Toolbar/>
      <div className="row">
        <div className="mx-auto text-center col-md-8 offset-md-2">
            {/* <img src={image} height={100} width={180} alt="" /> */}
            <h1 style={{color:"white"}}>Hello {pitch.name} Your Most Important 250 Words are</h1>
        </div>
        <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              minHeight: "380px",
              maxHeight: "800px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              color:'white',
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
                <h3 style={{borderBottom:"5px solid",width:'110px',margin:'10px auto'}}>Your Pitch</h3>
            {userPitch}
            </CardContent>
          </Card>
          {/* <div className="d-flex justify-content-between mt-3 mb-3">
         <Link to={'/UserDashboard'}><button
              type="button"

              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Go Back
            </button></Link>
            <Link to={"/DetailSaved"}>
              <button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
              >
                Edit Pitch
              </button>
            </Link>
          </div> */}
        </div>
      </div>
    </div>
  );
}
