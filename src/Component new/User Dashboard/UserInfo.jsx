import React, { useState, useEffect } from "react";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";

import axios from "axios";
import { useNavigate, useParams, Link } from "react-router-dom";
import Grid from "@mui/material/Grid";
import image from "../../mepic.png";
import Button from "@mui/material/Button";
import { useLocation } from "react-router-dom";
import { BackGround, newButton } from "../background";
import MeLogo from "../../assets/MeLogoMain";

// import "./Edit.css";
export default function UserInfo() {
  const params = useParams();
  // let id = sessionStorage.getItem("key");
  const navigate = useNavigate();
  const location = useLocation();
  const [inputField, setInputField] = useState({
    contact_info: "",
    dob: "",
    address: "",
    qualification: "",
    status: "",
  });
  const inputsHandler = (e) => {
    setInputField({
      ...inputField,
      [e.target.name]: e.target.value,
    });
    console.log(inputField);
  };
  useEffect(() => {
    setSuccess(true);
    setTimeout(() => {
      setSuccess(false);
    }, 5000);
  }, []);

  const submitButton = () => {
    const userData = {
      contact_info: inputField.contact_info,
      dob: inputField.dob,
      address: inputField.address,
      qualification: inputField.qualification,
      status: inputField.status,
    
    };
    console.log("userData-->", userData);
    axios
      .post("http://54.246.61.54:3002/edit-user", { userData: userData,id: location.state.id})
      .then((res) => {
        console.log("value", res.data);
        
        if (res.data.isSuccess) {
          console.log("data---> ", res.data);
       alert(res.data.message)
          // navigate("/SignIn");
        }
      })
      .catch((err) => console.log(err));
  };
  const [success, setSuccess] = React.useState(false);

  return (
    <div style={BackGround}>
      <Collapse in={success}>
        <Alert
          severity="success"
          variant="filled"
          onClose={() => setSuccess(false)}
        >
          Registration successfully
        </Alert>
      </Collapse>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
        sx={{ padding: "25px 5px" }}
      >
        <Grid item xs={1} md={1} sm={1}></Grid>
        <Grid
          item
          xs={10}
          md={10}
          sm={10}
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          {/* <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          /> */}
          <MeLogo/>
          <b>
            {" "}
            <p style={{ fontSize: "20px", color:"white" }}>Edit Details</p>
          </b>
          <form style={{ margin: "50px 0px" , color:"white"}}>
            <div class="form-group">
              <label htmlFor="">Contact Information </label>
              <input
                type="text"
                class="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                name="contact_info"
                onChange={inputsHandler}
                value={inputField.contact_info}
              />
            </div>
            <div class="form-group">
              <label htmlFor="">Date of Birth</label>
              <input
                type="date"
                class="form-control"
                id="exampleInputPassword1"
                name="dob"
                onChange={inputsHandler}
                value={inputField.dob}
              />
            </div>
            <div class="form-group">
              <label htmlFor="">Current Residence (City, State)</label>
              <input
                type="text"
                class="form-control"
                id="exampleInputPassword1"
                name="address"
                onChange={inputsHandler}
                value={inputField.address}
              />
            </div>
            <div class="form-group">
              <label htmlFor="">Highest Qualification</label>
              <input
                type="text"
                class="form-control"
                id="exampleInputPassword1"
                name="qualification"
                onChange={inputsHandler}
                value={inputField.qualification}
              />
            </div>
            <div class="form-group">
              <label htmlFor="">Employement Status</label>
              <input
                type="text"
                class="form-control"
                id="exampleInputPassword1"
                name="status"
                onChange={inputsHandler}
                value={inputField.status}
              />
            </div>
          </form>
        </Grid>
        <Grid item xs={1} md={1} sm={1}></Grid>
        <div className="btn m-auto">
        <Link to={'/Signup'}><Button variant="contained" sx={newButton}>
            Go Back
          </Button></Link> 
          <Button
            onClick={submitButton}
            variant="contained"
            sx={newButton}
            style={{ marginLeft: "20px" }}
          >
            Continue
          </Button>
        </div>
      </Grid>
    </div>
  );
}
