// import React from 'react'
// import Card from '@mui/material/Card';
// import CardContent from '@mui/material/CardContent';
// import CardMedia from '@mui/material/CardMedia';
// import Typography from '@mui/material/Typography';
// import { Button, CardActionArea, CardActions } from '@mui/material';
// import { newButton } from '../../background';

// const DashboardCard = () => {
//     const bg={background: 'linear-gradient(90deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)',maxWidth: 650,position:'absolute', border:'2px solid black', borderRadius:'15px', margin:'5rem'}
//   return (
// <>

// <div style={{margin: '15rem'}}>
// <CardMedia
//           component="img"
//           height="100"
//           width="100"
//           image="https://i.pinimg.com/originals/a4/96/c2/a496c2b6bc5d7cfe0e0674f6598c38ad.jpg"
//           alt="green iguana"
//           style={{position:'relative', width:100, height:100, border:'2px solid black'}}
//         />
//     <Card sx={bg}>

//       <CardActionArea>


//         <CardContent>
//           <Typography gutterBottom variant="h5" component="div" style={{color:'white'}}>
//             Lizard
//           </Typography>
//           <Typography variant="body2" style={{color:'white'}}>
//             Lizards are a widespread group of squamate reptiles, with over 6,000
//             species, ranging across all continents except Antarctica
//           </Typography>
//         </CardContent>
//       </CardActionArea>
//       {/* <CardActions>
//         <Button size="small" color="primary">
//           Share
//         </Button>
//       </CardActions> */}
//     </Card>

//     </div>
// </>
//     )
// }

// export default DashboardCard

import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import { SERVER } from '../../../server/server';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Box from '@mui/material/Box';
import { BackGround, textHover } from '../../background';
import Typography from '@mui/material/Typography';

import "./DashboardCard.css"
const DashboardCard = ({ work }) => {

    // for modal
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const navigate = useNavigate();
    const getImgURL = (name) => `${SERVER}/uploads/${name}`
    const getImgURLPdf = (name) => `${SERVER}/uploads/${name}#toolbar=0`

    const modalStyle = {
        ...BackGround,
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 900,
        // bgcolor: '',
        border: '2px solid #000',
        boxShadow: 24,
        overflow: 'auto',
        height: '100vh',
        p: 4,
    };

    const cardCategory = { margin: 0, color: 'lightGray', marginTop: "1rem" }


    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '1rem' }}>


            <div class="card">
                {/* work image */}
                <img id='workImg' src={getImgURL(work.coverPicture)} alt=""
                // onClick={()=>navigate(`/MainDashboard/WorkViewTop5/${work._id}`)}
                />


                {/* profileImage */}
                {/* <img id='profImg' src={getImgURL(work.userId?.pdf)} alt=""  onClick={()=>navigate(`/MainDashboard/UserProfile/${work.userId.name}/${work.userId._id}`)}/> */}
                {/* <h2 className='cardDetail'>{work.mainCharacter}</h2> */}

                <h3 className='cardDetail'>Tittle: {work.title}</h3>
                <div class="cont">
                    <p id='para' style={{ textAlign: "left" }} >Description: {work.description.slice(0, 30).join(" ")}
                        {`....`} <span onClick={handleOpen} style={{ color: 'black', cursor: 'pointer' }}>see more</span>
                    </p>
                </div>
            </div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                style={{ height: '100vh', overflow: 'scroll' }}
            // BackdropComponent={Backdrop}
            // BackdropProps={{
            //   timeout: 500,
            // }}
            >
                <Fade in={open}>
                    <Box sx={modalStyle}>
                        <img style={{ width: '100%', height: '100%', objectFit: 'contain' }} src={getImgURL(work.coverPicture)} />

                        <Typography variant="h5" gutterBottom sx={{ ...textHover }}
                        //  onClick={() => navigate(STORE.getProfileData.success ?
                        //     `/MainDashboard/WorkView/${work.userWork}` :
                        //     `/WorkView/${work.userWork}`)}
                        > Tittle:
                            {work?.title}
                        </Typography>
                        <Typography variant="body1" gutterBottom sx={cardCategory} > Description:
                            {
                                work?.description.join(" ")

                            }
                        </Typography>
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: "1rem" }}>
                            <embed src={getImgURLPdf(work.userWork)} type="" height='600' width='800' />
                        </div>


                    </Box>
                </Fade>
            </Modal>
        </div>


    )
}

export default DashboardCard