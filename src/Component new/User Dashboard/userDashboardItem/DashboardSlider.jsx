import React from 'react'
import './DashboardSlider.css'
import DashboardCard from './DashboardCard'

import Slider from "react-slick";



import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getImgURL } from '../../../util/getImgUrl';
const DashboardSlider = () => {
    const STORE = useSelector((state) => state)
    const navigate = useNavigate();
    // const [popularMovies, setPopularMovies] = useState([])
    // const [data,setData]=useState([])
    // console.log(Object.keys(STORE?.userDashboardDetail?.userWorkDetail?.top5_work?STORE?.userDashboardDetail?.userWorkDetail?.top5_work:""))
    const keys = Object.keys(STORE?.userDashboardDetail?.userWorkDetail?.top5_work ? STORE?.userDashboardDetail?.userWorkDetail?.top5_work : "")
    // console.log(STORE?.userDashboardDetail?.userWorkDetail?.top5_work?STORE?.userDashboardDetail?.userWorkDetail?.top5_work[keys[0]]:[])
    const data = STORE?.userDashboardDetail?.userWorkDetail?.top5_work ? STORE?.userDashboardDetail?.userWorkDetail?.top5_work[keys[0]] : []

    // useEffect(() => {
    //     axios.get(`${SERVER}/admin/getUserByCategory/${STORE?.userProfile?.userProfile?.meProfile?.category}`).then((res) => { 
    //         console.log(res)

    //     }).catch((err) =>
    //       console.log(err, "error")
    // )

    // },[]);

    // useEffect(() => {
    //     fetch("https://api.themoviedb.org/3/movie/popular?api_key=4e44d9029b1270a757cddc766a1bcb63&language=en-US")
    //     .then(res => res.json())
    //     .then(data => setPopularMovies(data.results))
    // }, [])
    // const settings = {
    //     dots: true,
    //     infinite: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     autoplay: true,
    //     speed: 500,
    //     autoplaySpeed: 2000,
    //     pauseOnHover: false,
    //     cssEase: "linear",
    // };

    return (
        <div className="poster">
            <Carousel
                showThumbs={false}
                autoPlay
                transitionTime={1000}
                infiniteLoop
                showStatus={false}
                interval={3000}
            >
                {
                    data.map((item, index) => (
                        <>
                            <div style={{ color: 'white', cursor: 'pointer' }}
                                onClick={() =>
                                    navigate(`WorkView/${item?.userId?._id}/${item?._id}`)
                                }
                            >
                                <div className="posterImage">
                                    <img
                                        //   src='https://static.vecteezy.com/system/resources/previews/001/849/553/original/modern-gold-background-free-vector.jpg'

                                        src={
                                            item?.userId ?

                                                getImgURL(item?.userId?.backGroundImage)
                                                :
                                                `https://www.deveninfotech.com/wp-content/themes/consultix/images/no-image-found-360x250.png`
                                        }
                                    />
                                </div>
                                <div className="posterImage__overlay">
                                    <div className="posterImage__title">
                                        {item?.title}

                                        <span className='DashboardCard'>
                                            {<DashboardCard src={getImgURL(item?.coverPicture)} />}
                                        </span>

                                    </div>
                                    <div className="posterImage__runtime">
                                        by &nbsp;
                                        {item?.userId?.name?.toUpperCase()}
                                        <span className="posterImage__rating">
                                            {/* {movie ? movie.vote_average :""} */}
                                            {/* <i className="fas fa-star" />{" "} */}
                                        </span>
                                    </div>
                                    <div className="posterImage__description">
                                        {/* {movie ? movie.overview : ""} */}
                                    </div>
                                </div>
                            </div>
                        </>
                    ))
                }
            </Carousel>
        </div >
    )
    // console.log("data", data);
    // const settings = {
    //     dots: true,
    //     infinite: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     autoplay: true,
    //     speed: 500,
    //     autoplaySpeed: 2000,
    //     cssEase: "linear",
    // };



    // return (
    //     <div className="poster">
    //         <Slider {...settings}>

    //             {
    //                 data.map((item, index) => (
    //                     <>
    //                         <div style={{ color: 'white', cursor: 'pointer' }}
    //                             onClick={() =>
    //                                 navigate(`WorkView/${item?.userId?._id}/${item?._id}`)
    //                             }
    //                         >
    //                             <div className="posterImage">
    //                                 <img
    //                                     //   src='https://static.vecteezy.com/system/resources/previews/001/849/553/original/modern-gold-background-free-vector.jpg'

    //                                     src={
    //                                         item?.userId ?

    //                                             getImgURL(item?.userId?.backGroundImage)
    //                                             :
    //                                             `https://www.deveninfotech.com/wp-content/themes/consultix/images/no-image-found-360x250.png`
    //                                     }
    //                                 />
    //                             </div>
    //                             <div className="posterImage__overlay">
    //                                 <div className="posterImage__title neonText">


    //                                     <span className='DashboardCard'>
    //                                         {item?.title}
    //                                         {<DashboardCard src={getImgURL(item?.coverPicture)} />}
    //                                     </span>
    //                                 </div>
    //                                 <div className="posterImage__runtime neonText">
    //                                     <p>{item?.description.slice(0, 10).join(' ')}...</p>
    //                                     by &nbsp;
    //                                     {item?.userId?.name?.toUpperCase()}
    //                                     <span className="posterImage__rating">
    //                                     </span>
    //                                 </div>
    //                                 <div className="posterImage__description">
    //                                 </div>
    //                             </div>
    //                         </div>
    //                     </>
    //                 ))
    //             }
    //         </Slider>
    //     </div >)
}

export default DashboardSlider