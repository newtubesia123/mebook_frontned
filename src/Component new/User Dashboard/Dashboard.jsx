import React, { useState } from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import pic from "../../Screenshot_2.png";
import { Link, useLocation, useNavigate } from "react-router-dom";
// import Box from '@mui/material/Box';
import Button from "@mui/material/Button";
import { QRCodeCanvas } from "qrcode.react";
// import Typography from '@mui/material/Typography';
import Modal from "@mui/material/Modal";
import UserProfile from "./UserProfile";
import MyPitch from "./MyPitch";
import { Toolbar, Tooltip } from "@mui/material";

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    height: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    borderRadius: "20px",
    boxShadow: 24,
    p: 4,
};

const obj = {
    1: <UserProfile />,
    2: <MyPitch />,
};

export default function Dashboard({ name }) {
    const [open, setOpen] = React.useState(false);
    const [content, setContent] = useState("");
    const [url, setUrl] = useState(
        `http://54.246.61.54:3000/UserDashboard/${name}`
    );
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    let historyUserPitch = localStorage.getItem("userPitch");
    const userPitch = (e) => {
        if (content.length <= 250) setContent(e.target.value);
        localStorage.setItem("userPitch", e.target.value);
    };

    const downloadQRCode = (e) => {
        e.preventDefault();
        setUrl("");
    };

    const qrCodeEncoder = (e) => {
        setUrl(e.target.value);
    };
    const qrcode = (
        <QRCodeCanvas
            id="qrCode"
            value={url}
            size={200}
            bgColor={"#ffff"}
            level={"H"}
        />
    );
    const navigate = useNavigate();
    return (
        <>
            <Toolbar />
            <div style={{ float: "right" }}>{qrcode}</div>
            <div className="container ">
                <Typography
                    sx={{ textAlign: "center", fontFamily: "Times New Roman" }}
                    variant="h3"
                    gutterBottom
                >
                    Welcome to Your <span style={{ color: "rgb(187 11 11)" }}>M</span>
                    <span style={{ color: "#100892" }}>eBook</span><span style={{ color: "rgb(187 11 11)" }}>M</span><span style={{ color: "#100892" }}>eta</span> Dashboard
                </Typography>
                <div className="row mx-auto mt-5">
                    <div className="col-md-6 col-sm-12 mt-2 ">
                        <Box sx={{ width: "100%" }}>
                            <Card sx={{ minWidth: 150, height: 220 }} elevation={10}>
                                <CardContent>
                                    <Typography
                                        sx={{ textAlign: "center", fontFamily: "Times New Roman" }}
                                        variant="h5"
                                        gutterBottom
                                    >
                                        Website Suggestions <br /> Tutorials
                                    </Typography>
                                    <Typography
                                        sx={{
                                            textAlign: "center",
                                            mt: 5,
                                            fontFamily: "Times New Roman",
                                        }}
                                        gutterBottom
                                    >
                                        Trending MeBookMeta Websites
                                    </Typography>
                                </CardContent>
                                <CardActions></CardActions>
                            </Card>
                        </Box>
                    </div>
                    <div className="col-md-6 col-sm-12 mt-2">
                        <Box sx={{ width: "100%" }}>
                            <Card sx={{ minWidth: 150, height: 230 }} elevation={10}>
                                <CardContent >
                                    <Typography
                                        sx={{ fontSize: 14 }}
                                        color="text.secondary"
                                        gutterBottom
                                    ></Typography>
                                    <Typography
                                        variant="h5"
                                        sx={{
                                            fontWeight: "bold",
                                            mt: 2,
                                            fontFamily: "Times New Roman",
                                            textAlign: "center",
                                        }}
                                        component="div"
                                    >
                                        Your Pitch
                                    </Typography>
                                    <Typography component='div'
                                        sx={{
                                            textAlign: "center",
                                            mt: 2,
                                            fontFamily: "Times New Roman",

                                        }}
                                    >
                                        {historyUserPitch
                                            ? historyUserPitch
                                            : !content
                                                ? "Write your Pitch in 250 words"
                                                : content}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    {content || historyUserPitch ? (
                                        <Button
                                            variant="contained"
                                            className="mx-auto"
                                            onClick={handleOpen}
                                            sx={{ marginBottom: "20px" }}
                                        >
                                            Edit
                                        </Button>
                                    ) : (
                                        <Button
                                            variant="contained"
                                            className="mx-auto"
                                            onClick={handleOpen}
                                        >
                                            Write
                                        </Button>
                                    )}
                                </CardActions>
                            </Card>
                        </Box>
                    </div>
                    <div className="col-md-6 col-sm-12 mt-2">
                        <Box sx={{ width: "100%" }}>
                            <Card sx={{ minWidth: 150, height: 220 }} elevation={10}>
                                <CardContent>
                                    <Typography
                                        variant="h5"
                                        component="div"
                                        sx={{
                                            textAlign: "center",
                                            mt: 2,
                                            fontFamily: "Times New Roman",
                                        }}
                                    >
                                        How to Make the Most of <br /> Your{" "}
                                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                                        <span style={{ color: "#100892" }}>eBook</span>{" "}
                                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                                        <span style={{ color: "#100892" }}>eta</span> Website
                                        Feature
                                    </Typography>
                                </CardContent>
                                <CardActions></CardActions>
                            </Card>
                        </Box>
                    </div>
                    <div className="col-md-6 mt-2 col-sm-12">
                        <Box sx={{ width: "100%" }}>
                            <Card sx={{ minWidth: 150, height: 220 }} elevation={10}>
                                <CardContent>
                                    <Typography
                                        variant="h5"
                                        component="div"
                                        sx={{
                                            textAlign: "center",
                                            mt: 3,
                                            fontFamily: "Times New Roman",
                                        }}
                                    >
                                        Setting Up Your <br />{" "}
                                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                                        <span style={{ color: "#100892" }}>eBook</span>{" "}
                                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                                        <span style={{ color: "#100892" }}>eta</span> Blog
                                    </Typography>
                                </CardContent>
                                <CardActions></CardActions>
                            </Card>
                        </Box>
                    </div>
                    <div className="col-md-6 mt-2 col-sm-12">
                        <Box sx={{ width: "100%" }}>
                            <Card sx={{ minWidth: 150, height: 300 }} elevation={10}>
                                <CardContent>
                                    <Typography
                                        sx={{ textAlign: "center", fontFamily: "Times New Roman" }}
                                        variant="h6"
                                    >
                                        Activity
                                    </Typography>
                                    <Typography
                                        component="div"
                                        sx={{
                                            textAlign: "center",
                                            mt: 3,
                                            fontFamily: "Times New Roman",
                                        }}
                                    >
                                        Recently published
                                    </Typography>
                                </CardContent>
                                <CardActions></CardActions>
                            </Card>
                        </Box>
                    </div>
                    <div className="col-md-6 mt-2 col-sm-12">
                        <Box sx={{ width: "100%" }}>
                            <Card sx={{ minWidth: 150, height: 300 }} elevation={10}>
                                <CardContent>
                                    <Typography variant="h5" component="div">
                                        Quick Draft
                                    </Typography>
                                    <Typography sx={{}} component="div">
                                        Title
                                    </Typography>
                                    <input type="text" name="" id="" />
                                    <Typography>Content</Typography>
                                    <textarea name="" id="" cols="22.5" rows="4"></textarea>
                                    <div>
                                        <button
                                            type="button"
                                            class="btn btn-primary"
                                            style={{ background: "#100892" }}
                                        >
                                            Save drafts
                                        </button>
                                    </div>
                                </CardContent>
                            </Card>
                        </Box>
                    </div>
                    <div className="col-md-6 mt-2 col-sm-12"></div>
                </div>
                <Modal
                    open={open}
                    onClose={handleClose}
                // aria-labelledby="modal-modal-title"
                // aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Edit Your Pitch (250 Words)
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            <textarea
                                name=""
                                id=""
                                cols="75"
                                rows="5"
                                value={content}
                                onChange={userPitch}
                                autoFocus={true}
                            ></textarea>
                            {/* {content.trim(" ").length} */}
                        </Typography>
                        <Button
                            sx={{ float: "right" }}
                            variant="contained"
                            onClick={handleClose}
                        >
                            Save
                        </Button>
                    </Box>
                </Modal>
            </div>
        </>
    );
}

// "qrcode-generator": "^1.4.4",
// "qrcode.react": "^3.1.0",
