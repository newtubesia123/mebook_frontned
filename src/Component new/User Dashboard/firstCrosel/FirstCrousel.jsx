import React from "react";
import { useEffect } from "react";
import Carousel from "react-multi-carousel";
import axios from "axios";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { useState } from "react";
import { Paper } from "@mui/material";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import { Box, Container } from "@mui/system";
import { SERVER } from "../../../server/server";
import Avatar from '@mui/material/Avatar';
import { deepOrange, deepPurple } from '@mui/material/colors';
import { BackGround } from '../../background'
import { useNavigate } from "react-router-dom";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function FirstCrousel({ name, data }) {
  const [allData, setAllData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [message, setMessage] = useState(false);
  const [getUserById, setGetUserById] = useState([]);
  const [incr, setIncr] = useState(0);

  const getUserDetails = () => {
    axios
      .get(`${SERVER}/getUser`)
      .then((e) => {
        // console.log(' user data', e.data)
        setAllData(e.data.data);

      })
      .catch((err) => console.log("err", err));
    // console.log("-->", allData.data.writerDetail);`
  };
  const handleClose = () => {
    setModalOpen(false);
  };
  const modelOpen = (i, j) => {
    setGetUserById(
      allData && allData.length ? allData.filter((e) => e._id == i)[0] : false
    );
    setModalOpen(true);
  };
  useEffect(() => {
    getUserDetails();
  }, []);
  const navigate = useNavigate();
  return (
    <Container sx={{ padding: "20px", backgroundColor: '' }}>
      <Box sx={{ paddingLeft: "30px" }}>
        <Typography color='white' variant="h4">{name}</Typography>
      </Box>






      {data && data.length ? (
        <Carousel responsive={responsive}>
          <Container style={{ padding: "20px", display: "flex" }}>

            {data.slice(0, 5).map((i) => (
              <Paper
                elevation={10}
                key={i._id}
                sx={{ height: 350, width: "19%", mx: 1 }}
                onClick={() => navigate(`/MainDashboard/UserProfile/${i.userId.name}/${i.userId._id}`

                )}
              // onClick={() => modelOpen(i._id, i.coverPicture)}
              >

                {i.coverPicture != null ? (
                  <>
                    <img
                      src={`${SERVER}/uploads/${i.coverPicture}`}
                      style={{ height: 350, width: "100%", cursor: 'pointer' }}
                      alt="image"
                      // onClick={()=>navigate(`/MainDashboard/WorkView/${i.userWork}`
                      // )}
                      onClick={() => navigate(`/MainDashboard/UserProfile/${i.userId.name}/${i.userId._id}`

                      )}

                    />
                    {/* <Typography variant="h4" sx={{ textAlign: 'center' }} color='black'>
                      {i.title}
                    </Typography> */}
                  </>
                ) :
                  (
                    <Typography variant="h4" sx={{ ...BackGround, height: 320, width: "100%", color: 'black' }}>
                      {i.title}
                    </Typography>)
                }

              </Paper>
            ))}

          </Container>
          <Container style={{ padding: "20px", display: "flex" }}>

            {data.slice(5, 10).map((i) => (
              <Paper
                elevation={10}
                key={i._id}
                sx={{ height: 350, width: "19%", mx: 1 }}
              // onClick={() => modelOpen(i._id, i.coverPicture)}
              >

                {i.coverPicture != null ? (
                  <img
                    src={`${SERVER}/uploads/${i.coverPicture}`}
                    style={{ height: 350, width: "100%" }}
                    alt="image"

                  />
                ) :
                  (
                    <Typography variant="h3" sx={{ ...BackGround, height: 320, width: "100%", color: 'white' }}>
                      {i.title}
                    </Typography>)
                }

              </Paper>
            ))}
          </Container>
          <Container style={{ padding: "20px", display: "flex" }}>

            {data.slice(11, 16).map((i) => (
              <Paper
                elevation={10}
                key={i._id}
                sx={{ height: 350, width: "19%", mx: 1 }}
              // onClick={() => modelOpen(i._id, i.coverPicture)}
              >
                {i.coverPicture != null ? (
                  <img
                    src={`${SERVER}/uploads/${i.coverPicture}`}
                    style={{ height: 350, width: "100%" }}
                    alt="image"

                  />
                ) :
                  (
                    <Typography variant="h3" sx={{ ...BackGround, height: 320, width: "100%", color: 'white' }}>
                      {i.title}
                    </Typography>)
                }

              </Paper>
            ))}
          </Container>
        </Carousel>
      ) : (
        <h1>No Data</h1>
      )}
      {/* <Modal
        open={modalOpen}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Card sx={style}>

          <div className="row">
            <div className="col-md-6">
              <img
                src={`http://localhost:3002/uploads/${getUserById.pdf}`}
                style={{ height: 250, width: "100%" }}
                alt=""
              />
            </div>
            <div className="col-md-6">
              <h3>
                <b>{getUserById.title}</b>
              </h3>
              <p> {getUserById.titleStory}</p>
            </div>
          </div>

          <div className="row justify-content-between mt-3">
            {message === false ? (
              <button
                type="button"
                class="btn btn-primary"
                style={{ background: "#100892" }}
              // onClick={() => setMessage(true)}
              >
                Message
              </button>
            ) : (
              <button
                type="button"
                class="btn btn-primary"
                style={{ background: "#100892" }}
              // onClick={() => setMessage(!message)}
              >
                Send
              </button>
            )}

            <button
              type="button"
              class="btn btn-primary"
              style={{ background: "#100892" }}
            // onClick={handleView}
            >
              Continue Reading
            </button>
          </div>
          {message === true ? (
            <div className="mt-3">
              <textarea name="" id="" cols="75" rows="5"></textarea>
            </div>
          ) : null}
        </Card>
      </Modal> */}
    </Container>
  );
}

export default FirstCrousel;
