import React from "react";
import Slider from "react-slick";
import '../../../component/crouselNew/CrouselNew.scss';
import { SERVER } from "../../../server/server.js";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import img from '../../.././profilepic.png'
import { getImgURL } from "../../../util/getImgUrl";
import DefaultImg from "../../../png/Avatar-Profile-PNG-Photos.png"
const ProfileCrousel = ({ data, loginUserId }) => {
    const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhKM3lzmdHj5x-JEaGMEcFpmGPKxwkeRQazw&usqp=CAU' alt="prevArrow" {...props} />
    );

    const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfwgab9nho5NaOWiW-o28osIFp4Y8LDIPmWw&usqp=CAU' alt="nextArrow" {...props} />
    );
    const STORE = useSelector((state) => state);
    const navigate = useNavigate();
    var settings = {
        dots: true,
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 3,
        // autoplay: true,
        speed: 300,
        autoplaySpeed: 3000,
        cssEase: 'linear',
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    infinite: false,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    const getDate = (str) => {
        // var str = "Fri Feb 08 2013 09:47:57 GMT +0530 (IST)";
        var date = new Date(str);
        var day = date.getDate(); //Date of the month: 2 in our example
        var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
        var year = date.getFullYear() //Year: 2013
        return `${day}/${month + 1}/${year}`;

    }
    // console.log("same cat user profile", data)
    // const isSameUser = data.matchedUserCat?.userId?._id;

    return (
        <div style={{ marginTop: '1rem', overflow: 'hidden' }}>
            <div className="card__container">
                {data.matchedUserCat && data.matchedUserCat.length ? <h1 style={{ color: 'white', fontSize: "150%", marginTop: '10vh' }}>Suggested users For <span style={{ color: 'red' }}>{data.user_Category.toUpperCase()}</span> </h1> :
                    ""
                }
                <Slider {...settings} className="card__container--inner">

                    {/* {
                    data.matchedUserCat && data.matchedUserCat.length ? data.matchedUserCat.filter((i) => loginUserId !== isSameUser)
                } */}

                    {data.matchedUserCat && data.matchedUserCat.length ? data.matchedUserCat.filter((i) => i?.userId?._id !== loginUserId).map((item, index) => {
                        return (
                            // <div
                            //     className="card__container--inner--card"
                            //     key={index} style={{ borderRadius: '50%!', }}
                            // >

                            //     {/* {item?.userId?.pdf != null ? ( */}
                            //         <img style={{ borderRadius: '50%', }} src={item?.userId?.pdf != null ? getImgURL(item.userId?.pdf) : "https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png"}
                            //             onClick={() =>
                            //                 navigate(`/MainDashboard/UserProfile/${item.userId?.name}/${item.userId?._id}`)
                            //             }
                            //         />
                            //     {/* ) : <img style={{ borderRadius: '50%' }} src={img }} */}
                            //     {/* <p style={{ paddingLeft: '10px', fontSize: '.6rem', color: 'lightgray', marginTop: '14px', textAlign: 'center', paddingRight: '1.5rem' }} */}
                            //     {/* <div style={{ display: "flex" }}> */}
                            //     <div style={{ fontSize: '.6rem', color: 'lightgray', marginTop: '14px' }}>
                            //         {item.userId?.name.slice(0, 15).toUpperCase()}
                            //         {item.userId?.name.length > 15 ?
                            //             `...`
                            //             : ""
                            //         }
                            //         {/* </div> */}
                            //     </div>
                            // </div>

                            <div className="card__container--inner--card"

                                key={index} style={{ borderRadius: '50%!', }}
                            >

                                {/* {item?.userId?.pdf != null ? ( */}
                                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    <img style={{ borderRadius: '50%', }} src={item?.userId?.pdf != null ? getImgURL(item.userId?.pdf) : DefaultImg}
                                        onClick={() =>
                                            navigate(`/MainDashboard/UserProfile/${item.userId?.name}/${item.userId?._id}`)
                                        }
                                    />
                                </div>

                                <div style={{ fontSize: '.6rem', color: 'lightgray', marginTop: '14px' }}>
                                    {item.userId?.name.slice(0, 15).toUpperCase()}
                                    {item.userId?.name.length > 15 ?
                                        `...`
                                        : ""
                                    }
                                </div>
                            </div>
                        );
                    })
                        : <h1>No Data</h1>
                    }


                </Slider>
            </div>


        </div>
    )
}

export default ProfileCrousel