import React, { useState, } from "react";
import Slider from "react-slick";
import '../../../component/crouselNew/CrouselNew.scss';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

// modal
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import CardMedia from '@mui/material/CardMedia';
import CloseIcon from '@mui/icons-material/Close';

import Slide from '@mui/material/Slide';
import { newButton } from "../../background";
import { getImgURL } from "../../../util/getImgUrl";
import { callPopUp } from "../../../Action";
import MainModal from "../../../components/MainModal/MainModal";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


const DashCrousel = ({ data, name }) => {

    const [open, setOpen] = useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));


    const [work, setWork] = useState({
        id: "",
        coverPicture: "",
        title: "",
        description: "",
        loading: false
    })
    const dispatch = useDispatch()
    const handleClickOpen = () => {
        setOpen(true);

    };
    const handleClose = () => {
        setOpen(false);
    };


    const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhKM3lzmdHj5x-JEaGMEcFpmGPKxwkeRQazw&usqp=CAU' alt="prevArrow" {...props} />
    );

    const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
        <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfwgab9nho5NaOWiW-o28osIFp4Y8LDIPmWw&usqp=CAU' alt="nextArrow" {...props} />
    );
    const STORE = useSelector((state) => state);
    const navigate = useNavigate();
    var settings = {
        dots: true,
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 3,
        // autoplay: true,
        speed: 300,
        autoplaySpeed: 3000,
        cssEase: 'linear',
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    infinite: false,
                    dots: true
                }
            },

            {
                breakpoint: 830,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 630,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 440,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    const getDate = (str) => {
        // var str = "Fri Feb 08 2013 09:47:57 GMT +0530 (IST)";
        var date = new Date(str);
        var day = date.getDate(); //Date of the month: 2 in our example
        var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
        var year = date.getFullYear() //Year: 2013
        return `${day}/${month + 1}/${year}`;

    }

    const getWork = (item) => {
        setWork({ ...work, loading: true })
        let res = (data.filter((i) => i._id == item))[0]
        setWork({ ...work, _id: res._id, coverPicture: res.coverPicture, title: res.title, description: res.description, loading: false, userId: res.userId._id })
        setOpen(true)
        dispatch(callPopUp({ type: "worlModal" }))
    }
    // console.log("work", work)
    // Work title slice
    const workTitle = work.title;
    const titleWord = workTitle.split(" ").slice(0, 4)
    const newTitle = titleWord.join(" ")
    // console.log("dashboard profile data",data)



    return (
        <div style={{ marginTop: '1rem', overflow: 'hidden' }}>
            <div className="card__container " >
                <h1 style={{ color: '#fff', fontSize: "150%" }}>{name}</h1>
                <Slider {...settings} className="card__container--inner" >
                    {data && data.length ? data.map((item, index) => {
                        return (
                            <div
                                className=" card__container--inner--card text-center "
                                key={item._id}
                            >
                                <span className="d-flex justify-content-center mx-auto">
                                    <img src={getImgURL(item.coverPicture)}
                                        onClick={() => {
                                            getWork(item._id)
                                        }} />
                                </span>
  
                                <p style={{ fontSize: '.6rem', color: 'lightgray', marginTop: '14px', textAlign: 'center' }}>
                                    {item.title.slice(0, 15).toUpperCase()}
                                    {item.title.length > 15 ?
                                        `...`
                                        : ""
                                    }
                                </p>
                                <div className="card__container--inner--card--date_time">
                                    <p className="mx-auto"><AccessTimeIcon style={{ fontSize: '1rem', objectFit: 'contain', }} />
                                        {getDate(item.createdAt)}
                                    </p>
                                    <p className="mx-auto"><ThumbUpAltIcon style={{ width: '1rem', height: '1rem', objectFit: 'contain' }} />
                                        {item.liked}
                                    </p>
                                </div>
                            </div>
                        );
                    })
                        : <h1>No Data</h1>
                    }
                </Slider>
            </div>
            {/* Work Modal */}
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogContent sx={{ display: 'flex', flexDirection: { xs: "column", sm: "column", md: "row", lg: "row" }, gap: "1rem", position: 'relative', backgroundColor: "black", border: '1px dotted white', height: { md: "350px", sm: "100%" }, width: { md: "600px", sm: "100%" } }}>
                    <CloseIcon onClick={handleClose} sx={{ cursor: "pointer", color: 'white', position: "absolute", right: "1%", }} />
                    <CardMedia
                        component="img"
                        sx={{ width: "250px", height: "300px", borderRadius: "3px", display: { xs: "flex" }, justifyContent: { xs: "center" }, alignItems: { xs: "center" }, margin: { xs: "auto" } }}
                        image={getImgURL(work.coverPicture)}
                    />
                    <DialogContentText sx={{ color: 'white', marginTop: '2rem' }}>
                        <Typography gutterBottom variant="h6" component="div" sx={{ width: "100%", }}>
                            <span><b>Title&nbsp;:  </b></span>  {newTitle}...
                        </Typography>
                        <div style={{ height: "60%" }}>
                            <p>
                                <b>Description &nbsp;:</b> {work?.description !== "" ? work.description.slice(0, 30).join(" ") : ""}...
                            </p>
                        </div>
                        <div style={{ height: "12%", position: { lg: "absolute" }, right: "0", marginTop: "12px", bottom: "5%" }}>
                            <Button sx={{ ...newButton }}
                                onClick={() => { navigate(`WorkView/${work?.userId !== "" ? work?.userId : ""}/${work?._id !== "" ? work?._id : ""}`) }}
                            >See More</Button>
                        </div>
                    </DialogContentText>
                </DialogContent>
            </Dialog>
            {/* Work Modal */}
            {/* <MainModal open={open} onClose={handleClose} 
                Component={
                    <DialogContent sx={{ display: 'flex', flexDirection: { xs: "column", sm: "column", md: "row", lg: "row" }, gap: "1rem", position: 'relative', backgroundColor: "black", border: '1px dotted white', height: { xs:"100%", sm: "100%" , md: "350px", xl:"350px", lg:"350px"}, width: {xs:"100%", sm:"100%",  md: "600px", lg:"600px", xl:"600px" } }}>
                        <CloseIcon onClick={handleClose} sx={{ cursor: "pointer", color: 'white', position: "absolute", right: "1%", }} />
                        <CardMedia
                            component="img"
                            sx={{ width: "250px", height: "300px", borderRadius: "3px", display: { xs: "flex" }, justifyContent: { xs: "center" }, alignItems: { xs: "center" }, margin: { xs: "auto" } }}
                            image={getImgURL(work.coverPicture)}
                        />
                        <DialogContentText sx={{ color: 'white', marginTop: '2rem' }}>
                            <Typography gutterBottom variant="h6" component="div" sx={{ width: "100%", }}>
                                <span><b>Title&nbsp;:  </b></span>  {work.title}
                            </Typography>
                            <div style={{ height: "60%" }}>
                                <p>
                                    <b>Description &nbsp;:</b> {work?.description !== "" ? work.description.slice(0, 30).join(" ") : ""}...
                                </p>
                            </div>
                            <div style={{ height: "12%", position: { lg: "absolute" }, right: "0", marginTop: "12px", bottom: "5%" }}>
                                <Button sx={{ ...newButton }}
                                    onClick={() => { navigate(`WorkView/${work?.userId !== "" ? work?.userId : ""}/${work?._id !== "" ? work?._id : ""}`) }}
                                >See More</Button>
                            </div>
                        </DialogContentText>
                    </DialogContent>
                }
            /> */}
        </div>
    )
}

export default DashCrousel