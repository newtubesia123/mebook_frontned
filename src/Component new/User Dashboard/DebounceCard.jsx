import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { SERVER } from '../../server/server'
import { getAuth } from '../../Action'
import './cardDashboard.css'
import { Box } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import Skeleton from '@mui/material/Skeleton';
import { getImgURL } from '../../util/getImgUrl'
// import Button from "@mui/material/Button";
function DebounceCard() {
    const [data, setData] = useState([])
    const [stage, setStage] = useState(0)
    const [loading, setLoading] = useState(false);
    const [next, setNext] = useState(true)
    // const controller = new AbortController();
    const navigate = useNavigate()
    useEffect(() => {
        if (next) {
            axios.get(`${SERVER}/dashboard/mainDashboard/debounce?debaunceStage=${stage}`,
                {
                    headers: { Authorization: `Bearer ${getAuth()}` }
                }
            ).then((res) => {
                switch (res.data.errorCode) {

                    case 300:
                        setNext(false)
                        break;
                    case 200:
                        // setData((prev) => [...prev, ...res.data.debaunceData]);
                        // let temp = [...data, ...res.data.debaunceData]
                        // let newArr = temp.filter((value, index, self) =>
                        // index === self.findIndex((t) => (
                        //     t._id === value._id
                        // )))
                        // if(newArr.length !== data.length) {
                        //     setLoading(true)
                        //     setTimeout(()=>{
                        //         setData(newArr)
                        //         setLoading(false)
                        //     },600)
                        // }
                        // setData((prev) => [...prev, ...res.data.debaunceData]);
                        let temp = [...data, ...res.data.debaunceData]
                        if (temp.length !== data.length) {
                            setLoading(res.data.debaunceData)
                            setTimeout(() => {
                                setData((prev) => [...prev, ...res.data.debaunceData])
                                setLoading(false)
                            }, 600)
                        }

                        break;

                    case 404:
                        localStorage.removeItem("token");
                        window.location.reload();
                        break;
                    default:
                        localStorage.removeItem("token");
                        window.location.reload();
                        break;
                }
            })
        }
    }, [stage, next])
    const handelInfiniteScroll = async () => {
        try {
            if (
                window.innerHeight + document.documentElement.scrollTop + 1 >=
                document.documentElement.scrollHeight

                // window.innerHeight + window.pageYOffset+1 >= document.body.offsetHeight
            ) {
                setStage((prev) => prev + 1);
            }
        } catch (error) {
            console.log(error);
        }
    };
    // useEffect(()=>{
    //     console.log("data",data)
    // },[data])
    useEffect(() => {
        window.scrollTo(0, 0)
        window.addEventListener("scroll", handelInfiniteScroll);
        return () => window.removeEventListener("scroll", handelInfiniteScroll);
    }, []);
    // const getImgURL = (name) => `${SERVER}/uploads/${name}`
    return (
        <div className="container">
            {data && data.length ? 
            <h1 style={{ color: 'white', margin: '1rem 0 2rem 0', fontSize:"150%" }}>Works You May Like In Other Categories</h1>
            :
            <h3 style={{cursor:'pointer',color:'white',textAlign:'center'}} onClick={()=>navigate('WriterStoryDetails')}>Please start uploading your work</h3>
            }
            <div className='row'>
                {data.map((val, idx) => {
                    return (
                        <article className="col-6 col-sm-6 col-md-4 col-lg-3 mt-2 mb-2 d-flex justify-content-center" style={{ paddingLeft: "0px" }} key={val._id + idx}>
                            <div className='universe_card' onClick={() => navigate(`/MainDashboard/WorkView/${val.userId}/${val._id}`)}>
                                <img className='card_img' src={getImgURL(val?.coverPicture)} loading="lazy" alt="" />
                                <div className="card_content">
                                    <h6 className="card_title">{val.title}</h6>
                                    {/* <span class="card_subtitle">Thsi is a subtitle of this page. Let <br /> us see how it looks on the Web.</span> */}
                                    <p className='card_category'>
                                        <span style={{ background: '#1C6DD0', padding: '2px', borderRadius: '5px' }}>
                                            <span>{val?.category}</span>
                                        </span>
                                    </p>

                                    <p className="card_description">{val?.description.slice(0, 40).join(' ')}...</p>
                                </div>
                            </div>
                        </article>
                    );
                })}
                {
                    loading && loading.length ?
                        loading.map((e, index) => (
                            <div className="col-lg-3 col-md-3 col-6 mt-2 mb-2 " key={"debounce" + index}>
                                <Box
                                    sx={{
                                        borderRadius: "20px"
                                    }}
                                >
                                    <Skeleton
                                        sx={{ bgcolor: 'grey.900', borderRadius: '20px' }}
                                        variant="rectangular"
                                        width={250}
                                        height={320}
                                    />
                                </Box>
                            </div>
                        )) :
                        ""

                }
                {/* {
                        loading && <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          width: "100%",
                          height: "3em",
                        }}
                      >
                        <CircularProgress sx={circularProgressbarStyle} />
                      </Box>
                    } */}
            </div>
        </div>
    )
}

export default DebounceCard