import React, { useState, useEffect } from "react";
import axios from "axios";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import Pic from '../../Screenshot_2.png'
import { Cascader } from 'antd';
import { useNavigate } from "react-router-dom";

import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
const options = [

  {
    value: 'Author',
    label: 'Author',
    children: [
      {
        value: 'Autobiography',
        label: 'Autobiography',

      },
      {
        value: 'Biography',
        label: 'Biography',

      },
      {
        value: "Children's",
        label: "Children's",

      },
      {
        value: 'Comics/Graphic Novels',
        label: 'Comics/Graphic Novels',

      },
      {
        value: 'Crime/Detective',
        label: 'Crime/Detective',

      },
      {
        value: 'Erotica',
        label: 'Erotica',

      },

    ],
  },
  {
    value: 'Writer',
    label: 'Writer',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    value: 'Film/Media',
    label: 'Film/Media',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    value: 'Music/Audio/Sound',
    label: 'Music/Audio/Sound',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    value: 'Visual/Graphics',
    label: 'Visual/Graphics',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    value: 'Performer',
    label: 'Performer',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },

];
const identity = [
  {
    value: 'Main Stage',
    label: 'Main Stage',
  },
  {
    value: 'Cutting Edge',
    label: 'Cutting Edge',
  },
  {
    value: 'Specialist',
    label: 'Specialist',
  },
  {
    value: 'MultiDiscipline',
    label: 'MultiDiscipline',
  },
  {
    value: 'Organic',
    label: 'Organic',
  },
  {
    value: 'AtLarge',
    label: 'AtLarge',
  },
]
const work = [
  {
    value: 'Creator',
    label: 'Creator',
  },
  {
    value: 'Collaborator',
    label: 'Collaborator',
  },
  {
    value: 'Influencer',
    label: 'Influencer',
  },
  {
    value: 'Project Developer',
    label: 'Project Developer',
  },
  {
    value: 'Provider',
    label: 'Provider',
  },
]
const pages = ['Products', 'Pricing', 'Blog'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
const displayRender = (labels) => labels[labels.length - 1];

function UserAppBar() {
  const navigate = useNavigate();

  const [userData, setUserData] = React.useState([])
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        setUserData(res.data.userData)

      })
      .catch((err) => console.log(err));
  }, []);
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const handleCreate = () => {
    navigate('/WriterStoryDetails')
  }
  return (
    <AppBar position="fixed" sx={{ backgroundColor: 'white', borderBottom: " 1px solid lightgray", zIndex: 1 }}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <IconButton
            size="large"
            edge="start"
            // color="white"
            aria-label="menu"
            sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
          >
            <img src={Pic} width="200px" alt="" />
          </IconButton>

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="black"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none', color: 'black' },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={handleCloseNavMenu}>
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <IconButton
            size="large"
            edge="start"
            // color="white"
            aria-label="menu"
            sx={{ mr: 2, display: { xs: 'flex', md: 'none' } }}
          >
            <img src={Pic} width="200px" alt="" />
          </IconButton>

          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>

            <Cascader
              options={options}
              expandTrigger="hover"
              // displayRender={displayRender}
              style={{ width: '17%', zIndex: 5 }}
              suffixIcon={<ArrowDropDownIcon sx={{ fontSize: '20px', fontWeight: '', color: 'black' }} />}
              bordered={false}
              placeholder={<p style={{ fontSize: '18px', color: 'black' }}>MeBookMeta Profile</p>}
            />
            <Cascader
              options={identity}
              expandTrigger="hover"
              // displayRender={displayRender}
              style={{ width: '15%', zIndex: 5 }}
              suffixIcon={<ArrowDropDownIcon sx={{ fontSize: '20px', fontWeight: '', color: 'black' }} />}
              bordered={false}
              placeholder={<p style={{ fontSize: '18px', color: 'black' }}>Identity Profile</p>}
            />
            <Cascader
              options={work}
              expandTrigger="hover"
              // displayRender={displayRender}
              style={{ width: '13%', zIndex: 5 }}
              suffixIcon={<ArrowDropDownIcon sx={{ fontSize: '20px', fontWeight: '', color: 'black' }} />}
              bordered={false}
              placeholder={<p style={{ fontSize: '18px', color: 'black' }}>Work Profile</p>}
            />
            <Button onClick={handleCreate} size="small" sx={{ color: 'black', textAlign: 'center', mr: 'auto', fontSize: '15px' }}> Add Work</Button>

          </Box>

          {/* <Button sx={{ color: 'black', textAlign: 'center', fontSize: '20px',mr:'200px' }}> Your Work</Button> */}

          <Typography color='black'>
            {userData.name}

          </Typography>
          <Box sx={{ display: 'flex', mx: 2 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <img src={`http://54.246.61.54:3002/uploads/${userData.pdf}`} width='50px' height='50px' style={{ borderRadius: '50%' }} alt="" />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default UserAppBar;
