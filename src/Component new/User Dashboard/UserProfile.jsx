import React, { useState, useEffect } from "react";
import axios from "axios";

import image from "../../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import PhoneEnabledIcon from '@mui/icons-material/PhoneEnabled';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import PhonelinkRingOutlinedIcon from '@mui/icons-material/PhonelinkRingOutlined';
import { Link, useNavigate, useParams } from "react-router-dom";
import CommonAppbar from "../CommonAppbar";
import GroupsIcon from '@mui/icons-material/Groups';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Tooltip from '@mui/material/Tooltip';
import UserprofileDetail from "./UserprofileDetail";
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import SettingsSuggestIcon from '@mui/icons-material/SettingsSuggest';
import { QRCodeCanvas } from "qrcode.react";
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import PinterestIcon from '@mui/icons-material/Pinterest';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import RedditIcon from '@mui/icons-material/Reddit';
import OutlinedInput from '@mui/material/OutlinedInput';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import TelegramIcon from '@mui/icons-material/Telegram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import me from '../../mepicnew.png';


import "./UserProfile.css";
import { useDispatch, useSelector } from "react-redux";
import { getAuth, getUserData } from "../../Action";
import { a } from "@react-spring/web";

const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
// import img from "../Screenshot_2.png";



export default function UserProfile({ name }) {
    let navigate = useNavigate()
    let { username } = useParams()
    const dispatch = useDispatch()

    const STORE = useSelector((state) => state)


    // console.log('getData',getData)

    // console.log(username)
    const [url, setUrl] = useState(
        `http://54.246.61.54:3000/UserProfile/${username}`
    );
    const qrcode = (
        <QRCodeCanvas
            id="qrCode"
            value={url}
            size={150}
            bgColor={"#ffff"}
            level={"H"}
        />
    );
    const [anchorElNav, setAnchorElNav] = React.useState(null);
    const [anchorElUser, setAnchorElUser] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };
    const [userData, setUserData] = useState('')
    const [media, setMedia] = useState('')
    console.log("media", media)
    const [userInfo, setuserInfo] = useState({
        pdf: "",
        file: "",
        filepreview: null,
    });

    const getUrl = (path) => {
        return `http://54.246.61.54:3002/uploads/${path}`
    }
    useEffect(() => {
        dispatch(getUserData())
        console.log(getAuth())
    }, []);

    const styleSheet = {
        fontSize: '23px',
        fontWeight: 'bolder'
    }

    // {userData.email}
    return (
        <>
            {/* <CommonAppbar /> */}
            <div style={{ height: '' }} className="container-fluid responsive-diagonal">

                <div style={{ float: "right", marginTop: 5 }}>{qrcode}</div>
                <div className="row">
                    <div className="col-md-8 mx-auto text-center">
                        <img src={getUrl(STORE.getProfileData?.userData?.pdf)} style={{ width: '150px', height: '150px', borderRadius: '50%', marginTop: '50px' }} alt="" />
                        <p style={{ fontSize: '25px', fontWeight: 'bolder', color: 'white', textTransform: 'uppercase', margin: '5px 0px 0px' }}> {STORE.getProfileData?.userData?.name} </p>
                        <p style={{ fontSize: '18px', color: 'white' }}> {STORE.getProfileData?.userData?.email} </p>
                        <div style={{ color: 'white' }} className="row my-3">
                            <div className="col-md-3"></div>
                            <div className="col-md-2">
                                1
                                <p>Work</p>
                            </div>
                            <div className="col-md-2">
                                2
                                <p> Reading List</p>
                            </div>
                            <div className="col-md-2">
                                0
                                <p>Followers </p>
                            </div>
                            <div className="col-md-3"></div>

                        </div>
                    </div>
                </div>
            </div>
            <AppBar sx={{ backgroundColor: '' }} position="static">
                <Container maxWidth="xl">
                    <Toolbar disableGutters>

                        <Typography
                            variant="h6"
                            noWrap
                            sx={{
                                mr: 2,
                                display: { xs: 'none', md: 'flex' },
                                fontWeight: "bolder",
                                letterSpacing: '.3rem',
                                color: 'inherit',
                            }}
                        >
                            About
                        </Typography>

                        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="inherit"
                            >
                                <MenuIcon />
                            </IconButton>

                        </Box>
                        <AdbIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href=""
                            sx={{
                                mr: 2,
                                display: { xs: 'flex', md: 'none' },
                                flexGrow: 1,
                                fontFamily: 'monospace',
                                fontWeight: 700,
                                letterSpacing: '.3rem',
                                color: 'inherit',
                                textDecoration: 'none',
                            }}
                        >
                            LOGO
                        </Typography>
                        {/* <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                            {pages.map((page) => (
                                <Button
                                    key={page}
                                    onClick={handleCloseNavMenu}
                                    sx={{ my: 2, color: 'white', display: 'block' }}
                                >
                                    {page}
                                </Button>
                            ))}
                        </Box> */}

                        <Box sx={{ flexGrow: 0, ml: 'auto' }}>
                            <Button onClick={() => navigate('/UserProfileEdit', { state: userData._id })} startIcon={<SettingsSuggestIcon />} variant="inherit">Edit Profile</Button>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
            <div className="row">
                <div className="col-md-4">
                    <Card sx={{ my: 3 }}>
                        <CardContent>
                            <Typography>Help People get  to Know you</Typography>
                            <button type='button' class='btn btn-primary'>Add Description</button>
                            <p>joined 18 may 2022</p>
                            <hr />
                            <Typography>SHARE PROFILE</Typography>
                            <div className='d-flex' style={{ width: '100%' }}>
                                <a href="https://www.facebook.com/"> <FacebookIcon sx={{ fontSize: '40px', color: '#3b5998' }} /></a>
                                <a href="https://twitter.com/i/flow/login"> <TwitterIcon sx={{ fontSize: '40px', color: '#00acee', mx: '20px' }} /></a>
                                <a href="https://in.pinterest.com/login/"> <PinterestIcon sx={{ fontSize: '40px', color: '#c8232c' }} /></a>
                                <a href="https://www.instagram.com/"><InstagramIcon sx={{ fontSize: '40px', color: '#bc2a8d', mx: '20px' }} /></a>
                                <a href="https://www.reddit.com/r/help/comments/1o7qrk/login/"> <RedditIcon sx={{ fontSize: '40px', color: '#ff4500', mr: '20px' }} /></a>
                                <img alt="Remy Sharp" src={me} onClick={() => navigate('/SignIn')} style={{ height: '40px', width: '40px', borderRadius: '50%', border: '1px solid black', }} />

                            </div>
                        </CardContent>
                    </Card>

                </div>
                <div className="col-md-8  ">

                    <Card sx={{ my: 3 }}>
                        <CardContent >
                            <div className="row d-flex ">
                                <div className="col-md-4">
                                    <Typography variant='h5'> <b> Your DOB :  {STORE.getProfileData.userData.date}</b></Typography>
                                </div>
                                <div className="col-md-4 ">
                                    <Typography variant='h5'> <b> Your Birth Place :  {STORE.getProfileData.userData.birthPlace}</b></Typography>
                                </div>
                                <div className="col-md-4 ">
                                    <Typography variant='h5'> <b> Your Hometown :  {STORE.getProfileData.userData.homeTown}</b></Typography>
                                </div>
                            </div>
                              <div className="row d-flex my-5 ">
                                <div className="col-md-4">
                                    <Typography variant='h5'> <b> Your Current Residence :  {STORE.getProfileData.userData.currentAddress}</b></Typography>
                                </div>
                                <div className="col-md-4 ">
                                    <Typography variant='h5'> <b> College :  {STORE.getProfileData.userData.birthPlace}</b></Typography>
                                </div>
                                <div className="col-md-4 ">
                                </div>
                            </div>
                        </CardContent>
                    </Card>

                </div>

            </div>
        </>
    );
}


{/* <div className="col-md-5" style={{ marginTop: '5%', color: "#06065e" }}>
    <h1 style={{ fontSize: '5vw', marginLeft: "5vw" }}>{userData.name}</h1>
    <p style={{ fontSize: '2vw', marginLeft: "5vw" }}>Specialized in {userData.specialized} having experience in {userData.experience}</p>
</div>
<div className="col-md-2 " style={{ marginTop: '16% ' }}>
    <img
        alt="Remy Sharp"
        src={userInfo.filepreview}
        style={{ width: "15vw", height: "15vw", border: '2px solid black', borderRadius: "50%", }}
    />
</div>
<div className="col-md-5 ">
    <div className=" row d-flex justify-content-between" style={{ marginTop: '60%', color: "white" }}>
        <div className="col-md-4 col-sm-12 col-12">
            <Avatar sx={{ backgroundColor: "black", m: 'auto' }}>
                <PhoneEnabledIcon />
            </Avatar>
            <p style={{ textAlign: 'center' }} >{userData.link1}</p>
        </div>
        <div className="col-md-4 ">
            <Avatar sx={{ backgroundColor: "black", m: 'auto' }}>
                <EmailOutlinedIcon />
            </Avatar>
            <p style={{ textAlign: 'center' }}>{userData.email}</p>
        </div>
        <div className="col-md-4 ">
            <Avatar sx={{ backgroundColor: "black", m: 'auto' }}>
                <GroupsIcon />
            </Avatar>
            <p style={{ textAlign: 'center' }}>Followers  5</p>
        </div>
    </div>
</div> */}