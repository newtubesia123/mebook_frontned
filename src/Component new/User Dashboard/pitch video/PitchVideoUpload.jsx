import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { Button } from '@mui/material'
import VideocamIcon from '@mui/icons-material/VideocamRounded';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import OutlinedInput from '@mui/material/OutlinedInput';
import FormControl from '@mui/material/FormControl';
import { SERVER } from '../../../server/server';
import { toast } from "react-toastify";


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

function BootstrapDialogTitle(props) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};


const PitchVideoUpload = () => {

    const [open, setOpen] = React.useState(false);

    // const [uploadLater, setUploadLater] = useState("");
    const [video, setvideo] = useState({file: ""});

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const handleInputChange = (event) => {
        console.log("event.target.files[0]", event.target.files[0]);
        setvideo({
            ...video,
            file: event.target.files[0],
            filepreview: URL.createObjectURL(event.target.files[0]),
        });
    };

    let token = JSON.parse(localStorage.getItem("token"));

    const hadleSubmit = async () => {
        // e.preventDefault()
        const formdata = new FormData();
        formdata.append("profiles", video.file);
        console.log(formdata);
        await axios.post(`${SERVER}/imageupload`, formdata, {
            headers: { Authorization: `Bearer ${token}` },
        })
            .then((res) => {
                if (res.data.isSuccess) {
                    // setUploadLater("later");
                    setvideo('block')
                    toast.success("video upload");
                }
            })
            .catch((error) => {
                console.log(error);
                alert("video not uploaded");
            });
    };
    return (

        <div>
            <Button variant="contained" endIcon={<VideocamIcon />} onClick={handleClickOpen} >Upload video</Button>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" sx={{ color: "black" }} onClose={handleClose}>
                    Upload your Pitch video here
                </BootstrapDialogTitle>
                <form onSubmit={hadleSubmit}>
                    <DialogContent dividers>
                        <FormControl
                            sx={{ m: 1, width: '25ch' }} variant="outlined">
                            <OutlinedInput
                                type="file" name="videos"
                                id="videos" multiple="false"
                                // accept=".mov,.mp4, mkv"
                                onChange={handleInputChange}
                            />
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" size='medium'>
                            Upload
                        </Button>
                    </DialogActions>
                </form>
            </BootstrapDialog>
        </div>
    )
}

export default PitchVideoUpload