import React, { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { SERVER } from "../../server/server";
function UsersFeed() {
    const STORE = useSelector((state) => state)

    const [story, setStory] = useState([])
    useEffect(() => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios
            .get(`${SERVER}/getWork`)

            .then((res) => {
                // console.log("token--->", id);
                // console.log("res.data---> ", res.data);
                setStory(res.data.writerDetail)
            })
            .catch((err) => console.log(err));
    }, []);
    return (
        <div className='container' style={{ backgroundColor: '' }}>

            {story && story.length?
            story.map((i) => {
                return (
                    <div>

                        <iframe style={{ width: '100%', height: '300px', margin: '10px auto' }} src={`https://www.youtube.com/embed/${i.youtubeUrl}`} frameborder='0' allowfullscreentitle='video' ></iframe>
                        <img
                            style={{ width: '100%', height: '300px', margin: '10px auto' }}
                            src={`${SERVER}/uploads/${i.coverPicture}`}
                            alt="" />
                    </div>
                )
            }):<h3>No story Yet</h3>
        }




        </div>
    )
}

export default UsersFeed