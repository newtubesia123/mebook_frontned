import React, { useState, useEffect } from "react";
import axios from "axios";

import image from "../../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import PhoneEnabledIcon from '@mui/icons-material/PhoneEnabled';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import PhonelinkRingOutlinedIcon from '@mui/icons-material/PhonelinkRingOutlined';
import { Link, useParams, useNavigate, useLocation } from "react-router-dom";
import CommonAppbar from "../CommonAppbar";
import GroupsIcon from '@mui/icons-material/Groups';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Tooltip from '@mui/material/Tooltip';
import UserprofileDetail from "./UserprofileDetail";
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import EditIcon from "@mui/icons-material/Edit";
import Badge from "@mui/material/Badge";
import PageviewIcon from "@mui/icons-material/Pageview";
import SettingsSuggestIcon from '@mui/icons-material/SettingsSuggest';
import "./UserProfile.css";

const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
// import img from "../Screenshot_2.png";

export default function UserProfileEdit() {
    const navigate = useNavigate();
const location = useLocation()
    let { username } = useParams()
    console.log(username)

    const [anchorElNav, setAnchorElNav] = React.useState(null);
    const [anchorElUser, setAnchorElUser] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };
    const [userImage, setUserImage] = useState('')

    const [userData, setUserData] = useState({
        name: '',
        pdf: ''
    })
    // const [media, setMedia] = useState('')

    const [userInfo, setuserInfo] = useState({
        pdf: "",
        file: "",
        filepreview: null,
    });
    const handleInputChange = (event) => {
        console.log("event.target.files[0]", event.target.files[0]);
        setuserInfo({
            ...userInfo,
            file: event.target.files[0],
            filepreview: URL.createObjectURL(event.target.files[0]),
        });
    };
    useEffect(() => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios
            .get("http://54.246.61.54:3002/getuserById", {
                headers: { Authorization: `Bearer ${token}` },
            })

            .then((res) => {
                console.log("res.data---> ", res.data);
                setUserData(res.data.userData)
                // setMedia(res.data.userData.socialMedia)
                if (res.data) {
                    setuserInfo({
                        ...userInfo,
                        file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
                        filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
                    });
                }
            })
            .catch((err) => console.log(err));
    }, []);
    const handleImageUpload = async () => {
        const formdata = new FormData();
        formdata.append("profiles", userInfo.file);

        await axios
            .post(
                "http://54.246.61.54:3002/CoverPicture", formdata)

            .then((res) => {
                console.log("res-->", res.data)
                if (res.data.success) {
                    setUserImage(res.data.file[0].filename)
                    alert(res.data.success);
                }
            });
    };

    const handleChange = (e) => {
        setUserData({
            ...userData,
            [e.target.name]: e.target.value,
        });
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const userEdit = {
            name: userData.name,
            pdf: userImage
        };
        await axios
            .post("http://54.246.61.54:3002/userDetail", { userData: userEdit,userId:location.state })
            .then((res) => {
                console.log("data->", res.data);
                if (res.data.isSuccess) {
                    alert(res.data.message);
                    navigate(`/UserProfile/${username}`);
                } else {
                    alert("something went wrong")
                }
            })
            .catch((err) => console.log(err));
    };
    return (
        <>
            {/* <CommonAppbar /> */}
            <div style={{ height: '' }} className="container-fluid profileEdit ">
                <div className="row">
                    <div className="col-md-8 mx-auto text-center">
                        <div className="text-center mt-5">
                            <Badge overlap="circular" badgeContent="">
                                {/* <div class="image-upload"> */}
                                <input
                                    hidden
                                    id="file-input"
                                    type="file"
                                    onChange={handleInputChange}
                                />
                                {userInfo.file.type == null ? (
                                    <Avatar
                                        className="img1"
                                        sx={{
                                            width: "150px",
                                            height: "120px",
                                        }}
                                    >
                                        <label for="file-input">
                                            <PageviewIcon sx={{ fontSize: "50px" }} />
                                            <p>Select Image</p>
                                        </label>
                                    </Avatar>
                                ) : (
                                    <img
                                        src={userInfo.filepreview}
                                        alt=""
                                        style={{
                                            width: "150px",
                                            height: "120px",
                                            borderRadius: "200px",
                                        }}
                                    />
                                )}
                                <label id="icon" for="file-input">
                                    <EditIcon
                                        style={{ color: "white", m: "auto", fontSize: "30px" }}
                                    />
                                </label>
                            </Badge>
                        </div>
                        <div className="d-flex justify-content-center mt-2">
                            <button
                                type="button"
                                class="btn btn-light btn-sm"
                                onClick={handleImageUpload}
                                style={{ marginRight: "10px" }}
                            >
                                Upload Image
                            </button>

                        </div>
                        <input type='text' class='form-control text-center' onChange={handleChange} name='name'
                            value={userData.name} style={{ fontSize: '25px', textTransform: 'uppercase', margin: '10px auto 0px', backgroundColor: 'white', opacity: 0.8, width: '35%' }} />

                        <div style={{ color: 'white' }} className="row my-3">
                            <div className="col-md-3"></div>
                            <div className="col-md-2">
                                1
                                <p>Work</p>
                            </div>
                            <div className="col-md-2">
                                2
                                <p> Reading List</p>
                            </div>
                            <div className="col-md-2">
                                0
                                <p>Followers </p>
                            </div>
                            <div className="col-md-3"></div>

                        </div>
                    </div>
                </div>
            </div>
            <AppBar sx={{ backgroundColor: 'inherit' }} position="static">
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <Box sx={{ flexGrow: 0, ml: 'auto' }}>
                            <Button onClick={() => navigate(-1)} sx={{ backgroundColor: '' }} variant="contained">Cancel</Button>
                        </Box>

                        <Box sx={{ flexGrow: 0, mx: 5 }}>
                            <Button onClick={handleSubmit} sx={{ backgroundColor: '#06065e' }} variant="contained">Save Changes</Button>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>

        </>
    );
}


