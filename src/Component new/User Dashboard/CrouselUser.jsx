import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import pic from "../../book.jpg";
export default function CrouselUser() {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  return (
    <div className="container">
      <div>
        <Card
          sx={{
            backgroundColor: "#100892",
            borderRadius: "30px",
            width: "100%",
            height: "250px",
          }}
        >
          <Carousel
            arrows={false}
            infinite={true}
            autoPlay={true}
            autoPlaySpeed={3000}
            responsive={responsive}
          >
            <CardContent>
              <div
                className="row mx-auto justify-content-center align-items-center   "
                style={{
                  width: "100%",
                  height: "250px",
                  // margin: "auto auto",
                }}
              >
                <div className="col-md-6">
                  <Typography variant="h4" sx={{ color: "white" }}>
                    The most Loved
                  </Typography>
                  <Typography variant="h3" color="" sx={{ color: "white" }}>
                    <span style={{ color: "rgb(187 11 11)" }}>M</span>
                    <span style={{ color: "white" }}>eBook</span>
                    <span style={{ color: "rgb(187 11 11)" }}>M</span>
                    <span style={{ color: "white" }}>eta</span> Hits
                  </Typography>
                </div>
                <div className="col-md-6 ">
                  <div
                    className="mx-auto"
                    style={{
                      width: "250px",
                      height: "200px",
                      // margin: "auto auto",
                    }}
                  >
                    <img
                      src={pic}
                      alt=""
                      style={{ width: "100%" }}
                      // height={200}
                      // width={250}
                    />
                  </div>
                </div>
              </div>
            </CardContent>
          </Carousel>
        </Card>
      </div>

      {/* <div>

<Card sx={{ backgroundColor: "#100892", borderRadius: '30px', width: '100%', height: '250px',mt:10 }}>
  <CardContent>
    <div className="row">
      <div className="col-md-6">
        <Typography
          variant="h4"

          sx={{ color: 'white', mt: 8 }}
        >
          The most Loved

        </Typography>
        <Typography
          variant="h3"
          color=""
          sx={{ color: 'white' }}
        >
          <span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: 'white' }}>eBook</span><span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: 'white' }}>eta</span>  Hits
        </Typography>
      </div>
      <div className="col-md-6 my-3">
        <img src={pic} alt="" style={{ width: '250px', height: '200px' }} />
      </div>
    </div>
  </CardContent>
</Card>
</div> */}
    </div>
  );
}
