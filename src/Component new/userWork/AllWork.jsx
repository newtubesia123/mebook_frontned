import { Typography } from "antd";
import React, { useEffect } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { userWorkDetail } from "../../Action";
import { BackGround, newButton } from "../background";
import AllWorkCard from "./AllWorkCard";
import WorkCard from "./WorkCard";
import { Box, CircularProgress } from "@mui/material";
import { circularProgressbarStyle } from "../../assets/common/theme";

const AllWork = () => {

  const { userId } = useParams();

  const bg = {
    ...BackGround,
    minHeight: "100vh",
    paddingTop: "10px",
    backgroundAttachment: "fixed",
    backgroundPosition: "center",
  };
  const navigate = useNavigate();

  const dispatch = useDispatch();
  const STORE = useSelector((state) => state.userWorkDetail);
  const progressStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "14em",
  };
  useEffect(() => {
    dispatch(userWorkDetail());
  }, []);

  return (
    <div className="container-fluid" style={{ background: 'black', }}>
      <div className='ml-auto' style={{position:"absolute", left:"80%", marginTop:"3.5rem", zIndex:"99"}}>
        <Button sx={{ ...newButton, fontSize: "75%", padding:"5px 10px" }} onClick={() => navigate(-1)} variant="contained">Go Back</Button>
      </div>
      <div
        className="row justify-content-center"
        style={{
          minHeight: "100vh",
          paddingTop: "10px",
          backgroundAttachment: "fixed",
          backgroundPosition: "center",
          // ...BackGround,
          background: 'black',
          margin: '0 0'
        }}
      >

        <div className="col-md-10 col-sm-11 d-flex flex-column align-items-center">


          <div
            style={{ display: "flex", justifyContent: "center", alignItems: "center", marginTop: "3rem" }}
          >
            <Typography
              style={{ color: "white", fontSize: "200%", letterSpacing: "0.5rem" }}
              variant="h1"
              component="h2"
            >
              All Work
            </Typography>
          </div>
          {STORE.loading ? (
            // 
            <Box sx={progressStyle}>
              <CircularProgress sx={circularProgressbarStyle} />
            </Box>
          ) : STORE.userWorkDetail?.message &&
            STORE.userWorkDetail?.message.length ? (
            STORE.userWorkDetail?.message?.map((e, idx) => (
              <WorkCard id={userId} work={e} key={e._id} idx={idx} />))
          ) : (
            <h4 style={{ color: "white", textAlign: "center" }}>No Work..</h4>
          )}

          {/* <AllWorkCard/> */}

          {/* <AllWorkCard/>
            <AllWorkCard/>
            <AllWorkCard/> */}


        </div>
      </div>
    </div>
  );
};

export default AllWork;
