import React, { useEffect } from "react";
import "./MyWork.css";
import Button from "@mui/material/Button";
import { newButton } from "../background";
import AddBoxIcon from "@mui/icons-material/AddBox";
import DnsIcon from "@mui/icons-material/Dns";
import EditOffIcon from "@mui/icons-material/EditOff";
import WorkCard from "./WorkCard";
import { BackGround } from "../background";
import { Typography } from "antd";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { userProfile, userWorkDetail } from "../../Action";
import { useDispatch, useSelector } from "react-redux";
import Loading from "../loading/Loading";
import pic from "../../mepic.png";
import { Box } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import { circularProgressbarStyle } from "../../assets/common/theme";
import MeLogo from "../../assets/MeLogoMain";
import MeCircularProgress from "../../components/componentsC/meCircularProgress/MeCircularProgress";

const progressStyle = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  height: "14em",
};

const MyWork = () => {
  const saveBtn = { ...newButton, width: "12rem", letterSpacing: "1px" };
  const location = useLocation();
  const navigate = useNavigate();
  const { userId } = useParams();
  const dispatch = useDispatch();
  const STORE = useSelector((state) => state);

  useEffect(() => {
    window.scroll(0, 0)
    dispatch(userWorkDetail(userId));
    dispatch(userProfile(userId));
  }, [userId]);

  // To make first character of every word of a string Capital
  const fstLetCaps = (strr) => {
    const str = strr.toLowerCase();
    const arr = str.split(" ");
    for (var i = 0; i < arr.length; i++) {
      arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
    }
    const str2 = arr.join(" ");
    return str2;
  };
  return (

    <div
      className="row justify-content-center"
      style={{
        // ...BackGround,
        background: 'black',
        minHeight: "100vh",
        // backgroundAttachment: "fixed",
        margin: '0 0'
      }}
    >
      <div className="imgMywork col-md-10 col-sm-11 d-flex flex-column align-items-center">
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            marginTop: "0",
            marginBottom: ".2rem",
            paddingTop: "1.5rem",
            height: '15%',
            width: '15%'
          }}
        >
          <Button
            variant="contained"
            sx={{ ...newButton, position:"absolute" , right:5 ,mt: 3, fontSize:"70%" }}
            onClick={() => navigate(-1)}
          > 
            Go Back
          </Button>
          {/* <img src={pic} alt="" style={{ width: '10rem', height: '10rem' }} /> */}
          <MeLogo/>
        </div>
        <div>
          {userId== STORE.getProfileData?.userData?._id? (
            <div className="btnFlex">
              <div className="singleBtn">
                <Button
                  variant="contained"
                  onClick={() =>
                    navigate("/MainDashboard/createStory/Add", {
                      state: { id: "" },
                    })
                  }
                  size="small"
                  sx={saveBtn}
                >
                  Share Work
                  <AddBoxIcon sx={{ marginLeft: "1px" }} className="btnIcon" />
                </Button>
              </div>

              <div className="singleBtn">
                <Button
                  variant="contained"
                  size="small"
                  sx={saveBtn}
                  onClick={() => navigate(`/MainDashboard/AllWork/${userId}`)}
                >
                  All Work
                  <DnsIcon sx={{ marginLeft: "1px" }} className="btnIcon" />
                </Button>
              </div>

            </div>
          ) : (
            ""
          )}

          {STORE.userProfile.loading ? (
            <Box sx={progressStyle}>
              <MeCircularProgress />
            </Box>
          ) : (

            <div
              style={{
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                alignItems: "center",
                border: '1px dotted #00E7FF',
                marginTop: '2rem',
                padding: '10px',
                background: '#181823',
                borderRadius: '5px'
              }}
            >
              {STORE.userProfile?.userProfile?.meProfile ? (
                <Typography
                  style={{
                    color: "white",
                    fontSize: "100%",
                    letterSpacing: "0.1rem",
                    justifyContent: 'center'
                  }}
                  variant="h4"
                >
                  <span style={{ fontSize: "2rem", justifyContent: 'center' }}>
                    {" "}
                    {STORE.userProfile?.userProfile?.meProfile?.userId?.name}

                    's MeBookMeta <br /> </span>{" "}
                  {userId== STORE.getProfileData?.userData?._id && (
                    <span style={{ fontSize: "3rem", textAlign: "center" }}>WorkShare Page</span>
                  )}
                </Typography>
              ) : (
                ""
              )}


              {userId== STORE.getProfileData?.userData?._id && STORE.userProfile?.userProfile ? (
                <Typography style={{ fontSize: "100%", color: "white" }}>
                  <strong> YOUR MEBOOKMETA FINGERPRINT TAGS : </strong> &nbsp;
                  <span style={{ fontSize: "100%", color: "white" }}>
                    {STORE.userProfile?.userProfile?.meProfile.subCategory.map(
                      (item, i) => (
                        <span key={i} style={{ marginRight: "0.1rem" }}>
                          {item.name}
                          {STORE.userProfile.userProfile.meProfile.subCategory
                            .length -
                            1 ===
                            i
                            ? ""
                            : ", "}
                        </span>
                      )
                    )}
                  </span>
                </Typography>
              ) : (
                ""
              )}
            </div>
          )}

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "4rem",
            }}
          >
            {userId != STORE.getProfileData?.userData?._id ? (
              <Typography
                style={{
                  color: "white",
                  fontSize: "3rem",
                  letterSpacing: "0.2rem",
                }}
                variant="h1"
              >
                WorkShare Page
              </Typography>
            ) : (
              <Typography
                style={{
                  color: "white",
                  fontSize: "175%",
                  letterSpacing: "0.2rem",
                }}
                variant="h4"
                component="h2"
              >
                My Show and Tell
              </Typography>
            )}
          </div>
        </div>
        {STORE.loading ? (
          <h3 style={{ textAlign: "center", color: "white" }}>
            {" "}
            <Loading />
          </h3>
        ) : STORE.userWorkDetail?.userWorkDetail.message &&
          STORE.userWorkDetail?.userWorkDetail.message.length ? (
          STORE.userWorkDetail?.userWorkDetail.message.map((e, idx) =>
          userId != STORE.getProfileData?.userData?._id ? (
              <WorkCard id={userId} work={e} key={e._id} idx={idx} />
            ) : idx < 5 ? (
              <WorkCard id={userId} work={e} key={e._id} idx={idx} />
            ) : (
              ""
            )
          )
        ) : (
          <h4 style={{ color: "white", textAlign: "center" }}>No Work..</h4>
        )}
      </div>
    </div>
  );
};

export default MyWork;
