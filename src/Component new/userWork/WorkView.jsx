import React, { useEffect, useState } from 'react'
import './WorkView.scss'
import { useDispatch, useSelector } from 'react-redux'
import { useParams, useNavigate } from 'react-router-dom'
import { SERVER } from '../../server/server'
import { newButton } from '../background'
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import axios from 'axios'
import WorkPdf from './workDisplay/WorkPdf';
import WorkViewProfile from './WorkViewProfile';
import AudioPlayer from './workDisplay/audioPlayer/AudioPlayer'

import CircularProgress from "@mui/material/CircularProgress";
import { circularProgressbarStyle } from '../../assets/common/theme'
import { callPopUp, getAuth, getUserData, handleWorkLikes } from '../../Action';
import Cookies from 'universal-cookie';
import { descriptionFunction } from '../../assets/descriptionFun';
import WVSocial from './workViewSocial/WVSocial'
import { getImgURL } from '../../util/getImgUrl'
import Badge from '@mui/material/Badge';
import { toast } from 'react-toastify'
import MeCircularProgress from '../../components/componentsC/meCircularProgress/MeCircularProgress'
import BackBtn from '../../components/componentsB/btn/backBtn/BackBtn'
import SignInModal from '../../SignInModal'
import { useWindowSize } from '../../Hooks/useWindowSize'

const WorkView = () => {


    const resize = useWindowSize();
    const cookies = new Cookies()
    const bg = { backgroundColor: "black", minHeight: '100vh', overflowX: 'hidden' }
    const params = useParams();
    const STORE = useSelector((state) => state)
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [workData, setWorkData] = useState([]);
    const [profileData, setProfileData] = useState([]);
    const [allWorkData, setAllWorkData] = useState([]);
    const [commentArr, setCommentArr] = useState([]);
    const [hitApi, setHitApi] = useState(false)
    const [dep, setDep] = useState(false)
    const [like, setLike] = useState(false);
    const [ModalSignin, setModalSignin] = useState(false);
    const getPdf = (name) => `${SERVER}/uploads/${name}#toolbar=0`
    const getImg = (name) => `${SERVER}/uploads/${name}`


    useEffect(() => {
        window.scrollTo(0, 0)
        axios.all([axios.get(`${SERVER}/work_1/getUserWorkById/${params.id}`),
        axios.get(`${SERVER}/users/getProfileDetails/${params.userId}`),
        axios.get(`${SERVER}/work_1/getIndividualWork/${params.userId}`)])
            .then(axios.spread((firstResponse, secondResponse, thirdResponse) => {

                setTimeout(() => {
                    setWorkData(firstResponse.data.data[0]);
                    setLike(firstResponse.data.data[0].likedBy.includes(STORE.getProfileData?.userData?._id))
                    cookies.set('currentLikeState', firstResponse.data.data[0].liked);
                    setProfileData(secondResponse.data);
                    setAllWorkData(thirdResponse.data.message);

                }, (200))

            }))
            .catch(error => console.log(error));

    }, [params])
    const autoComment = (arg, type) => {
        if (getAuth()) {
            setHitApi(false)
            if (arg && hitApi) {
                axios.post(`${SERVER}/work_1/${type}`, { autoCommentWorkId: arg },
                    {
                        headers: { Authorization: `Bearer ${getAuth()}` }
                    })
                    .then((res) => {
                        if (res?.data?.errorCode == 200) {
                            setDep(true)

                        }
                        if (res?.data?.errorCode == 200 && type == 'addAutoComment') {
                            toast.success('Comment Added', { autoClose: 2000, pauseOnHover: false, position: 'top-right' })
                        }
                        else if (res?.data?.errorCode == 200 && type == 'removeAutoComment') {
                            toast.success('Comment Removed', { autoClose: 2000, pauseOnHover: false, position: 'top-right' })
                        }
                        else {
                            toast.error('Something Error', { autoClose: 2000, pauseOnHover: false, position: 'top-right' })
                        }

                    })
                    .catch((error) => console.log('Error in addAutoComment or removeAutoComment', error))
            }
        } else {
            dispatch(callPopUp({ type: "signin" }))
        }

    }

    useEffect(() => {
        axios.get(`${SERVER}/work_1/getAutoCommentById/${params.id}`)
            .then((res) => {

                setCommentArr(res?.data?.data)
                setDep(false)
                setHitApi(true)
            })
            .catch((error) => console.log('error in getAutoCommentById', error))



    }, [dep, params])

    const pdfSize = (size) => {
        if (size[0] <= 390) {
            return '320'
        }
        if (size[0] <= 500 && size[0] >= 390) {
            return '350'
        }
        else if (size[0] <= 700 && size[0] >= 500) {
            return '450'
        }
        else if (size[0] <= 900 && size[0] >= 700) {
            return '700'
        }
        else if (size[0] >= 900) {
            return '1000'
        }
    }


    // to find extention of work
    console.log("UserWork", workData);
    const findWorkType = workData?.userWork;
    const workTypeSplit = findWorkType?.split('.')
    const workType = workTypeSplit ? workTypeSplit[workTypeSplit.length - 1] : 'JPEG';
    const getDate = (str) => {
        // var str = "Fri Feb 08 2013 09:47:57 GMT +0530 (IST)";
        var date = new Date(str);
        var day = date.getDate(); //Date of the month: 2 in our example
        var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
        var year = date.getFullYear() //Year: 2013
        return `${day}/${month + 1}/${year}`;

    }
    const tagsHover = {
        color: 'black',
        fontSize: "50%",
        '&:hover': {
            color: 'red',
            animation: 'ease .1s',

        }
    }
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
    function onDocLoadSuccess({ numPages }) {
        setNumPages(numPages);
        setPageNumber(1);

    }
    function changePage(offset) {
        setPageNumber(prevPageNo => prevPageNo + offset)
    }
    function changePageBack() {
        changePage(-1)
    }
    function changePageNext() {
        changePage(+1)
    }
    function handleLikes(type) {
        if (getAuth()) {
            if (type === "like") {
                setLike(true)
                setWorkData({ ...workData, liked: workData.liked + 1 })
            } else {
                setLike(false)
                setWorkData({ ...workData, liked: workData.liked - 1 })
            }
        } else {
            navigate("/SignIn")
        }

    }
    // STORE.getProfileData?.userData?._id,workData._id,
    window.onpopstate = () => {
        cookies.remove("currentLikeState")
    }
    useEffect(() => {
        const callAfter5Sec = setTimeout(() => {
            if (cookies.get('currentLikeState') === workData.liked) {
                console.log("do nothing", workData.liked, cookies.get('currentLikeState'))
            } else {// its false means remove and if its true means add in follow api
                console.log("else", workData.liked, cookies.get('currentLikeState'))
                if (cookies.get('currentLikeState') === undefined) return console.log("not calling cookie undefined")
                if (cookies.get('currentLikeState') > workData.liked) dispatch(handleWorkLikes(STORE.getProfileData?.userData?._id, workData._id, "disLike", {
                    send: () => cookies.set('currentLikeState', workData.liked, { path: "/" })
                }))
                if (cookies.get('currentLikeState') < workData.liked) dispatch(handleWorkLikes(STORE.getProfileData?.userData?._id, workData._id, "like", {
                    send: () => cookies.set('currentLikeState', workData.liked, { path: "/" })
                }))
            }
        }, 2000)
        return () => clearInterval(callAfter5Sec)
    }, [workData?.liked])


    return (

        <>
            <div style={bg} className='col-md-12'>
                <WVSocial social={profileData?.meProfile?.userId} />

                <div className='col-md-12 justify-content-center ' style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', }} >

                    <div className='ml-auto mt-5'>
                        {/* <Button sx={{ ...newButton, fontSize: "75%", marginTop: "2.5rem" }} onClick={() => navigate(-1)} variant="contained">Go Back</Button> */}
                        <BackBtn />
                    </div>
                    {
                        profileData.length === 0 || allWorkData.length === 0 || workData.length == 0
                            ?
                            <div style={{ margin: '10rem', padding: 0 }}>
                                {/* <CircularProgress sx={circularProgressbarStyle} /> */}
                                <MeCircularProgress />
                            </div>
                            :

                            <>
                                <WorkViewProfile profileData={profileData} allWorkData={allWorkData} params={params} />

                                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', }}>
                                    {/* <img src={pic} width={200} height={90} alt="" /> */}
                                    <Typography variant="h4" gutterBottom sx={{ color: 'white', margin: '1rem 0 1rem 0', maxWidth: '82%', fontSize: "150%" }}>
                                        <span style={{ fontWeight: 'bold', marginRight: '5px' }}>TITLE:</span>
                                        {workData?.title?.toUpperCase()}
                                    </Typography>

                                    <img style={{ width: '20%', height: '20%', margin: '1rem 0 1rem 0', }}
                                        src={getImgURL(workData?.coverPicture)} />
                                    <Typography variant="h6" gutterBottom sx={{
                                        color: 'black', display: 'flex', flexWrap: 'wrap',
                                        lineHeight: '1.6', margin: '1rem 0 1rem 0', maxWidth: '82%',
                                    }}
                                    >
                                        {
                                            workData?.subCategory ?
                                                workData.subCategory.map((item, idx) => (
                                                    <span key={idx} style={{ background: '#BFDCE5', borderRadius: '8px', marginRight: '4px', padding: '4px 4px 4px 4px', cursor: 'pointer', margin: ".1rem .1rem ", }}>
                                                        <span style={tagsHover}>

                                                            {
                                                                item.name?.toUpperCase()}
                                                            {workData.subCategory.length - 1 === idx ? '' : '  '}

                                                        </span>
                                                    </span>
                                                ))
                                                : ""

                                        }
                                    </Typography>
                                    <Box style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', margin: '1rem 0 1rem 0', maxWidth: '82%' }}>
                                        <p style={{ margin: '0 15px 0 15px', display: 'flex', alignItems: 'center' }}>
                                            {
                                                like ?
                                                    <ThumbUpAltIcon onClick={() => localStorage.getItem('token') ? handleLikes("disLike") : setModalSignin(true)} style={{ width: '20px', height: '20px', cursor: 'pointer', color: 'red' }} />
                                                    :
                                                    <ThumbUpOffAltIcon onClick={() => localStorage.getItem('token') ? handleLikes("like") : setModalSignin(true)} sx={{ color: 'white' }} style={{ width: '20px', height: '20px', cursor: 'pointer' }} />
                                            }

                                            <span style={{ fontSize: '1rem', color: 'lightgray' }}>{workData?.liked}</span>
                                        </p>
                                        <p style={{ margin: '0 15px 0 15px' }}><AccessTimeIcon sx={{ color: 'white' }} style={{ width: '20px', height: '20px' }} /><span style={{ fontSize: '0.8rem', color: 'lightgray' }}>{getDate(workData.createdAt
                                        )} </span></p>

                                    </Box>
                                    <Typography gutterBottom style={{ color: 'white', margin: '1rem 0 1rem 0', maxWidth: '82%', margin: "2rem", fontSize: "80%" }} >DESCRIPTION:</Typography><br></br>
                                    {/* <Typography sx={{ backgroundColor: 'black', color: 'white', border: 'none', padding: 10, maxWidth: 845, }}>  {workData?.description?.join(' ')} </Typography> */}
                                    {/* <textarea disabled style={{width:'700px',height:'200px',background:'black',color:'white',border:'none',margin:'1rem',padding:'10px'}}>
                                {workData?.description?.join(' ')}
                                </textarea> */}
                                    {descriptionFunction(workData?.description)?.map((item, index) => (
                                        <div style={{ color: 'white', maxWidth: '65%' }}>
                                            <p style={{ fontSize: "80%" }}>{item}</p>
                                        </div>
                                    ))

                                    }



                                </div>


                                <div className='col-md-12 justify-content-center ' style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', paddingBottom: '8%' }}
                                // style={{ boxShadow: 'rgba(252, 252, 252, 0.35) 0px 5px 15px' }}
                                >
                                    {workData?.youtubeUrl && workData?.youtubeUrl != null ? (
                                        <embed src={`https://www.youtube.com/embed/${workData?.youtubeUrl}`} type="" height='575' width='100%' />

                                    ) : (

                                        <div className='col-md-12 justify-content-center ' style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', paddingBottom: '8%' }}
                                        // style={{ boxShadow: 'rgba(252, 252, 252, 0.35) 0px 5px 15px' }}
                                        >
                                            {workData?.youtubeUrl && workData?.youtubeUrl != null ? (
                                                <iframe src={`https://www.youtube.com/embed/${workData?.youtubeUrl}?controls=1`} height='575' width='100%' frameborder="0" allow='fullScreen;' />

                                            ) : (

                                                workType && workType === 'pdf' ?
                                                    <WorkPdf file={workData?.userWork} />
                                                    :
                                                    workType && workType === 'mp3' ?
                                                        <AudioPlayer file={workData?.userWork} coverPicture={workData?.coverPicture} title={workData?.title} name={profileData?.meProfile?.userId?.name} />
                                                        :
                                                        workType && workType === 'mp4' ?
                                                            <video src={getImgURL(workData?.userWork)} controlsList='nodownload' controls width={pdfSize(resize)} height='475' type="" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen />
                                                            :

                                                            <video src={getImgURL(workData?.userWork)} controlsList='nodownload' controls width={pdfSize(resize)} height='475' type="" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen />

                                                // <div className="video-container">
                                                //     <div className="video">
                                                //         <video controls>
                                                //             <source src={getImgURL(workData?.userWork)} type="video/mp4" />
                                                //         </video>
                                                //     </div>
                                                // </div>
                                                // :
                                                // <div className="video-container">
                                                //     <div className="video">
                                                //         <video controls>
                                                //             <source src={getImgURL(workData?.userWork)} type="video/mp4" />
                                                //         </video>
                                                //     </div>
                                                // </div>





                                                // <embed src={getImgURL(workData?.userWork)} type="" height='1100' width='845' />


                                                // <WorkPdf file={workData?.userWork} />

                                                // <AudioPlayer file={workData?.userWork}/>



                                                // <>
                                                //     <div style={{ position: '' }}>
                                                //         <header>
                                                //             <Document style={{ backgroundColor: 'blue' }}
                                                //                 // file='http://54.246.61.54:3002/uploads/1675190001943assignmentsNew.pdf'
                                                //                 file={getImgURL(workData?.userWork)}
                                                //                 onLoadSuccess={onDocLoadSuccess}>
                                                //                 <Typography color='white'>Page{pageNumber} of {numPages}</Typography>
                                                //                 {
                                                //                     pageNumber > 1 &&
                                                //                 <Button sx={{ m: 2, ...newButton }} onClick={changePageBack} variant="contained">Prev Page</Button>

                                                //                 }
                                                //                 {
                                                //                     pageNumber < numPages &&
                                                //                     <Button sx={{ m: 2, ...newButton }} onClick={changePageNext} variant="contained">Next Page</Button>


                                                //                 }
                                                //                 <Page height='200' width='800' pageNumber={pageNumber} />

                                                //             </Document>
                                                //         </header>

                                                //     </div>
                                                //     {/* <center>
                                                //         <div>
                                                //             <Document file='http://54.246.61.54:3002/uploads/1675190001943assignmentsNew.pdf' onLoadSuccess={onDocLoadSuccess}>
                                                //                 {Array.from(
                                                //                     new Array(numPages),
                                                //                     (el, index) => (
                                                //                         <Page
                                                //                             key={`page_${index + 1}`}
                                                //                             pageNumber={index + 1}
                                                //                         />
                                                //                     )
                                                //                 )}
                                                //             </Document>
                                                //         </div>
                                                //     </center> */}
                                                // </>
                                            )}
                                        </div>
                                    )}

                                    {/* <Box sx={{ minWidth: 800, }}>
        <Textarea sx={{ backgroundColor: 'black', color: 'white', border: 'none', padding: 10, }} value={workData?.description?.join(' ')} disabled cols="92" rows={workData?.description?.length > 1000 ? 20 : 12}></Textarea>
    </Box> */}


                                    {/* Auto generated comments work starts */}

                                    <div className="col-md-8 comment_main">
                                        <h5>I'd love to hear your thoughts</h5>
                                        <span>
                                            {
                                                commentArr?.length ? (
                                                    commentArr.map(
                                                        (item, i) => (

                                                            <span className='span_2'
                                                                key={"profileSubCat" + i}
                                                                style={{
                                                                    background: (item?.userData?.includes(STORE?.getProfileData?.userData?._id) ? 'rgb(242, 255, 0)' : 'lightgray'),


                                                                }}
                                                                onClick={() =>
                                                                    autoComment(item?._id,
                                                                        item?.userData?.includes(STORE?.getProfileData?.userData?._id) ? 'removeAutoComment' : 'addAutoComment'
                                                                    )}
                                                            >
                                                                {
                                                                    item?.count > 0 ?
                                                                        <div style={{ display: 'flex', alignItems: 'center', }}>
                                                                            {/* <MoreVertIcon onClick={() => alert('meraj')} /> */}
                                                                            <Badge badgeContent={item.count} color="primary">
                                                                                <span>{item.comment}&nbsp; &nbsp;</span>
                                                                            </Badge>

                                                                        </div>
                                                                        :
                                                                        <div style={{ display: 'flex', alignItems: 'center', }}>
                                                                            <span>{item.comment}&nbsp; &nbsp;</span>
                                                                        </div>
                                                                }

                                                            </span>

                                                        )
                                                    )
                                                ) : (
                                                    <span className='span_3' style={{ display: "none" }}></span>
                                                )}
                                        </span>
                                    </div>


                                </div>
                                {ModalSignin ? <SignInModal setModalSignin={setModalSignin} username={params.userId} userId={params.id} path={"WorkView"} /> : ''}
                            </>
                    }
                </div>
            </div>
        </>

    )
}

export default WorkView