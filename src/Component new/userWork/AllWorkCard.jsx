
import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import { SERVER } from '../../server/server';
import "../User Dashboard/userDashboardItem/DashboardCard.css"
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import { BackGround, textHover } from '../background';
import Typography from '@mui/material/Typography';
import CancelIcon from '@mui/icons-material/Cancel';
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import CloseIcon from "@mui/icons-material/Close";
import Slide from "@mui/material/Slide";
import PropTypes from "prop-types";
import StarsIcon from '@mui/icons-material/Stars';
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import IconButton from '@mui/material/IconButton';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    "& .MuiDialogContent-root": {
        padding: theme.spacing(2),
    },
    "& .MuiDialogActions-root": {
        padding: theme.spacing(1),
    },
}));

function BootstrapDialogTitle(props) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    className=""
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: "absolute",
                        right: 20,
                        top: 20,
                        //    backgroundColor: (theme) => theme.palette.black,
                    }}
                >
                    <CloseIcon
                        sx={{
                            fontSize: "50px",
                            color: "black",
                            borderRadius: "50%",
                            backgroundColor: "white",
                        }}
                    />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

const AllWorkCard = ({ work }) => {

    const getImgURLPdf = (name) => `${SERVER}/uploads/${name}#toolbar=0`

    const modalStyle = {
        ...BackGround,
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 700,
        // bgcolor: '',
        border: '2px solid #000',
        boxShadow: 24,
        overflow: 'auto',
        height: '100vh',
        p: 4,
    };

    const navigate = useNavigate();
    const getImgURL = (name) => `${SERVER}/uploads/${name}`
    // console.log("meraj", work)

    const cardCategory = { margin: 0, color: 'lightGray', marginTop: "1rem" }

    // for modal
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const tagsHover = {
        color: 'white',
        fontSize: '.8rem',
        '&:hover': {
            color: 'red',
            animation: 'ease .1s',

        }
    }
    const getDate = (str) => {
        // var str = "Fri Feb 08 2013 09:47:57 GMT +0530 (IST)";
        var date = new Date(str);
        var day = date.getDate(); //Date of the month: 2 in our example
        var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
        var year = date.getFullYear() //Year: 2013
        return `${day}/${month + 1}/${year}`;

    }

    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '3rem' }}>


            <div class="card">
                {/* work image */}
                <img id='workImg' src={getImgURL(work.coverPicture)} alt="" onClick={handleOpen} style={{ marginBottom: "3rem" }} />



                {/* <img id='workImg' src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png' alt="" /> */}


                {/* profileImage */}
                {/* <img id='profImg' src={getImgURL(work.userId.pdf)} alt=""  onClick={()=>navigate(`/MainDashboard/UserProfile/${work.userId.name}/${work.userId._id}`)}/> */}



                {/* <img id='profImg' src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png' alt="" /> */}
                <h3 className='cardDetail'
                // onClick={() => navigate(`/MainDashboard/WorkViewTop5/${work._id}`)} 
                >Tittle:{`${work.title}`.toLocaleUpperCase()}</h3>
                <div class="cont">
                    <p id='para' style={{ textAlign: "left" }}>Description: {work.description[0]}
                        {`....`} <span onClick={handleOpen} style={{ color: 'black', cursor: 'pointer' }}>see more</span>
                    </p>
                </div>
            </div>


            {/* <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                style={{ height: '100vh', overflow: 'scroll' }}
            // BackdropComponent={Backdrop}
            // BackdropProps={{
            //   timeout: 500,
            // }}
            >
                <Fade in={open}>
                    <Box sx={modalStyle}>
                        <CancelIcon onClick={() => setOpen(!open)} sx={{ fontSize: '50px', color: 'white', float: 'right', cursor: 'pointer' }} />
                        <img style={{ width: '100%', height: '70%', objectFit: 'contain' }} src={getImgURL(work.coverPicture)} />

                        <Typography variant="h5" gutterBottom sx={{ ...textHover }}
                        //  onClick={() => navigate(STORE.getProfileData.success ?
                        //     `/MainDashboard/WorkView/${work.userWork}` :
                        //     `/WorkView/${work.userWork}`)}
                        > Tittle:
                            {work?.title}
                        </Typography>
                        <Typography variant="body1" gutterBottom sx={cardCategory} > Description:
                            {
                                work?.description.join(" ")

                            }
                        </Typography>
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: "1rem" }}>
                            {work?.userWork ? (
                                <embed src={getImgURLPdf(work.userWork)} type="" height='500' width='600' />

                            ) : (
                                <embed src={`https://www.youtube.com/embed/${work.youtubeUrl}`} type="" height='500' width='600' />

                            )}
                        </div>


                    </Box>
                </Fade>
            </Modal> */}
            <BootstrapDialog
                className=""
                fullScreen
                TransitionComponent={Transition}
                open={open}
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"

            >
                <DialogContent
                    className=""
                    // style={BackGround}
                    dividers
                >
                    <BootstrapDialogTitle
                        id="customized-dialog-title"
                        onClose={() => setOpen(!open)}
                    >

                    </BootstrapDialogTitle>
                    <div className='row justify-content-center'>
                        <div className='col-md-10' style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Typography variant="h4" gutterBottom sx={{ ...textHover, color: 'black' }}

                            ><span style={{ fontWeight: 'bold', marginRight: '5px' }}>TITLE:</span>
                                {work?.title.toUpperCase()}
                            </Typography>




                            <img style={{ width: '50%', height: '50%', marginBottom: '1rem', }} src={getImgURL(work.coverPicture)} alt='Work Cover Picture' />
                            <Typography variant="h4" gutterBottom sx={{ color: 'black', textAlign: 'justify', lineHeight: '1.6' }}

                            >
                                {
                                    work.subCategory.map((item, idx) => (
                                        <span key={idx} style={{ background: '#30E3DF', borderRadius: '10px', marginRight: '4px', padding: '0 8px 8px 8px', cursor: 'pointer', margin: '5px' }}>
                                            <span style={tagsHover}>
                                                {
                                                    item.name.toUpperCase()}
                                                {work.subCategory.length - 1 === idx ? '' : ', '}

                                            </span>
                                        </span>


                                    ))
                                }
                            </Typography>

                            <Box style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', marginTop: '10px' }}>
                                <p style={{ margin: '0 15px 0 15px' }}><ThumbUpOffAltIcon sx={{ color: 'blue' }} style={{ width: '30px', height: '30px' }} /><span style={{ fontSize: '1.2rem', color: 'blue' }}>{work.liked}</span></p>
                                <p style={{ margin: '0 15px 0 15px' }}><AccessTimeIcon sx={{ color: 'blue' }} style={{ width: '30px', height: '30px' }} /><span style={{ fontSize: '1.2rem', color: 'blue' }}>{getDate(work.createdAt
                                )}</span></p>

                            </Box>
                            <Typography gutterBottom sx={cardCategory} style={{ color: 'black', marginTop: '1rem' }} ><span style={{ fontWeight: 'bold', display: 'flex', flexDirection: 'column', fontSize: '1.3rem', alignItems: 'center' }}>DESCRIPTION:</span><br></br>
                                <span style={{ fontSize: '1rem' }}>{
                                    work?.description.join(".")

                                }</span>

                            </Typography>


                            <div className='col-md-12 justify-content-center' style={{ marginTop: '1rem', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>

                                {work.youtubeUrl ? (<embed src={`https://www.youtube.com/embed/${work.youtubeUrl}`} type="" height='50%' width='100%' />)
                                    : (
                                        <embed src={getImgURLPdf(work.userWork)} type="" height='90%' width='85%' />
                                    )}

                            </div>


                        </div>


                    </div>
                </DialogContent>
            </BootstrapDialog>
        </div>


    )
}

export default AllWorkCard