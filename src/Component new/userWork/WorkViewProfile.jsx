import React from 'react'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import './WorkViewProfile.css'
import { getImgURL } from '../../util/getImgUrl';
import DefaultImg from "../../png/Avatar-Profile-PNG-Photos.png"
const WorkViewProfile = ({ profileData, allWorkData, params }) => {
    const STORE = useSelector((state) => state);
    const navigate = useNavigate();
    // console.log("workview Params", params)
    return (
        <div className=" mt-5 mb-5 col-md-10 col-lg-8 justify-content-center">
            <div className="row no-gutters Work__card">
                <div className="col-md-4 col-lg-4"
                    onClick={() => {
                        params?.userId === STORE.getProfileData?.userData?._id
                            ? navigate(`/MainDashboard/UserProfile/${STORE.getProfileData?.userData?.name}/${STORE.getProfileData?.userData?._id}`)
                            : STORE.getProfileData?.success
                                ? navigate(`/MainDashboard/UserProfile/${profileData?.meProfile?.userId?.name}/${profileData?.meProfile?.userId?._id}`)
                                : navigate(`/Profile/${profileData?.meProfile?.userId?.name}/${profileData?.meProfile?.userId?._id}`)
                    }}
                >
                    {/* {profileData?.meProfile?.userId?.pdf ? <img className='workProfile' src={getImgURL(profileData?.meProfile?.userId?.pdf)} />
                        :
                        <img className='workProfile' src={DefaultImg}
                            alt="profile picture" style={{ border: '1px solid white' }} />} */}

                    <img className='workProfile' src={profileData?.meProfile?.userId?.pdf === null ? DefaultImg : getImgURL(profileData?.meProfile?.userId?.pdf)} />

                </div>
                <div className="col-md-8 col-lg-8">
                    <div className="d-flex flex-column" style={{ gap: "1.5rem" }}>
                        <div className="d-flex flex-row justify-content-center align-items-center p-3 bg-dark text-white">
                            <h3 className="display-5" style={{ cursor: 'pointer' }}>{profileData?.meProfile?.userId?.name}</h3>
                            {/* <i className="fa fa-facebook"></i><i className="fa fa-google"></i><i className="fa fa-youtube-play"></i><i className="fa fa-dribbble"></i><i className="fa fa-linkedin"></i> */}
                        </div>
                        <div className="d-flex flex-row justify-content-center align-items-center p-2 bg-black text-white">
                            <h6>
                                {profileData?.meIdentity?.position}, &nbsp;
                                {profileData?.meIdentity?.status}, &nbsp;
                                {profileData?.meProfile?.category}
                            </h6>
                        </div>
                        <div className="d-flex flex-row text-white " style={{ gap: "5px" }}>
                            {
                                allWorkData && allWorkData.length > 0 ? (
                                    allWorkData.map((item, idx) =>
                                        idx < 3 ? (
                                            <div className="p-3  text-center skill-block"
                                                onClick={() => {
                                                    localStorage.getItem('token')
                                                        ? navigate(`/MainDashboard/WorkView/${item?.userId?._id}/${item?._id}`)
                                                        : navigate(`/WorkView/${item?.userId?._id}/${item?._id}`);
                                                }}
                                                key={"workProfile" + idx}>

                                                <img src={getImgURL(item.coverPicture)} style={{ width: '100%', height: '100%' }} />
                                            </div>
                                        )
                                            : ''
                                    ))
                                    : ''
                            }
                            {/* <div className="p-3  text-center skill-block">
                                
                                 <img src='https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8&w=1000&q=80' style={{width:'100%',height:'100%'}}/>
                             </div> */}
                            {/* <div className="p-3 bg-success text-center skill-block">
                               
                                 <img src='https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8&w=1000&q=80' style={{width:'100%',height:'100%'}}/>
                            </div> */}
                            {/* <div className="p-3 bg-warning text-center skill-block">
                               
                                 <img src='https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8&w=1000&q=80' style={{width:'100%',height:'100%',cursor:'pointer'}}/>
                            </div> */}
                            {
                                allWorkData && allWorkData.length > 3 ?


                                    <div className="p-3 text-center skill-block" style={{ cursor: 'pointer', display: "flex", justifyContent: "center", alignItems: "center", background: "#343a40" }}
                                        onClick={() => {
                                            params?.userId === STORE.getProfileData?.userData?._id
                                                ? navigate(`/MainDashboard/AllWork`)
                                                : navigate(`/WriterStoryDetails/${params?.userId}`);
                                        }}>
                                        <h2>+{allWorkData.length - 3} </h2>
                                        <h5>Works</h5>

                                    </div>
                                    : ""
                            }
                        </div>
                    </div>
                </div>
            </div>
            <p></p>
        </div>
    )
}

export default WorkViewProfile