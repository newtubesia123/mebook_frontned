import React from 'react'
import { useSelector } from 'react-redux'
import { useParams,useNavigate } from 'react-router-dom'
import { SERVER } from '../../server/server'
import { BackGround } from '../background'
import pic from '../../mepic.png';

const WorkViewTop5 = () => {
    const bg = { ...BackGround, paddingTop: '10px', height: '100vh' }
    const params = useParams();
    const navigate=useNavigate();
    const getImgURL = (name) => `${SERVER}/uploads/${name}#toolbar=0`
    const STORE = useSelector((state) => state)
    // if(!STORE.getProfileData){
    //     navigate(-1);
    // }

    // console.log(STORE)
    return (
        <div style={bg}>
            <div style={{display:'flex',flexDirection: 'column',alignItems:'center' }}>
            <embed src={getImgURL(STORE.userDashboardDetail?.userWorkDetail?.mostLiked?.userWork)} type="" height='500' width='auto' />
            
            </div>



        </div>
    )
}

export default WorkViewTop5