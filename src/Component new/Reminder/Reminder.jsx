import React, { useState } from 'react'
import ChatIcon from '@mui/icons-material/Chat';
import NavigationIcon from '@mui/icons-material/Navigation';
import Fab from '@mui/material/Fab';
import { Tooltip } from 'antd';
import './Reminder.css'
import { Box } from '@mui/material';
const Reminder = ({ getNow }) => {
    let time = new Date().toLocaleTimeString()

    const [ctime, setTime] = useState(time)
    const UpdateTime = () => {
        time = new Date().toLocaleTimeString()
        setTime(time)
    }
    setInterval(UpdateTime)

    return (
        <Box className='rmd-div' sx={{ display: { xs: 'none', sm: 'none', md: 'block' } }}>
            <div className='reminder-btn' >
                <Fab variant="extended" color="primary" aria-label="add" style={{ width: '100%' }}>
                    &nbsp; &nbsp; &nbsp;  Your 14 Days Trial Expiring Soon &nbsp; &nbsp; {getNow} &nbsp; Left
                </Fab>
            </div>
        </Box>
    )
}

export default Reminder