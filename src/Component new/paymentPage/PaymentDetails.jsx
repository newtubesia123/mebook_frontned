import React, { useState } from 'react'
import './allCss/PaymentDetails.css'
import image from '../../../src/america.png'
import image3 from '../../../src/whitepaypal.png'
import image2 from '../../../src/bankAmerica.png'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Button, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import SaveIcon from '@mui/icons-material/Save';
import dayjs from 'dayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { Navigate, useNavigate } from 'react-router'
import { BackGround } from '../background'


function PaymentDetails() {
    const navigate = useNavigate()
    let userName = JSON.parse(localStorage.getItem('userData'))

    let selectedCard = localStorage.getItem('cardContent')
    // console.log(JSON.parse(selectedCard))
    const style = {
        // CardImg: {
        //     position: 'absolute',
        //     left: '80px',
        //     top: '200px'
        // },
        PDMain: {
            height: '80vh',
            border: '1px solid rgb(121,9,22)',
            borderRadius: '20px',
            paddingX:"20px",
            background: JSON.parse(selectedCard).background1,
            background: JSON.parse(selectedCard).background2,
        },
        PDMain768: {
            height: '80vh',
            border: '1px solid rgb(121,9,22)',
            borderRadius: '20px',
            paddingX:"20px",
            // backgroundColor:'white',
            background: JSON.parse(selectedCard).background1,
            background: JSON.parse(selectedCard).background3,
        },
        cardName: JSON.parse(selectedCard).cardName
    }
    const [cardDetail, setCardDetails] = useState({
        cardNumber: 12222,
        cardName: "Athar",
        expiry: "2028/04",
        cvv:'',
    })
    const [APICalling, setAPICalling] = useState(false)
    const [card, setCard] = useState('')
    const [value, setValue] = React.useState(dayjs(Date.now()));

    const handleCardNumber = (e) => {
        if (card.split('   ').join('').length < 16) {
            setCard(e.target.value)
            setCardDetails({...cardDetail,cardNumber:e.target.value})
        }
    }
    const handleCardDisplay = () => {

        const rawText = [...card.split(' ').join('')] // Remove old space
        const creditCard = [] // Create card as array
        rawText.forEach((t, i) => {
            if (i % 4 === 0) creditCard.push('   ') // Add space
            creditCard.push(t)
        })
        return creditCard.join('') // Transform card array to string
    }
    const handleCardDetail = (e,name)=>{
        if(name=="cvv" && cardDetail.cvv.length<3){
            console.log('cvv')
            setCardDetails({...cardDetail,[name]:e}) 
        }
        if(name!="cvv"){
            setCardDetails({...cardDetail,[name]:e}) 
        }  
        
    }

    const dynamicImage = {
        "Debit Card": image2,
        "Credit Card": image,
        "Paypal": image3
    }
const callPayment =()=>{
    setAPICalling(true)
    let cardDea = {...cardDetail,cardNumber:card.split('   ').join('')}
    // console.log(cardDetail)
    console.log(cardDea)
    navigate(`/MainDashboard`)
}
    //   console.log(window.innerWidth)
    return (
        <div className='container'>
            <div className="row justify-content-end pt-5 mt-5" style={window.innerWidth>768?style.PDMain:style.PDMain768}>

                <img className='paymentCardImage'  src={dynamicImage[style.cardName]} alt="" width={300} height={200} />

                <div className="col-md-5 col-10 ">
                    <Typography sx={{ fontSize: '2.5rem' }}>
                        {style.cardName}
                    </Typography>

                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '80%' },
                        }}
                        noValidate
                        autoComplete="off"
                    >

                        <TextField fullWidth id="standard-basic" label="Card Holder Name" variant="standard" 
                        onChange={(e)=>handleCardDetail(e.target.value,"cardName")}
                        size="medium"
                            inputProps={{ style: { fontSize: 25 } }}
                            InputLabelProps={{ style: { fontSize: 20 } }}
                        />
                        <TextField fullWidth id="standard-basic" value={handleCardDisplay()} onChange={handleCardNumber} size="medium" label="Card Number" variant="standard"
                            inputProps={{ style: { fontSize: 25 } }}
                            InputLabelProps={{ style: { fontSize: 20 } }}
                            maxLength={12}
                        />
                        <div className='d-flex justify-content-between mt-3 mb-5'>
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                <DatePicker
                                    views={['year', 'month']}
                                    label="Expiration"
                                    minDate={dayjs('2022-03-01')}
                                    maxDate={dayjs('2029-06-01')}
                                    inputProps={{ style: { fontSize: 25 } }}
                                    InputLabelProps={{ style: { fontSize: 20 } }}
                                    value={value}
                                    onChange={(newValue) => {
                                        setCardDetails({ ...cardDetail, expiry: `${newValue.$y}/${newValue.$M + 1}` })
                                        setValue(newValue);
                                    }}
                                    renderInput={(params) => <TextField variant='standard'
                                        {...params}
                                        sx={{ width: "60%" }}
                                        // inputProps={{style: {fontSize: 25}}}
                                        // InputLabelProps={{style: {fontSize: 20}}} 
                                        helperText={null} />}
                                />
                            </LocalizationProvider>
                            <TextField type='password' id="standard-basic" label="CVV" variant="standard" 
                            value={cardDetail.cvv}
                            onChange={(e)=>handleCardDetail(e.target.value,"cvv")}
                            size="medium"
                                sx={{ width: "30%" }}
                                InputLabelProps={{ style: { fontSize: 20 } }}
                                inputProps={{ inputMode: 'numeric', pattern: '[0-9]*', style: { fontSize: 25 } }}
                            />
                        </div>


                        {
                            APICalling ?
                                <LoadingButton
                                    variant='contained'
                                    sx={{ backgroundColor: 'rgb(121,9,22)' }}
                                    loading
                                    loadingPosition="start"
                                    startIcon={<SaveIcon />}
                                >
                                    checking
                                </LoadingButton>
                                : <Button
                                    variant='contained'
                                    sx={{
                                        backgroundColor: 'black',
                                        '&:hover': {
                                            backgroundColor: 'rgb(121,9,22)'
                                        }
                                    }}
                                    onClick={() => {
                                        callPayment()
                                    }}>Pay Now</Button>
                        }



                    </Box>
                </div>
            </div>

        </div>
    )
}

export default PaymentDetails
