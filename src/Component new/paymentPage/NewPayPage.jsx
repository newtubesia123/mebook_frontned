import { Button } from '@mui/material'
import React from 'react'
import './NewPayPage.css'
import CheckIcon from '@mui/icons-material/Check';
import CancelIcon from '@mui/icons-material/Cancel';
import { useNavigate } from 'react-router-dom';
import { newButton } from '../background';
import { SERVER } from '../../server/server';
import { getAuth } from '../../Action/index'
import axios from 'axios';
import { toast } from 'react-toastify';
import MeLogo from '../../assets/MeLogoMain';
import PayCard from './PayCard';
import { EplanData, cardData, } from '../../Component new/User Dashboard/sideBarItem/SideBar'

export const Card = ({ item }) => (
    <div className="col-md-4" style={{ border: '2px solid white', margin: '20px', marginTop: '10px', color: 'white' }}>
        <span>{item?.logo}</span>
        <h5>{item?.title}</h5>
        <p style={{ fontSize: "75%" }}>{item?.content}</p>
    </div>
);

const List = ({ data }) => {
    return (
        <>
            <li style={{ display: 'flex', margin: '3px', fontSize: "75%" }}>
                <div>
                    {data.applied == true ?
                        <CheckIcon sx={{ color: 'white', background: 'green', marginRight: '5px', borderRadius: '50%', fontSize: "90%" }} />
                        :
                        <CancelIcon sx={{ color: 'white', background: 'red', marginRight: '5px', borderRadius: '50%', fontSize: "90%" }} />
                    }
                </div>
                <div>
                    {data.title}
                </div>
            </li>
        </>

    )
}

const NewPayPage = () => {
    const navigate = useNavigate();
    const saveBtn = { ...newButton, width: 350, height: 40, margin: '20%' };
    const planBtn = { ...newButton, width: 350, height: 40, marginBottom: '1rem' }

    function handlePayment(type) {
        const paymentApi = `${SERVER}/payment/checkout`
        const method = {
            1: "standard",
            2: "enhanced"
        }
        axios.post(paymentApi, { product: method[type] },
            {
                headers: { Authorization: `Bearer ${getAuth()}` }
            }).then((response) => {
                if (response.data.errorCode == 200) {
                    console.log("response", response?.data, response?.data?.sessionurl)
                    window.location.href = response?.data?.sessionurl
                } else {
                    toast.success(response.data?.messaage)
                    console.log("response", response)
                }
            }).catch((error) => {
                console.log(error)
            })
    }



    return (
        <div className=" container-fluid dashBody d-flex justify-content-center flex-column" style={{ background: 'black', color: 'white', padding: '0', minHeight: '100vh' }}>
            <div className='d-flex justify-content-center m-5'>
                <MeLogo />
            </div>

            <div className="row d-flex justify-content-center align-items-center my-5" >
                <div className="col-10 col-sm-10 col-md-10 col-lg-9 col-xl-9 ">
                    <span className="row d-flex justify-content-center align-items-center" style={{ border: '2px solid white' }}>
                        <span className="col-12 col-sm-10 col-md-10 col-lg-6  my-auto txt">
                            <span style={{ width: '40%', height: '100%', background: 'none', margin: '50px 15px' }}>
                                <h3>Choose Your MeBookMeta <br />
                                    Custom Payment Experience</h3>
                                <p style={{ fontSize: '100%' }}>Be a part of the platform that will revolutionize the
                                    Global Media Marketplace!</p>
                            </span>
                        </span>
                        <span className="btnnewpage col-12 col-sm-10 col-md-10 col-lg-6 d-flex justify-content-center align-items-center">
                            <Button className='col-12 col-sm-10 col-md-10 col-lg-10 col-xl-8'
                                variant="contained"
                                size="small"
                                sx={saveBtn}
                                onClick={() => window.scrollTo(0, document.body.scrollHeight)}

                            >
                                Begin Your Free Two-Week Trial
                                {/* <StartIcon sx={{ marginLeft: "1px" }} className="btnIcon" /> */}
                            </Button>
                        </span>
                    </span>
                </div>

            </div>


            {/* Section 2  */}
            <div className="col-md-10" style={{ margin: '0 auto', backgroundColor: 'transparent', padding: '0px 10px 0px 10px', color: 'white' }}>
                <h4 style={{ textAlign: 'center' }}>Features You’ll Enjoy with Your MeBookMeta Standard Profile</h4>
                <div className="row" style={{ display: 'flex', justifyContent: 'space-evenly', marginTop: '3rem' }}>
                    {cardData.map((item, index) => (

                        index < 4 ? (
                            <Card
                                key={index}
                                item={item}
                            />
                        )
                            : ""


                    ))}
                </div>
            </div>

            {/* section 3  */}
            <div className="col-md-10" style={{ margin: '0 auto', backgroundColor: 'transparent', marginBottom: '3rem', padding: '0px 10px 0px 10px', color: 'white', marginTop: '2rem' }}>
                {/* <h4 style={{ textAlign: 'center' }}>Features You’ll Enjoy with Your MeBookMeta Enhanced Profile</h4> */}
                <div className="row" style={{ display: 'flex', justifyContent: 'space-evenly', marginTop: '3rem' }}>
                    {cardData.map((item, index) => (

                        index >= 4 ? (
                            <Card
                                key={index}
                                item={item}
                            />
                        )
                            : ""


                    ))}
                </div>
            </div>
            {/* Payment Cards  */}

            <div className="col-md-12 pay_card_div " >
                <PayCard data={EplanData} onClick={() => handlePayment(2)} />
            </div>


        </div>
    )
}


export default NewPayPage