import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link, useNavigate } from "react-router-dom";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
// import CommonAppbar from "../Component new/CommonAppbar";
import WriterNav from "../../Component new/WriterNav";
import { BackGround, newButton } from "../background";

export default function BankSelect() {
  const obj = {
    MainStage: "/MainStage",
    CuttingEdge: "/CuttingEdge",
    Specialist: "/Specialist",
    MultiDiscipline: "/MultiDiscipline",
    Organic: "/Organic",
    AtLarge: "/AtLarge",
  };
  const navigate = useNavigate();
  const [checkValue, setCheckValue] = useState("");

  const handleChange = (val) => {
    setCheckValue(val.target.id);
  };
  useEffect(() => {
    console.log("checkValue", checkValue);
  }, [checkValue]);
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="container-fluid" style={BackGround}>
      <WriterNav />
      <div className="row">
        <div className=" col-md-6 offset-md-3">
          {/* <Card
            sx={{
              backgroundColor: "transparent",
              borderRadius: "30px",
              textAlign: "center",
              m:'10px auto',
              width:'80%',
            }}
          >
            <CardContent>
              <div>
              
            </CardContent>
          </Card> */}
          
          <div
            className="head"
            style={{
              margin: "15px 0px",
              fontSize: "2.5vw",
              fontWeight: "bold",
              color: "#100892",
              textAlign: "center",
              color: "black",
            }}
          >
            {/* <span style={{  fontFamily: "Times New Roman", }}>M</span><span style={{  }}>eBook</span><span style={{  }}>M</span><span style={{  }}>eta</span> Profile: Identity */}
            <p>Select Bank</p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <button
            type="button"
            class="button"
            id="MainStage"
            style={{
              width: "75%",
              fontSize: "20px",
              margin: "15px 13% 15px ",
              borderRadius: "10px",
              padding: "10px 0px",
              fontWeight: "bold",
              fontFamily: "Times New Roman",
             
              opacity: 0.85
            }}
            onClick={handleChange}
            // value="MainStage"
          >
            JPMorgan Chase
          </button>
          <button
            type="button"
            class="button"
            id="CuttingEdge"
            style={{
              width: "75%",
              fontSize: "20px",
              color: "black",
              margin: "15px 13% 15px ",
              borderRadius: "10px",
              padding: '10px 0px',
              fontWeight: 'bold',
              
              opacity: 0.85
            }}
            onClick={handleChange}
            value="CuttingEdge"
          >
            Bank of America
          </button>
          <button
            type="button"
            class="button"
            id="Specialist"
            style={{
              width: "75%",
              fontSize: "20px",
              color: "black",
              margin: "15px 13% 15px ",
              borderRadius: "10px",
              padding: '10px 0px',
              fontWeight: 'bold',
            
              opacity: 0.85
            }}
            onClick={handleChange}
            value="Specialist"
          >
            Wells Fargo & Co.
          </button>
          <button
            type="button"
            class="button"
            id="MultiDiscipline"
            style={{
              width: "75%",
              fontSize: "20px",
             
              margin: "15px 13% 15px ",
              borderRadius: "10px",
              padding: '10px 0px',
              fontWeight: 'bold',
             
              opacity: 0.85
            }}
            onClick={handleChange}
            value="MultiDiscipline"
          >
            Citigroup
          </button>
          <button
            type="button"
            class="button"
            id="Organic"
            style={{
              width: "75%",
              fontSize: "20px",
              color: "black",
              margin: "15px 13% 15px ",
              borderRadius: "10px",
              padding: "10px 0px",
              fontWeight: "bold",
            
              opacity: 0.85,
            }}
            onClick={handleChange}
            value="Organic"
          >
            U.S. Bancorp
          </button>
          <button
            type="button"
            class="button"
            id="AtLarge"
            style={{
              width: "75%",
              fontSize: "20px",
              color: "black",
              margin: "15px 13% 15px ",
              borderRadius: "10px",
              padding: '10px 0px',
              fontWeight: 'bold',
            
              opacity: 0.85
            }}
            onClick={handleChange}
            value="AtLarge"
          >
            Capital One
          </button>

          <div className="d-flex justify-content-between mt-3 mb-3">
            <Link to={-1}>
              {" "}
              <button
                type="button"
                class="btn btn-lg"
                style={{ background: "white" }}
              >
                Go Back
              </button>
            </Link>
            <Link to={`/PaymentPage/BankSelect/PaymentDetails`}>
              {" "}
              <button
                type="button"
                class="btn  btn-lg"
                style={{ background: "white" }}
              >
                Continue
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
