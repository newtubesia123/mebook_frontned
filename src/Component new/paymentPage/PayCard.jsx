import React, { useState } from 'react'
import './PayCard.css'
import CheckIcon from '@mui/icons-material/Check';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { callPopUp } from '../../Action';
import { useDispatch } from 'react-redux';
const PayCard = ({ data, onClick }) => {
  const [checked, setChecked] = useState(false);
  const navigate = useNavigate()
const dispatch=useDispatch()
  const handleChange = (event) => {
    setChecked(event.target.checked);
  };
  
  const handlePayment=()=>{
    if (checked){
      dispatch(callPopUp({type:"CouponCheck"}))
    }else{
      toast.error('Please agree to terms and conditions', { position: 'top-right', autoClose: 2000, pauseOnHover: false })
    }
  }
  // dispatch(callPopUp({type:"CouponCheck"}))
  return (
    <>
      <section class="section-plans" id="section-plans">
        <div class="u-center-text u-margin-bottom-big">
          {/* <h2 class="heading-secondary">
            Premium Plan
          </h2> */}
        </div>
        <div class="pay_row">
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front-2">
                <div class="card__title card__title--2">
                  <h4 class="card__heading">Standard</h4>
                  <h4 class="card__heading__dol"><span>$</span>35/month</h4>
                </div>
                <div class="card__details">
                  <div className='card__details__inner' >
                    {
                      data.map((item, index) => (index <= 10 ?
                        <div key={index} style={{ display: 'flex', margin: '3px 0' }}>
                          <div style={{}}><CheckIcon sx={{ color: 'white', background: 'green', marginRight: '5px', borderRadius: '50%', fontSize: '1.5rem' }} /></div>
                          <h6>{item.title}</h6>
                        </div> : ''
                      ))
                    }
                  </div>
                  <div className='card__details__inner' >
                    {
                      data.map((item, index) => (index > 10 ?
                        <div key={index} style={{ display: 'flex', margin: '3px 0' }}>
                          <div style={{}}><CheckIcon sx={{ color: 'white', background: 'green', marginRight: '5px', borderRadius: '50%', fontSize: '1.5rem' }} /></div>
                          <h6>{item.title}</h6>
                        </div> : ''
                      ))
                    }
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-2">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">Only</p>
                    <p class="card__price-value">$35/month</p>
                  </div>
                  <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                    <div style={{ marginBottom: '1rem', maxWidth: '', maxHeight: '2rem', display: 'flex', justifyContent: 'space-evenly', flexDirection: 'row', alignItems: 'center' }}>
                      <input
                        type="checkbox"
                        checked={checked}
                        onChange={handleChange}
                        style={{ width: '25px', height: '25px', marginRight: '.5rem' }}
                      />
                      <h6 style={{ margin: 0, color: 'white' }}>Agree <span style={{ color: 'blue', cursor: 'pointer' }}
                        onClick={() => navigate('/privacypolicy')}
                      >
                        Terms & Conditions</span></h6>
                    </div>
                    <div class="u-center-text u-margin-top-huge">
                      {/* <button onClick={checked ? (onClick) : throwError} class="butn butn--white">Choose Plan</button> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
          <div style={{ marginBottom: '1rem', maxWidth: '', maxHeight: '2rem', display: 'flex', justifyContent: 'space-evenly', flexDirection: 'row', alignItems: 'center' }}>
            <input
              type="checkbox"
              checked={checked}
              onChange={handleChange}
              style={{ width: '25px', height: '25px', marginRight: '.5rem' }}
            />
            <h6
              style={{ margin: 0, color: 'white' }}
              onClick={() => navigate('/privacypolicy')}
            >
              Agree
              <span style={{ color: 'blue', cursor: 'pointer' }}> Terms & Conditions</span>
            </h6>
          </div>
          <div class="u-center-text u-margin-top-huge">

            {/* <button onClick={checked ? (onClick) : throwError} class="butn butn--green">Choose Plan</button> */}
            <button onClick={handlePayment} class="butn butn--green">Choose Plan</button>
            {/* dispatch(callPopUp({type:"login"}))}  */}
          </div>
        </div>

        <div>
        </div>
      </section>

    </>
  )
}

export default PayCard

// isCheckedCopyright
