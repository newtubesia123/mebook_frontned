import WriterNav from "./WriterNav";
import React, { useState, useEffect } from "react";
import pic from "../mepic.png";
import { Link, useNavigate, useLocation } from "react-router-dom";
import ImportContactsOutlinedIcon from "@mui/icons-material/ImportContactsOutlined";
import SaveAsOutlinedIcon from "@mui/icons-material/SaveAsOutlined";
import CreateIcon from '@mui/icons-material/Create';
import { Button } from "@mui/material";
import { BackGround, newButton } from "./background";


export default function UserChoice() {
  const navigate = useNavigate();
  
  const bg ={...BackGround, minHeight:"100vh", minWidth:"100wh"}
  const bgBut = {...newButton}

  const styleSheet = {
    fontSize: "30px",
    fontWeight: "bold",
    color:"white"
  };
  // const handleChoose = (val) => {

  //
  // }
  const [checkValue, setCheckValue] = useState([]);
  const handleChoose = (val) => {
    let temp = checkValue;
    if (temp.some((item) => item.id === val.id)) {
      temp = temp.filter((item) => item.id !== val.id);
    } else {
      temp.push(val);
    }
    setCheckValue(temp);
    if (checkValue == "Creator" || checkValue == "Both") {
      navigate("/CreatorNew");
    } else if (checkValue == "Reader") {
      navigate("/ReaderProfile");
    }
    sessionStorage.setItem("option", checkValue);
  };
  const handleBack = () => {
    navigate(-1);
  };
  console.log("first", checkValue);
  let creator = "Creator";
  let Reader = "Reader";
  return (
    <div className="container-fluid text-center" style={bg}>
      <WriterNav />
      <div className="row">
        <div className="col-md-12 my-3">
          <img src={pic} width={180} height={100} alt="" />
          <h2 style={styleSheet}>Your Identity Profile Has Been Saved!</h2>
          <p
            style={{
              color: "white",
              fontSize: "35px",
              fontWeight: "bold",
              margin: "30px 0px 30px",
            }}
          >
            Set Up Your <br />{" "}
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#100892" }}>eBook</span>
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#100892" }}>eta</span>{" "}
          </p>
          <p style={{ fontWeight: "bold", fontSize: "60px", color:"white" }}> Work Profile</p>
          <h3>
            <b style={{color:"white"}}>
              To get the best Mebook Meta experience,we want to know a bit about
              you
            </b>
          </h3>
          <div
            className="d-flex justify-content-center"
            style={{ margin: "10vh 0vh 10vh" }}
          >
            <div className="col-md-3 ">
              <CreateIcon
                id="creator"
                className="icon"
                value={checkValue}
                onClick={() => handleChoose(creator)}
                sx={{ fontSize: "5rem", color: "white" }}
              />
              <h5 className="">
                {" "}
                <b style={{color:"white"}}> Creator/Artist/Industry</b>
              </h5>
            </div>
            <div className="col-md-3 ">
              <ImportContactsOutlinedIcon
                id="reader"
                className="icon"
                value={checkValue}
                onClick={() => handleChoose(Reader)}
                sx={{ fontSize: "5rem", color: "white" }}
              />{" "}
              <h5 className="">
                {" "}
                <b style={{color:"white"}}> Reader/Consumer</b>
              </h5>
            </div>
            <div className="col-md-3 ">
              <SaveAsOutlinedIcon
                id="both"
                className="icon"
                value={checkValue}
                onClick={() => handleChoose(creator)}
                sx={{ fontSize: "5rem", color: "white" }}
              />{" "}
              <h5 className="">
                {" "}
                <b style={{color:"white"}}> Both</b>
              </h5>
            </div>
          </div>

          <div
            className=" d-flex justify-content-center"
            style={{ margin: "" }}
          >
            <div className="col-md-3">
              <Button
              variant="contained" size="large"
                sx={bgBut}
                onClick={handleBack}
              >
                Go Back
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
