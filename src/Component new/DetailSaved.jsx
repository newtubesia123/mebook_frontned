import React, { useEffect } from "react";
import pic from '../mepic.png';
import { useNavigate } from "react-router-dom";
import WriterNav from "./WriterNav";
import { BackGround, newButton } from "./background";
import { useDispatch, useSelector } from "react-redux";
import { getUserData } from "../Action";
import Button from "@mui/material/Button";
import MeLogo from "../assets/MeLogoMain";


export default function DetailSaved() {
    const styleSheet = {
        fontSize: '35px',
        fontWeight: "bold",
        fontFamily: "Times New Roman",
        margin: '30px 0px',

    }
    // const [bg,setBg] = useState("")
    const bg = { ...BackGround, minHeight: "100vh" }
    const navigate = useNavigate();
    const dispatch = useDispatch()
    const STORE = useSelector((state) => state)
    useEffect(() => {
        window.scrollTo(0, 0)
        dispatch(getUserData())
    },[])

    if (STORE.errorPage) {
        navigate('/ErrorPage')
    }

   
    let user = localStorage.getItem('userData')
    let userName = sessionStorage.getItem('userName')

    return (
        <div className='container-fluid text-center ' style={bg}>
            <div className="row " >
                <div className="col-md-12 mt-2">
                    {/* <img src={pic}  alt="" style={{width:'15%',height:'25%', marginTop:"1rem"}} /> */}
                    <MeLogo/>
                    <h2 style={{ margin: '10px auto', color: "white", fontSize:"150%" }}><b>Hi, {userName}</b></h2>

                    <h2 className="" style={{ color: "white",fontSize:"150%"}}>Your Personal Profile Has Been Saved!</h2>
                    <p style={{
                        color: "white", fontSize:"150%",
                        fontWeight: "bold",
                        margin: '10px 0px 10px'
                    }}>Set Up Your <br /> <span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#100892' }}>eBook</span><span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#100892' }}>eta</span> </p>
                    <p style={{ fontWeight: "bold", fontSize:"225%", color: 'white' }}> Identity Profile</p>
                    <div className=" d-flex justify-content-around " style={{ marginBottom: "" }}>
                        <Button
                            variant="contained" size="small"
                            
                            sx={{...newButton, width:{xs:"100%", sm:"25%", md:"25%", lg:"25%", xl:"25%"}}}
                        // onClick={()=>navigate('/CreatorNew1')}
                        onClick={()=>navigate('/Identity')}

                        >
                            Continue
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    )
}
