
import React from 'react'
import pic from '../mepic.png';
import { useNavigate, } from "react-router-dom";
import { BackGround, newButton } from './background';
import Button from "@mui/material/Button";
import MeLogo from '../assets/MeLogoMain';

export default function CompletionPhase() {

    const background = { ...BackGround, minHeight: '100vh' }
    const navigate = useNavigate();
    let userName = JSON.parse(localStorage.getItem('userData'))
    // console.log('id', userName._id)

    // let val = JSON.parse(localStorage.getItem('MultiOptions'))
    // console.log('MultiOptions', val)
    const styleSheet = {
        fontSize: '200%',
        fontWeight: "bold",
        fontFamily: "Times New Roman",
        margin: '80px 0px',
        color: "white"

    }
    // const handleNavigate = () => {

    //     navigate(-1)

    // }

    return (
        <div className='container-fluid text-center' style={background} >
            {/* <CommonAppbar /> */}
            <div className="row">
                <div className="col-md-12 ">
                    {/* <img src={pic} alt="" style={{ width: "10rem", marginTop: "1rem" }} /> */}
                    <MeLogo/>
                    <h2 style={styleSheet}>Your MeBookMeta Profile Has Been Saved!</h2>
                    <Button
                        size="small" variant="contained"
                        sx={{...newButton, width:{xs:"100%", sm:"50%", lg:"25%", xl:"25%"}}}
                        onClick={() => navigate('/NewPayPage')}
                    >
                        Continue
                    </Button>
                </div>
            </div>
        </div>
    )
}
