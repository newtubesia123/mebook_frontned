import React, { useState } from "react";
import Typography from "@mui/material/Typography";

import { styles } from './styles'

const Avatar = props => {
    const [hovered, setHovered] = useState(false)

    return (
        <div style={props.style}>
            {hovered && (

                <div
                    className='transition-3'
                    style={{
                        ...styles.avatarHello,

                    }}
                >
                    Hey it's Ron 🤙
                </div>
            )}

            <div
                onMouseEnter={() => setHovered(true)}
                onMouseLeave={() => setHovered(false)}
                onClick={() => props.onClick && props.onClick()}
                className='transition-3'
                style={{
                    ...styles.chatWithMeButton,
                    ...{ borderz: hovered ? '3px solid #00E7FF' : '1px solid  #00E7FF' }
                }}
            />
            <Typography color='white' variant=''>
                <b>Help Desk </b>
            </Typography>
        </div>
    )
}

export default Avatar;