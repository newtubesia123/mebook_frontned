export const styles = {
    chatWithMeButton: {
        cursor: 'pointer',
        boxShadow: '0px 0px 16px 6px rgba(0, 0, 0, 0.33)',
        // Border
        borderRadius: '50%',
        // Background 
        // backgroundImage: `url(https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png)`, 
        backgroundImage: 'linear-gradient(90deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: '84px',
        // Size
        width: '60px',
        height: '60px',
    },
    
    avatarHello: { 
        // Position
        position: 'absolute', 
        left: 'calc(-100% - 44px - 28px)', 
        top: 'calc(50% - 24px)', 
        // Layering
        zIndex: '10000',
        boxShadow: '0px 0px 16px 6px rgba(0, 0, 0, 0.33)',
        // Border
        padding: '12px 12px 12px 16px',
        borderRadius: '24px', 
        // Color
        backgroundColor: '#f9f0ff',
        color: 'black',
    },
    supportWindow: {
        // Position
        position: 'fixed',
        bottom: '116px',
        right: '24px',
        // Size
        width: '400px',
        height: '450px',
        maxWidth: 'calc(100% - 48px)',
        maxHeight: 'calc(100% - 48px)',
        backgroundColor: 'black',
        // Border
        borderRadius: '12px',
        border: `2px solid #181823`,
        // overflow: 'hidden',
        // Shadow
        boxShadow: '0px 0px 16px 6px rgba(0, 0, 0, 0.33)',
    },
    emailFormWindow: { 
        width: '100%',  
        overflow: 'hidden',
        transition: "all 0.5s ease",
        WebkitTransition: "all 0.5s ease",
        MozTransition: "all 0.5s ease",
    },
    stripe: {
        position: 'relative',
        top: '-45px',
        width: '100%',
        height: '308px',
        backgroundColor: `linear-gradient(rgba(0,0,0,0.9),rgba(255,0,0,1))`,
        transform: 'skewY(-12deg)',
    },
    topText: { 
        position: 'relative',
        width: '100%', 
        top: '6%', 
        color: 'white', 
        fontSize: '16px', 
        fontWeight: '600',
        padding:'10px'
    },
    emailInput: { 
        width: '66%',
        textAlign: 'center',
        outline: 'none',
        padding: '10px',
        borderRadius: '12px',
        border: '2px solid #7a39e0',
    },
    userText :{
        borderRadius: '12px',
        border: '2px solid #7a39e0',
    },
    btn :{
        borderRadius: '10px',
        backgroundColor: '#7a39e0',
    },
    btnPosition :{
        position: 'absolute', 
        top: '350',
        right:'1%'
    },
    bottomText: { 
        position: 'absolute', 
        width: '100%', 
        top: '115%', 
        color: '#7a39e0', 
        // fontSize: '24px', 
        fontWeight: '600'
        },
    loadingDiv: { 
        position: 'absolute', 
        height: '100%', 
        width: '100%', 
        textAlign: 'center', 
        backgroundColor: 'white',
    },
    loadingIcon: { 
        color: '#00E7FF', 
        position: 'absolute', 
        top: 'calc(50% - 51px)', 
        left: 'calc(50% - 51px)',  
        fontWeight: '600',
    },
    chatEngineWindow: {
        width: '100%',  
        backgroundColor: '#fff',
    }
}