import React, { useEffect, useState } from "react"

import { styles } from "./styles"

import axios from 'axios'
import Button from "@mui/material/Button";
import { toast } from "react-toastify";
import { LoadingOutlined } from '@ant-design/icons'
import { getAuth, getUserData, getUserId } from "../../Action";
import Avatar from './Avatar'
import SendIcon from '@mui/icons-material/Send';
import { SERVER } from "../../server/server";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { newButton } from "../background";
const EmailForm = props => {
    const [help, setHelp] = useState('')
    const [loading, setLoading] = useState(false)
    const STORE = useSelector((state) => state);
    console.log('STORE',STORE)
    const dispatch = useDispatch();
    const navigate = useNavigate();
    // function getOrCreateUser(callback) {
    //     axios.put(
    //         'https://api.chatengine.io/users/',
    //         { username: email, email: email, secret: email },
    //         { headers: { "Private-Key": process.env.REACT_APP_PRIVATE_KEY } }
    //     )
    //         .then(r => callback(r.data))
    //         .catch(e => console.log('Get or create user error', e))
    // }

    // function getOrCreateChat(callback) {
    //     axios.put(
    //         'https://api.chatengine.io/chats/',
    //         { usernames: [email, 'Adam La Morre'], is_direct_chat: true },
    //         {
    //             headers: {
    //                 "Project-ID": process.env.REACT_APP_PROJECT_ID,
    //                 "User-Name": email,
    //                 "User-Secret": email,
    //             }
    //         }
    //     )
    //         .then(r => callback(r.data))
    //         .catch(e => console.log('Get or create chat error', e))
    // }
    useEffect(() => {
        dispatch(getUserData())
    }, [])
    console.log('e.target.help',help,STORE?.getProfileData?.userData?.email)
    const handleSubmit = async (event) => {
        event.preventDefault();
        setLoading(true)
        if (getAuth()) {
            axios.post(`${SERVER}/supportEngine/${await getUserId()}`, { data: help, mail: STORE?.getProfileData?.userData?.email })
                .then((res) => {
                    toast.success(res.data.message, {
                        position: 'top-center',
                        autoClose: 2000,
                        pauseOnHover: false
                    })
                    setLoading(false)
                }).catch((e) => {
                    console.log(e)
                })

        }
        else {
            toast.success('Please Sign In first', {
                position: 'top-center',
                autoClose: 2000,
                pauseOnHover: false
            })
            setLoading(false)

        }
        // console.log('Sending Email', email)

        // getOrCreateUser(
        //     user => {
        //         props.setUser && props.setUser(user)
        //         getOrCreateChat(chat => {
        //             // setLoading(false)
        //             props.setChat && props.setChat(chat)
        //         })
        //     }
        // )
    }

    return (
        <div
            style={{
                ...styles.emailFormWindow,
                ...{
                    height: props.visible ? '100%' : '0px',
                    opacity: props.visible ? '1' : '0'
                }
            }}
        >
            <div style={{ height: '0px' }}>
                <div style={styles.stripe} />
            </div>

            <div
                className='transition-5'
                style={{
                    ...styles.loadingDiv,
                    ...{
                        zIndex: loading ? '10' : '-1',
                        opacity: loading ? '0.33' : '0',
                    }
                }}
            />
            <LoadingOutlined
                className='transition-5'
                style={{
                    ...styles.loadingIcon,
                    ...{
                        zIndex: loading ? '10' : '-1',
                        opacity: loading ? '1' : '0',
                        fontSize: '82px',
                        top: 'calc(50% - 41px)',
                        left: 'calc(50% - 41px)',
                    }
                }}
            />

            <div style={{ position: 'absolute', height: '100%', width: '100%', textAlign: 'center' }}>
                {/* <Avatar
                    style={{
                        position: 'relative',
                        left: 'calc(50% - 44px)',
                        top: '5%',
                    }}
                /> */}
                <div
                    className='transition-3 mx-auto mt-2'
                    style={{
                        ...styles.chatWithMeButton
                        // ...{ border: hovered ? '2px solid #f9f0ff' : '4px solid  white' }
                    }}
                />

                <div style={styles.topText}>
                    Hi, This is Ron from MebookMeta support team. Can you please explain me what is the issue your facing right now ? I will try to resolve it as soon as possible. 👋
                </div>

                <form
                    onSubmit={e => handleSubmit(e)}
                    style={{ position: 'relative', width: '100%', top: '10%' }}
                >
                    {/* <input
                        placeholder='Your Email'
                        value={STORE?.getProfileData?.userData?.email}
                        // onChange={e => setEmail(e.target.value)}
                        style={styles.emailInput}
                    /> */}


                    <div style={styles.bottomText}>

                        <textarea style={styles.userText} onChange={e => setHelp(e.target.value)} name="" id="" cols="29" rows="6" placeholder="Enter your text here"></textarea>
                    </div>
                    <div style={styles.btnPosition}>
                        <Button type='submit' sx={{background: 'linear-gradient(90deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)', border:"none"}} variant='contained'><SendIcon sx={{color:"#18123"}} /></Button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default EmailForm;