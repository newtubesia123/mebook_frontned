import React, { useState, useEffect } from "react";
import axios from "axios";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MenuIcon from '@mui/icons-material/Menu';
import Pic from '../Screenshot_2.png'
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import PersonAdd from '@mui/icons-material/PersonAdd';
import Settings from '@mui/icons-material/Settings';
import Logout from '@mui/icons-material/Logout';
import TimelineIcon from '@mui/icons-material/Timeline';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { deepOrange, deepPurple } from '@mui/material/colors';
import Fab from '@mui/material/Fab';
import { useNavigate } from "react-router-dom";


export default function CommonAppbar({ name }) {
    const navigate = useNavigate();
    let userName = JSON.parse(localStorage.getItem('userData'))
    // console.log('first', userName.name)
    let option = sessionStorage.getItem('option')
    // let userName = sessionStorage.getItem('userName')
    const auth = localStorage.getItem("token");
    const logOut = () => {
        localStorage.clear(auth);
        navigate('/SignIn')
    }
    let letter = userName.name.substring(0, 1) || name.substring(0, 1)

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleStory = () => {
        navigate('/WriterStory')
    }
    const handleCreate = () => {
        navigate('/WriterStoryDetails')
    }
    const [anchorProfile, setAnchorProfile] = React.useState(null);
    const openProfile = Boolean(anchorProfile);
    const handleClickProfile = (event) => {
        setAnchorProfile(event.currentTarget);
    };
    const handleCloseProfile = () => {
        setAnchorProfile(null);
    };
    const [imageSrc, setImageSrc] = useState("")
    const [userInfo, setuserInfo] = useState({
        pdf: "",
        file: "",
        filepreview: null,
    });
    useEffect(() => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios
            .get("http://54.246.61.54:3002/getuserById", {
                headers: { Authorization: `Bearer ${token}` },
            })

            .then((res) => {
                console.log("res.data---> ", res.data);
                setImageSrc(res.data.userData.pdf)

                if (res.data) {
                    setuserInfo({
                        ...userInfo,
                        file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
                        filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
                    });
                }
            })
            .catch((err) => console.log(err));
    }, []);
    const HandleProfile = () => {
        navigate(`/UserProfile/${userName.name.toLowerCase() || name.toLowerCase()}`)
        console.log('clg',)
    }

    return (
        <div>
            <Box sx={{ flexGrow: 1, mb: 8 }}>
                <AppBar elevation position="fixed" sx={{ backgroundColor: 'white', borderBottom: " 1px solid lightgray" }}>
                    <Toolbar>
                        <IconButton
                            size="large"
                            edge="start"
                            // color="white"
                            aria-label="menu"
                            sx={{ mr: 2 }}
                        >
                            <img src={Pic} width="200px" alt="" />
                        </IconButton>
                    
                        <Box sx={{ ml: "auto" }}>
                            <Button startIcon={<TimelineIcon />} sx={{ backgroundColor: '#2a1c60', borderRadius: '70px', width: '160px', mx: '5px' }} variant='contained'>try premium</Button>
                        </Box>
                        <Tooltip >
                            <Button
                                onClick={handleClickProfile}

                                size="large"
                                sx={{ fontWeight: 'bolder', width: '255px' }}
                                aria-controls={openProfile ? 'demo-positioned-menu' : undefined}
                                aria-haspopup="true"
                                aria-expanded={openProfile ? 'true' : undefined}
                                endIcon={<ArrowDropDownIcon />}

                            >
                                {imageSrc == null && userInfo.file.type == null ? (

                                    <Avatar className="img1" sx={{ bgcolor: deepPurple[500], mr: 1 }}>
                                        {letter}
                                    </Avatar>)

                                    : (<img
                                        src={userInfo.filepreview}
                                        alt=""
                                        style={{
                                            width: "50px",
                                            height: "50px",
                                            borderRadius: "50%",
                                            marginRight: 2
                                        }}
                                    />)}
                                {userName.name || name}
                            </Button>
                        </Tooltip>
                        <Menu
                            anchorProfile={anchorProfile}
                            id="demo-positioned-menu"
                            aria-labelledby="demo-positioned-button"
                            open={openProfile}
                            onClose={handleCloseProfile}
                            onClick={handleCloseProfile}
                            PaperProps={{
                                elevation: 0,
                                sx: {
                                    overflow: 'visible',
                                    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',

                                    '& .MuiAvatar-root': {
                                        width: 32,
                                        height: 32,
                                        ml: -0.5,
                                        mr: 1,
                                    },
                                    '&:before': {
                                        content: '""',
                                        display: 'block',
                                        position: 'absolute',
                                        top: 0,
                                        right: 14,
                                        width: 10,
                                        height: 10,
                                        bgcolor: 'background.paper',
                                        transform: 'translateY(-50%) rotate(45deg)',
                                        zIndex: 0,
                                    },
                                },
                            }}
                            transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                            anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                        >
                            <MenuItem >
                                <Avatar /> Profile
                            </MenuItem>
                            <MenuItem>
                                <Avatar /> My account
                            </MenuItem>
                            <Divider />
                            <MenuItem>
                                <ListItemIcon>
                                    <PersonAdd fontSize="small" />
                                </ListItemIcon>
                                Add another account
                            </MenuItem>
                            <MenuItem>
                                <ListItemIcon>
                                    <Settings fontSize="small" />
                                </ListItemIcon>
                                Settings
                            </MenuItem>
                            <MenuItem >
                                <ListItemIcon >
                                    <Logout fontSize="small" />
                                </ListItemIcon>
                                Logout
                            </MenuItem>
                        </Menu>
                        <Fab size="medium" color="primary" aria-label="add" sx={{ backgroundColor: '#2a1c60' }}>
                            Help
                        </Fab>


                    </Toolbar>
                </AppBar>
            </Box>
        </div>
    )
}
