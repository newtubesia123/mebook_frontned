import React, { useState } from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import image from "../mepic.png";
import { useNavigate } from "react-router-dom";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";
import { useDispatch, useSelector } from "react-redux";
import { BackGround, newButton } from "./background";
import { SERVER } from "../server/server";
import { toast } from "react-toastify";
import {
  getMusicSubCategory,
  getUserData,
  userDashboardDetail,
  userProfile,
} from "../Action";

export default function SignIn() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const STORE = useSelector((state) => state);
  const signInBack = { ...BackGround, minHeight: "100vh",};
  if (STORE.errorPage) {
    navigate("/ErrorPage");
  }

  const [passwordMatch, setpasswordMatch] = React.useState(false);
  const [findUser, setFindUser] = React.useState(false);

  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
  });
  // useEffect(() => {
  //   if (STORE?.getProfileData?.userData?.homeTown && STORE?.getProfileData?.userData?.metaProfile) {
  //     toast.error("unexpected access", {
  //       position: 'top-center',
  //       autoClose: 500,
  //     })
  //     navigate('/MainDashboard')
  //   }
  // }, [])
  const handleChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch({ type: "CALLING__PROGRESS", payload: 10 });

    const LoginData = {
      email: values.email,
      password: values.password,
    };
    await axios
      .post(`${SERVER}/login`, { LoginData: LoginData })
      .then((res) => {
        dispatch({ type: "COMPLETE__PROGRESS", payload: 100 });
        dispatch({ type: "NO_ERROR" });
        if (res.data.data?.isVerified === true) {
          toast.success(res.data.success, {
            position: "top-center",
            autoClose: 500,
          });
          if (res.data.data.metaProfile && res.data.data.identityProfile) {
            dispatch(userDashboardDetail(res.data.token));
            dispatch(userProfile(res.data.data._id));
            dispatch(getUserData(res.data.token));
            dispatch({ type: "GET_DATA", payload: res.data });
            setTimeout(() => {
              navigate("/MainDashboard");
            }, 500);
          } else {
            dispatch(getMusicSubCategory("Music"));
            navigate("/ProfileDetails");
          }

          localStorage.setItem("token", JSON.stringify(res.data.token));
          localStorage.setItem("userData", JSON.stringify(res.data.data));
          sessionStorage.setItem("userName", res.data.data.name);
        } else if (res.data.data?.isVerified === false) {
          // alert('Please Verified Your Email id Once')
          toast.success("Please Verify Your Email id Once", {
            position: "top-center",
            autoClose: 3000,
            pauseOnHover: false,
          });
          navigate(`/ConfirmMail/${LoginData.email}`);
        } else if (res.data?.error) {
          toast.error(res.data.error, {
            position: "top-right",
            autoClose: 1000,
          });
        } else if (res.data?.Error) {
          toast.error(res.data.Error, {
            position: "top-right",
            autoClose: 1000,
          });
        }
      })
      .catch((err) => {
        dispatch({ type: "GOT_ERROR" });
      });
  };
  return (
    <div className="container-fluid">
      
      <div className=" row text-center" style={signInBack}>
      <span className='px-3 pt-3'>
        <Button sx={{...newButton, fontSize: "75%" }} onClick={() => navigate(-1)} variant="contained">Go Back</Button>
    </span>
        <Collapse in={passwordMatch} sx={{ position: "absolute", right: "0%" }}>
          <Alert
            severity="error"
            variant="filled"
            onClose={() => setpasswordMatch(false)}
          >
            Alert ! password not matched
          </Alert>
        </Collapse>
        <Collapse in={findUser} sx={{ position: "absolute", right: "0%" }}>
          <Alert
            severity="error"
            variant="filled"
            onClose={() => setFindUser(false)}
          >
            User Not Found
          </Alert>
        </Collapse>
        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 2 }} sx={{margin:"10px",}}>
          <Grid item xs></Grid>

          <Grid item>
            <div
              className="mt-3 tagname"
              style={{ fontFamily: "Times New Roman", fontSize: "3rem" }}
            >
              <span style={{ color: "rgb(187 11 11)" }}>M</span>
              <span style={{ color: "#0a0a8a" }}>eBook</span>
              <span style={{ color: "rgb(187 11 11)" }}>M</span>
              <span style={{ color: "#0a0a8a" }}>eta</span>
            </div>
            <h5
              style={{
                fontFamily: "Times New Roman",
                fontWeight: "bold",
                color: "white", 
                fontSize: "1.5rem"
              }}
            >
              Welcome to the Global Media Marketplace
            </h5>
            <div style={{ margin: "10px 0px" }}>
              <img
                src={image}
                alt=""
                style={{
                  width: "30%",
                  height: "20%",
                  margin: "10px 0px",
                }}
              />
            </div>
            <form onSubmit={handleSubmit} style={{display:"flex", flexDirection:"column", justifyContent:"center", alignItems:"center"}}>
              <div class="form-group">
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputPassword1"
                  placeholder="Enter your Email "
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  style={{height:"2rem", fontSize:"1rem", width:"18rem", textAlign:"center"}}
                />
              </div>
              <div class="form-group">
                <input
                  type="password"
                  class="form-control"
                  id="exampleInputPassword1"
                  placeholder="Enter your Password "
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  style={{height:"2rem", fontSize:"1rem", width:"18rem", textAlign:"center"}}
                />
              </div>

              <span
                style={{
                  color: "white",
                  cursor: "pointer",
                  display: "block",
                  fontSize:"0.8rem"
                }}
                onClick={() => navigate("/ForgotPassword")}
              >
                Forgot Password ?
              </span>
              <Button
                type="submit"
                variant="contained"
                sx={{ ...newButton, mt: 2 , fontSize:"0.8rem", width:"60%"}}
              >
                Sign In
              </Button>
            </form>
            <p style={{ margin: "5px auto 60px", color: "white", fontSize:"0.8rem" }}>
              {" "}
              Don't have an account ?
              <div
                onClick={() => {
                  sessionStorage.removeItem("confirm");
                  sessionStorage.removeItem("verification");
                  navigate(`/Signup`)
                }}
                className="nav-link"
                // to={"/Signup"}
                style={{ display: "inline-block",color:"blue" , cursor:"pointer", fontSize:"0.8rem" }}
              >
                Sign Up
              </div>
            </p>
          </Grid>
          <Grid item xs></Grid>
        </Grid>
      </div>
    </div>
  );
}
