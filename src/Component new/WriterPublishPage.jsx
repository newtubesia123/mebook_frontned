import React, { useState, useEffect } from "react";
import axios from "axios";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ButtonGroup from '@mui/material/ButtonGroup';
import ImageIcon from '@mui/icons-material/Image';
import Divider from '@mui/material/Divider';
import VideocamIcon from '@mui/icons-material/Videocam';
import Input from '@mui/material/Input';
import TextField from '@mui/material/TextField';
import InputBase from '@mui/material/InputBase';
import { Link, useNavigate } from "react-router-dom";
import { BackGround, newButton } from "./background";


export default function WriterPublishPage() {
    
    const bg = {...BackGround, minHeight:"100vh"}
    const bgBut = {...newButton, margin: '0px 10px'}

    let userId = sessionStorage.getItem("writerId")
    console.log('userId', userId)
    let userTypeId = localStorage.getItem('userData');
    let userName = JSON.parse(localStorage.getItem('userData'))

    const [inputBox, setInputBox] = useState(true)
    const handleConvertInput = () => {
        setInputBox(false)
    }
    const [userImage, setUserImage] = useState('')
    console.log("image", userImage)
    const [userInfo, setuserInfo] = useState({
        file: "",
        filepreview: null,
    });
    const handleInputChange = (event) => {
        setuserInfo({
            ...userInfo,
            file: event.target.files[0],
            filepreview: URL.createObjectURL(event.target.files[0]),
        });
    };
    let token = JSON.parse(localStorage.getItem("token"));
    const submitFiles = async () => {
        const formdata = new FormData();
        formdata.append("profiles", userInfo.file);

        await axios
            .post(
                "http://54.246.61.54:3002/pdfFile", formdata)

            .then((res) => {
                console.log("res-->", res.data)
                if (res.data.success) {
                    setUserImage(res.data.file[0].filename)
                    alert(res.data.success);
                }
            });
    };

    // writer text 
    let user = localStorage.getItem('userData')
    const navigate = useNavigate();
    const [text, setText] = useState({
        textStory: "",
        titleStory: "",
        pdfFile: '',
        youtubeUrl: '',
        userTypeId: ''
    });
    const handleChange = (e) => {
        setText({
            ...text,
            [e.target.name]: e.target.value,
        });
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const userData = {
            textStory: text.textStory,
            titleStory: text.titleStory,
            pdfFile: userImage,
            youtubeUrl: baseUrl,
            userTypeId: userTypeId
        };
        if (userId !== null) {

            await axios
                .post("http://54.246.61.54:3002/updateWork", { userData: userData, userId: userId })
                .then((res) => {
                    console.log("data->", res.data);
                    if (res.data.isSuccess) {
                        alert(res.data.message);
                        navigate(`/UserDashboard/:${userName.name}`)
                    } else {
                        alert("something went wrong")
                    }
                })
                .catch((err) => console.log(err));
        } else {
            await axios
                .post("http://54.246.61.54:3002/addWork", userData,
                )
                .then((res) => {
                    console.log("data->", res.data)
                    if (res.data.isSuccess) {
                        alert(res.data.message);
                        navigate(`/UserDashboard/:${userName.name}`)
                    }
                })
                .catch((err) => console.log(err));
        }
    };
    const [url, setUrl] = useState("")
    const fun = (e) => {
        setUrl(e.target.value)
    }
    let baseUrl = url.substring(32, 43)
    if (url.length == 28) {
        baseUrl = url.substring(17)
        console.log("url", baseUrl)
    }

    return (
        <div className="container-fluid" style={bg} >
            <Box sx={{ flexGrow: 1, mb: 10 }}>
                <AppBar elevation position="fixed" sx={{ backgroundColor: 'white', borderBottom: " 1px solid lightgray" }}>
                    <Toolbar>
                        <Link to='/WriterStoryDetails'><IconButton
                            size="large"
                            edge="start"
                            // color="white"
                            aria-label="menu"
                            sx={{ mr: 2 }}
                        >
                            <ArrowBackIosIcon />
                        </IconButton></Link>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: "black" }}>
                            Untitled Part 1
                        </Typography>
                        <Button sx={newButton} onClick={handleSubmit}>Publish</Button>
                        <Button onClick={submitFiles} sx={bgBut}>Save</Button>
                    </Toolbar>
                </AppBar>
            </Box>
            <div className="container-fluid text-center" style={{ height: '125px', backgroundColor: '#eee' }}>
                <input type="file" id="pdf" style={{ display: 'none' }} onChange={handleInputChange} />
                {inputBox ? <><button style={{ backgroundColor: 'white', margin: '30px 0px', width: '120px' }} type='button' class="btn btn-light btn-sm"> <label for="pdf"> <ImageIcon color="action" sx={{ fontSize: '38px', margin: 'auto 30px' }} /></label>  </button>
                    <button style={{ backgroundColor: 'white', margin: '30px 0px', width: '120px' }} type='button' class="btn btn-light btn-sm">  <VideocamIcon onClick={handleConvertInput} color="action" sx={{ fontSize: '45px', margin: '' }} /> </button></>
                    : (<form class="row g-3 justify-content-center"> <div className='col-auto' style={{ padding: '35px 0px' }}> <input value={url} onChange={fun} style={{ margin: " ", width: '250px', padding: '25px 0px', fontSize: '20px' }} class="form-control" placeholder='Paste Your YouTube URL' /></div>
                        <div class="col-auto my-auto ">
                            <button style={{ backgroundColor: 'white', margin: '30px 0px', width: '120px' }} type='button' class="btn btn-light btn-sm"> <label for="pdf"> <ImageIcon color="action" sx={{ fontSize: '38px', margin: 'auto 30px' }} /></label>  </button>
                        </div></form>)}
            </div>

            {url.includes("https://www.youtube.com") || url.includes("https://youtu.be") ? (
                <div className="container-fluid text-center" style={{ backgroundColor: '#eee', margin: '10px 0px' }} ><iframe style={{ width: '500px', height: '300px' }} src={`https://www.youtube.com/embed/${baseUrl}`} frameborder='0'
                    allow='autoplay'
                    allowfullscreen
                    title='video' ></iframe>
                </div>) : ""}

            {userInfo.filepreview !== null ? (<div className="text-center" style={{ backgroundColor: '#eee', margin: "10px 0px" }}>
                <img style={{ height: '300px', width: '500px' }} src={userInfo.filepreview} frameborder="0" />
            </div>) : ""}
            <div className='text-center'>
                <InputBase
                    sx={{ mt: 8, fontWeight: 'bolder', flex: 1, fontSize: '35px' }}
                    placeholder="Untitled part1"
                    name="titleStory"
                    onChange={handleChange}
                    value={text.titleStory}
                />
                <Divider sx={{ width: '50%', mx: "auto" }} orientation="" />

            </div>
            <div className='col-md-6 mx-auto'>
                <InputBase
                    multiline
                    sx={{ flex: 1, mt: '5px', mb: '100px', fontSize: '20px', width: '100%' }}
                    placeholder="Type your text"
                    name="textStory"
                    onChange={handleChange}
                    value={text.textStory}
                />
            </div>
        </div >
    )
}
