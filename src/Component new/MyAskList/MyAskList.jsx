import axios from 'axios'
import React, { useState } from 'react'
import './MyAskList.css'
import { SERVER } from '../../server/server'
import { callPopUp, getAuth } from '../../Action'
import { toast } from 'react-toastify'
import { useDispatch, useSelector } from 'react-redux';
import { getImgURL } from '../../util/getImgUrl'

export default function MyAskList({ data, setDepend, noData, userId, setAskValue, askValue, setIsEdit, isEdit, setCurrMediaLink }) {
    // const [askToDelete, setAskToDelete] = useState(null);

    const STORE = useSelector((state) => state);
    // const dispatch = useDispatch();

    // const deleteAsk = (askId) => {
    //     setAskToDelete(askId);
    //     dispatch(callPopUp({ type: "askDelete", handleDelte: handleDelte, askToDelete: askId }));
    // }

    const handleDelte = (askId) => {
        axios.post(`${SERVER}/my_ask/delAskById/${askId}`, { body: "askDelete" },
            {
                headers: { Authorization: `Bearer ${getAuth()}` }
            })
            .then((res) => {
                setDepend(true)
                toast.success(res.data.message, { autoClose: 2000, pauseOnHover: false, position: "top-center" })
            })
            .catch((err) => console.log(err))
    }

    const handleEdit = (e) => {
        setIsEdit(true)
        // console.log( e?.askTitle, e?.askDescription, e?.askFile, e?._id)
        setAskValue({
            ...askValue,
            askTitle: e?.askTitle,
            askDescription: e?.askDescription?.join(' '),
            askFile: e?.askFile,
            id: e?._id
        });
        setCurrMediaLink(e?.askFile)
        window.scrollTo(0, 0)

    }
    return (
        <div>
            <div className="row py-4">
                {
                    data && data.length > 0 ?

                        data && data?.length > 0 && data.reverse().map((e, i) => {
                            return (
                                <div key={i} className="col-12 py-3 my-4 bgchange shadow-lg text-light" style={{ background: "#181823" }}>
                                    <span className='d-flex justify-content-end btnpart'>
                                        {
                                            STORE.getProfileData.success ?
                                                <>
                                                    {
                                                        !userId ?
                                                            <>
                                                                <button className='profile-card__button button--orange  me-2 px-3 mx-3 btnCss'
                                                                    onClick={() => handleDelte(e?._id)}
                                                                >Delete</button>
                                                                {/* <button className='profile-card__button button--orange  me-2 px-3 mx-3 btnCss'
                                                                    onClick={() => deleteAsk(e._id)}
                                                                >Delete</button> */}
                                                                <button className='profile-card__button button--orange  me-2 px-3 btnCss'
                                                                    onClick={() => {
                                                                        handleEdit(e)
                                                                    }}
                                                                >Edit</button>
                                                            </>
                                                            : ""
                                                    }
                                                </>
                                                : ""
                                        }
                                    </span>
                                    <h4 className='py-1'><b>{e?.askTitle}</b></h4>
                                    <h4 className='py-2'>{e?.askDescription?.join(' ')}</h4>
                                    {
                                        e?.askFile !== "" ?
                                            <embed src={getImgURL(e?.askFile)} alt="" style={{ width: '100%', height: '450px' }} />
                                            : ""
                                    }
                                </div>
                            )
                        })
                        : <div style={{ display: "flex", margin: "auto" }}>{noData}</div>
                }
            </div>
        </div>
    )
}
