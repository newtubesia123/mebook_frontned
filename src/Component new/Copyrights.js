import React, { useEffect,} from 'react'
import './Copyrights.css'
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getUserData } from '../Action';
import { toast } from 'react-toastify';


export default function Copyrights({formValue,setFormValue,confirm,setConfirm,handleClose,tick,setTick}) {
    const STORE = useSelector((state) => state)
    const dispatch = useDispatch()

    const params=useParams()
    

    useEffect(() => {
        dispatch(getUserData())
    }, [])
  const checkTick=(e)=>{

setTick({...tick,
    [e.target.name]: e.target.checked
})


  }
    const handleFormValue=(e)=>{
        setFormValue({
            ...formValue,
            [e.target.name]: e.target.value,
        });
    }

   
    const confirmSubmit=()=>{
        if(formValue?.copyright==='' || formValue?.copyright==='Select'){
            toast.error('Please Select Copyright',{autoClose:2000,pauseOnHover:false,position:'top-right'})
        }
        else if(!tick?.copyright || !tick?.tnc){
            toast.error('Please agree to Copyright and Terms and Conditions both',{autoClose:2000,pauseOnHover:false,position:'top-right'})
        }
        else{
            setConfirm(true)
            handleClose()
        }
    }

  


    return (

        <div className="container flex " style={{ maxHeight: '80vh', color: 'white' }}>
            <div className="row d-flex justify-content-center align-items-center ">
                <div className="col-xl-7 col-lg-8 col-md-9 col-sm-11 col-11 text-center BgCopyright" style={{ borderRadius: '0px' }}>
                    <h3 className='copyrightHead' style={{ fontSize: "125%" }}>COPYRIGHT RELEASE FORM</h3>
                    <div className="row d-flex justify-content-center align-items-center" >
                        <div className="col-12 col-sm-10 col-md-10">

                           
                            <form className="form-card">

                                <label className=" px-3">Full name</label>
                                <input  className='sss disIn' type="text"  name="name" disabled  value={formValue?.name} placeholder="Enter your first name" onblur="validate(1)" style={{ height: "2rem", fontSize: "100%" }}onChange={handleFormValue} />

                                <label className="form-control-label px-3 mt-2">Email Id </label>
                                <input disabled value={formValue?.email} className='sss disIn' type="text" id="email" name="email" placeholder="" onblur="validate(3)" style={{ height: "2rem", fontSize: "100%" }} onChange={handleFormValue}/>
                               


                                <label className="form-control-label px-3 mt-2">Address</label>
                                <input disabled value={formValue?.address} className='sss disIn' type="text"  name="ans" placeholder="" onblur="validate(6)" style={{ height: "2rem", }} onChange={handleFormValue}/>



                                <label className="form-control-label px-3 mt-2">Copyright</label>
                                <select className='sss' name="copyright" value={formValue?.copyright} id="ans" onChange={handleFormValue} disabled={params.process==='Edit'?true:false}>
                                <option value="Select">Select</option>
                                    <option value="All Rights Reserved">All Rights Reserved</option>
                                    <option value="Public Domain">Public Domain</option>

                                </select>

                            </form>
                            <div className='my-2' style={{ textAlign: 'start', }}>
                                <p> <b style={{ fontSize: '100%' }}>
                                    For the Purpose of Sharing Out my Work/Content, as to the Corresponding Work/Content
                                    Shared on this Site, I (the MeBookMeta User on record) Warrant that:</b> <br />
                                    <ol type='a' style={{ marginLeft: 20, fontSize: "75%" }}>
                                        <li className='my-1'>The Work/Content submitted on this site is my/our original Work/Content;</li>
                                        <li className='my-1'>All acknowledged users who participated to the Work/Content in a substantive way are
                                            prepared to take public responsibility for the Work/Content;</li>
                                        <li className='my-1'>I am authorizing MeBookMeta to share on its platform the Work/Content samples that I
                                            have uploaded to the platform;</li>
                                        <li className='my-1'>I was authorized by all other contributors (if any) to share the following Work/Content
                                            with the MeBookMeta team for the purpose of sharing a Work/Content sample with the
                                            platform audience, and I will be responsible in the event of all disputes that have occurred
                                            and that may occur;</li>
                                        <li className='my-1'>All relevant contributors to the Work/Content have seen and approved of the
                                            Work/Content sample as submitted;</li>
                                        <li className='my-1'>The text copy, photos, illustrations and any other materials in the Work/Content
                                            submission do not infringe upon any existing copyright or other rights of anyone;</li>
                                        <li className='my-1'>While I am authorizing MeBookMeta to share the Work/Content I have submitted, I
                                            retain all my original rights to the Work/Content.</li>
                                    </ol>

                                </p>
                                <div className=" ">
                                    <div className="form-group d-flex"> 
                                        <input 
                                        type="checkbox" 
                                        name='copyright'
                                        checked={tick?.copyright}
                                        onChange={checkTick}
                                        style={{ height: '35px', width: '35px', marginRight: '15px' }} />
                                        <p style={{ fontSize: "90%" }}>I acknowledge and authorize the inherent warrants, waivers and permissions contained above.</p>
                                    </div>
                                    <div className="form-group d-flex"> 
                                        <input 
                                        type="checkbox" 
                                        name='tnc'
                                        checked={tick?.tnc}
                                        onChange={checkTick}
                                        style={{ height: '20px', width: '20px', marginRight: '15px' }} />
                                        <p style={{ fontSize: "90%" }}>I Agree <span style={{color:'blue'}}>Terms And Conditions</span></p>
                                    </div>
                                </div>
                                <div className="row justify-content-center">
                                    <div className="form-group col-sm-6"> <button className="btn-block btn-primary btn-sm" 
                                    onClick={confirmSubmit}
                                    >Submit</button> </div>
                                </div>
                            </div>
                        </div>
                    </div>


                   
                </div>
            </div>
        </div>

    );
}


