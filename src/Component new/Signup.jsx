import React, { useEffect, useState } from "react";
import axios from "axios";
import Grid from "@mui/material/Grid";
import { useNavigate } from "react-router-dom";
import { newButton } from "./background";
import { SERVER } from "../server/server";
import { toast } from "react-toastify";
import Button from "@mui/material/Button";
import "../component/common.css";
import pic from "../mepic.png";
// import GoogleAuthSignIn from "./GoogleAuth";
import jwt_decode from "jwt-decode";
import { GoogleOAuthProvider, GoogleLogin } from "@react-oauth/google";
import MeLogo from "../assets/MeLogoMain";

export default function Signup() {
  const navigate = useNavigate();


  const [authUser, setAuthUser] = useState({
    name: "",
    email: "",
    email_verified: false,
  });

  console.log("authuser", authUser);

  const [authForm, setAuthForm] = useState(true);

  const [formValueObject, setFormValueObject] = useState({
     usernameValue : "",
     emailValue : "",
     passwordValue : "",
     confirmpassword : ""
  })

  const [validatorObject, setValidatorObject ] = useState({
    email : false,
    username : false,
    password : false,
    error : false

  });

  console.log("validatorObject", validatorObject);

  const clientId =
    "1084042994316-hhruvb92ilnt4ejr0s28u70ehtu1ibv0.apps.googleusercontent.com";

  
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
  });
  console.log("values", values);

  // email Handle
  const handleEmailChange = (e) => {
    let item = e.target.value;
    // setEmailValue(item);

    setFormValueObject((preVal) => ({
      ...preVal, 
      emailValue : item
    }))


    
    if (item.includes("@")) {
      // setEmail(false);
      setValidatorObject(prevState => ({
        ...prevState,
        email: false
      }));
      const { name, value } = e.target;
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
      setAuthUser({
        ...values,
        [e.target.name]: e.target.value,
      });
    } else {
      // setEmail(true);
      setValidatorObject(prevState => ({
        ...prevState,
        email: true
      }));
    }
  };

  // password Handle

  const handlePasswordChange = (e) => {
    let item = e.target.value;
    // setPasswordValue(item);
    setFormValueObject((preVal) => ({
      ...preVal, 
      passwordValue : item
    }))

    if (item.length < 8) {
      // setPassword(true);
      
      setValidatorObject(prevState => ({
        ...prevState,
        password: true
      }));
    } else {
      // setPassword(false);
      setValidatorObject(prevState => ({
        ...prevState,
        password: false
      }));

      const { name, value } = e.target;
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
      setAuthUser({
        ...values,
        [e.target.name]: e.target.value,
      });
    }

    if(item != formValueObject.confirmpassword){
      //  setError(true)
      setValidatorObject(prevState => ({
        ...prevState,
        error: true
      }));

    }else{
      // setError(false)
      setValidatorObject(prevState => ({
        ...prevState,
        error: false
      }));

    }

  };

  

  // name handle
  const handleChange = (e) => {
    console.log(e.target.value);
    let item = e.target.value;
    // setUsernameValue(item)
    setFormValueObject((preVal) => ({
      ...preVal, 
      usernameValue : item
    }))
    if(item.length < 5){
      // setUsername(true)
      setValidatorObject(prevState => ({
        ...prevState,
        username: true
      }));

    }else{
      
      setValues({
        ...values,
        [e.target.name]: e.target.value,
      });
      // setUsername(false);
      setValidatorObject(prevState => ({
        ...prevState,
        username: false
      }));
  
      setAuthUser({
        ...values,
        [e.target.name]: e.target.value,
      });
      // setAuthUser({
      //   ...values,
      //   [e.target.name]: e.target.value,
      // });
    }
    
    // console.log("input check", authUser);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const registerData = {
      name: authForm ? values.name : authUser.name,
      email: authForm ? values.email : authUser.email,
      password: values.password,
      confirmpassword: values.confirmpassword,
    };
    console.log("btn check", registerData);

    if (
      (registerData.name &&
      registerData.email &&
      registerData.password &&
      registerData.confirmpassword) && (validatorObject.email == false && validatorObject.username == false && validatorObject.password == false && validatorObject.error == false)
    ) {
      await axios
        .post(`${SERVER}/signup`, { registerData: registerData })
        .then((res) => {
          console.log("res.data", res.data);
          if (res.data.success) {
            // sessionStorage.setItem("id", res.data.data._id);
            {
              authForm
                ? navigate(`/ConfirmMail/ProfileDetails}`)
                : navigate(`/ConfirmMail/${registerData.email}`);
            }
            navigate(`/ConfirmMail/${registerData.email}`);
          } else {
            setUserExist(true);
            toast.error("Email Already Exist", {
              position: "top-center",
              autoClose: 2000,
              pauseOnHover: false,
              theme: "light",
            });
            return;
          }
        })
        .catch((err) => console.log(err));
    } else {
      

      if (formValueObject.emailValue === "" && formValueObject.passwordValue === "" && formValueObject.usernameValue === "") {
        // setEmail(true);
        // setError(true);
        // setPassword(true);
        // setUsername(true);

        setValidatorObject(prevState => ({
          ...prevState,
          email: true,
          password : true,
          username : true,
          error : true
        }));
        
        setOpen(true);
        toast.error("please enter", {
          position: "top-center",
        });
        return false;
      }

      if(formValueObject.emailValue === "" && formValueObject.passwordValue === ""){
        // setEmail(true);
         // setPassword(true);
        setValidatorObject(prevState => ({
          ...prevState,
          email: true,
          password : true,
          error : true
        }));
        
      }

      // name
      if (formValueObject.usernameValue == "") {
        setUser(true);
        // setUsername(true);
        setValidatorObject(prevState => ({
          ...prevState,
          username: true
        }));
        setTimeout(() => {
          setUser(false);
        }, 5000);
        return false;
      }

      // email
      if (formValueObject.emailValue == "") {
        // setEmail(true);
        setValidatorObject(prevState => ({
          ...prevState,
          email: true
        }));
        setMail(true);
        return false;
      }

      if ( formValueObject.passwordValue == "") {
        setPwd(true);
        // setError(true);
         // setPassword(true);
        setValidatorObject(prevState => ({
          ...prevState,
          error: true,
          password: true,
        })); 
       
      
        setTimeout(() => {
          setPwd(false);
        }, 5000);
        return false;
      }
      

      if(formValueObject.confirmpassword === ""){
          //  setError(true)
          setValidatorObject(prevState => ({
            ...prevState,
            error: true
          }));
      }

    }

    // if (email === true && password === true && username === true) {
    //   setEmail(true);
    //   setError(true);
    //   setPassword(true);
    //   setUsername(true);
    //   // setOpen(true);
    //   toast.error("please enter", {
    //     position: "top-center",
    //   });
    //   return false;
    // }
    // if (email === true) {
    //   setEmail(true);
    //   setMail(true);
    //   return false;
    // }
    // if (error === true || password === true) {
    //   setPwd(true);
    //   setError(true);
    //   setPassword(true);
    //   setTimeout(() => {
    //     setPwd(false);
    //   }, 5000);
    //   return false;
    // }
    // if (username === true) {
    //   setUser(true);
    //   setUsername(true);
    //   setTimeout(() => {
    //     setUser(false);
    //   }, 5000);
    //   return false;
    // }
    // await axios
    //   .post(`${SERVER}/signup`, { registerData: registerData })
    //   .then((res) => {
    //     console.log("res.data", res.data);
    //     if (res.data.success) {
    //       // sessionStorage.setItem("id", res.data.data._id);
    //       { authForm ?  navigate(`/ConfirmMail/ProfileDetails}`) : navigate(`/ConfirmMail/${registerData.email}`)  }
    //       navigate(`/ConfirmMail/${registerData.email}`);
    //     } else {

    //       setUserExist(true);
    //       toast.error("Email Already Exist", {
    //         position: "top-center",
    //         autoClose: 2000,
    //         pauseOnHover: false,
    //         theme: "light",
    //       });
    //       return;
    //     }
    //   })
    //   .catch((err) => console.log(err));
  };

  // confirm password handle
  const checkValidation = (e) => {
    // setConfirmpassword(e.target.value);
    setFormValueObject((preVal) => ({
      ...preVal, 
      confirmpassword : e.target.value
    }))
    if (formValueObject.passwordValue !== e.target.value) {
      // setError(true);
      setValidatorObject(prevState => ({
        ...prevState,
        error: true
      }));
    } else {
      // setError(false);
      setValidatorObject(prevState => ({
        ...prevState,
        error: false
      }));
      const { name, value } = e.target;
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    }
   
  };
  // Alert hooks
  const [open, setOpen] = React.useState(false);

  const [userExist, setUserExist] = React.useState(false);
  const [mail, setMail] = React.useState(false);
  const [pwd, setPwd] = React.useState(false);
  const [user, setUser] = React.useState(false);

  const responseGoogle = (response) => {
    console.log("response", response);
    setAuthForm(false);
    const userObject = jwt_decode(response.credential);
    console.log("userObj", userObject);
    sessionStorage.setItem("user", JSON.stringify(userObject));
    const { name, email, email_verified } = userObject;
    // setEmailValue(email);
    // setUsernameValue(name);
    setFormValueObject((preVal) => ({
      ...preVal, 
      usernameValue : name,
      emailValue : email
    }))
  
    // setPasswordValue("");
    // setConfirmpassword("");

    setFormValueObject((preVal) => ({
      ...preVal, 
      passwordValue : "",
      confirmpassword : ""
    }))
    
    const doc = {
      name: name,
      email: email,
      email_verified: email_verified,
    };
    console.log(doc, "doc");
    setAuthUser(doc, "doc");
    setValues(doc);
    // setEmail(false);
    setValidatorObject(prevState => ({
      ...prevState,
      email: false
    }));
    // setError(false);
    setValidatorObject(prevState => ({
      ...prevState,
      error: false
    }));
    // setPassword(false);
    setValidatorObject(prevState => ({
      ...prevState,
      password: false
    }));
    // setUsername(false);
    setValidatorObject(prevState => ({
      ...prevState,
      username: false
    }));

  };
  // console.log(authUser, "authuser")

  return (
    <div className="header MebookMetaAuthor p-1">
      <div className="mr-auto ">
        <Button
          sx={{ ...newButton, fontSize: "75%" }}
          onClick={() => navigate(-1)}
          variant="contained"
        >
          Go Back
        </Button>
      </div>
      <Grid container rowSpacing={0} columnSpacing={{ xs: 1, sm: 2, md: 2 }}>
        <Grid item xs={1} md={4}></Grid>
        <Grid item xs={10} md={4}>
          <div
            className=" tagname text-center"
            style={{ fontFamily: "Times New Roman", fontSize: "3rem" }}
          >
            {/* <img src={pic} alt="" style={{
              width: "30%",
              height: "20%",
              margin: "10px 0px",
            }} /> */}
            {/* <MeLogo /> */}
            {/* <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eBook</span>
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eta</span> */}
          </div>
          <h5
            style={{
              textAlign: "center",
              fontWeight: "bold",
              color: "black",
              fontSize: "1.5rem",
            }}
          >
            Welcome to{" "}
            <span
              style={{
                color: "rgb(187 11 11)",
                fontFamily: "Times New Roman",
                fontWeight: "bold",
                fontSize: "1.5rem",
              }}
            >
              M
            </span>
            <span
              style={{
                color: "rgb(12 7 107)",
                fontFamily: "Times New Roman",
                fontWeight: "bold",
                fontSize: "1.5rem",
              }}
            >
              eBook
            </span>
            <span
              style={{
                color: "rgb(187 11 11)",
                fontFamily: "Times New Roman",
                fontWeight: "bold",
                fontSize: "1.5rem",
              }}
            >
              M
            </span>
            <span
              style={{
                color: "rgb(12 7 107)",
                fontFamily: "Times New Roman",
                fontWeight: "bold",
                fontSize: "1.5rem",
              }}
            >
              eta
            </span>{" "}
          </h5>
          <h1
            className="head"
            style={{
              textAlign: "center",
              fontSize: "150%",
              fontWeight: "bolder",
              marginBottom: "10px",
            }}
          >
            {" "}
            The Global Media Marketplace!
          </h1>
          <h5
            // className="head"
            style={{
              margin: "10px 0px",
              textAlign: "center",
              fontWeight: "bold",
              color: "black",
              fontSize: "0.9rem",
            }}
          >
            MeBookMeta creates a sustainable community of creators,
            collaborators, mentors, influencers, providers, educators, consumers
            and readers – The Global Media Marketplace!
          </h5>
          <>
            {authForm ? (
              <form style={{ marginTop: "20px" }}>
                <div class="form-group">
                  <input
                    type="text"
                    class="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    placeholder="Enter your Full Name"
                    name="name"
                    // value={values.name}
                    value={formValueObject.usernameValue}
                    onChange={handleChange}
                    style={{ height: "2rem", fontSize: "1rem" }}
                  />
                  {validatorObject.username ? (
                    <p style={{ color: "red", float: "left" }}>
                      Please Enter At Least 5 Character
                    </p>
                  ) : (
                    ""
                  )}
                </div>

                <div class="form-group">
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputPassword1"
                    placeholder="Enter your Email "
                    name="email"
                    // value={values.email}
                    value={formValueObject.emailValue}
                    onChange={handleEmailChange}
                    style={{ height: "2rem", fontSize: "1rem" }}
                  />
                  {validatorObject.email ? (
                    <p style={{ color: "red", float: "left" }}>
                      Please Enter a valid Email address
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div class="form-group">
                  <input
                    type="password"
                    class="form-control"
                    id="exampleInputPassword1"
                    placeholder="Enter your Password"
                    name="password"
                    // value={values.password}
                    value={formValueObject.passwordValue}
                    onChange={handlePasswordChange}
                    style={{ height: "2rem", fontSize: "1rem" }}
                  />
                  {validatorObject.password ? (
                    <p className="" style={{ color: "red", float: "left" }}>
                      password must be 8 characters{" "}
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div class="form-group">
                  <input
                    type="password"
                    class="form-control"
                    id="exampleInputPassword1"
                    placeholder="Confirm Password "
                    name="confirmpassword"
                    onChange={(e) => checkValidation(e)}
                    value={formValueObject.confirmpassword}
                    style={{ height: "2rem", fontSize: "1rem" }}
                  />
                  {validatorObject.error ? (
                    <p className="" style={{ color: "red", float: "left" }}>
                      Password does not match
                    </p>
                  ) : (
                    ""
                  )}
                </div>
              </form>
            ) : (
              <>
                <form style={{ marginTop: "20px" }}>
                  <div class="form-group">
                    <input
                      type="text"
                      disabled
                      class="form-control"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Enter your Full Name auth"
                      name="name"
                      value={authUser.name}
                      onChange={handleChange}
                      style={{ height: "2rem", fontSize: "1rem" }}
                    />
                    {validatorObject.username ? (
                      <p style={{ color: "red", float: "left" }}>
                        Please Enter a valid Nam
                      </p>
                    ) : (
                      ""
                    )}
                  </div>

                  <div class="form-group">
                    <input
                      type="email"
                      disabled
                      class="form-control"
                      id="exampleInputPassword1"
                      placeholder="Enter your Email auth "
                      name="email"
                      value={authUser.email}
                      onChange={handleEmailChange}
                      style={{ height: "2rem", fontSize: "1rem" }}
                    />
                    {validatorObject.email ? (
                      <p style={{ color: "red", float: "left" }}>
                        Please Enter a valid Emai address
                      </p>
                    ) : (
                      ""
                    )}
                  </div>
                  <div class="form-group">
                    <input
                      type="password"
                      class="form-control"
                      id="exampleInputPassword1"
                      placeholder="Enter your Password"
                      name="password"
                      // value={values.password}
                      value={formValueObject.passwordValue}
                      onChange={handlePasswordChange}
                      style={{ height: "2rem", fontSize: "1rem" }}
                    />
                    {validatorObject.password ? (
                      <p className="" style={{ color: "red", float: "left" }}>
                        password must be 8 auth characters{" "}
                      </p>
                    ) : (
                      ""
                    )}
                  </div>
                  <div class="form-group">
                    <input
                      type="password"
                      class="form-control"
                      id="exampleInputPassword1"
                      placeholder="Confirm Password "
                      name="confirmpassword"
                      onChange={(e) => checkValidation(e)}
                      value={formValueObject.confirmpassword}
                      style={{ height: "2rem", fontSize: "1rem" }}
                    />
                    {validatorObject.error ? (
                      <p className="" style={{ color: "red", float: "left" }}>
                        Password does not match
                      </p>
                    ) : (
                      ""
                    )}
                  </div>
                </form>
              </>
            )}
          </>

          <div className="text-center " style={{marginTop:"44px"}}>
            {authForm ? (
             
              <Button
                sx={newButton}
                style={{ fontSize: "75%" }}
                onClick={handleSubmit}
              >
                MebookMeta Account Verification
              </Button>
            
            ) : (
              <Button
                sx={newButton}
                style={{ fontSize: "75%", width: "100%" }}
                onClick={handleSubmit}
              >
                Sign Up
              </Button>
            )}
            {authForm ? (
              <div className="d-flex justify-content-center align-items-center my-2">
                <GoogleOAuthProvider clientId="1084042994316-hhruvb92ilnt4ejr0s28u70ehtu1ibv0.apps.googleusercontent.com">
                  <GoogleLogin
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle}
                  />
                </GoogleOAuthProvider>
              </div>
            ) : (
              ""
            )}
            <p
              style={{
                marginBottom: "5px",
                fontFamily: "Times New Roman",
                fontSize: "1rem",
                color: "black",
              }}
            >
              Already have an account?
              <div
                className="nav-link"
                onClick={() => {
                  navigate(`/SignIn`);
                }}
                to={"/SignIn"}
                style={{
                  display: "inline-block",
                  color: "blue",
                  cursor: "pointer",
                  fontSize: "1rem",
                }}
              >
                Sign in
              </div>
            </p>
          </div>
        </Grid>
        <Grid item xs={1} md={4}></Grid>
      </Grid>
    </div>
  );
}
