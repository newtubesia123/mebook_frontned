import { color } from '@mui/system';
import React from 'react'
import pic from '../mepic.png';
import { Link, useNavigate, generatePath } from "react-router-dom";
import CommonAppbar from './CommonAppbar';
import { BackGround, newButton } from './background';
import Button from "@mui/material/Button";

export default function CompletionIdentity() {
    const styleSheet = {
        fontSize: '35px',
        fontWeight: "bold",
        fontFamily: "Times New Roman",
        margin: '100px 0px',
        color:"white"

    }
    const bgBut = {...newButton, marginLeft:"40px"}
    const navigate = useNavigate();

    let userName = JSON.parse(localStorage.getItem('userData'))

    return (
        <div className='container-fluid text-center' style={BackGround}>
            <CommonAppbar />
            <div className="row">
                <div className="col-md-12">
                    <img src={pic} width={250} height={150} alt="" />
                    <h2 style={styleSheet}>Your Identity Profile Has Been Saved!</h2>
                    <p style={{
                        color: '#0a0a8a', fontSize: '35px',
                        fontWeight: "bold",
                        fontFamily: "Times New Roman",
                        marginBottom: '150px', color:"white"
                    }}>Please make <span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#0a0a8a' }}>ebook</span><span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#0a0a8a' }}>eta</span> Payment to access further</p>
                    <div className=" " style={{ marginBottom: '80px' }}>
                        <Button onClick={() => navigate(-1)} sx={newButton} size="large" variant="contained">
                            Go Back
                        </Button>
                        {/* <Link to={generatePath("/UserDashboard/:username", { username: userName.name.toLowerCase() })}> */}
                        <Link to='/MainDashboard'>
                            <Button
                                variant="contained" size="large"
                                sx={bgBut}
                            >
                                Continue
                            </Button></Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
