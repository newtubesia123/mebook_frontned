import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { BackGround, newButton } from "./background";
import { toast } from "react-toastify";
import { Button } from "@mui/material";
import axios from "axios";
import image from "../mepic.png";
import { SERVER } from "../server/server";
import MeLogo from "../assets/MeLogoMain";

export default function ResetPassword() {
  const navigate = useNavigate();
  const [inputValue, setInputValue] = useState({
    email: localStorage.getItem('forgotPasswordEmail'),
    password: "",
    cPassword: ''
  });

  const bg = { ...BackGround, minHeight: "100vh" }

  const handleChange = (e) => {
    setInputValue({
      ...inputValue,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      email: inputValue.email,
      password: inputValue.password,
    };
    if (inputValue.password == '' || inputValue.cPassword == '') {
      toast.error('All Fields Are Required', { position: 'top-center', autoClose: 2000, pauseOnHover: false })
    } else if (inputValue.password !== inputValue.cPassword) {
      toast.error('Password and Confirm Password must be same', { position: 'top-center', autoClose: 2000, pauseOnHover: false })
    } else {
      await axios
        .post(`${SERVER}/resetPassword`, { data: data })
        .then((res) => {
          // console.log("data->", emailData);
          if (res.data.isSuccess) {
            toast.success(res.data.message);
            localStorage.removeItem('forgotPasswordEmail')
            navigate("/SignIn");
            // sessionStorage.setItem("key", id);
          } else {
            toast.error(res.data.Error);
          }
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <div className='container-fluid text-center' style={bg}>
      <div>
        {/* <img src={image} width='10%' height='10%' style={{ margin: '50px auto' }} alt="" /> */}
        <MeLogo />
      </div>
      <div className="row">
        <div className="col-md-6 mx-auto">
          <h1> <b style={{ color: 'white', fontSize: "80%" }}> Reset Password </b></h1>
          <form className='my-5'>
            <div className="form-group">
              <input disabled id="codeBox1" type="email" name="email" value={inputValue.email} onChange={handleChange} class="form-control"
                style={{ fontSize: "80%", height: "2rem" }} placeholder='Enter Your Email' />
            </div>
            <div className="form-group">
              <input id="codeBox1" type="password" name="password" value={inputValue.password} onChange={handleChange} class="form-control" placeholder='Enter New Password' style={{ fontSize: "80%", height: "2rem" }} />
            </div>
            <div className="form-group">
              <input id="codeBox1" type="password" name="cPassword" value={inputValue.cPassword} onChange={handleChange} class="form-control" placeholder='Confirm Password' style={{ fontSize: "80%", height: "2rem" }} />
            </div>
          </form>
        </div>
      </div>
      <div>
        <Button
          type="submit"
          variant="contained"
          sx={{ ...newButton, }}
          onClick={handleSubmit}
        >
          Submit
        </Button>
      </div>
    </div>
  )
}
