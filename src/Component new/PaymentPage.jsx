import { color } from "@mui/system";
import React from "react";

import pic from "../mepic.png";
import { Link, useNavigate } from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormLabel from "@mui/material/FormLabel";
import FormControl from "@mui/material/FormControl";
import pay from "../pay.jpeg";
import card from "../visa.webp";
import credit from "../credit.webp";
import WriterNav from "./WriterNav";
import america from "../bankAmerica.png";
import pic1 from "../america.png";
import pay1 from "../whitepaypal.png";
import { BackGround, newButton } from "./background";
import "./CrouselPage.css";
import { useState } from "react"
export default function PaymentPage() {
  const obj = {
    1: { cardName: "Debit Card", background1: 'rgb(187,44,53)', background2: 'linear-gradient(110deg, rgba(187,44,53,1) 45%, rgba(255,255,255,1) 45%)', background3: 'linear-gradient(90deg, rgba(187,44,53,1) 10%, rgba(255,255,255,1) 10%)' },
    2: { cardName: "Credit Card", background1: 'rgb(121,9,22)', background2: 'linear-gradient(110deg, rgba(121,9,22,1) 35%, rgba(255,255,255,1) 35%)', background3: 'linear-gradient(90deg, rgba(121,9,22,1) 10%, rgba(255,255,255,1) 10%)' },
    3: { cardName: "Paypal", background1: 'rgb(15,9,121)', background2: 'linear-gradient(110deg, rgba(15,9,121,1) 45%, rgba(255,255,255,1) 45%)', background3: 'linear-gradient(90deg, rgba(15,9,121,1) 10%, rgba(255,255,255,1) 10%)' }
  }
  const navigate = useNavigate();
  const [isSelected, setIsSelected] = useState(false);

  const styleSheet = {
    fontSize: "35px",
    fontWeight: "bold",
    fontFamily: "Times New Roman",
    margin: "100px 0px",
  };
  const bg = { ...BackGround, minHeight: "100vh" };
  const bgBtn = {
    ...newButton, width: "50%",
    margin: "auto auto",
    borderRadius: "30px",
    padding: "10px",
  }
  const selectCard = (i) => {
    setIsSelected(i.target.id);
  };
  const handlePayment = () => {
    localStorage.setItem('cardContent', JSON.stringify(obj[isSelected]))
    navigate("/PaymentPage/PaymentDetails")
  }
  return (
    <div className="container-fluid text-center mainDiv" style={bg}>
      <WriterNav />
      <div className="row ">
        <div className="col-md-10 text-center my-5 mx-auto ">
          <div
            className="d-flex flex-column justify-content-between"
            // style={{ height: "390px" }}
          >
            <div className="row my-5">
              <div className=" col-md-4" onClick={selectCard}>
                <img
                  className={
                    isSelected == "1" ? "credit creditSelected" : "credit"
                  }
                  id="1"
                  src={america}
                  height="180px"
                  width="300px"
                  style={{ borderRadius: "10px" }}
                  alt=""
                />
                <div className="pt-3 text-white">
                  <h5 className="cardSelect">
                    Debit card{" "}
                    <input
                      type="checkbox"
                      checked={isSelected === "1" ? true : false}
                    />
                  </h5>
                </div>

                {/* <FormControlLabel value="Debit" sx={{position:"absolute",right:'10px'}} control={<Radio />}  /> */}
              </div>
              <div className=" col-md-4 " onClick={selectCard}>
                <img
                  className={
                    isSelected == "2" ? "credit creditSelected" : "credit"
                  }
                  id="2"
                  src={pic1}
                  height="180px"
                  width="320px"
                  style={{ borderRadius: "10px" }}
                  alt=""
                />
                <div className="pt-3 text-white">
                  <h5 className="cardSelect">
                    Credit card{" "}
                    <input
                      type="checkbox"
                      checked={isSelected === "2" ? true : false}
                    />
                  </h5>
                </div>

                {/* <FormControlLabel value="Credit" sx={{position:"absolute",right:'10px'}} control={<Radio />} /> */}
              </div>
              <div className=" col-md-4" onClick={selectCard}>
                {/* <FormControlLabel value="paypal"sx={{position:"absolute",right:'10px'}} control={<Radio />} /> */}

                <img
                  className={
                    isSelected == "3" ? "credit creditSelected" : "credit"
                  }
                  id="3"
                  src={pay1}
                  height="180px"
                  width="320px"
                  style={{ borderRadius: "25px" }}
                  alt=""
                />
                <div className="pt-3 text-white">
                  <h5 className="cardSelect">
                    Paypal{" "}
                    <input
                      type="checkbox"
                      checked={isSelected === "3" ? true : false}
                    />
                  </h5>
                </div>
              </div>
            </div>
            <button
              onClick={handlePayment}
              type="button"
              class="btn btn-primary btn-lg "
              style={bgBtn}
            >
              Continue
            </button>
          </div>
          {/* </Card> */}
        </div>
        <div className="col-md-6"></div>
      </div>
    </div>
  );
}
