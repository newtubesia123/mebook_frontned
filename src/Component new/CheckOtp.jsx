import image from "../mepic.png";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useLocation } from "react-router-dom";
import './CheckOtp.css';
import { BackGround, newButton } from "./background";
import { SERVER } from "../server/server";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import MeLogo from "../assets/MeLogoMain";

export default function CheckOtp() {

  const bg = { ...BackGround, minHeight: "100vh", display: "flex", flexDirection: "column", alignItems: "center", gap: "1rem" }

  function getCodeBoxElement(index) {
    return document.getElementById('codeBox' + index);
  }
  function onKeyUpEvent(index, event) {
    const eventCode = event.which || event.keyCode;
    if (getCodeBoxElement(index).value.length === 1) {
      if (index !== 7) {
        getCodeBoxElement(index + 1).focus();
      } else {
        getCodeBoxElement(index).blur();
        // Submit code
        console.log('submit code ');
      }
    }
    if (eventCode === 8 && index !== 1) {
      getCodeBoxElement(index - 1).focus();
    }
  }
  function onFocusEvent(index) {
    for (let item = 1; item < index; item++) {
      const currentElement = getCodeBoxElement(item);
      if (!currentElement.value) {
        currentElement.focus();
        break;
      }
    }
  }
  const navigate = useNavigate();
  const [inputValue1, setInputValue1] = useState('');
  const [inputValue2, setInputValue2] = useState('');
  const [inputValue3, setInputValue3] = useState('');
  const [inputValue4, setInputValue4] = useState('');
  const [inputValue5, setInputValue5] = useState('');
  const [inputValue6, setInputValue6] = useState('');
  let otp = '' + inputValue1 + inputValue2 + inputValue3 + inputValue4 + inputValue5 + inputValue6;
  console.log('otp', otp)
  // const handleChange = (e) => {
  //   setInputValue({
  //         ...inputValue,
  //         [e.target.name]: e.target.value,
  //     });
  // };
  const handleSubmit = async (e) => {
    e.preventDefault();
    await axios
      .post(`${SERVER}/checkOtp`, { otp: otp})
      .then((res) => {
        // console.log("data->", emailData);
        if (res.data.success) {
          toast.success(res.data.success);
          navigate("/ResetPassword");
          // sessionStorage.setItem("key", id);
        } else {
          toast.error(res.data.error);
        }
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className='text-center' style={bg}>
      <div>
        {/* <img src={image} width='10%' height='10%' style={{ margin: '50px auto' }} alt="" /> */}
        <MeLogo />
      </div>
      <h1 style={{ color: "white", fontSize: "150%" }}> <b> One-time Password Verification Code </b></h1>
      <form className='my-5' onSubmit={handleSubmit} >
        <div>
          <input id="codeBox1" type="number" name="email" value={inputValue1} onChange={(e) => setInputValue1(e.target.value)} onKeyUp={(event) => onKeyUpEvent(1, event)} onFocus={() => onFocusEvent(1)} />
          <input id="codeBox2" type="number" name="email" value={inputValue2} onChange={(e) => setInputValue2(e.target.value)} onKeyUp={(event) => onKeyUpEvent(2, event)} onFocus={() => onFocusEvent(2)} />
          <input id="codeBox3" type="number" name="email" value={inputValue3} onChange={(e) => setInputValue3(e.target.value)} onKeyUp={(event) => onKeyUpEvent(3, event)} onFocus={() => onFocusEvent(3)} />
          <input id="codeBox4" type="number" name="email" value={inputValue4} onChange={(e) => setInputValue4(e.target.value)} onKeyUp={(event) => onKeyUpEvent(4, event)} onFocus={() => onFocusEvent(4)} />
          <input id="codeBox5" type="number" name="email" value={inputValue5} onChange={(e) => setInputValue5(e.target.value)} onKeyUp={(event) => onKeyUpEvent(5, event)} onFocus={() => onFocusEvent(5)} />
          <input id="codeBox6" type="number" name="email" value={inputValue6} onChange={(e) => setInputValue6(e.target.value)} onKeyUp={(event) => onKeyUpEvent(6, event)} onFocus={() => onFocusEvent(6)} />
        </div>
        <div>
          <Button sx={newButton} varient="contained" size="large" type="submit">
            Submit
          </Button>
        </div>
      </form>
    </div>
  )
}
