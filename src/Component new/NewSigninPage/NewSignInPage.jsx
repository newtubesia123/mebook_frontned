import React, { useRef } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import img1 from '../../MeBookMeta First Page Image.jpg'
import MeLogo from '../../assets/MeLogoMain';
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { newButton } from "../background";
import { toast } from "react-toastify";
import './NewSigninPage.css'

import { SERVER } from "../../server/server";
import {
    getMusicSubCategory,
    getUserData,
    userDashboardDetail,
    userProfile,
} from "../../Action";
import BackBtn from "../../components/componentsB/btn/backBtn/BackBtn";
import { SOCKET } from "../../App";

const cardLoginDiv = { display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }

export default function NewSignInPage() {

    const navigate = useNavigate();
    const LoginInputData = useRef();
    const dispatch = useDispatch();
    const STORE = useSelector((state) => state);

    if (STORE.errorPage) {
        navigate("/ErrorPage");
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        dispatch({ type: "CALLING__PROGRESS", payload: 10 });

        const LoginData = {
            email: LoginInputData.current.email.value,
            password: LoginInputData.current.password.value,
        };
        e.target.reset();
        await axios
            .post(`${SERVER}/login`, { LoginData: LoginData })
            .then((res) => {
                dispatch({ type: "COMPLETE__PROGRESS", payload: 100 });
                dispatch({ type: "NO_ERROR" });
                if (res.data.data?.isVerified === true) {
                    toast.success(res.data.success, {
                        position: "top-center",
                        autoClose: 500,
                    });
                    if (res.data.data.metaProfile && res.data.data.identityProfile) {
                        dispatch(userDashboardDetail(res.data.token));
                        dispatch(userProfile(res.data.data._id));
                        dispatch(getUserData(res.data.token));
                        dispatch({ type: "GET_DATA", payload: res.data });
                        setTimeout(() => {
                            navigate("/MainDashboard");
                        }, 500);
                    } else {
                        dispatch(getMusicSubCategory("Music"));
                        navigate("/ProfileDetails");
                    }
                    localStorage.setItem("token", JSON.stringify(res.data.token));
                    localStorage.setItem("userData", JSON.stringify(res.data?.data));
                    SOCKET.timeout(2000).emit("add-user",{ownId:res.data?.data?._id,id:SOCKET.id})
                    sessionStorage.setItem("userName", res.data.data.name);
                } else if (res.data.data?.isVerified === false) {
                    toast.success("Please Verified Your Email id Once", {
                        position: "top-center",
                        autoClose: 3000,
                        pauseOnHover: false,
                    });
                    navigate(`/ConfirmMail/${LoginData.email}`);
                } else if (res.data?.error) {
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 1000,
                    });
                } else if (res.data?.Error) {
                    toast.error(res.data.Error, {
                        position: "top-right",
                        autoClose: 1000,
                    });
                }
            })
            .catch((err) => {
                dispatch({ type: "GOT_ERROR" });
            });
    };

    return (
        <>
            <div className="main-login">
                <div className="left-login">
                    <div
                        className="mt-3 tagname"
                        style={{ fontFamily: "Times New Roman", fontSize: "3rem" }}
                    >
                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                        <span style={{ color: "#0a0a8a" }}>eBook</span>
                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                        <span style={{ color: "#0a0a8a" }}>eta</span>
                    </div>
                    <h5
                        style={{
                            fontFamily: "Times New Roman",
                            fontWeight: "bold",
                            color: "white",
                            fontSize: "1.5rem", textAlign: "center",
                            marginBottom: '1rem'
                        }}
                    >
                        Welcome to the Global Media Marketplace
                    </h5>
                    <img src={img1} className="left-login-image" alt={img1} />
                </div>
                <div className="right-login">
                    <form className="card-login" ref={LoginInputData} onSubmit={handleSubmit} style={cardLoginDiv}
                    >
                        <div className='ml-auto'><BackBtn style={{ margin: '1rem' }} /></div>
                        <MeLogo />
                        <div className="textfield">
                            <label htmlFor="usuario">Email</label>
                            <input type="email" placeholder="Enter your Email "
                                name="email" id="email"
                            />
                        </div>
                        <div className="textfield">
                            <label htmlFor="password">Password</label>
                            <input type="password" placeholder="Enter your Password "
                                name="password" id="password"
                            />
                        </div>
                        <Button type="submit" value="Send" className="btn-login" style={{ ...newButton, marginTop: '5px' }}>Login</Button>

                        <p id="fp" onClick={() => navigate("/ForgotPassword")}>Forget password ?</p>
                        <p id="fp">Don't have an account ? <span style={{ color: 'lightblue', marginLeft: '0.2rem' }} onClick={() => {
                            sessionStorage.removeItem("confirm");
                            sessionStorage.removeItem("verification");
                            navigate(`/Signup`)
                        }}>SignUp</span>
                        </p>
                    </form>
                </div>
            </div>
        </>
    )
}