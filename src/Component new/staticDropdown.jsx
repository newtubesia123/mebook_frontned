import React, { useState, useEffect } from "react";
import axios from "axios";
import image from '../mepic.png'
import CardContent from "@mui/material/CardContent";
import { Link, useNavigate } from "react-router-dom";
import WriterNav from "../Component new/WriterNav";
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getCreatorData, getSubCategory } from '../Action/index';
import { BackGround, newButton } from "../Component new/background";
import Button from "@mui/material/Button";
import ButtonGroup from '@mui/material/ButtonGroup';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';

const options = ['Create a merge commit', 'Squash and merge', 'Rebase and merge'];


export default function Dropdown() {

    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);
    const [selectedIndex, setSelectedIndex] = React.useState(1);

    const handleClick = () => {
        console.info(`You clicked ${options[selectedIndex]}`);
    };

    const handleMenuItemClick = (event, index) => {
        setSelectedIndex(index);
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    const bg = { ...BackGround, minHeight: "100vh" };
    const bgBut = {
        ...newButton,  color: 'black',
        borderRadius: "10px", fontFamily: "Times New Roman", color: "white"
    };


    return (
        <div className="container-fluid MebookMetaCreator" style={bg}>
            <WriterNav />
            <div className="img text-center">
                <img
                    src={image}
                    alt=""
                    style={{
                        width: "232.5px",
                        height: "131px",
                        margin: "5px 0px 0px",
                    }}
                />
            </div>
            <div className="row">
                <div className=" col-md-6 offset-md-3 text-center ">
                    <div
                        id="font"
                        style={{
                            margin: "5px 0px",
                            fontSize: "2.5vw",
                            fontWeight: "bold",
                            textAlign: "center",
                            color: "white"
                        }}
                    >
                        hi  Please Setup Your <span style={{}}>M</span><span style={{}}>eBook</span><span style={{}}>M</span><span style={{}}>eta</span> Profile
                    </div>
                </div>
            </div>
            <div className="row">
                <div className=" col-md-6 offset-md-3">
                    <ButtonGroup variant="contained" ref={anchorRef} aria-label="split button">
                        <Button onClick={handleClick} sx={bgBut}>{options[selectedIndex]}</Button>
                        <Button
                            size="large" sx={bgBut}
                            aria-controls={open ? 'split-button-menu' : undefined}
                            aria-expanded={open ? 'true' : undefined}
                            aria-label="select merge strategy"
                            aria-haspopup="menu"
                            onClick={handleToggle}
                        >
                            <ArrowDropDownIcon />
                        </Button>
                    </ButtonGroup>
                    <Popper
                        sx={{
                            zIndex: 1,
                        }}
                        open={open}
                        anchorEl={anchorRef.current}
                        role={undefined}
                        transition
                        disablePortal
                    >
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{
                                    transformOrigin:
                                        placement === 'bottom' ? 'center top' : 'center bottom',
                                }}
                            >
                                <Paper>
                                    <ClickAwayListener onClickAway={handleClose}>
                                        <MenuList id="split-button-menu" autoFocusItem>
                                            {options.map((option, index) => (
                                                <MenuItem
                                                    key={option}
                                                    disabled={index === 2}
                                                    selected={index === selectedIndex}
                                                    onClick={(event) => handleMenuItemClick(event, index)}
                                                >
                                                    {option}
                                                </MenuItem>
                                            ))}
                                        </MenuList>
                                    </ClickAwayListener>
                                </Paper>
                            </Grow>
                        )}
                    </Popper>
                    <div className="d-flex justify-content-between mt-3 mb-3">
                    </div>
                </div>
            </div>
        </div >
    );
}
