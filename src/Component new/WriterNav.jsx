import React, { useState } from "react";
import { useDispatch, } from "react-redux";
import { toast } from "react-toastify";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Pic from "../Screenshot_2.png";
import Tooltip from "@mui/material/Tooltip";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import { deepPurple } from "@mui/material/colors";
import { useNavigate, } from "react-router-dom";

export default function WriterNav() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const [isLogout, setIsLogout] = useState(false);
  if (isLogout) {
    localStorage.removeItem("token");
    toast.success("Logout Successfully");
    navigate("/SignIn");
  }
  // if (isLogout) {
  //   localStorage.removeItem("token");
  //   dispatch({ type: "LOG_OUT" });
  //   dispatch({ type: "CLEAR_USER_DATA" });
  //   dispatch({ type: "CLEAR_DASHBOARD" });
  //   setTimeout(() => {
  //     toast.success("Logout Successfully", {
  //       position: "bottom-right",
  //       autoClose: 1000,
  //       pauseOnHover: false,
  //     });

  //     navigate("/SignIn");
  //     window.location.reload();
  //   }, 500);
  // }
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [anchorProfile, setAnchorProfile] = React.useState(null);
  const openProfile = Boolean(anchorProfile);
  const handleClickProfile = (event) => {
    setAnchorProfile(event.currentTarget);
  };
  const handleCloseProfile = () => {
    setAnchorProfile(null);
  };
  // const auth = localStorage.getItem("token");
  // const logOut = () => {
  //   localStorage.clear(auth);
  //   navigate("/SignIn");
  // };
  let userName = sessionStorage.getItem("userName");
  let letter = userName.substring(0, 1);
  return (
    <div>
      <Box sx={{ flexGrow: 1, mb:8 }}>
        <AppBar
          elevation
          position="fixed"
          sx={{
            backgroundColor: "white",
            borderBottom: " 1px solid lightgray",
          }}
        >
          <Toolbar>
            <IconButton
              size="small"
              edge="start"
              // color="white"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <img src={Pic} width="200px" alt="" />
            </IconButton>
            {/* <Tooltip>
                            <Button
                                sx={{ color: 'black', ml: 'auto' }}
                                id="demo-positioned-button"
                                aria-controls={open ? 'demo-positioned-menu' : undefined}
                                aria-haspopup="true"
                                aria-expanded={open ? 'true' : undefined}
                                onClick={handleClick}
                                endIcon={<ArrowDropDownIcon />}
                            >
                                Write
                            </Button>
                        </Tooltip>
                        <Menu
                            id="demo-positioned-menu"
                            aria-labelledby="demo-positioned-button"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            PaperProps={{
                                elevation: 0,
                                sx: {
                                    overflow: 'visible',
                                    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',

                                    '& .MuiAvatar-root': {
                                        width: 32,
                                        height: 32,
                                        ml: -0.5,
                                        mr: 1,
                                    },
                                    '&:before': {
                                        content: '""',
                                        display: 'block',
                                        position: 'absolute',
                                        top: 0,
                                        right: 14,
                                        width: 10,
                                        height: 10,
                                        bgcolor: 'background.paper',
                                        transform: 'translateY(-50%) rotate(45deg)',
                                        zIndex: 0,
                                    },
                                },
                            }}
                            transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                        >
                            <MenuItem onClick={handleClose}>Create a new story</MenuItem>
                            <MenuItem onClick={handleClose}>My Stories</MenuItem>

                        </Menu> */}
            <Tooltip>
              <Button
                onClick={handleClickProfile}
                size="small"
                sx={{ fontWeight: "bolder", }}
                aria-controls={openProfile ? "demo-positioned-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={openProfile ? "true" : undefined}
                // endIcon={<ArrowDropDownIcon />}
              >
                <Avatar sx={{ bgcolor: deepPurple[500], mr: 1, }}>
                  {letter}
                </Avatar>
                {userName}
              </Button>
            </Tooltip>
            {/* <Menu
              anchorProfile={anchorProfile}
              id="demo-positioned-menu"
              aria-labelledby="demo-positioned-button"
              open={openProfile}
              onClose={handleCloseProfile}
              onClick={handleCloseProfile}
              PaperProps={{
                elevation: 0,
                sx: {
                  overflow: "visible",
                  filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",

                  "& .MuiAvatar-root": {
                    width: 32,
                    height: 32,
                    ml: -0.5,
                    mr: 1,
                  },
                  "&:before": {
                    content: '""',
                    display: "block",
                    position: "absolute",
                    top: 0,
                    right: 14,
                    width: 10,
                    height: 10,
                    bgcolor: "background.paper",
                    transform: "translateY(-50%) rotate(45deg)",
                    zIndex: 0,
                  },
                },
              }}
              transformOrigin={{ horizontal: "right", vertical: "top" }}
              anchorOrigin={{ horizontal: "right", vertical: "top" }}
            >
              {/* <MenuItem>
                <Avatar /> Profile
              </MenuItem>
              <MenuItem>
                <Avatar /> My account
              </MenuItem>
              <Divider />
              <MenuItem>
                <ListItemIcon>
                  <PersonAdd fontSize="small" />
                </ListItemIcon>
                Add another account
              </MenuItem>
              <MenuItem>
                <ListItemIcon>
                  <Settings fontSize="small" />
                </ListItemIcon>
                Settings1
              </MenuItem>
              <MenuItem>
                <ListItemIcon>
                  <Logout fontSize="small" />
                </ListItemIcon>
                Logout
              </MenuItem> */}
            {/* </Menu>  */}
            {/* <Fab
              size="medium"
              color="primary"
              aria-label="add"
              sx={{ backgroundColor: "#2a1c60" }}
            >
              Log
            </Fab> */}
            <button
              type="submit"
              // className="profile-card__button button--orange"
              style={{
                backgroundColor: "#2a1c60",
                color: "white",
                borderRadius: "48px",
                width: "76px",
                fontSize:"80%"
              }}
              onClick={() => setIsLogout(true)}
            >
              logout
            </button>
          </Toolbar>
        </AppBar>
      </Box>
    </div>
  );
}
