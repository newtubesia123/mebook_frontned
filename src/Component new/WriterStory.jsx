import React, { useState, useEffect } from "react";
import axios from "axios";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import StarOutlinedIcon from '@mui/icons-material/StarOutlined';
import ModeCommentIcon from '@mui/icons-material/ModeComment';
import WriterNav from './WriterNav'
import { Link, useNavigate, useLocation } from "react-router-dom";
import { BackGround, newButton } from "./background";

export default function WriterStory() {
    let userId = sessionStorage.getItem("writerId")
    const navigate = useNavigate();

    const [WriterPage, setWriterPage] = useState([]);
    const bg = { ...BackGround, minHeight: '90vh' }
    useEffect(() => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios
            .get("http://54.246.61.54:3002/getUserWork",
                {
                    headers: { Authorization: `Bearer ${token}` }
                }
            )
            .then((res) => {
                // console.log("token--->", id);
                console.log("res.data---> ", res.data.writerDetail);
                setWriterPage(res.data.writerDetail)
            })
            .catch((err) => console.log(err));
    }, []);
    const handleDelete = (id) => {
        console.log('id', id)
        axios.post("http://54.246.61.54:3002/deleteWork", { id: id })
            .then((res) => {
                alert(res.data.message)
                window.location.reload(false);
            })
    }
    const handleView = (id) => {
        navigate('/ViewPublishedStory', { state: { userid: id } })
    }
    const handleEdit = (id) => {
        navigate('/UpdatePublishedStory', { state: { userid: id } })
    }
    var date = new Date()
    return (
        <>
            <WriterNav />
            <div className='container-fluid' style={bg}>
                <div className="row">
                    <div className="col-md-8 mt-5">
                        <div className='d-flex justify-content-between '>
                            <h1> <b className="text-centre" style={{ color: "white" }}> My Works </b> </h1>
                            <Link to='/WriterStoryDetails'> <Button style={{ textTransform: 'none' }} sx={newButton} variant="contained">Create New</Button></Link>

                        </div>
                        {WriterPage.map((index) => {
                            return (
                                <Card sx={{ minWidth: 275, my: 3 }}>
                                    <CardContent key={index} >
                                        <h4><b>Published work</b></h4>
                                        <div className="row d-flex ">
                                            <div className="col-md-2"><img src={`http://54.246.61.54:3002/uploads/${index.coverPicture}`} width={90} height={130} alt="" /> </div>
                                            <div className="col-md-5">
                                                <h6> <b> {index.titleStory} </b></h6>
                                                <h6><b>1 published part</b></h6>
                                                <p>updated  hrs ago</p>
                                                <div style={{ fontSize: '10px' }}>
                                                    <RemoveRedEyeOutlinedIcon sx={{ fontSize: "15px", color: 'gray' }} /> 112
                                                    <span style={{ margin: ' 0px 10px' }}> <StarOutlinedIcon sx={{ fontSize: "15px", color: 'gray' }} />12 </span>
                                                    <ModeCommentIcon sx={{ fontSize: "15px", color: 'gray' }} />01
                                                </div>
                                            </div>
                                            <div className="col-md-2"></div>
                                            <div className="col-md-3 text-center ">
                                                <Button sx={{ textTransform: 'none', m: 1 }} variant="contained" onClick={() => handleEdit(index._id)}> Continue Writing</Button>
                                                <button type='button' class="btn btn-danger" onClick={() => handleDelete(index._id)}> Delete  </button>
                                                <button type='button' className="btn btn-info my-2" onClick={() => handleView(index._id)}> View Page  </button>
                                            </div>
                                            {/* <MoreHorizIcon /> */}
                                        </div>
                                    </CardContent>
                                </Card>
                            )
                        })}
                    </div>
                    <div className="col-md-4"></div>
                </div>

            </div>
        </>
    )
}
