const initialState = {
    userWorkDetail: false,
    loading: false
}

export const userWorkDetail = (state = initialState, action) => {
    switch (action.type) {
        case "CALLING_USER_WORK_DETAIL":
            return {
                ...state,
                loading: true
            }
        case "GETTING_USER_WORK_DETAIL":
            return {
                ...state,
                loading: false,
                userWorkDetail: action.payload,
            }
        default:
            return state
    }
}
