

export const callPopUp = (state = { showPop: false, PopType: "SuccessPayment" }, action) => {

    switch (action.type) {
        case "CALL_POPUP":

            if (action.payload.type === "signin") {
                return { showPop: true, PopType: action.payload.type }
            } else if (action.payload.type === "login") {
                return { showPop: true, PopType: action.payload.type }
            } else if (action.payload.type === "SuccessPayment") {
                return { showPop: true, PopType: action.payload.type }
            } else if (action.payload.type === "makeSubscription") {
                return { showPop: true, PopType: action.payload.type }
            } else if (action.payload.type === 'CouponCheck') {
                return { showPop: 'true', PopType: action.payload.type }
            } else if (action.payload.type === 'successPayment') {
                return { showPop: 'true', PopType: action.payload.type }
            } else if (action.payload.type === 'workDelete') {
                return { showPop: 'true', PopType: action.payload.type }
            } else if (action.payload.type === 'pitchDelete') {
                return { showPop: 'true', PopType: action.payload.type }
            } else if (action.payload.type === 'askDelete') {
                return { showPop: 'true', PopType: action.payload.type }
            } else if (action.payload.type === 'CancelSubscription') {
                return { showPop: 'true', PopType: action.payload.type }
            }
            else if (action.payload.type === 'ShowProfile') {
                return { showPop: 'true', PopType: action.payload.type }
            }
            else if (action.payload.type === 'ShowPitch') {
                return { showPop: 'true', PopType: action.payload.type }
            } else if (action.payload.type === 'ShowFollowers') {
                return { showPop: 'true', PopType: action.payload.type, data: action.payload.data }
            } else if (action.payload.type === 'ShowFPTags') {
                return { showPop: 'true', PopType: action.payload.type, data: action.payload.data }
            }
            else {
                return { showPop: false, PopType: action.payload.type }
            }

        case "REMOVE_POPUP":
            return { showPop: false, PopType: "SuccessPayment" }
        default:
            return state
    }
}
