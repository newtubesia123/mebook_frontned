
import { useSelector } from "react-redux";

export function useRecognization (userId) {
   const STORE = useSelector((state)=>state)
   return userId === STORE.getProfileData?.userData?._id
}