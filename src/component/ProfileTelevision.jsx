import React, { useState, useEffect } from "react";
import axios from "axios";
import Popover from '@mui/material/Popover';

// import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link, useNavigate } from "react-router-dom";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { CardActionArea } from "@mui/material";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MenuItem from '@mui/material/MenuItem';
import ListSubheader from '@mui/material/ListSubheader';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { BackGround, newButton } from "../Component new/background";

import "./common.css";
import img from "../Screenshot_2.png";
import { Cascader } from 'antd';
const { SHOW_CHILD } = Cascader;

const treeData = [

  {
    label: <b style={{ fontSize: '25px' }}>Actor</b>,
    value: 'Actor',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Drama',
            value: 'Drama',
            key: '0-0-1-1',
          },
          {
            label: 'Comedy',
            value: 'Comedy',
            key: '0-0-1-2',
          },
          {
            label: 'Thriller/Action/Suspense',
            value: 'Thriller/Action/Suspense',
            key: '0-0-1-3',
          },
          {
            label: 'Detective/Mystery',
            value: 'Detective/Mystery',
            key: '0-0-1-4',
          },
          {
            label: 'Horror',
            value: 'Horror',
            key: '0-0-1-5',
          },
          {
            label: 'Documentary',
            value: 'Documentary',
            key: '0-0-1-6',
          },
          {
            label: 'Franchise',
            value: 'Franchise',
            key: '0-0-1-7',

          },
          {
            label: 'Biographical',
            value: 'Biographical',
            key: '0-0-1-8',
          },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Television',
        key: '0-0-1',
        children: [
          {
            label: 'Drama',
            value: 'Drama',
            key: '0-0-1-1',
          },
          {
            label: 'Comedy',
            value: 'Comedy',
            key: '0-0-1-2',
          },
          {
            label: 'Thriller/Action/Suspense',
            value: 'Thriller/Action/Suspense',
            key: '0-0-1-3',
          },
          {
            label: 'Detective/Mystery',
            value: 'Detective/Mystery',
            key: '0-0-1-4',
          },
          {
            label: 'Horror',
            value: 'Horror',
            key: '0-0-1-5',
          },
          {
            label: 'Documentary',
            value: 'Documentary',
            key: '0-0-1-6',
          },
          {
            label: 'Franchise',
            value: 'Franchise',
            key: '0-0-1-7',

          },
          {
            label: 'Biographical',
            value: 'Biographical',
            key: '0-0-1-8',
          },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Television',
        key: '0-0-1',
        children: [
          {
            label: 'Drama',
            value: 'Drama',
            key: '0-0-1-1',
          },
          {
            label: 'Comedy',
            value: 'Comedy',
            key: '0-0-1-2',
          },
          {
            label: 'Thriller/Action/Suspense',
            value: 'Thriller/Action/Suspense',
            key: '0-0-1-3',
          },
          {
            label: 'Detective/Mystery',
            value: 'Detective/Mystery',
            key: '0-0-1-4',
          },
          {
            label: 'Horror',
            value: 'Horror',
            key: '0-0-1-5',
          },
          {
            label: 'Documentary',
            value: 'Documentary',
            key: '0-0-1-6',
          },
          {
            label: 'Franchise',
            value: 'Franchise',
            key: '0-0-1-7',

          },
          {
            label: 'Biographical',
            value: 'Biographical',
            key: '0-0-1-8',
          },

        ],
      },

    ],
  },
];
const Animation = [
  { label: 'Traditional/Cel', value: 'Traditional/Cel' },
  { label: '2D/Vector-Based', value: '2D/Vector-Based' },
  { label: '3D Animation', value: '3D Animation' },
  { label: 'Stop Motion', value: 'Stop Motion' },
  { label: 'Motion Graphics', value: 'Motion Graphics' },

  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Camera = [

  {
    label: <b style={{ fontSize: '25px' }}>Camera Related</b>,
    value: 'Camera Related',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Director of Photography (Cinematographer)',
            value: 'Director of Photography (Cinematographer)',
            key: '0-0-1-1',
          },
          {
            label: 'Camera Operator',
            value: 'Camera Operator',
            key: '0-0-1-2',
          },
          {
            label: '1st Assistant Camera',
            value: '1st Assistant Camera',
            key: '0-0-1-3',
          },
          {
            label: '2nd Assistant Camera',
            value: '2nd Assistant Camera',
            key: '0-0-1-4',
          },
          {
            label: 'Freelance Camera Operator',
            value: 'Freelance Camera Operator',
            key: '0-0-1-5',
          },
          {
            label: 'Digital Imaging Technician (DIT)',
            value: 'Digital Imaging Technician (DIT)',
            key: '0-0-1-6',
          },
          {
            label: 'Camera Intern',
            value: 'Camera Intern',
            key: '0-0-1-7',

          },
          {
            label: 'Rental Agent',
            value: 'Rental Agent',
            key: '0-0-1-8',
          },
          {
            label: ' Production Crew',
            value: ' Production Crew',
            key: '0-0-1-8',
          },
          {
            label: ' Loader',
            value: ' Loader',
            key: '0-0-1-8',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Director of Photography (Cinematographer)',
            value: 'Director of Photography (Cinematographer)',
            key: '0-0-1-1',
          },
          {
            label: 'Camera Operator',
            value: 'Camera Operator',
            key: '0-0-1-2',
          },
          {
            label: '1st Assistant Camera',
            value: '1st Assistant Camera',
            key: '0-0-1-3',
          },
          {
            label: '2nd Assistant Camera',
            value: '2nd Assistant Camera',
            key: '0-0-1-4',
          },
          {
            label: 'Freelance Camera Operator',
            value: 'Freelance Camera Operator',
            key: '0-0-1-5',
          },
          {
            label: 'Digital Imaging Technician (DIT)',
            value: 'Digital Imaging Technician (DIT)',
            key: '0-0-1-6',
          },
          {
            label: 'Camera Intern',
            value: 'Camera Intern',
            key: '0-0-1-7',

          },
          {
            label: 'Rental Agent',
            value: 'Rental Agent',
            key: '0-0-1-8',
          },
          {
            label: ' Production Crew',
            value: ' Production Crew',
            key: '0-0-1-8',
          },
          {
            label: ' Loader',
            value: ' Loader',
            key: '0-0-1-8',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Director of Photography (Cinematographer)',
            value: 'Director of Photography (Cinematographer)',
            key: '0-0-1-1',
          },
          {
            label: 'Camera Operator',
            value: 'Camera Operator',
            key: '0-0-1-2',
          },
          {
            label: '1st Assistant Camera',
            value: '1st Assistant Camera',
            key: '0-0-1-3',
          },
          {
            label: '2nd Assistant Camera',
            value: '2nd Assistant Camera',
            key: '0-0-1-4',
          },
          {
            label: 'Freelance Camera Operator',
            value: 'Freelance Camera Operator',
            key: '0-0-1-5',
          },
          {
            label: 'Digital Imaging Technician (DIT)',
            value: 'Digital Imaging Technician (DIT)',
            key: '0-0-1-6',
          },
          {
            label: 'Camera Intern',
            value: 'Camera Intern',
            key: '0-0-1-7',

          },
          {
            label: 'Rental Agent',
            value: 'Rental Agent',
            key: '0-0-1-8',
          },
          {
            label: ' Production Crew',
            value: ' Production Crew',
            key: '0-0-1-8',
          },
          {
            label: ' Loader',
            value: ' Loader',
            key: '0-0-1-8',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
    ],
  },
];
const Director = [

  {
    label: <b style={{ fontSize: '25px' }}>Director</b>,
    value: 'Director',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Director ',
            value: 'Director ',
            key: '0-0-1-1',
          },
          {
            label: '1st Assistant Director',
            value: '1st Assistant Director',
            key: '0-0-1-2',
          },
          {
            label: '2nd Assistant Director',
            value: '2nd Assistant Director',
            key: '0-0-1-3',
          },
          {
            label: '2nd 2nd Assistant Director',
            value: '2nd 2nd Assistant Director',
            key: '0-0-1-4',
          },
          {
            label: 'Script Supervisor',
            value: 'Script Supervisor',
            key: '0-0-1-5',
          },
          {
            label: 'Technical Director',
            value: 'Technical Director',
            key: '0-0-1-6',
          },
          {
            label: 'Performance Director',
            value: 'Performance Director',
            key: '0-0-1-7',

          },
          {
            label: 'Arts and Crafts Director',
            value: 'Arts and Crafts Director',
            key: '0-0-1-8',
          },
          {
            label: ' Casting Director',
            value: ' Casting Director',
            key: '0-0-1-8',
          },
          {
            label: ' Still Photographer',
            value: ' Still Photographer',
            key: '0-0-1-8',
          },
          {
            label: ' Production Assistant',
            value: ' Production Assistant',
            key: '0-0-1-8',
          },
          {
            label: ' Location Manager',
            value: ' Location Manager',
            key: '0-0-1-8',
          },
          {
            label: ' Accountant',
            value: ' Accountant',
            key: '0-0-1-8',
          },
          {
            label: '  Teacher/Educator',
            value: '  Teacher/Educator',
            key: '0-0-1-8',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Director ',
            value: 'Director ',
            key: '0-0-1-1',
          },
          {
            label: '1st Assistant Director',
            value: '1st Assistant Director',
            key: '0-0-1-2',
          },
          {
            label: '2nd Assistant Director',
            value: '2nd Assistant Director',
            key: '0-0-1-3',
          },
          {
            label: '2nd 2nd Assistant Director',
            value: '2nd 2nd Assistant Director',
            key: '0-0-1-4',
          },
          {
            label: 'Script Supervisor',
            value: 'Script Supervisor',
            key: '0-0-1-5',
          },
          {
            label: 'Technical Director',
            value: 'Technical Director',
            key: '0-0-1-6',
          },
          {
            label: 'Performance Director',
            value: 'Performance Director',
            key: '0-0-1-7',

          },
          {
            label: 'Arts and Crafts Director',
            value: 'Arts and Crafts Director',
            key: '0-0-1-8',
          },
          {
            label: ' Casting Director',
            value: ' Casting Director',
            key: '0-0-1-8',
          },
          {
            label: ' Still Photographer',
            value: ' Still Photographer',
            key: '0-0-1-8',
          },
          {
            label: ' Production Assistant',
            value: ' Production Assistant',
            key: '0-0-1-8',
          },
          {
            label: ' Location Manager',
            value: ' Location Manager',
            key: '0-0-1-8',
          },
          {
            label: ' Accountant',
            value: ' Accountant',
            key: '0-0-1-8',
          },
          {
            label: '  Teacher/Educator',
            value: '  Teacher/Educator',
            key: '0-0-1-8',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },

      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Director ',
            value: 'Director ',
            key: '0-0-1-1',
          },
          {
            label: '1st Assistant Director',
            value: '1st Assistant Director',
            key: '0-0-1-2',
          },
          {
            label: '2nd Assistant Director',
            value: '2nd Assistant Director',
            key: '0-0-1-3',
          },
          {
            label: '2nd 2nd Assistant Director',
            value: '2nd 2nd Assistant Director',
            key: '0-0-1-4',
          },
          {
            label: 'Script Supervisor',
            value: 'Script Supervisor',
            key: '0-0-1-5',
          },
          {
            label: 'Technical Director',
            value: 'Technical Director',
            key: '0-0-1-6',
          },
          {
            label: 'Performance Director',
            value: 'Performance Director',
            key: '0-0-1-7',

          },
          {
            label: 'Arts and Crafts Director',
            value: 'Arts and Crafts Director',
            key: '0-0-1-8',
          },
          {
            label: ' Casting Director',
            value: ' Casting Director',
            key: '0-0-1-8',
          },
          {
            label: ' Still Photographer',
            value: ' Still Photographer',
            key: '0-0-1-8',
          },
          {
            label: ' Production Assistant',
            value: ' Production Assistant',
            key: '0-0-1-8',
          },
          {
            label: ' Location Manager',
            value: ' Location Manager',
            key: '0-0-1-8',
          },
          {
            label: ' Accountant',
            value: ' Accountant',
            key: '0-0-1-8',
          },
          {
            label: '  Teacher/Educator',
            value: '  Teacher/Educator',
            key: '0-0-1-8',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },


    ],
  },
];
const Electric = [

  {
    label: <b style={{ fontSize: '25px' }}>Electric Related</b>,
    value: 'Electric Related',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Gaffer ',
            value: 'Gaffer ',
            key: '0-0-1-1',
          },
          {
            label: 'Best Boy Electric (BBE)',
            value: 'Best Boy Electric (BBE)',
            key: '0-0-1-2',
          },
          {
            label: 'Electrical Lighting Technician (ELT)',
            value: 'Electrical Lighting Technician (ELT)',
            key: '0-0-1-3',
          },
          {
            label: 'Generator Operator',
            value: 'Generator Operator',
            key: '0-0-1-4',
          },
          {
            label: 'Rigging Electrician',
            value: 'Rigging Electrician',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Gaffer ',
            value: 'Gaffer ',
            key: '0-0-1-1',
          },
          {
            label: 'Best Boy Electric (BBE)',
            value: 'Best Boy Electric (BBE)',
            key: '0-0-1-2',
          },
          {
            label: 'Electrical Lighting Technician (ELT)',
            value: 'Electrical Lighting Technician (ELT)',
            key: '0-0-1-3',
          },
          {
            label: 'Generator Operator',
            value: 'Generator Operator',
            key: '0-0-1-4',
          },
          {
            label: 'Rigging Electrician',
            value: 'Rigging Electrician',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Gaffer ',
            value: 'Gaffer ',
            key: '0-0-1-1',
          },
          {
            label: 'Best Boy Electric (BBE)',
            value: 'Best Boy Electric (BBE)',
            key: '0-0-1-2',
          },
          {
            label: 'Electrical Lighting Technician (ELT)',
            value: 'Electrical Lighting Technician (ELT)',
            key: '0-0-1-3',
          },
          {
            label: 'Generator Operator',
            value: 'Generator Operator',
            key: '0-0-1-4',
          },
          {
            label: 'Rigging Electrician',
            value: 'Rigging Electrician',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },

    ],
  },
];
const Grip = [

  {
    label: <b style={{ fontSize: '25px' }}>Grip Related</b>,
    value: 'Grip Related',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Key Grip ',
            value: 'Key Grip ',
            key: '0-0-1-1',
          },
          {
            label: 'Best Boy Grip',
            value: 'Best Boy Grip',
            key: '0-0-1-2',
          },
          {
            label: 'Grip',
            value: 'Grip',
            key: '0-0-1-3',
          },
          {
            label: 'Dolly Grip',
            value: 'Dolly Grip',
            key: '0-0-1-4',
          },
          {
            label: 'Swing',
            value: 'Swing',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Key Grip ',
            value: 'Key Grip ',
            key: '0-0-1-1',
          },
          {
            label: 'Best Boy Grip',
            value: 'Best Boy Grip',
            key: '0-0-1-2',
          },
          {
            label: 'Grip',
            value: 'Grip',
            key: '0-0-1-3',
          },
          {
            label: 'Dolly Grip',
            value: 'Dolly Grip',
            key: '0-0-1-4',
          },
          {
            label: 'Swing',
            value: 'Swing',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Key Grip ',
            value: 'Key Grip ',
            key: '0-0-1-1',
          },
          {
            label: 'Best Boy Grip',
            value: 'Best Boy Grip',
            key: '0-0-1-2',
          },
          {
            label: 'Grip',
            value: 'Grip',
            key: '0-0-1-3',
          },
          {
            label: 'Dolly Grip',
            value: 'Dolly Grip',
            key: '0-0-1-4',
          },
          {
            label: 'Swing',
            value: 'Swing',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },

    ],
  },
];
const Hair = [

  {
    label: <b style={{ fontSize: '25px' }}>Hair and Make-Up</b>,
    value: 'Hair and Make-Up',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Key Make-Up Artist ',
            value: 'Key Make-Up Artist ',
            key: '0-0-1-1',
          },
          {
            label: 'Key Hair Stylist',
            value: 'Key Hair Stylist',
            key: '0-0-1-2',
          },
          {
            label: 'Make-Up Assistant',
            value: 'Make-Up Assistant',
            key: '0-0-1-3',
          },
          {
            label: 'Hair Assistant',
            value: 'Hair Assistant',
            key: '0-0-1-4',
          },
          {
            label: 'Special Effects Make-Up Artist',
            value: 'Special Effects Make-Up Artist',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Key Make-Up Artist ',
            value: 'Key Make-Up Artist ',
            key: '0-0-1-1',
          },
          {
            label: 'Key Hair Stylist',
            value: 'Key Hair Stylist',
            key: '0-0-1-2',
          },
          {
            label: 'Make-Up Assistant',
            value: 'Make-Up Assistant',
            key: '0-0-1-3',
          },
          {
            label: 'Hair Assistant',
            value: 'Hair Assistant',
            key: '0-0-1-4',
          },
          {
            label: 'Special Effects Make-Up Artist',
            value: 'Special Effects Make-Up Artist',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Key Make-Up Artist ',
            value: 'Key Make-Up Artist ',
            key: '0-0-1-1',
          },
          {
            label: 'Key Hair Stylist',
            value: 'Key Hair Stylist',
            key: '0-0-1-2',
          },
          {
            label: 'Make-Up Assistant',
            value: 'Make-Up Assistant',
            key: '0-0-1-3',
          },
          {
            label: 'Hair Assistant',
            value: 'Hair Assistant',
            key: '0-0-1-4',
          },
          {
            label: 'Special Effects Make-Up Artist',
            value: 'Special Effects Make-Up Artist',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },

    ],
  },
];
const Wardrobe = [

  {
    label: <b style={{ fontSize: '25px' }}>Wardrobe</b>,
    value: 'Wardrobe',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Costume Designer ',
            value: 'Costume Designer ',
            key: '0-0-1-1',
          },
          {
            label: 'Wardrobe Supervisor',
            value: 'Wardrobe Supervisor',
            key: '0-0-1-2',
          },
          {
            label: 'Set Costumer',
            value: 'Set Costumer',
            key: '0-0-1-3',
          },
          {
            label: 'Costume Coordinator',
            value: 'Costume Coordinator',
            key: '0-0-1-4',
          },
          {
            label: 'Tailor',
            value: 'Tailor',
            key: '0-0-1-5',
          },
          {
            label: 'Shopper',
            value: 'Shopper',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Costume Designer ',
            value: 'Costume Designer ',
            key: '0-0-1-1',
          },
          {
            label: 'Wardrobe Supervisor',
            value: 'Wardrobe Supervisor',
            key: '0-0-1-2',
          },
          {
            label: 'Set Costumer',
            value: 'Set Costumer',
            key: '0-0-1-3',
          },
          {
            label: 'Costume Coordinator',
            value: 'Costume Coordinator',
            key: '0-0-1-4',
          },
          {
            label: 'Tailor',
            value: 'Tailor',
            key: '0-0-1-5',
          },
          {
            label: 'Shopper',
            value: 'Shopper',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Costume Designer ',
            value: 'Costume Designer ',
            key: '0-0-1-1',
          },
          {
            label: 'Wardrobe Supervisor',
            value: 'Wardrobe Supervisor',
            key: '0-0-1-2',
          },
          {
            label: 'Set Costumer',
            value: 'Set Costumer',
            key: '0-0-1-3',
          },
          {
            label: 'Costume Coordinator',
            value: 'Costume Coordinator',
            key: '0-0-1-4',
          },
          {
            label: 'Tailor',
            value: 'Tailor',
            key: '0-0-1-5',
          },
          {
            label: 'Shopper',
            value: 'Shopper',
            key: '0-0-1-5',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
    ],
  },
];
const sound = [

  {
    label: <b style={{ fontSize: '25px' }}>Sound Related</b>,
    value: 'Sound Related',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Production Sound Mixer ',
            value: 'Production Sound Mixer ',
            key: '0-0-1-1',
          },
          {
            label: 'Boom Operator',
            value: 'Boom Operator',
            key: '0-0-1-2',
          },
          {
            label: 'Sound Assistant/Cable',
            value: 'Sound Assistant/Cable',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Production Sound Mixer ',
            value: 'Production Sound Mixer ',
            key: '0-0-1-1',
          },
          {
            label: 'Boom Operator',
            value: 'Boom Operator',
            key: '0-0-1-2',
          },
          {
            label: 'Sound Assistant/Cable',
            value: 'Sound Assistant/Cable',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Production Sound Mixer ',
            value: 'Production Sound Mixer ',
            key: '0-0-1-1',
          },
          {
            label: 'Boom Operator',
            value: 'Boom Operator',
            key: '0-0-1-2',
          },
          {
            label: 'Sound Assistant/Cable',
            value: 'Sound Assistant/Cable',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },

    ],
  },
];
const Stunts = [

  {
    label: <b style={{ fontSize: '25px' }}>Stunts Related</b>,
    value: 'Stunts Related',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Stunt Coordinator ',
            value: 'Stunt Coordinator ',
            key: '0-0-1-1',
          },
          {
            label: 'Stunt Performer',
            value: 'Stunt Performer',
            key: '0-0-1-2',
          },
          {
            label: 'Set Medic',
            value: 'Set Medic',
            key: '0-0-1-3',
          },
          {
            label: 'Pyro',
            value: 'Pyro',
            key: '0-0-1-3',
          },
          {
            label: 'Weapons Super',
            value: 'Weapons Super',
            key: '0-0-1-3',
          },
          {
            label: 'Armorer',
            value: 'Armorer',
            key: '0-0-1-3',
          },
          {
            label: 'Animal Wrangler',
            value: 'Animal Wrangler',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Stunt Coordinator ',
            value: 'Stunt Coordinator ',
            key: '0-0-1-1',
          },
          {
            label: 'Stunt Performer',
            value: 'Stunt Performer',
            key: '0-0-1-2',
          },
          {
            label: 'Set Medic',
            value: 'Set Medic',
            key: '0-0-1-3',
          },
          {
            label: 'Pyro',
            value: 'Pyro',
            key: '0-0-1-3',
          },
          {
            label: 'Weapons Super',
            value: 'Weapons Super',
            key: '0-0-1-3',
          },
          {
            label: 'Armorer',
            value: 'Armorer',
            key: '0-0-1-3',
          },
          {
            label: 'Animal Wrangler',
            value: 'Animal Wrangler',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Stunt Coordinator ',
            value: 'Stunt Coordinator ',
            key: '0-0-1-1',
          },
          {
            label: 'Stunt Performer',
            value: 'Stunt Performer',
            key: '0-0-1-2',
          },
          {
            label: 'Set Medic',
            value: 'Set Medic',
            key: '0-0-1-3',
          },
          {
            label: 'Pyro',
            value: 'Pyro',
            key: '0-0-1-3',
          },
          {
            label: 'Weapons Super',
            value: 'Weapons Super',
            key: '0-0-1-3',
          },
          {
            label: 'Armorer',
            value: 'Armorer',
            key: '0-0-1-3',
          },
          {
            label: 'Animal Wrangler',
            value: 'Animal Wrangler',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
    ],
  },
];
const Special = [

  {
    label: <b style={{ fontSize: '25px' }}>Special FX/Visual FX</b>,
    value: 'Special FX/Visual FX',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'VFX Supervisor ',
            value: 'VFX Supervisor ',
            key: '0-0-1-1',
          },
          {
            label: 'VFX Coordinator ',
            value: 'VFX Coordinator',
            key: '0-0-1-2',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'VFX Supervisor ',
            value: 'VFX Supervisor ',
            key: '0-0-1-1',
          },
          {
            label: 'VFX Coordinator ',
            value: 'VFX Coordinator',
            key: '0-0-1-2',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'VFX Supervisor ',
            value: 'VFX Supervisor ',
            key: '0-0-1-1',
          },
          {
            label: 'VFX Coordinator ',
            value: 'VFX Coordinator',
            key: '0-0-1-2',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
    ],
  },
];
const Production = [

  {
    label: <b style={{ fontSize: '25px' }}>Production Related</b>,
    value: 'Production Related',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Executive Producer ',
            value: 'Executive Producer ',
            key: '0-0-1-1',
          },
          {
            label: 'Producer',
            value: 'Producer',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant to Producer',
            value: 'Assistant to Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Associate Producer',
            value: 'Associate Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Line',
            value: 'Line',
            key: '0-0-1-3',
          },
          {
            label: 'Unit Production Manager (UPM)',
            value: 'Unit Production Manager (UPM)',
            key: '0-0-1-3',
          },
          {
            label: 'Production Coordinator',
            value: 'Production Coordinator',
            key: '0-0-1-3',
          },
          {
            label: 'Set Accountant ',
            value: 'Set Accountant',
            key: '0-0-1-3',
          },
          {
            label: 'Office Production Assistant (PA)',
            value: 'Office Production Assistant (PA)',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Executive Producer ',
            value: 'Executive Producer ',
            key: '0-0-1-1',
          },
          {
            label: 'Producer',
            value: 'Producer',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant to Producer',
            value: 'Assistant to Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Associate Producer',
            value: 'Associate Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Line',
            value: 'Line',
            key: '0-0-1-3',
          },
          {
            label: 'Unit Production Manager (UPM)',
            value: 'Unit Production Manager (UPM)',
            key: '0-0-1-3',
          },
          {
            label: 'Production Coordinator',
            value: 'Production Coordinator',
            key: '0-0-1-3',
          },
          {
            label: 'Set Accountant ',
            value: 'Set Accountant',
            key: '0-0-1-3',
          },
          {
            label: 'Office Production Assistant (PA)',
            value: 'Office Production Assistant (PA)',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Executive Producer ',
            value: 'Executive Producer ',
            key: '0-0-1-1',
          },
          {
            label: 'Producer',
            value: 'Producer',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant to Producer',
            value: 'Assistant to Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Associate Producer',
            value: 'Associate Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Line',
            value: 'Line',
            key: '0-0-1-3',
          },
          {
            label: 'Unit Production Manager (UPM)',
            value: 'Unit Production Manager (UPM)',
            key: '0-0-1-3',
          },
          {
            label: 'Production Coordinator',
            value: 'Production Coordinator',
            key: '0-0-1-3',
          },
          {
            label: 'Set Accountant ',
            value: 'Set Accountant',
            key: '0-0-1-3',
          },
          {
            label: 'Office Production Assistant (PA)',
            value: 'Office Production Assistant (PA)',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
    ],
  },
];
const Props = [

  {
    label: <b style={{ fontSize: '25px' }}>Props Related</b>,
    value: 'Props Related',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Prop Master ',
            value: 'Prop Master ',
            key: '0-0-1-1',
          },
          {
            label: 'Assistant Props',
            value: 'Assistant Props',
            key: '0-0-1-2',
          },
          {
            label: 'Weapons Wrangler',
            value: 'Weapons Wrangler',
            key: '0-0-1-3',
          },
          {
            label: 'Greensman',
            value: 'Greensman',
            key: '0-0-1-3',
          },
          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Executive Producer ',
            value: 'Executive Producer ',
            key: '0-0-1-1',
          },
          {
            label: 'Producer',
            value: 'Producer',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant to Producer',
            value: 'Assistant to Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Associate Producer',
            value: 'Associate Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Line',
            value: 'Line',
            key: '0-0-1-3',
          },
          {
            label: 'Unit Production Manager (UPM)',
            value: 'Unit Production Manager (UPM)',
            key: '0-0-1-3',
          },
          {
            label: 'Production Coordinator',
            value: 'Production Coordinator',
            key: '0-0-1-3',
          },
          {
            label: 'Set Accountant ',
            value: 'Set Accountant',
            key: '0-0-1-3',
          },
          {
            label: 'Office Production Assistant (PA)',
            value: 'Office Production Assistant (PA)',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Executive Producer ',
            value: 'Executive Producer ',
            key: '0-0-1-1',
          },
          {
            label: 'Producer',
            value: 'Producer',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant to Producer',
            value: 'Assistant to Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Associate Producer',
            value: 'Associate Producer',
            key: '0-0-1-3',
          },
          {
            label: 'Line',
            value: 'Line',
            key: '0-0-1-3',
          },
          {
            label: 'Unit Production Manager (UPM)',
            value: 'Unit Production Manager (UPM)',
            key: '0-0-1-3',
          },
          {
            label: 'Production Coordinator',
            value: 'Production Coordinator',
            key: '0-0-1-3',
          },
          {
            label: 'Set Accountant ',
            value: 'Set Accountant',
            key: '0-0-1-3',
          },
          {
            label: 'Office Production Assistant (PA)',
            value: 'Office Production Assistant (PA)',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },

    ],
  },
];
const Transportation = [

  {
    label: <b style={{ fontSize: '25px' }}>Transportation</b>,
    value: 'Transportation',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Grip Truck Driver ',
            value: 'Grip Truck Driver ',
            key: '0-0-1-1',
          },
          {
            label: 'Camera Truck Driver ',
            value: 'Camera Truck Driver',
            key: '0-0-1-2',
          },
          {
            label: 'Pick Up Driver',
            value: 'Pick Up Driver',
            key: '0-0-1-3',
          },
          {
            label: 'Crew Van Driver',
            value: 'Crew Van Driver',
            key: '0-0-1-3',
          },
          {
            label: ' Generator Driver',
            value: ' Generator Driver',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Grip Truck Driver ',
            value: 'Grip Truck Driver ',
            key: '0-0-1-1',
          },
          {
            label: 'Camera Truck Driver ',
            value: 'Camera Truck Driver',
            key: '0-0-1-2',
          },
          {
            label: 'Pick Up Driver',
            value: 'Pick Up Driver',
            key: '0-0-1-3',
          },
          {
            label: 'Crew Van Driver',
            value: 'Crew Van Driver',
            key: '0-0-1-3',
          },
          {
            label: ' Generator Driver',
            value: ' Generator Driver',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Grip Truck Driver ',
            value: 'Grip Truck Driver ',
            key: '0-0-1-1',
          },
          {
            label: 'Camera Truck Driver ',
            value: 'Camera Truck Driver',
            key: '0-0-1-2',
          },
          {
            label: 'Pick Up Driver',
            value: 'Pick Up Driver',
            key: '0-0-1-3',
          },
          {
            label: 'Crew Van Driver',
            value: 'Crew Van Driver',
            key: '0-0-1-3',
          },
          {
            label: ' Generator Driver',
            value: ' Generator Driver',
            key: '0-0-1-3',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
    ],
  },
];
const Catering = [

  {
    label: <b style={{ fontSize: '25px' }}>Catering</b>,
    value: 'Catering',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Caterer ',
            value: 'GCaterer',
            key: '0-0-1-1',
          },
          {
            label: 'Craft Services ',
            value: 'Craft Services',
            key: '0-0-1-2',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Caterer ',
            value: 'GCaterer',
            key: '0-0-1-1',
          },
          {
            label: 'Craft Services ',
            value: 'Craft Services',
            key: '0-0-1-2',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Caterer ',
            value: 'GCaterer',
            key: '0-0-1-1',
          },
          {
            label: 'Craft Services ',
            value: 'Craft Services',
            key: '0-0-1-2',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
    ],
  },
];
const Post = [

  {
    label: <b style={{ fontSize: '25px' }}>Post Production</b>,
    value: 'Post Production',
    key: '0-0',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Film</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Editor ',
            value: 'Editor',
            key: '0-0-1-1',
          },
          {
            label: 'Sound Editor ',
            value: 'Sound Editor',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant Editor ',
            value: 'Assistant Editor',
            key: '0-0-1-2',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Television</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Editor ',
            value: 'Editor',
            key: '0-0-1-1',
          },
          {
            label: 'Sound Editor ',
            value: 'Sound Editor',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant Editor ',
            value: 'Assistant Editor',
            key: '0-0-1-2',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Internet</b>,
        value: 'Film',
        key: '0-0-1',
        children: [
          {
            label: 'Editor ',
            value: 'Editor',
            key: '0-0-1-1',
          },
          {
            label: 'Sound Editor ',
            value: 'Sound Editor',
            key: '0-0-1-2',
          },
          {
            label: 'Assistant Editor ',
            value: 'Assistant Editor',
            key: '0-0-1-2',
          },

          { label: 'Suggest Tag', value: 'Suggest Tag' },
        ],
      },
    ],
  },
];



export default function TelevisionProfile() {
  const navigate = useNavigate();

  const [open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseOption = () => {
    setOpen1(false);
  };
  let user = sessionStorage.getItem('userName')
  var identity = sessionStorage.getItem('identity Option')
  let workProfile = localStorage.getItem("Work profile")
  // console.log("first",JSON.parse(user).name)
  const [checkValue, setCheckValue] = useState([])
  const handleChange = (val) => {
    setOpen1(true);
    let temp = checkValue;
    if (temp.some((item) => item.id === val.id)) {
      temp = temp.filter((item) => item.id !== val.id);
    } else {
      temp.push(val);
    }
    setCheckValue(temp);

  };

  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);
  const handleSubmit = (e) => {
    navigate('/CompletionPhase', { state: { option: 'Film/Television' } })
  };
  // let Array = [];
  // for (let i = 0; i < checkValue.length; i++) {
  //   Array.push(checkValue[i].option1);
  // }
  const arry = [
    { option1: "Actor ➢", id: '1' },
    { option1: "Animation ➢", id: '2' },
    { option1: "Art/Photography", id: '3' },
    { option1: "Camera Related ➢", id: '4' },
    { option1: "Casting", id: '5' },
    { option1: "Director ➢", id: '6' },
    { option1: "Electric Related ➢", id: '7' },
    { option1: "Foreign Language ➢", id: '8' },
    { option1: "Grip Related ➢", id: '9' },
    { option1: "Hair and Make-up➢", id: '10' },
    { option1: "Production Team", id: '11' },
    { option1: "Production Related ➢", id: '12' },
    { option1: "Props ➢", id: '13' },
    { option1: "Rights, Brand, Royalties", id: '14' },
    { option1: "Screenwriter/ Screenplay", id: '15' },
    { option1: "Set Design ➢", id: '16' },
    { option1: "Sound Related ➢", id: '17' },
    { option1: "Special FX ➢", id: '18' },
    { option1: "Voice/Voice Over ➢", id: '19' },
    { option1: "Wardrobe ➢", id: '20' },
  ];
  const styleSheet = {
    fontSize: '25px',
    fontWeight: "bold",

  }
  const styleOption = {
    fontSize: '20px',
    fontWeight: "bold"
  }
  const styleMenu = {
    marginLeft: '15px'
  }
  const [select1, setSelect1] = React.useState('');
  const handleChange1 = (event) => {
    setSelect1(event.target.value);
  };
  const [select2, setSelect2] = React.useState('');
  const handleChange2 = (event) => {
    setSelect2(event.target.value);
  };
  const [select3, setSelect3] = React.useState('');
  const handleChange3 = (event) => {
    setSelect3(event.target.value);
  };
  const [select4, setSelect4] = React.useState('');
  const handleChange4 = (event) => {
    setSelect4(event.target.value);
  };
  const [select5, setSelect5] = React.useState('');
  const handleChange5 = (event) => {
    setSelect5(event.target.value);
  };
  const [select6, setSelect6] = React.useState('');
  const handleChange6 = (event) => {
    setSelect6(event.target.value);
  };
  const [select7, setSelect7] = React.useState('');
  const handleChange7 = (event) => {
    setSelect7(event.target.value);
  };
  const [select8, setSelect8] = React.useState('');
  const handleChange8 = (event) => {
    setSelect8(event.target.value);
  };
  const [select9, setSelect9] = React.useState('');
  const handleChange9 = (event) => {
    setSelect9(event.target.value);
  };
  const [select10, setSelect10] = React.useState('');
  const handleChange10 = (event) => {
    setSelect10(event.target.value);
  };
  const [select11, setSelect11] = React.useState('');
  const handleChange11 = (event) => {
    setSelect11(event.target.value);
  };
  const [select12, setSelect12] = React.useState('');
  const handleChange12 = (event) => {
    setSelect12(event.target.value);
  };
  const [select13, setSelect13] = React.useState('');
  const handleChange13 = (event) => {
    setSelect13(event.target.value);
  };
  const [select14, setSelect14] = React.useState('');
  const handleChange14 = (event) => {
    setSelect14(event.target.value);
  };
  const [select15, setSelect15] = React.useState('');
  const handleChange15 = (event) => {
    setSelect15(event.target.value);
  };
  const [select16, setSelect16] = React.useState('');
  const handleChange16 = (event) => {
    setSelect16(event.target.value);
  };
  let Array = ['Film/Television'];
  if (select1 != '') {
    Array.push(select1);
  }
  if (select2 != '') {
    Array.push(select2);
    // Array.push(anchorEl);
  }
  if (select3 != '') {
    Array.push(select3);
  }
  if (select4 != '') {
    Array.push(select4);
  }
  if (select5 != '') {
    Array.push(select5);
  }
  if (select6 != '') {
    Array.push(select6);
  }
  if (select7 != '') {
    Array.push(select7);
  }
  if (select8 != '') {
    Array.push(select8);
  }
  if (select9 != '') {
    Array.push(select9);
  }
  if (select10 != '') {
    Array.push(select10);
  }
  if (select11 != '') {
    Array.push(select11);
  }
  if (select12 != '') {
    Array.push(select12);
  }
  if (select13 != '') {
    Array.push(select13);
  }
  if (select14 != '') {
    Array.push(select14);
  }
  if (select15 != '') {
    Array.push(select15);
  }
  if (select16 != '') {
    Array.push(select16);
  }
  console.log("Array", Array)
  localStorage.setItem('MultiOptions', JSON.stringify(Array))
  const [addTag, setAddTag] = useState(false)
  const handleTag = (event) => {
    setAddTag(true);
  }
  const [anchorEl, setAnchorEl] = React.useState('');

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseTag = () => {
    setAddTag(false);
    // Array.push(anchorEl);
  };
  if (anchorEl != '') {
    Array.push(anchorEl);
  }
  console.log("anchorEl", anchorEl)
  return (
    <div className="container-fluid" style={BackGround}>
      {/* <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div> */}
      <div className="row">
        <div className="col-md-8 offset-md-2 text-center">
         
            <CardContent>
              {/* <div>
                <Avatar
                  alt="Remy Sharp"
                  src={userInfo.filepreview}
                  sx={{
                    width: "150px",
                    height: "130px",
                    margin: "auto",
                  }}
                />
              </div> */}

              <Typography
                variant="body2"
                color=""
                sx={{ margin: "15px 0px", fontSize: "25px", color:"white" }}
              >
                {user}, {identity}, {workProfile} <br />
                Film/Television/Internet
              </Typography>
            </CardContent>
        
          <div
            style={{
              margin: "15px auto",
              fontSize: "2.4vw",
              fontWeight: "bold",
              color: "white",
              textAlign: "center",

            }}
          >
            <span style={{  }}>M</span><span style={{  }}>ebook</span> <span style={{  }}>M</span><span style={{  }}>eta</span> Profile: {identity}, {workProfile}, Film/Television/Internet
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-8 offset-md-2 ">
       
            <CardContent>
              <div className="row mx-2  my-2">
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select1}
                      onChange={handleChange1}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Actor ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Actor, Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Drama'} >Drama</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Comedy'} >Comedy</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Thriller/Action/Suspense'}>Thriller/Action/Suspense</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Detective/Mystery'}>Detective/Mystery</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Horror'}>Horror</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Documentary'}>Documentary</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Franchise'}>Franchise</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, Film, Biographical'}>Biographical</MenuItem>
                      <MenuItem sx={styleOption} value={'Actor, Television'} >Television ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV,  Drama'} >Drama</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV, Comedy'} >Comedy</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV, Thriller/Action/Suspense'}>Thriller/Action/Suspense</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV, Detective/Mystery'}>Detective/Mystery</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV, Horror'}>Horror</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV, Documentary'}>Documentary</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV, Television Series'}>	Television Series</MenuItem>
                      <MenuItem sx={styleMenu} value={'Actor, TV, Biographical'}>Biographical</MenuItem>

                    </Select>
                  </FormControl> */}
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={treeData}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Actor</b>}
                  />
                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Animation}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Animation</b>}
                  />
                  <Dialog
                    open={addTag}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                  >
                    <DialogTitle id="alert-dialog-title">
                      Add your Tag here
                    </DialogTitle>
                    <DialogContent>
                      <input type="text" value={anchorEl} onChange={(e) => setAnchorEl(e.target.value)} class="form-control" />

                    </DialogContent>
                    <DialogActions>
                      <Button onClick={handleCloseTag}>Ok</Button>

                    </DialogActions>
                  </Dialog>
                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select3}
                      onChange={handleChange3}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Camera Related ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Camera Related,  Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, Director of Photography (Cinematographer)'} >Director of Photography (Cinematographer)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, 1Camera Operator'} >1Camera Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, 1st Assistant Camera'}>1st Assistant Camera</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, 2nd Assistant Camera'}>2nd Assistant Camera</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, Freelance Camera Operator'}>Freelance Camera Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, Digital Imaging Technician (DIT)'}>Digital Imaging Technician (DIT)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, Camera Intern'}>Camera Intern</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, Rental Agent'}>Rental Agent</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, Production Crew'}>Production Crew</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, Film, Loader'}>Loader</MenuItem>



                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Camera Related'} >Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Camera Related, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, Director of Photography (Cinematographer)'} >Director of Photography (Cinematographer)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, 1Camera Operator'} >1Camera Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, 1st Assistant Camera'}>1st Assistant Camera</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, 2nd Assistant Camera'}>2nd Assistant Camera</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, Freelance Camera Operator'}>Freelance Camera Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, Digital Imaging Technician (DIT)'}>Digital Imaging Technician (DIT)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, Camera Intern'}>Camera Intern</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, Rental Agent'}>Rental Agent</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, Production Crew'}>Production Crew</MenuItem>
                      <MenuItem sx={styleMenu} value={'Camera Related, TV, Loader'}>Loader</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} >Suggest Tag ➢</MenuItem>
                    </Select>
                  </FormControl> */}
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Camera}
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Camera</b>}
                  />
                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select4}
                      onChange={handleChange4}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Director ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Director, Film,'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Director'} >Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, 1st Assistant Director'} >1st Assistant Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, 2nd Assistant Director'}>2nd Assistant Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, 2nd 2nd Assistant Director'}>2nd 2nd Assistant Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Script Supervisor'}>	Script Supervisor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Technical Director'}>Technical Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, 	Performance Director'}>	Performance Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Arts and Crafts Director'}>Arts and Crafts Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Casting Director'}>Casting Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Extras Casting'}>Extras Casting</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Still Photographer'}>Still Photographer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Production Assistant'}>Production Assistant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Location Manager'}>Location Manager</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Accountant'}>Accountant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, Film, Teacher/Educator'}>Teacher/Educator</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={"Director, Film"} >Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Director, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Director'} >Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, 1st Assistant Director'} >1st Assistant Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, 2nd Assistant Director'}>2nd Assistant Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, 2nd 2nd Assistant Director'}>2nd 2nd Assistant Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Script Supervisor'}>	Script Supervisor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Technical Director'}>Technical Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, 	Performance Director'}>	Performance Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Casting Director'}>Casting Director</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Extras Casting'}>Extras Casting</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Still Photographer'}>Still Photographer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Production Assistant'}>Production Assistant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Location Manager'}>Location Manager</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Accountant'}>Accountant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Director, TV, Teacher/Educator'}>Teacher/Educator</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Director, TV'} >Suggest Tag ➢</MenuItem>
                    </Select>
                  </FormControl> */}
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    options={Director}
                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Director</b>}
                  />
                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select5}
                      onChange={handleChange5}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Electric Related ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Electric Related, Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, Film, Gaffer'} >Gaffer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, Film, Best Boy Electric (BBE)'} >Best Boy Electric (BBE)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, Film, Electrical Lighting Technician (ELT)'}>Electrical Lighting Technician (ELT)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, Film, Generator Operator'}>Generator Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, Film, Rigging Electrician'}>Rigging Electrician</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Electric Related, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Electric Related, TV'} >TV ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, TV, Gaffer'} >Gaffer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, TV, Best Boy Electric (BBE)'} >Best Boy Electric (BBE)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, TV, Electrical Lighting Technician (ELT)'}>Electrical Lighting Technician (ELT)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, TV, Generator Operator'}>Generator Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Electric Related, TV, Rigging Electrician'}>Rigging Electrician</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Electric Related, Film'}>Suggest Tag ➢</MenuItem>
                    </Select>
                  </FormControl> */}
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    options={Electric}
                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Electric</b>}
                  />
                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select6}
                      onChange={handleChange6}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Grip Related ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Grip Related, Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, Film, Key Grip'} >Key Grip</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, Film, Best Boy Grip'} >Best Boy Grip</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, Film, Grip'}>Grip</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, Film, Dolly Grip'}>Dolly Grip</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, Film, Swing'}>Swing</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Grip Related, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Grip Related, TV'} >TV ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, TV, Gaffer'} >Gaffer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, TV, Best Boy Electric (BBE)'} >Best Boy Electric (BBE)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, TV, Electrical Lighting Technician (ELT)'}>Electrical Lighting Technician (ELT)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, TV, Generator Operator'}>Generator Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Grip Related, TV, Rigging Electrician'}>Rigging Electrician</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Grip Related, TV'}>Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    size="large"
                    options={Grip}
                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Grip Related</b>}
                  />
                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select7}
                      onChange={handleChange7}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Hair and Make-Up ➢ </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Hair and Make-Up, Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, Film, Key Make-Up Artist'} >Key Make-Up Artist</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, Film, Key Hair Stylist'} >Key Hair Stylist</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, Film, Make-Up Assistant'}>Make-Up Assistant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, Film, Hair Assistant'}>Hair Assistant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, Film, Special Effects Make-Up Artist'}>Special Effects Make-Up Artist</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Hair and Make-Up, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Hair and Make-Up, TV,'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, TV, Key Make-Up Artist'} >Key Make-Up Artist</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, TV, Key Hair Stylist'} >Key Hair Stylist</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, TV, Make-Up Assistant'}>Make-Up Assistant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, TV, Hair Assistant'}>Hair Assistant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Hair and Make-Up, TV, Special Effects Make-Up Artist'}>Special Effects Make-Up Artist</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Hair and Make-Up, TV,'}>Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    size="large"
                    options={Hair}
                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Hair & Make-Up</b>}
                  />
                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select8}
                      onChange={handleChange8}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Wardrobe ➢ </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Wardrobe, Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, Film, Costume Designer'} >Costume Designer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, Film, Wardrobe Supervisor'} >Wardrobe Supervisor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, Film, Set Costumer'}>Set Costumer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, Film, Costume Coordinator'}>Costume Coordinator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, Film, Tailor'}>Tailor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, Film, Shopper'}>Shopper</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Wardrobe, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Wardrobe, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, TV, Costume Designer'} >Costume Designer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, TV, Wardrobe Supervisor'} >Wardrobe Supervisor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, TV, Set Costumer'}>Set Costumer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, TV, Costume Coordinator'}>Costume Coordinator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, TV, Tailor'}>Tailor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Wardrobe, TV, Shopper'}>Shopper</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Wardrobe, TV'} >Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    size="large"
                    options={Wardrobe}
                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Wardrobe</b>}
                  />

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={sound}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Sound Related</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select9}
                      onChange={handleChange9}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Sound Related ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Sound Related, Film'} >Film ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Sound Related, Film, Production Sound Mixer'} >Production Sound Mixer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Sound Related, Film, Boom Operator'} >Boom Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Sound Related, Film, Sound Assistant/Cable'}>Sound Assistant/Cable</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Sound Related, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Sound Related, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Sound Related, TV, Production Sound Mixer'} >Production Sound Mixer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Sound Related, TV, Boom Operator'} >Boom Operator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Sound Related, TV, Sound Assistant/Cable'}>Sound Assistant/Cable</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Sound Related, TV'}>Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Stunts}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Stunts Related</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select10}
                      onChange={handleChange10}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Stunts Related ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Stunts Related, Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, Film, Stunt Coordinator'} >Stunt Coordinator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, Film, Stunt Performer'} >Stunt Performer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, Film, Set Medic'}>Set Medic</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, Film, Pyro'}>Pyro</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, Film, Weapons Super'}>Weapons Super</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, Film, Armorer'}>Armorer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, Film, Animal Wrangler'}>Animal Wrangler</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Stunts Related, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Stunts Related, TV'} >TV ➢ </MenuItem>

                      <MenuItem sx={styleMenu} value={'Stunts Related, TV, Stunt Coordinator'} >Stunt Coordinator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, TV, Stunt Performer'} >Stunt Performer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, TV, Set Medic'}>Set Medic</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, TV, Pyro'}>Pyro</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, TV, Weapons Super'}>Weapons Super</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, TV, Armorer'}>Armorer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Stunts Related, TV, Animal Wrangler'}>Animal Wrangler</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Stunts Related, TV'} >Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Special}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Special FX</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select11}
                      onChange={handleChange11}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Special FX/Visual FX ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Special FX/Visual FX, Film'} >Film ➢ </MenuItem>
                      <MenuItem sx={styleMenu} value={'Special FX/Visual FX, Film, VFX Supervisor'} >VFX Supervisor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Special FX/Visual FX, Film, VFX Coordinator'} >VFX Coordinator</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Special FX/Visual FX, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Special FX/Visual FX, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Special FX/Visual FX, TV, VFX Supervisor'} >VFX Supervisor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Special FX/Visual FX, TV, VFX Coordinator'} >VFX Coordinator</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Special FX/Visual FX, TV'}>Suggest Tag ➢</MenuItem>
                    </Select>
                  </FormControl> */}

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Production}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Production Related</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select12}
                      onChange={handleChange12}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Production Related ➢ </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Production Related, Film'} >Film ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film ,  Executive Producer'} >Executive Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Producer'} >Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Assistant to Producer'}>Assistant to Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Associate Producer'}>Associate Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Line Producer'}>Line Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Unit Production Manager (UPM)'}>Unit Production Manager (UPM)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Production Coordinator'}>Production Coordinator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Set Accountant'}>Set Accountant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, Film, Office Production Assistant (PA)'}>Office Production Assistant (PA)</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Production Related, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Production Related, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Executive Producer'} >Executive Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Producer'} >Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Assistant to Producer'}>Assistant to Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Associate Producer'}>Associate Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Line Producer'}>Line Producer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Unit Production Manager (UPM)'}>Unit Production Manager (UPM)</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Production Coordinator'}>Production Coordinator</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Set Accountant'}>Set Accountant</MenuItem>
                      <MenuItem sx={styleMenu} value={'Production Related, TV, Office Production Assistant (PA)'}>Office Production Assistant (PA)</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Production Related, TV'} >Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    options={Props}
                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Props Related</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select13}
                      onChange={handleChange13}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Props Related ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Props Related, Film'} >Film ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, Film, Prop Master'} >Prop Master</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, Film, Assistant Props'} >Assistant Props</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, Film, Weapons Wrangler'}>Weapons Wrangler</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, Film, Greensman'}>Greensman</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Props Related, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Props Related, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, TV, Prop Master'} >Prop Master</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, TV, Assistant Props'} >Assistant Props</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, TV, Weapons Wrangler'}>Weapons Wrangler</MenuItem>
                      <MenuItem sx={styleMenu} value={'Props Related, TV, Greensman'}>Greensman</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Props Related, TV'}>Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    options={Transportation}
                    // onChange={onChange1}
                    multiple
                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Transportation</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select14}
                      onChange={handleChange14}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Transportation ➢ </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Transportation, Film'}>Film ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, Film, Grip Truck Driver'} >Grip Truck Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, Film, Camera Truck Driver'} >Camera Truck Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, Film, Pick Up Driver'}>Pick Up Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, Film, Crew Van Driver'}>Crew Van Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, Film, Generator Driver'}>Generator Driver</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Transportation, Film'}>Suggest Tag ➢</MenuItem>
                      <MenuItem sx={styleOption} value={'Transportation, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, TV,  Grip Truck Driver'} >Grip Truck Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, TV,  Camera Truck Driver'} >Camera Truck Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, TV,  Pick Up Driver'}>Pick Up Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, TV,  Crew Van Driver'}>Crew Van Driver</MenuItem>
                      <MenuItem sx={styleMenu} value={'Transportation, TV,  Generator Driver'}>Generator Driver</MenuItem>
                      <MenuItem onClick={handleTag} sx={styleMenu} value={'Transportation, TV'}>Suggest Tag ➢</MenuItem>


                    </Select>
                  </FormControl> */}

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Catering}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Catering</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select15}
                      onChange={handleChange15}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Catering ➢  </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Catering, Film'} >Film ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Catering, Film, Caterer'} >Caterer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Catering, Film, Craft Services'} >Craft Services</MenuItem>

                      <MenuItem sx={styleOption} value={'Catering, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Catering, TV,  Caterer'} >Caterer</MenuItem>
                      <MenuItem sx={styleMenu} value={'Catering, TV, Craft Services'} >Craft Services</MenuItem>




                    </Select>
                  </FormControl> */}

                </div>
                <div className=" col-md-4 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Post}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{fontSize:'20px',color:'black'}}/> </b> }

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Post Production</b>}
                  />
                  {/* <FormControl className='button' fullWidth sx={{ background: 'white' }}>
                    <Select
                      value={select16}
                      onChange={handleChange16}
                      displayEmpty
                      inputProps={{ 'aria-label': 'Without label' }}
                      MenuProps={{
                        PaperProps: {
                          sx: {

                            "& .MuiMenuItem-root:hover": {
                              backgroundColor: "#03a9f4"
                            },
                            "& .MuiMenuItem-root.Mui-selected": {
                              backgroundColor: ""
                            }

                          }
                        }
                      }}

                    >
                      <MenuItem value='' sx={styleSheet}  > <em >Post Production ➢ </em></MenuItem>
                      <MenuItem sx={styleOption} value={'Post Production, Film'} >Film ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Post Production, Film, Editor'} >	Editor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Post Production, Film, Sound Editor'} >Sound Editor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Post Production, Film, Assistant Editor'} >Assistant Editor</MenuItem>

                      <MenuItem sx={styleOption} value={'Post Production, TV'} >TV ➢</MenuItem>
                      <MenuItem sx={styleMenu} value={'Post Production, TV, Editor'} >	Editor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Post Production, TV, Sound Editor'} >Sound Editor</MenuItem>
                      <MenuItem sx={styleMenu} value={'Post Production, TV, Assistant Editor'} >Assistant Editor</MenuItem>




                    </Select>
                  </FormControl> */}

                </div>
              </div>
            </CardContent>
        
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              Your Selected Options are
            </DialogTitle>
            <DialogContent>
              {Array.map((i) => {
                return (
                  <p style={{ display: 'inline', fontWeight: 'bold', fontSize: '25px', color: '#100892' }}>
                    {i},
                  </p>
                )
              })}


              <DialogContentText id="alert-dialog-description">
                If want to Continue Please click on Agree otherwise click on Disagree
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Disagree</Button>
              <Button onClick={handleSubmit} autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <Link to={'/CreatorNew1'}><button
              type="button"
              class="btn  btn-lg"
              style={newButton}
            >
              Go Back
            </button></Link>
            <button
              type="button"
              class="btn  btn-lg"
              style={newButton}
              onClick={handleClickOpen}
            >
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
