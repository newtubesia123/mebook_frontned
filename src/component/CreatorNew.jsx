import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { useNavigate, useParams } from "react-router-dom";
import Badge from "@mui/material/Badge";
import PageviewIcon from "@mui/icons-material/Pageview";
import "./creatornew.css";
import CnPop from "./CreatorNewPop/CnPop";
import { useDispatch, useSelector } from "react-redux";
import { getUserData, userIdentity } from "../Action";
import { SERVER } from "../server/server";
import { BackGround, newButton } from "../Component new/background";
import { toast } from "react-toastify";
import MeLogo from "../assets/MeLogoMain";
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';

const BootstrapTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} arrow classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.arrow}`]: {
    color: theme.palette.common.black,
  },
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    maxWidth: 600,
    fontSize: "0.8rem",
    color:"black"
  },
}));

export default function CreatorNew() {
  const bgBut = {
    ...newButton,
    width: "80%",
    fontSize: "90%",
    color: "white",
    margin: "15px 10%",
    borderRadius: "10px",
    padding: "10px 0px",
    fontWeight: "bold",
  };

  const [a, setA] = useState(false);
  const params = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  let user = localStorage.getItem("userData");
  let work = localStorage.getItem("Work profile");
  const STORE = useSelector((state) => state);
  if (
    !STORE.userIdentity.loading &&
    STORE.userIdentity.userIdentity.errorCode == 200
  ) {
    // navigate("/CompletionIdentity");
    console.log(
      "!STORE.userIdentity.loading && STORE.userIdentity.userIdentity.errorCode == 200"
    );
  }
  if (
    !STORE.userIdentity.loading &&
    STORE.userIdentity.userIdentity.errorCode == 400
  ) {
    // toast.error(STORE.mebookProfile.subCategory.message,{position:'top-right'})
    // navigate("/CompletionIdentity");
    console.log(
      "!STORE.userIdentity.loading && STORE.userIdentity.userIdentity.errorCode == 400"
    );
  }
  const [identity, setIdentity] = useState({
    position: params.position,
    status: "",
  });
  const arry = [
    {
      option: "Creator",
      id: "1",
      value:
        "This profile is reserved for content creators, for those who generate original entertaining or educational material, to be expressed through any medium or channel. In the Global Media Marketplace, creators include presenters of all forms or writing, speech, performance, graphic and visual arts, music, sound and audio, dance and choreography, design, all aspects of media, television, film, and other original material.",
    },
    {
      option: "Collaborator",
      id: "2",
      value:
        "This profile is reserved for those who work along with content creators, who generate original entertaining or educational material, to be expressed through any medium or channel. In the Global Media Marketplace, collaborators include those who work with presenters of all forms or writing, speech, performance, graphic and visual arts, music, sound and audio, dance and choreography, design, all aspects of media, television, film, and other original material. Those who work with any form of derivative work are also considered as collaborators.",
    },
    {
      option: "Influencer",
      id: "3",
      value:
        "This profile is reserved for those whose work product exerts a measurable influencer effect, on the Global Media Marketplace. Influencers include editorial writers, reviewers, commentary, criticism, opinion writers, bloggers, vloggers, ad writers, copy writers, satirists, video and short film makers, television and film favorites, celebrities, sports stars, media personalities and other niche personalities.",
    },
    {
      option: "Project Developer",
      id: "4",
      value:
        "This profile is reserved for those whose work involves the discovery and acquisition of original or existing work and developing that work to achieve a pre-determined goal. Project developers are often producers or entrepreneurs who are required to expand ideas to create overall visions, locate talented players for teams over multiple projects, develop budgets, find investors, manage, and complete projects on-time and within budget. Project Developers work with creators, collaborators, influencers and providers to bring work to the Global Media Marketplace.",
    },
    {
      option: "Provider",
      id: "5",
      value:
        "This profile is reserved for those who provide necessary materials and services to creators, collaborators, influencers, project managers and reader/consumers to facilitate the proliferation and enjoyment of work. This vast category includes advertisers, app and website makers, beta testers, bookstores, casting agencies, caterers, coaches, coders, colleges and universities, designers, editors, equipment providers, financial institutions, investors, law firms, libraries, software makers, printers, promoters, publicists, publishers, rental and retail sources, scouts, security, software companies, talent agents, travel agents, and many other providers not listed above.",
    },
  ];
  const [uploadLater, setUploadLater] = useState("");
  const handleUploadLater = () => {
    setUploadLater("later");
  };
  const [checkValue, setCheckValue] = useState([]);

  const handleChange = (val) => {
    setCheckValue(val.option);
  };
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  const [imageSrc, setImageSrc] = useState("none");
  const handleInputChange = (event) => {
    console.log("event.target.files[0]", event.target.files[0]);
    setuserInfo({
      ...userInfo,
      file: event.target.files[0],
      filepreview: URL.createObjectURL(event.target.files[0]),
    });
  };
  let token = JSON.parse(localStorage.getItem("token"));
  useEffect(() => {
    // if (!STORE.userIdentity.loading && STORE.userIdentity.userIdentity.errorCode == 200) {
    //   navigate("/CompletionIdentity");
    // }
    // if (!STORE.userIdentity.loading && STORE.userIdentity.userIdentity.errorCode == 400) {
    //   // toast.error(STORE.mebookProfile.subCategory.message,{position:'top-right'})
    //   navigate("/CompletionIdentity");
    // }
    dispatch(getUserData());
  }, []);

  if (STORE.errorPage) {
    navigate("/ErrorPage");
  }

  const handleSubmit = async () => {
    const formdata = new FormData();
    if (userInfo.file == "") {
      toast.error("Please select image first", {
        position: "top-center",
        autoClose: 2000,
      });
    } else {
      formdata.append("profiles", userInfo.file);

      await axios
        .post(
          `${SERVER}/imageupload/?picType=${"userProfileImage"}`,
          formdata,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .then((res) => {
          if (res.data.isSuccess) {
            setUploadLater("later");
            setImageSrc("block");
            toast.success(res.data.message);
          }
        });
      dispatch(getUserData(token));
    }
  };
  const handleNavigate = () => {
    if (identity.status != "") {
      dispatch(
        userIdentity(identity, () => {
          setTimeout(() => {
            navigate("/CreatorNew1");
          }, 2000);
        })
      );
    }
    // if (userInfo.file == "") {
    //   toast.error("Please select image first", {
    //     position: "top-center",
    //     autoClose: 2000,
    //   });
    // }

    else {
      toast.info("Please Choose Any Options");
    }
  };
  return (
    <div className="container-fluid" style={BackGround}>
      {a ? (
        <CnPop value={a ? a : "null"} />
      ) : (
        <span style={{ display: "none" }}></span>
      )}
      <div className="img text-center">
        {/* <img
          src={image}
          alt=""
          style={{
            width: "180px",
            height: "100px",
            margin: "10px 0px",
          }}
        /> */}
        <MeLogo />
      </div>
      <div className="row">
        <div className="col-md-6 offset-md-3 ">
          <div
            sx={{
              backgroundColor: "transparent",
              borderRadius: "30px",
              textAlign: "center",
              // width: "80%",
              m: "2px auto",
            }}
          >
            <CardContent sx={{ textAlign: "center" }}>
              <div className="text-center">
                <Badge overlap="circular" badgeContent="">
                  <input
                    hidden
                    id="file-input"
                    type="file"
                    onChange={handleInputChange}
                  />
                  {userInfo.filepreview == null ? (
                    <Avatar
                      className="img1"
                      src={userInfo.filepreview}
                      // sx={{
                      //   width: "140px",
                      //   height: "120px",
                      // }}
                      style={{
                        background:
                          "linear-gradient(245deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)",
                        animation: "ease .1s",
                        width: "100px",
                        height: "90px",
                      }}
                    >
                      <label for="file-input">
                        <PageviewIcon sx={{ fontSize: "150%" }} />
                        <p style={{ fontSize: "70%" }}>Select Image</p>
                      </label>
                    </Avatar>
                  ) : (
                    <Avatar
                      className="img1"
                      src={userInfo.filepreview}
                      sx={{
                        width: "120px",
                        height: "90px",
                      }}
                    ></Avatar>
                  )}
                  {/* <label id="icon" for="file-input">
                    <EditIcon
                      style={{ color: "black", m: "auto", fontSize: "30px",color:'white' }}
                    />
                  </label>
                </Badge>                
                  </label> */}
                </Badge>
              </div>
              <div className="d-flex justify-content-center mt-2">
                {/* <button
                  type="button"
                  class="btn btn-light btn-sm"
                  onClick={handleSubmit}
                  style={{ marginRight: "10px" }}
                >
                  Upload Image
                </button> */}
                {/* NEW BUTTON ADD */}
                <Button
                  type="button"
                  variant="contained" size="small"
                  onClick={handleSubmit}
                  sx={{ ...newButton, mt: 1, fontSize: "75%" }}
                >
                  Upload Image
                </Button>
                {/* <button
                  type="button"
                  class="btn btn-light btn-sm"
                  onClick={handleUploadLater}
                >
                  Upload Later
                </button> */}
              </div>
              <Typography
                variant="body2"
                color="black"
                sx={{ margin: "10px auto", fontSize: "150%", color: "white" }}
              >
                Welcome, {JSON.parse(user).name} <br /> To the MebookMeta
                Universe!
                <br />
                Please set up your profile picture, image or avatar
              </Typography>
            </CardContent>
          </div>
          <div
            id="font"
            style={{
              margin: "0px 0px",
              fontSize: "150%",
              // fontWeight: "bold",
              color: "white",
              textAlign: "center",
            }}
          >
            Set Up Your  <br /> <span style={{}}>M</span>
            <span style={{}}>eBook</span>
            <span style={{}}>M</span>
            <span style={{}}>eta</span> <br />{" "}
            <b style={{ fontWeight: "bolder" }}> Work Profile</b>
          </div>
        </div>
      </div>
      <div className="row">
        <div className=" col-md-6 offset-md-3 text-white">
          <CardContent>
            {arry.map((val) => {
              return (
                <>
                  <BootstrapTooltip title={val.value} placement="top-end" arrow>
                    <Button
                      className={
                        identity.status === val.option
                          ? " button afterClick"
                          : " button"
                      }
                      sx={bgBut}
                      value={checkValue}
                      // onMouseEnter={() => {
                      //   setA(val);
                      // }}
                      // onMouseLeave={() => {
                      //   setA(false);
                      // }}
                      onClick={() =>
                        setIdentity({ ...identity, status: val.option })
                      }
                    >
                      {val.option}
                    </Button>
                  </BootstrapTooltip>
                </>

              );
            })}
          </CardContent>
          <div className="d-flex justify-content-between mt-3 mb-5">
            {/* <Link to={"/Identity"}> */}
            <Button
              onClick={() => navigate("/Identity")}
              variant="contained"
              size="small"
              sx={{ ...newButton, marginBottom: "1rem" }}
            >
              Go Back
            </Button>
            {/* </Link> */}
            <Button
              onClick={handleNavigate}
              variant="contained"
              size="small"
              sx={{ ...newButton, marginBottom: "1rem" }}
            >
              Continue
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
