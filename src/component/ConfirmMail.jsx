import React, { useState, useEffect } from "react";
import axios from "axios";
import Grid from "@mui/material/Grid";
import image from "../mepic.png";
import Button from "@mui/material/Button";
import { Link, useParams } from "react-router-dom";
import { useNavigate,useLocation } from "react-router-dom";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";
import { BackGround, newButton } from "../Component new/background";

import "./Edit.css";
import { SERVER } from "../server/server";
import { toast } from "react-toastify";
import MeLogo from "../assets/MeLogoMain";
export default function ConfirmMail() {

  const [success, setSuccess] = React.useState(false);
  const param = useParams()
  const navigate = useNavigate();
  // const id=location.state.id
  // const location = useLocation();
  useEffect(() => {
    setSuccess(true);
    if(sessionStorage.getItem("confirm")){
      navigate("/SignIn")
    }
    
    setTimeout(() => {
      setSuccess(false);
    }, 5000);
  }, []);
  const [values, setValues] = useState({
    email: "",
  });

  useEffect(() => {
    setValues({...values,email:param.email})
    setSuccess(true);
    setTimeout(() => {
      setSuccess(false);
    }, 5000);
  }, []);
  const handleChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();

    const emailData = {
      email: values.email,
    };
    
    await axios
      .post(`${SERVER}/confirm`, { emailData: emailData })
      .then((res) => {
        // console.log("data->", emailData);
        if (res.data.isSuccess) {
          // console.log("successdata--->", id);
          // sessionStorage.setItem("id", res.data.data._id);
          // alert(res.data.message);
          toast.success(res.data.message,{
            position:'top-center',
            autoClose:1000,
            pauseOnHover:false
          })
          // navigate("/SignIn");
          sessionStorage.setItem("confirm", true);
        } else {
          // alert(res.data.Error);
          toast.error(res.data.Error,{
            position:'top-center',
            autoClose:1000,
            pauseOnHover:false
          })
        }
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className="MebookMetaimage" style={BackGround}>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
        sx={{ padding: "25px 5px" }}
      >
        <Grid item xs={1} md={1} sm={1}></Grid>
        <Grid
          item
          xs={10}
          md={10}
          sm={10}
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          {/* <img
            src={image}
            alt=""
            style={{
              width: "300px",
              height: "153.1px",
            }}
          /> */}
          <MeLogo/>
          <b>
            {" "}
            <p style={{ fontSize: "17px", color:"white" }}>
              Please confirm your email address. We have sent a confirmation
              link to
            </p>
          </b>
          <form style={{ margin: "50px 0px" , color:'white'}}>
            <div class="form-group">
           <b>   <label htmlFor="">Enter your Email</label></b>
              <input
                type="email"
                disabled
                class="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                name="email"
                value={values.email}
                onChange={handleChange}
              />
            </div>
          </form>
        </Grid>
        <Grid item xs={1} md={1} sm={1}></Grid>
        <div className="btn m-auto">
          <Button
            onClick={handleSubmit}
            variant="contained"
            sx={newButton}
            style={{ marginBottom: "260px" }}
          >
            Send Confirmation
          </Button>
        </div>
      </Grid>
    </div>
  );
}
