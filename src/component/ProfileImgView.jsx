import React from 'react'
import './ProfilePageNew.scss'
import { getImgURL } from '../util/getImgUrl';
import CloseIcon from '@mui/icons-material/Close';
import AvatarImg from '../png/Avatar-Profile-PNG-Photos.png'

const ProfileImgView = ({ setViewProfile, data }) => {
    return (
        <>
            <div class="wrap" onClick={() => { setViewProfile(false) }}>
                <div class="box d-flex flex-column" style={{ alignItems: "center" }}> <CloseIcon sx={{ fontSize: "30px", background: "#fff", cursor: "ponter" }} onClick={() => { setViewProfile(false) }} />
                    <img src={getImgURL(data)} alt=""
                        onError={(e) => {
                            e.target.src = AvatarImg
                        }}
                    />
                </div>
            </div>
        </>
    )
}

export default ProfileImgView