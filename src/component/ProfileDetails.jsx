import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import { useTheme } from '@mui/material/styles';
import { useNavigate, } from "react-router-dom";
import "./Edit.css";
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import PinterestIcon from '@mui/icons-material/Pinterest';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import RedditIcon from '@mui/icons-material/Reddit';
import OutlinedInput from '@mui/material/OutlinedInput';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import TelegramIcon from '@mui/icons-material/Telegram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import { ToastContainer, toast } from 'react-toastify';
import FormControl from '@mui/material/FormControl';
import { Select } from 'antd';
import 'react-toastify/dist/ReactToastify.css';
import { BackGround, newButton } from "../Component new/background";
import { useDispatch, useSelector } from "react-redux";
import { getUserData, updateUserDetail } from "../Action";
import Button from "@mui/material/Button";
import './common.css'
import MeLogo from "../assets/MeLogoMain";
import useSound from "use-sound";
import profileCreated from '../components/voiceMessage/ProfileCreatedVoice.mp3'
// import Calendar from 'react-calendar';
// import 'react-calendar/dist/Calendar.css';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    sx: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
      "& .MuiMenuItem-root:hover": {
        backgroundColor: "#03a9f4"
      },
      "& .MuiMenuItem-root.Mui-selected": {
        backgroundColor: "#8bc34a"
      }
    },
  },
};

const options = [
  { label: <FacebookIcon sx={{ fontSize: '30px', color: '#3b5998' }} />, value: 'Facebook' },
  { label: <TwitterIcon sx={{ fontSize: '30px', color: '#00acee' }} />, value: 'Twitter' },
  { label: <PinterestIcon sx={{ fontSize: '30px', color: '#c8232c' }} />, value: 'Pinterest' },
  { label: <InstagramIcon sx={{ fontSize: '30px', color: '#bc2a8d' }} />, value: 'Instagram' },
  { label: <YouTubeIcon sx={{ fontSize: '30px', color: '	#FF0000' }} />, value: 'Youtube' },
  { label: <RedditIcon sx={{ fontSize: '30px', color: '#ff4500' }} />, value: 'Reddit' },
  { label: <WhatsAppIcon sx={{ fontSize: '30px', color: '#4FCE5D' }} />, value: 'WhatsApp' },
  { label: <TelegramIcon sx={{ fontSize: '30px', color: '#0088cc' }} />, value: 'Telegram' },
  // { label: <FontAwesomeIcon icon="fa-brands fa-snapchat" />, value: 'Telegram' },

  { label: <LinkedInIcon sx={{ fontSize: '30px', color: '#0A66C2' }} />, value: 'LinkedIn' },
  { label: 'Others', value: 'Others' },
];

export default function ProfileDetails() {
  const [play, { stop }] = useSound(profileCreated);
  const theme = useTheme();
  const [smedia, setSmedia] = React.useState([]);
  const [socialmedia, setSocialMedia] = React.useState(true);
  const [birth, setBirth] = React.useState(true);
  const [month, setMonth] = React.useState(true);
  const [place, setPlace] = React.useState(true);
  const [town, setTown] = React.useState(true);
  const [slink1, setSlink1] = React.useState(true);
  const [slink2, setSlink2] = React.useState(true);
  const [city, setCity] = React.useState(true);
  const [school, setSchool] = React.useState(true);
  const [college, setCollege] = React.useState(true);
  const [err, setErr] = React.useState(false);
  const handleMedia = (e) => {
    setSmedia(e)
  }
  const navigate = useNavigate();
  const dispatch = useDispatch()
  const store = useSelector((state) => state)

  if (store?.getProfileData?.userData?.homeTown && store?.getProfileData?.userData?.metaProfile) {
    localStorage.removeItem("token")
    toast.error("unexpected access", {
      position: 'top-center',
      autoClose: 500,

    })
    navigate('/SignIn')
  }
  if (store?.getProfileData?.userData?.homeTown && !store?.getProfileData?.userData?.identityProfile && !store?.getProfileData?.userData?.metaProfile) {

    navigate('/DetailSaved')
  }
  if (store?.getProfileData?.userData?.homeTown && store?.getProfileData?.userData?.identityProfile && !store?.getProfileData?.userData?.metaProfile) {
    navigate('/CreatorNew1')
  }

  const STORE = useSelector((state) => state)

  const nb = { ...newButton, margin: "1rem", textDecoration: "none", fontSize: "80%" }
  useEffect(() => {
    dispatch(getUserData())

  }, [])

  // To get date  before 'X' number of years from current date
  const today = new Date();
  let x = 13;
  const year = today.getFullYear() - x;
  const mnth = today.getMonth();
  const day = today.getDate();
  const date = new Date(year, mnth, day);
  const maxDate = date.toISOString().split('T')[0]; // format the date as yyyy-mm-dd

  // console.log(maxDate); 

  const [values, setValues] = useState({
    socialMedia: "",
    link1: "",
    link2: "",
    link3: "",
    link4: "",
    link5: "",
    link6:'',
    link7:'',
    link8:'',
    link9:'',
    link10:'',
    month: "",
    date: "",
    birthPlace: "",
    homeTown: "",
    currentAddress: "",
    schooling: "",
    college: "",
    specialized: "",
    experience: "",
    skills: "",
    projects: "",
    currentJob: "",
    creative: "",
    raison: "",
    sacrificed: "",
    challenges: "",
    successes: "",
    description: "",
    ambition: "",
  });
  // console.log(values.date, "checked date")
useEffect(()=>{

  if (STORE.errorPage) {
    navigate('/ErrorPage')
  }
},[])

  const handleChange = (e) => {
    // console.log(e.target.value);
    setValues({
      ...values,
      [e?.target?.name]: e?.target?.value,
    });

  };
  const handleSubmit = async (e) => {
    e.preventDefault();

    const userData = {
      socialMedia: smedia.slice(0,5),
      link1: values.link1,
      link2: values.link2,
      link3: values.link3,
      link4: values.link4,
      link5: values.link5,
      month: values.month,
      date: values.date,
      birthPlace: values.birthPlace,
      homeTown: values.homeTown,
      currentAddress: values.currentAddress,
      schooling: values.schooling,
      college: values.college,
      specialized: values.specialized,
      experience: values.experience,
      skills: values.skills,
      projects: values.projects,
      currentJob: values.currentJob,
      creative: values.creative,
      raison: values.raison,
      sacrificed: values.sacrificed,
      challenges: values.challenges,
      successes: values.successes,
      description: values.description,
      ambition: values.ambition,
    };
    if (values.birthPlace == "") {
      setErr(true)
      return false;
    }

    if (
      // !smedia.length && 
      // values.date == '' && 
      // values.month == '' && 
      // values.birthPlace == '' &&
      values.homeTown == '' &&
      values.link1 == '' &&
      values.link2 == '' &&
      values.currentAddress == '' &&
      values.currentAddress == '' &&
      values.college == '') {
      // setMonth(false);
      // setBirth(false);
      // setSocialMedia(false)

      setTown(false)
      // setSlink1(false)

      setCity(false)
      // setSchool(false)

      return false;
    }


    // if (!smedia.length) {
    //   setSocialMedia(false)
    //   return false;
    // }
    // if (values.date == '') {
    //   setBirth(false)
    //   return false;
    // }
    if (values.homeTown == '') {
      setTown(false)
      return false;
    }
    if (values.currentAddress == '') {
      setCity(false)
      return false;
    }
   if(smedia.length>0 && smedia[0]!=='' && values.link1==''){
    toast.error(`Please Provide ${smedia[0]} URL`)
    return false
   }else if(smedia.length>1 && smedia[1]!=='' && values.link2==''){
    toast.error(`Please Provide ${smedia[1]} URL`)
    return false
   }else if(smedia.length>2 && smedia[2]!=='' && values.link3==''){
    toast.error(`Please Provide ${smedia[2]} URL`)
    return false
   }else if(smedia.length>3 && smedia[3]!=='' && values.link4==''){
    toast.error(`Please Provide ${smedia[3]} URL`)
    return false
   }else if(smedia.length>4 && smedia[4]!=='' && values.link5==''){
    toast.error(`Please Provide ${smedia[4]} URL`)
    return false
   }


    // if (values.link1 == '') {
    //   setSlink1(false)
    //   return false;
    // }
    // if (values.currentAddress == '') {
    //   setCity(false)
    //   return false;
    // }
    // if (values.schooling == '') {
    //   setSchool(false)
    //   return false;
    // }


    dispatch(updateUserDetail(userData, STORE.getProfileData.userData._id, {
      send: (e) => {
        if (e.code == 200) {
          navigate('/DetailSaved')
          play()
        } else {
          alert('something went wrong')
        }
      }
    }))



    // await axios
    //   .post(`${SERVER}/userDetail`, { userData: userData, userId: JSON.parse(user)._id })
    //   .then((res) => {
    //     console.log("data->", res.data);
    //     if (res.data.isSuccess) {
    //       // alert(res.data.message)
    //       toast.success(res.data.message, {
    //         position: 'top-center'
    //       })
    //       navigate('/DetailSaved')
    //     } else if (res.data.error) {

    //       setTimeout(() => {

    //       }, 5000);
    //     } else if (res.data.Error) {
    //       setFindUser(true);
    //       setTimeout(() => {
    //         setFindUser(false);
    //       }, 5000);
    //     }
    //   })
    //   .catch((err) => console.log(err));
  };



  const styleSheet = {
    fontFamily: 'Times New Roman',
    fontSize: '20px'

  }
  const [inputValue, setInputValue] = useState(true)
  const inputText = () => {
    setInputValue(false)
  }
  const newStyle = { color: 'white', borderRadius: "1rem", fontSize: "75%" }

  return (
    // style={BackGround}
    <div className="MebookMetaEdit " style={BackGround}>
      {/* <WriterNav /> */}
      <Grid
        container
      >
        <Grid sx={{ textAlign: 'center' }} item xs={12} md={12} sm={12}>
          {/* <img
            src={image}
            alt=""
            style={{
              width: "13rem",
              height: "9rem",
              // marginLeft:'80px'
            }}
          /> */}
          <MeLogo />
          <h4 style={{ fontFamily: "Times New Roman", fontSize: "125%", fontWeight: 'bold', color: "white" }}> Set Up Your <span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#100892' }}>eBook</span><span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#100892' }}>eta</span> Personal Profile</h4>

          <b><p style={{ fontSize: "100%", color: "white" }}>
            Your Detailed Personal Profile Will Help You
          </p></b>
        </Grid>
        <Grid item xs={3} md={5} sm={4}>
        </Grid>
        <Grid item xs={6} md={4} sm={4}>
          <b>
            {" "}
          </b>
          <ul style={{ fontFamily: "Times New Roman", fontSize: '100%', fontWeight: 'bold', color: "white" }}>
            <li>Identify Your Audience</li>
            <li>Build Your Following</li>
            <li>Share Your On-Board Website</li>
            <li>Discover Your Niche</li>
            <li>Create Powerful and Useful Connections</li>
            <li>Collaborate with Other Creators and Professionals</li>
          </ul>
        </Grid>
        <Grid item xs={3} md={3} sm={4}>

        </Grid>
      </Grid>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
        sx={{ padding: "25px 5px" }}
      >
        <Grid item xs={1} md={1} sm={1}></Grid>
        <Grid
          item
          xs={10}
          md={10}
          sm={10}
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block", color: "white"
          }}
        >
          <h3><b style={{ fontSize: "80%" }}>To offer you the very best MeBookMeta experience, we would like to know a little about you</b></h3>
          <form className="" style={{ margin: "50px 0px", fontFamily: "Times New Roman", fontSize: '20px' }}>

            <div className="form-group   ">
              <div className="row d-flex">
                {/* <div className={!smedia.length ? "col-md-12" : "col-md-4 mt-5"} > */}
                <div className="col-md-12">
                  <Select
                    mode="tags"
                    style={{
                      width: !smedia.length ? "50%" : "100%", borderRadius: "0.5rem"
                    }}
                    size='medium'
                    onChange={handleMedia}
                    tokenSeparators={[',']}
                    options={options}
                    name="socialMedia"
                    placeholder={<p style={{ fontSize: "1rem", color: 'black' }}>Select Your Social Media/Internet Sphere</p>}
                  // value={options.value}
                  />
                  {socialmedia == false ? <p className="" style={{ color: 'red', fontSize: '1rem', textAlign: 'left' }}>please choose any social Media</p> : ''}
                </div>

              </div>
            </div>

            <div className="form-group ">
              <div className="row ">
                {smedia[0] != null ? (
                  <div className={smedia.length === 1 ? 'col-md-12' : smedia.length === 2 ? 'col-md-8 mt-5' : 'col-md-4 mt-5'}>
                    <input type="text" className="form-control" name="link1"
                      value={values.link1} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[0]} profile link`} />

                  </div>
                ) : ""}
                {smedia[1] != null ? (
                  <div className="col-md-4 mt-5">
                    <input type="text" className="form-control" name="link2"
                      value={values.link2} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[1]} profile link`} />
                  </div>
                ) : ""}
                {smedia[2] != null ? (
                  <div className="col-md-4 mt-5">
                    <input type="text" className="form-control" name="link3"
                      value={values.link3} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[2]} profile link`} />

                  </div>
                ) : ""}
                {smedia[3] != null ? (
                  <div className={smedia.length === 4 ? 'col-md-12 mt-5' : smedia.length === 5 ? 'col-md-8 mt-5' : 'col-md-4 mt-5'}>
                    <input type="text" className="form-control" name="link4"
                      value={values.link4} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[3]} profile link`} />
                  </div>
                ) : ""}
                {smedia[4] != null ? (
                  <div className="col-md-4 mt-5">
                    <input type="text" className="form-control" name="link5"
                      value={values.link5} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[4]} profile link`} />
                  </div>
                ) : ""}
                {smedia[5] != null ? (
                  <div className="col-md-4 mt-5">
                    <input type="text" className="form-control" name="link6"
                      value={values.link6} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[5]} profile link`} />

                  </div>
                ) : ""}
                {smedia[6] != null ? (
                  <div className={smedia.length === 7 ? 'col-md-12 mt-5' : smedia.length === 8 ? 'col-md-8 mt-5' : 'col-md-4 mt-5'}>
                    <input type="text" className="form-control" name="link7"
                      value={values.link7} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[6]} profile link`} />
                  </div>
                ) : ""}
                {smedia[7] != null ? (
                  <div className="col-md-4 mt-5">
                    <input type="text" className="form-control" name="link8"
                      value={values.link8} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[7]} profile link`} />
                  </div>
                ) : ""}
                {smedia[8] != null ? (
                  <div className="col-md-4 mt-5">
                    <input type="text" className="form-control" name="link9"
                      value={values.link9} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[8]} profile link`} />
                  </div>
                ) : ""}
                {smedia[9] != null ? (
                  <div className={smedia.length === 10 ? 'col-md-12 mt-5' : 'col-md-4 mt-5'}>
                    <input type="text" className="form-control" name="link10"
                      value={values.link10} style={{ height: "2rem", padding: "0 15px", borderRadius: "0.5rem" }}
                      onChange={handleChange} placeholder={`Enter your ${smedia[9]} profile link`} />
                  </div>
                ) : ""}
              </div>
            </div>

            <div className="form-group  ">
              <div className="row">
                <div className="col-md-4 col-sm-12 col-12 ">
                  <div><label style={newStyle} htmlFor="cars">Birthdate</label></div>
                  <input  type="date" className="form-control datepicker" value={values.date} name="date" onChange={handleChange} style={{ height: "2rem" }} max={maxDate} />
                  {birth == false ? <p className="" style={{ color: 'red', fontSize: '16px', textAlign: 'left', borderRadius: "0.5rem" }}>Please Enter Your date of Birth</p> : ''}
                </div>
                <div className="col-md-4 col-sm-12 col-12 ">
                  <label style={newStyle} htmlFor="">
                    Birthplace*
                  </label>
                  <FormControl fullWidth sx={{ backgroundColor: 'white', borderRadius: "0.5rem" }} variant="outlined" size="small">
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      multiline sx={{ height: "2rem" }}
                      maxRows={3} required
                      value={values.birthPlace}
                      onChange={handleChange}
                      name="birthPlace"
                      aria-describedby="outlined-weight-helper-text"
                    />
                  </FormControl>
                  {err && <p className="" style={{ color: 'red', fontSize: '16px', textAlign: 'left', borderRadius: "0.5rem" }}>Please Enter Your Birthplace</p>}
                </div>
                <div className="col-md-4">
                  <label style={newStyle} htmlFor="">Hometown(City, State)*</label>
                  <FormControl fullWidth sx={{ backgroundColor: 'white', borderRadius: "0.5rem" }} variant="outlined" size="small">
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      multiline required
                      maxRows={3} sx={{ height: "2rem" }}
                      value={values.homeTown}
                      onChange={handleChange}
                      name="homeTown"
                      aria-describedby="outlined-weight-helper-text"
                    />

                  </FormControl>
                  {town == false ? <p className="" style={{ color: 'red', fontSize: '16px', textAlign: 'left', borderRadius: "0.5rem" }}>Please Enter Your Hometown</p> : ''}

                </div>
              </div>
            </div>
            <div className="form-group  mt-4">
              <div className="row ">

                <div className="col-md-4">
                  <label style={newStyle} htmlFor="">Current Residence* (City, State)</label>
                  <FormControl fullWidth sx={{ backgroundColor: 'white', borderRadius: "0.5rem" }} variant="outlined" size="small">
                    <OutlinedInput
                      error={!city}
                      id="outlined-adornment-weight"
                      multiline sx={{ height: "2rem" }}
                      maxRows={3} required
                      name="currentAddress"
                      value={values.currentAddress}
                      onChange={handleChange}
                    />

                  </FormControl>
                  {city == false ? <p className="" style={{ color: 'red', fontSize: '16px', textAlign: 'left' }}>Please Enter Your Current Residence</p> : ''}
                </div>
                <div className="col-md-4">
                  <label style={newStyle} htmlFor="">High School (optional) </label>
                  <FormControl fullWidth sx={{ backgroundColor: 'white', borderRadius: "0.5rem" }} variant="outlined" size="small">
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      multiline
                      maxRows={3} sx={{ height: "2rem" }}
                      name="schooling"
                      value={values.schooling}
                      onChange={handleChange}
                      placeholder='Type your school name'
                    />

                  </FormControl>
                  {school == false ? <p className="" style={{ color: 'red', fontSize: '16px', textAlign: 'left' }}>Please Enter Your School Name</p> : ''}
                </div>
                <div className="col-md-4">
                  <label style={newStyle} htmlFor="">College, University (optional)</label>
                  <FormControl fullWidth sx={{ backgroundColor: 'white', borderRadius: "0.5rem" }} variant="outlined" size="small">
                    <OutlinedInput
                      id="outlined-adornment-weight"
                      multiline
                      maxRows={3} sx={{ height: "2rem" }}
                      name="college"
                      value={values.college}
                      onChange={handleChange}
                    />

                  </FormControl>
                </div>
              </div>
            </div>

            {/* <div className="col-md-4">
                <label style={newStyle} htmlFor="">Specialized Training Provider (Interest or Expertise)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="specialized"
                    value={values.specialized}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
              <div className="col-md-4">
                <label style={{color: 'white',marginBottom:'38px'}} htmlFor="">Work-Related or Unique Creator Experience</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="experience"
                    value={values.experience}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
            </div>
            <div className="row meta my-3">
              <div className="col-md-4">
                <label style={{color: 'white',marginBottom:'38px'}} htmlFor="">Specialized Skills/Training</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="skills"
                    value={values.skills}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
              <div className="col-md-4">
                <label style={{color: 'white',marginBottom:'38px'}} htmlFor="">Past/Current/Future Projects</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="projects"
                    value={values.projects}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
              <div className="col-md-4">
                <label style={newStyle} htmlFor="">Current Employer or Business Affiliation (primary source of income)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="currentJob"
                    value={values.currentJob}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
            </div>
            <div className="row meta my-3">
              <div className="col-md-4">
                <label style={newStyle} htmlFor="">Creative Persona (DBA, Pen/Stage Name, Pseudonym, Alias, Ananym, other)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="creative"
                value={values.creative}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
              <div className="col-md-4">
                <label style={{color: 'white',marginBottom:'38px'}} htmlFor="">Raison d’Etre (Your Purpose, at least for now)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="raison"
                value={values.raison}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
              <div className="col-md-4">
                <label style={newStyle} htmlFor="">Skin in the Game (What You Have Sacrificed to Bring Your Work)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="sacrificed"
                    value={values.sacrificed}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
            </div>
            <div className="row meta my-3">
              <div className="col-md-4">
                <label style={newStyle} htmlFor="">Your Challenges (The Obstacles You Have Had to Overcome)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="challenges"
                    value={values.challenges}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
              <div className="col-md-4">
                <label style={{color: 'white',marginBottom:'38px'}} htmlFor="">Your Perceived Audience (Description)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="description"
                    value={values.description}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
              <div className="col-md-4">
                <label style={{color: 'white',marginBottom:'38px'}} htmlFor="">Your Perceived Success (your ultimate ambition)</label>
                <FormControl fullWidth sx={{ backgroundColor: 'white' }} variant="outlined" size="small">
                  <OutlinedInput
                    id="outlined-adornment-weight"
                    multiline
                    maxRows={3}
                    name="ambition"
                value={values.ambition}
                    onChange={handleChange}
                  />

                </FormControl>
              </div>
            </div> */}
          </form>
        </Grid>
        <Grid item xs={1} md={1} sm={1}></Grid>
        <div className="btn mx-auto">
          {/* <Link to={'/SignIn'}><Button className="mt-2" onClick={()=>navigate('/')}  sx={nb}>
            Go Back
          </Button></Link> */}
          <Button
            // variant="contained"
            className="mt-2"
            sx={nb}
            onClick={handleSubmit}
          >
            Confirm Answers for Your MeBookMeta Personal Profile
          </Button>
        </div>
      </Grid>

      <ToastContainer />

    </div>
  );
}
