import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link, useNavigate, useParams } from "react-router-dom";
import img from "../Screenshot_2.png";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { CardActionArea } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import CommonAppbar from "../Component new/CommonAppbar";
import MenuItem from "@mui/material/MenuItem";
import ListSubheader from "@mui/material/ListSubheader";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import AddHomeWorkIcon from '@mui/icons-material/AddHomeWork';
import "./common.css";
import { useDispatch, useSelector } from "react-redux";
import { getSubCategory, getUserData, mebookProfile } from "../Action";
import { toast } from "react-toastify";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { BackGround, newButton } from "../Component new/background";

export default function AuthorProfile() {

  const bg = { ...BackGround };
  const bgBut = { ...newButton }


  const navigate = useNavigate();
  const params = useParams()
  const dispatch = useDispatch()
  const [open, setOpen] = React.useState(false);
  const [hover, setHover] = useState(false);
  const [category, setCategory] = useState("")

  const STORE = useSelector((state) => state)
  const [backBut, setBackBut] = useState([])
  // if (!STORE.mebookProfile.loading && STORE.mebookProfile.subCategory.errorCode == 200) {
  //   navigate("/CompletionPhase");
  // }
  // if (!STORE.mebookProfile.loading && STORE.mebookProfile.subCategory.errorCode == 400) {
  //   // toast.error(STORE.mebookProfile.subCategory.message,{position:'top-right'})
  //   navigate("/CompletionPhase");
  // }


  const handleClickOpen = () => {
    setOpen(true);
    console.log("selectedId", selectedId);
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (STORE.errorPage) {
    navigate('/ErrorPage')
  }
  useEffect(() => {
    setCategory(params.category)
  }, [])
  const userId = sessionStorage.getItem("userId");
  const [check, setCheck] = useState("");
  const [checkValue, setCheckValue] = useState([]);
  const [selectedId, setSelectedId] = useState([]);
  const [id, setId] = useState(0)
  const handleChange = (val) => {
    // console.log('id', selectedId)
    let temp = checkValue;
    if (temp.some((item) => item === val._id)) {
      selectedId.splice(selectedId.indexOf(val.name), 1);
      setSelectedId([...selectedId]);
      temp = temp.filter((item) => item !== val._id);
    } else {
      temp.push(val._id);
      setSelectedId([...selectedId, val.name]);
    }
    setCheckValue(temp);
  };

  // useEffect(() => {
  //   console.log(selectedId);
  // }, [selectedId]);
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  // useEffect(() => {
  //   dispatch(getUserData())
  //   setTimeout(()=>{
  //     if(!STORE.getSubCategory.subCategory && !STORE.getSubCategory.loading){
  //       navigate('/CreatorNew1')
  //     }
  //   },1000)
  // }, []);
  const handleSubmit = (e) => {
    console.log("checkValue",checkValue, STORE.getProfileData?.userData?._id)
    dispatch(mebookProfile({ subCategory: checkValue, userId: STORE.getProfileData?.userData?._id, category: params.category }))
    setTimeout(() => {
      // navigate("/CompletionPhase");

    }, 2000);
  };

  const arry = [
    { option1: "Autobiography", id: "1" },
    { option1: "Biography", id: "2" },
    { option1: "Children's", id: "3" },
    { option1: "Comics/Graphic Novels", id: "4" },
    { option1: "Crime/Detective", id: "5" },
    { option1: "Erotica", id: "6" },
    { option1: "Essay", id: "7" },
    { option1: "Ethnic", id: "8" },
    { option1: "Fantasy", id: "9" },
    { option1: "Fiction-Mainstream", id: "10" },
    { option1: "Horror", id: "11" },
    { option1: "History", id: "12" },
    { option1: "Humor", id: "13" },
    { option1: "Literary", id: "14" },
    { option1: "Memoir", id: "15" },
    { option1: "Mystery/Thriller", id: "16" },
    { option1: "Non-Fiction", id: "17" },
    { option1: "Poetry", id: "18" },
    { option1: "Politics", id: "19" },
    { option1: "Religion", id: "20" },
    { option1: "Romance", id: "21" },
    { option1: "Science Fiction", id: "22" },
    { option1: "Short Story", id: "23" },
    { option1: "Spiritual", id: "24" },
    { option1: "Suspense", id: "25" },
    { option1: "Thesis/ Dissertation", id: "26" },
    { option1: "True Crime", id: "27" },
    { option1: "Urban", id: "28" },
    { option1: "Women's", id: "29" },
    { option1: "Young Adult", id: "30" },
  ];
  // let Array = ["Author"];
  // for (let i = 0; i < checkValue.length; i++) {
  //   Array.push(checkValue[i].option1);
  //   localStorage.setItem("MultiOptions", JSON.stringify(Array));
  // }
  // console.log("Array-->", check);

  return (
    <div className="container-fluid" style={bg}>
      {/* <CommonAppbar /> */}
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className=" col-md-8 offset-md-2  text-center">

          <CardContent>


            <Typography
              variant="body2"
              color=""
              sx={{ margin: "5px 0px", fontSize: "25px", color: "white" }}
            >
              {STORE.getProfileData?.userData?.name}, {params.category}

            </Typography>
          </CardContent>

          <div
            style={{
              margin: "15px 0px",
              fontSize: "3vw",
              fontWeight: "bold",
              color: "white",
              textAlign: "center",
            }}
          >
            <span
              style={{}}
            >
              M
            </span>
            <span style={{}}>eBook</span>
            <span style={{}}>M</span>
            <span style={{}}>eta</span> Profile:  {category}
          </div>
        </div>
      </div>
      <div className="row" >
        <div className="col-md-8 offset-md-2 ">

          <CardContent>
            <div className="row mx-2" style={{ minHeight: "50vh" }}>
              {
                STORE.getSubCategory.loading ?
                  <div className="col-md-12">
                    <p className="text-center text-white">Loading....</p>
                  </div>
                  :
                  STORE.getSubCategory.subCategory && STORE.getSubCategory.subCategory.length ? STORE.getSubCategory.subCategory.map((val) => {
                    return (
                      <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div
                          key={val._id}

                          onMouseEnter={() => {
                            setId(val._id)
                            setHover(true)
                          }}
                          onMouseLeave={() => {
                            setId(0)
                            setHover(false)
                          }}
                          className={
                            selectedId.includes(val.name)
                              ? "afterClick"
                              : "beforeClick"
                          }
                          value={checkValue}
                          onClick={() => {
                            if (val.haveSubCategory) {
                              setBackBut([...backBut, category])
                              dispatch(getSubCategory(val.name))
                              setCategory(val.name)
                            } else {
                              handleChange(val)
                            }

                          }}
                          style={hover && (!selectedId.includes(val.name)) && id === val._id ? { backgroundColor: "white", border: '2px solid black', color: 'black !important', outlineColor: 'transparent', outlineStyle: 'solid', boxShadow: '0px 0px 10px 8px black' } : {}}
                        >
                          {/* <AddHomeWorkIcon sx={{ color: 'blue', fontSize: '30px' }} /> <br /> */}
                          <span>{val.name}</span>
                          {
                            val.haveSubCategory ? <ArrowRightIcon sx={{ fontSize: '30px' }} /> :
                              ""
                          }
                        </div>
                      </div>
                    );
                  })
                    : <div className="col-md-12">
                      <h1 className="text-center text-white">No SubCategory yet!</h1>
                      {navigate('/CreatorNew1')}
                    </div>
              }
            </div>
          </CardContent>

          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title" sx={{ fontFamily: "Times New Roman" }}>
              You have Selected {checkValue.length} Options
            </DialogTitle>
            <DialogContent>
              {checkValue !== null ? (
                <>
                  {selectedId.map((i, index) => (
                    <p
                      style={{
                        display: "inline",
                        fontWeight: "bold",
                        fontSize: "25px",
                        color: "#100892",
                        fontFamily: "Times New Roman"
                      }}
                      key={index}
                    >
                      {i},&nbsp;
                    </p>
                  ))}
                </>
              ) : null}
              <DialogContentText id="alert-dialog-description">
                If these are your final choices for your {params.category} profile, please click to confirm
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Go Back</Button>
              <Button onClick={handleSubmit} autoFocus>
                Confirm
              </Button>
            </DialogActions>
          </Dialog>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <Button
              onClick={() => {
                if (backBut && backBut.length) {
                  console.log(backBut)
                  dispatch(getSubCategory(backBut.at(-1)))
                  setCategory(backBut.at(-1))
                  let dummyBack = backBut.pop()
                  setTimeout(() => {
                    setBackBut([...backBut])
                  }, 500)
                } else {
                  navigate(-1)
                }
              }}
              sx={newButton} variant="outlined" size="large"
            >
              Go Back
            </Button>
            <Button
              disabled={!selectedId.length ? true : false}
              variant="outlined"
              size="large"
              sx={newButton}
              onClick={handleClickOpen}
            >
              Continue
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}