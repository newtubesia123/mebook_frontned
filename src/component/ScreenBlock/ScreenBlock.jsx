import React, { useState, useEffect } from 'react'
import './ScreenBlock.scss'
// import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import '../../dashboardComponent/ActivePlan.css'
import Button from '@mui/material/Button';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import { useSelector } from 'react-redux';
import LogoutIcon from '@mui/icons-material/Logout';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  // display: "flex",
  // justifyContent: "center",
  background: "black",
  // alignItems: "center",
  borderRadius: "28px",
  padding: 0,
  margin: 0
};

const ScreenBlock = ({ logOut, isBlock }) => {

  const [open, setOpen] = React.useState(true);
  const handleClose = () => setOpen(false);
  const STORE = useSelector((state) => state)

  return (
    <div>
      <Modal
        open={isBlock}
        // onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description" sx={{ background: "rgb(0,0,0,1)" }}
      >
        <Box sx={style}>
          <div class="ag-courses_box">
            <div class="ag-courses_item">
              <a href="" class="ag-courses-item_link">
                <div class="ag-courses-item_bg"></div>

                <div class="ag-courses-item_title">
                  Your <span style={{ textTransform: 'capitalize' }}>{STORE?.userDashboardDetail?.userWorkDetail?.subscriptionType?.subscriptionType}</span> plan is expired!
                </div>
                <div class="ag-courses-item_title">
                  To enjoy it pay now
                </div>
                <div class="ag-courses-item_date-box" style={{ display: "flex", gap: "2rem" }}>
                  <Button variant="outlined" color="error" size="medium" onClick={() => logOut()}>
                    Logout <LogoutIcon />
                  </Button>
                  <Button variant="contained" color="success" size="medium">
                    Pay Now <AttachMoneyIcon />
                  </Button>
                </div>
              </a>
            </div>
          </div>
        </Box>
      </Modal>
    </div>


  )
}


export default ScreenBlock