import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link, useNavigate } from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { BackGround, newButton } from "../Component new/background";
import "./common.css";
import { Cascader } from 'antd';
const { SHOW_CHILD } = Cascader;


const Genre = [
    { label: 'Ballet', value: 'Ballet' },
    { label: 'Comedy', value: 'Comedy' },
    { label: 'Drama', value: 'Drama' },
    { label: 'Experimental', value: 'Experimental' },
    { label: 'Fantasy', value: 'Fantasy' },
    { label: 'Farcical', value: 'Farcical' },
    { label: 'History', value: 'History' },
    { label: 'Magic', value: 'Magic' },
    { label: 'Musical', value: 'Musical' },
    { label: 'Play', value: 'Play' },
    { label: 'Opera', value: 'Opera' },
    { label: 'Physical', value: 'Physical' },
    { label: 'Puppetry', value: 'Puppetry' },
    { label: 'Tragedy', value: 'Tragedy' },
    { label: 'Variety', value: 'Variety' },
    { label: 'Ventriloquism', value: 'Ventriloquism' },
    { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Talent = [
    { label: 'Actor', value: 'Actor' },
    { label: 'Choir', value: 'Choir' },
    { label: 'Chorus Line', value: 'Chorus Line' },
    { label: 'Dancer', value: 'Dancer' },
    { label: 'Musician', value: 'Musician' },
    { label: 'Singer', value: 'Singer' },
    { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Jobs = [
    {
        label: <b style={{ fontSize: '20px' }}>Designer</b>, value: 'Designer',
        children: [
            { label: 'Costume/Clothing', value: 'Costume/Clothing' },
            { label: 'Hair', value: 'Hair' },
            { label: 'Make-up', value: 'Make-up' },
            { label: 'Set', value: 'Set' },
        ]
    },
    {
        label: <b style={{ fontSize: '20px' }}>Director</b>, value: 'Director',
        children: [
            { label: 'Artistic', value: 'Artistic' },
            { label: 'Musical', value: 'Musical' },
            { label: 'Special Effects', value: 'Special Effects' },
            { label: 'Stage', value: 'Stage' },
            { label: 'Technical', value: 'Technical' },
        ]
    },
    {
        label: <b style={{ fontSize: '20px' }}>Manager</b>, value: 'Manager',
        children: [
            { label: 'Costume', value: 'Costume' },
            { label: 'Production', value: 'Production' },
            { label: 'Prop Master', value: 'Prop Master' },
            { label: 'Script', value: 'Script' },
        ]
    },
    {
        label: <b style={{ fontSize: '20px' }}>Production</b>, value: 'Production',
        children: [
            { label: 'Director', value: 'Director' },
            { label: '1st Assistant Director', value: '1st Assistant Director' },
            { label: '2nd Assistant Director', value: '2nd Assistant Director' },
            { label: '2nd 2nd Assistant Director', value: '2nd 2nd Assistant Director' },
            { label: 'Script Supervisor', value: 'Script Supervisor' },
            { label: 'Technical Director', value: 'Technical Director' },
            { label: 'Performance Director', value: 'Performance Director' },
            { label: 'Arts and Crafts Director', value: 'Arts and Crafts Director' },
            { label: 'Casting Director', value: 'Casting Director' },
            { label: 'Still Photographer', value: 'Still Photographer' },
            { label: 'Production Assistant', value: 'Production Assistant' },
            { label: 'Location Manager', value: 'Location Manager' },
            { label: 'Accountant', value: 'Accountant' },
            { label: 'Teacher/Educator', value: 'Teacher/Educator' },
            { label: 'Suggest Tag', value: 'Suggest Tag' },

        ]
    }
]

export default function CreatorTheatrical() {
    const navigate = useNavigate();

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const user = sessionStorage.getItem('userName')
    var identity = sessionStorage.getItem('identity Option')
    let workProfile = localStorage.getItem("Work profile")
    const [userInfo, setuserInfo] = useState({
        pdf: "",
        file: "",
        filepreview: null,
    });
    useEffect(() => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios
            .get("http://54.246.61.54:3002/getuserById", {
                headers: { Authorization: `Bearer ${token}` },
            })

            .then((res) => {
                console.log("res.data---> ", res.data);
                if (res.data) {
                    setuserInfo({
                        ...userInfo,
                        file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
                        filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
                    });
                }
            })
            .catch((err) => console.log(err));
    }, []);

    const arry = [
        { option1: "Dance ➢", id: '1' },
        { option1: "Humor/Stand-up ➢", id: '2' },
        { option1: "Lecture ➢", id: '3' },
        { option1: "Music➢", id: '4' },
        { option1: "Pitch ➢", id: '5' },
        { option1: "Poetry➢", id: '6' },
        { option1: "Spoken Word ➢", id: '7' },
        { option1: "PR/Promotion", id: '8' },
        { option1: "Recital ➢", id: '9' },
        { option1: "Rights, Brand, Royalties", id: '10' },
    ];
    const [checkValue, setCheckValue] = useState([])
    const handleChange = (val) => {
        let temp = checkValue;
        if (temp.some((item) => item.id === val.id)) {
            temp = temp.filter((item) => item.id !== val.id);
        } else {
            temp.push(val);
        }
        setCheckValue(temp);
    };
    const handleSubmit = (e) => {
        navigate('/CompletionPhase', { state: { option: 'Performer' } })
    };
    let Array = ['Performer'];
    for (let i = 0; i < checkValue.length; i++) {
        Array.push(checkValue[i].option1);
        localStorage.setItem('MultiOptions', JSON.stringify(Array))

    }
    return (
        <div className="container-fluid" style={BackGround}>
            <div className="img text-center">
                <img
                    src={image}
                    alt=""
                    style={{
                        width: "232.5px",
                        height: "131px",
                        margin: "20px 0px",
                    }}
                />
            </div>
            <div className="row">
                <div className="col-md-8 offset-md-2 text-center">

                    <CardContent>
                        <div>
                            <Avatar
                                alt="Remy Sharp"
                                src={userInfo.filepreview}
                                sx={{
                                    width: "150px",
                                    height: "130px",
                                    margin: "auto",
                                }}
                            />
                        </div>

                        <Typography
                            variant="body2"
                            color=""
                            sx={{
                                margin: "15px 0px",
                                fontSize: "25px",
                                color:"white"
                            }}
                        >
                            {user}, {identity}, {workProfile} <br /> Performer
                        </Typography>
                    </CardContent>

                    <div
                        style={{
                            margin: "0px 0px",
                            fontSize: "2.5vw",
                            fontWeight: "bold",
                            color: "white",
                            textAlign: "center",
                            fontFamily: "Times New Roman"

                        }}
                    >
                        <span style={{ color: '' }}>M</span><span style={{ color: '' }}>ebook</span> <span style={{ color: '' }}>M</span><span style={{ color: '' }}>eta</span> Book Profile: {identity}, {workProfile}, Theatrical
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-8 offset-md-2">

                    <CardContent>
                        <div className="row mx-2">

                            <div className=" col-md-6 col-sm-6 col-12 mt-4">
                                <Cascader
                                    style={{
                                        width: '100%',
                                    }}
                                    size="large"
                                    options={Genre}
                                    // onChange={onChange1}
                                    multiple
                                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                                    maxTagCount="responsive"
                                    showCheckedStrategy={SHOW_CHILD}
                                    defaultValue={<b style={{ fontSize: '20px' }}>Genre</b>}
                                />

                            </div>
                            <div className=" col-md-6 col-sm-6 col-12 mt-4">
                                <Cascader
                                    style={{
                                        width: '100%',
                                    }}
                                    size="large"
                                    options={Talent}
                                    // onChange={onChange1}
                                    multiple
                                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                                    maxTagCount="responsive"
                                    showCheckedStrategy={SHOW_CHILD}
                                    defaultValue={<b style={{ fontSize: '20px' }}>Talent</b>}
                                />

                            </div>
                            <div className=" col-md-6 col-sm-6 col-12 mt-4">
                                <Cascader
                                    style={{
                                        width: '100%',
                                    }}
                                    size="large"
                                    options={Jobs}
                                    // onChange={onChange1}
                                    multiple
                                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                                    maxTagCount="responsive"
                                    showCheckedStrategy={SHOW_CHILD}
                                    defaultValue={<b style={{ fontSize: '20px' }}>Jobs</b>}
                                />

                            </div>


                        </div>
                    </CardContent>

                    <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">
                            You have Selected {checkValue.length} Options
                        </DialogTitle>
                        <DialogContent>
                            {checkValue !== null ? (
                                <>
                                    {Array.map((i, index) => (
                                        <p style={{ display: 'inline', fontWeight: 'bold', fontSize: '25px', color: '#100892' }} key={index}>
                                            {i},
                                        </p>
                                    ))}
                                </>
                            ) : null}
                            <DialogContentText id="alert-dialog-description">
                                If want to Continue Please click on Agree otherwise click on Disagree
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>Disagree</Button>
                            <Button onClick={handleSubmit} autoFocus>
                                Agree
                            </Button>
                        </DialogActions>
                    </Dialog>
                    <div className="d-flex justify-content-between my-1 ">
                        <Link to={'/CreatorNew1'}><button
                            type="button"
                            class="btn  btn-lg"
                            style={newButton}
                        >
                            Go Back
                        </button></Link>
                        <button
                            type="button"
                            class="btn  btn-lg"
                            style={newButton}
                            onClick={handleClickOpen}
                        >
                            Continue
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}
