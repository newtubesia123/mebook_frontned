import React, { Component } from "react";
import Slider from "react-slick";
import './CrouselNew.scss';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import CategoryIcon from '@mui/icons-material/Category';
import { SERVER } from "../../server/server";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
const CrouselNew = ({ id, work, title, exTitle }) => {
    const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
        <img src='https://www.pngfind.com/pngs/m/198-1986126_png-file-svg-left-arrow-svg-transparent-png.png' alt="prevArrow" {...props} />
    );

    const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
        <img src='https://www.svgrepo.com/show/55411/right-arrow.svg' alt="nextArrow" {...props} />
    );
    const getImgURL = (name) => `${SERVER}/uploads/${name}`
    const STORE = useSelector((state) => state);
    const navigate = useNavigate();


    var settings = {
        dots: true,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 3000,
        cssEase: 'linear',
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: false,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    const getDate = (str) => {
        // var str = "Fri Feb 08 2013 09:47:57 GMT +0530 (IST)";
        var date = new Date(str);
        var day = date.getDate(); //Date of the month: 2 in our example
        var month = date.getMonth(); //Month of the Year: 0-based index, so 1 in our example
        var year = date.getFullYear() //Year: 2013
        return `${day}/${month + 1}/${year}`;

    }

    return (
        <div style={{ marginTop: '1rem', overflow: 'hidden' }}>
            <div className="card__container">
                <h1 className="profile-card-inf__txt" style={{color: '#2B3467',height:'50px',paddingTop:'10px'}}>
                    <span style={{backgroundColor:'#2B3467',marginRight:'10px',padding:'5px 15px 5px 15px',borderRadius:'50%',color:'white'}}>
                    {STORE.userWorkDetail?.userWorkDetail?.message?.length}
                    </span>
                    
                 {exTitle}</h1>
                {/* <div className="profile-card-inf__item">
                                <div className="profile-card-inf__title" style={{
                                    color: 'rgb(0, 0, 0)'
                                }}>
                                    {STORE.userWorkDetail?.userWorkDetail?.message?.length}
                                </div>
                                <div className="profile-card-inf__txt" style={{
                                    color: 'rgb(113, 5, 125)'
                                }}>
                                    Work Samples
                                </div>
                            </div> */}
                <Slider {...settings} className="card__container--inner">

                    {work.map((item, index) => {
                        return (
                            <div
                                className="card__container--inner--card"
                                key={index}>


                                <img src={getImgURL(item.coverPicture)} onClick={() => {

                                    navigate(STORE.getProfileData.success ?
                                        `/MainDashboard/WorkView/${item.userWork}/${item.youtubeUrl}` :
                                        `/WorkView/${item.userWork}/${item.youtubeUrl}`)

                                }} />

                                <div className="card__container--inner--card--date_time">
                                    {/* <img src="https://www.wanderon.in/svg/clock.svg" alt="time" /> */}
                                    {/* <div className="card__container--inner--card--date_time--date_icon">

                                    </div> */}

                                    <p><AccessTimeIcon style={{ fontSize: '13px', objectFit: 'contain', paddingRight: '2px' }} />{getDate(item.createdAt
                                    )}</p>

                                    <p><CategoryIcon style={{ width: '10px', height: '10px', objectFit: 'contain', paddingRight: '2px' }} />{item.subCategory.name}</p>
                                    {/* <img src="https://www.wanderon.in/svg/map-pin.svg" alt="location" style={{ marginLeft: 10 }} /> */}
                                    <p><ThumbUpAltIcon style={{ width: '10px', height: '10px', objectFit: 'contain', paddingRight: '2px' }} />{item.liked}</p>
                                </div>


                                <h2>{item.title}</h2>
                                <p style={{ paddingLeft: '5px' }}
                                // onClick={() =>{ navigate(`/MainDashboard/UserProfile/:${item.userId.name}/:${item.userId._id}`)}}
                                >
                                    by {item.userId.name.toUpperCase()}</p>

                                {/* <p>starts at <span>₹15,999/-</span></p> */}
                            </div>
                        );
                    })}


                </Slider>
            </div>


        </div>
    )
}

export default CrouselNew;