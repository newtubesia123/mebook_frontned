import React from 'react'
import Button from '@mui/material/Button';
import image from "../mepic.png";
import { toast } from 'react-toastify';

import { BackGround, newButton } from '../Component new/background';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import MeLogo from '../assets/MeLogoMain';
const ErrorPage = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const logOut = () => {
    localStorage.removeItem("token")
    // dispatch({ type: 'LOG_OUT' })
    // dispatch({ type: 'CLEAR_USER_DATA' })
    // dispatch({ type: 'CLEAR_DASHBOARD' })
    toast.success('Please Login Again', { position: 'bottom-right', autoClose: 5000, pauseOnHover: false })
    navigate('/SignIn')
    dispatch({ type: "USER_LOGOUT" })
  }
  const bg = { ...BackGround, padding: 0, margin: 0, textAlign: 'center', minHeight: "100vh" };
  const bgButt = { ...newButton, marginTop: "1rem" }

  return (
    <div style={bg}>
      <div className='container-fluid' style={{ padding: '1rem' }} >
        <h1 style={{ fontSize: '3rem', color: 'white' }}>OOPS !</h1>
        <h2 style={{ color: 'white', fontSize: '2rem' }}>Network Error</h2>
        <div style={{ margin: "20px 0px" }}>
          <MeLogo />
        </div>
        <div >
          <Button onClick={() => {
            logOut()
          }} variant="contained" size="large" sx={bgButt}>
            Go BACK</Button>
        </div>
      </div>
    </div>
  )
}

export default ErrorPage