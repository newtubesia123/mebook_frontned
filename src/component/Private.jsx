import React, { useEffect } from "react";
import { Navigate, Outlet } from "react-router-dom";

export default function Private() {
  const routesPath = () => {
    let routes = window.location.href.split('/');
    let position = routes.splice(0, 4);
    let index = routes.indexOf('UserProfile')
    if (index !== -1) {
      routes[index] = 'Profile'
      return routes.join('/')
    } else {
      return routes.join('/')
    }
  }
  const auth = localStorage.getItem("token");
  return auth ? <Outlet /> : <Navigate to={routesPath()} />
}
