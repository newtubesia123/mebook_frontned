import { Dialog, styled, Box } from "@mui/material";
//components
import Menu1 from "./menu/Menu1";
import ChatBox1 from "./chat/ChatBox1";

const Component = styled(Box)`
  display: flex;
`;

const LeftComponent = styled(Box)`
  minWidth: 450px;
`;

const RightComponent = styled(Box)`
  width: 76%;
  minWidth: 300px;
  height: 100%;
  borderLeft: 3px solid rgba(0, 0, 0, 0.14);
  background: black;
`;

const dialogStyle = {
  height: "100%",
  width: "100%",
  margin: "0px",
  maxWidth: "100%",
  maxHeight: "100%",
  borderRadius: 0,
  boxShadow: "none",
  overflow: "hidden",
};


const ChatDialog1 = () => {
  return (
    <Dialog
      open={true}
      BackdropProps={{ style: { backgroundColor: "unset" } }}
      PaperProps={{ sx: dialogStyle }}
    >
        <Component>
          <LeftComponent>
            <Menu1 />
          </LeftComponent>
          <RightComponent>
                <ChatBox1/> 
          </RightComponent>
        </Component>
    </Dialog>
  );
};

export default ChatDialog1;
