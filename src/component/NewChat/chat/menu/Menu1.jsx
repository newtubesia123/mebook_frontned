import React, { useState } from 'react';

import { Box, List } from '@mui/material';

//components
import Header from './Header';
import Search1 from './Search1';
import Conversations1 from './Conversations1';

const Menu1 = () => {

    const [seachedList, setSearhList] = useState([])

    function getSearcList(list){
        setSearhList(list);
    }
    
    return (
        <Box>
            <Header/>
            <div style={{background: "linear-gradient(329.04deg, #3981ED 1.96%, #000000 95.27%"}}>
            <Search1 getSearcList={getSearcList}  />
            <Conversations1 seachedList={seachedList} />
            </div>
           
        </Box>
    )
}

export default React.memo(Menu1);