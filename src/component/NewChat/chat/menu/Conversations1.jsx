import React  from "react";
import { Box, styled, Divider } from "@mui/material";

//components
import { useSelector } from "react-redux";
import Conversation1 from "./Conversation1";
import MeCircularProgress from "../../../../components/componentsC/meCircularProgress/MeCircularProgress";

const Component = styled(Box)`
  overflow: overlay;
  height: 81vh;
  ::-webkit-scrollbar {
    width: 12px;
    border: 2px solid black;
    border-radius: 8px;
    background: white;
  }

  ::-webkit-scrollbar-thumb {
    background: black;
    border-radius: 8px;
  }
`;

const StyledDivider = styled(Divider)`
  margin: 0 0 0 0;
  background-color: #e9edef;
  opacity: 0.6;
`;
const EmptyComponent = styled(Box)`
  display:flex;
  justify-content:center;
  height: 81vh;
`;
const Conversations1 = ({seachedList}) => {
  const STORE = useSelector((state) => state);
  const {list,loading} = STORE?.messageSection?.userList;
//   useEffect(()=>{
// console.log("STORE?.messageSection?.userList",STORE?.messageSection)
//   },[STORE])
  return (
    <>
      <Component>
            {   
            loading ?
                <EmptyComponent>
                    <MeCircularProgress />
                </EmptyComponent>
                :

                 seachedList == "No User Found" ?
                  <EmptyComponent>
                         <h3 style={{color:"white", fontSize:"2rem"}}>No User Found</h3>
                  </EmptyComponent>
                :
                seachedList?.length>0 && seachedList?.map((value, ind) => {
                    return <Conversation1 key={"Conversation1"+ind} value={value} />;
                }) 

               
            }
        {/* <StyledDivider /> */}
      </Component>
    </>
  );
};

export default Conversations1;

// trash
 

  // let togle = false;

  // useEffect(() => {
  //   setList(filterListState);
  // }, [filterListState]);

  // useEffect(() => {
  //   setList(filterListState);
  // }, []);


  // function formatTimestamp(timestamp) {
  //   const now = new Date();
  //   const providedDate = new Date(timestamp);

  //   const isToday = now.toDateString() === providedDate.toDateString();
  //   const isYesterday =
  //     new Date(
  //       now.getFullYear(),
  //       now.getMonth(),
  //       now.getDate() - 1
  //     ).toDateString() === providedDate.toDateString();

  //   let formattedTime = "";

  //   if (isToday) {
  //     formattedTime = providedDate.toLocaleTimeString("en-US", {
  //       hour: "numeric",
  //       minute: "numeric",
  //     });
  //     formattedTime += " today";
  //   } else if (isYesterday) {
  //     formattedTime = providedDate.toLocaleTimeString("en-US", {
  //       hour: "numeric",
  //       minute: "numeric",
  //     });
  //     formattedTime += " yesterday";
  //   } else {
  //     formattedTime = providedDate.toLocaleTimeString("en-US", {
  //       hour: "numeric",
  //       minute: "numeric",
  //       day: "numeric",
  //       month: "numeric",
  //       year: "numeric",
  //     });
  //   }

  //   console.log("formattedTime", formattedTime);
  // }


  // formatTimestamp(userList.getAllUserList[0].createdAt  )