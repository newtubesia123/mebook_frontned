import React, { useEffect } from "react";

import { styled, Box, Typography } from "@mui/material";
import { getImgURL } from "../../../../util/getImgUrl";
import { useDispatch } from "react-redux";
import { getRecognization } from "../../../../Action/messages/actions";
import { SOCKET } from "../../../../App";
import { useOwnIdentity } from "../../../../Hooks/useOwnIdentity";
import axios from "axios";
import { SERVER } from "../../../../server/server";

const Component = styled(Box)`
  height: 4.5rem;
  display: flex;
  padding: 0.8rem 10px;
  gap: 1rem;
  cursor: pointer;
  border: 2px solid black;
  border-radius: 1rem;
  margin: 7px 14px;
  justify-content: center;
  align-items: center;
  background: white;
`;

const Image = styled("img")({
  width: "3.5rem",
  height: "3.5rem",
  objectFit: "cover",
  borderRadius: "50%",
  border: "2px solid black",
});

const Container = styled(Box)`
  display: flex;
`;

const Timestamp = styled(Typography)`
  font-size: 12px;
  margin-left: auto;
  color: #00000099;
  margin-right: 20px;
`;

const Text = styled(Typography)`
  display: block;
  color: rgba(0, 0, 0, 0.6);
  font-size: 14px;
`;

const nofication = {
  border: "2px solid black",
  padding: ".2rem 1rem",
  "border-radius": "18px 2px 18px 2px",
  background: "#0dabed",
  "font-weight": " 700",
};

const Conversation1 = ({ value }) => {
  const dispatch = useDispatch();
  const {ownId} = useOwnIdentity()



  function getFndChat(user){
    dispatch(getRecognization(user))
    global.ACTIVE_USER = user?._id
  }



//   useEffect(()=>{
//     SOCKET.on("isonlinecallback",(e)=>{
//       console.log("isonlinecallback",e)
//     });
//   return ()=>{
//     SOCKET.off("isonlinecallback")
//   }
// },[global.ACTIVE_USER,SOCKET])

  return (
    <>
      <Component sx={global.ACTIVE_USER==value?.reciever?._id?{backgroundColor:'lightblue'}:{}} onClick={() => getFndChat(value?.reciever)}>
        <Box sx={{ display: "flex" }}>
          <Image
            src={
              value?.reciever?.pdf !== null
                ? getImgURL(value?.reciever?.pdf)
                : `https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`
            }
            alt="display picture"
          />
        </Box>
        <Box style={{ width: "100%" }}>
          <Container>
            <Typography sx={{ fontSize: "1.5rem", fontWeight: "600" }}>
              {/* {value?.reciever?.name} */}
              {value?.reciever?.name?.split(' ')?.slice(0,2)?.join(' ')}{value?.reciever?.name?.split(' ')?.length>2?'...':''}
            </Typography>
            
          </Container>
          {/* <Box>
            <Text
              sx={{ fontSize: "1rem", fontWeight: "600", color: " #0dabed" }}
            >
            </Text>
          </Box> */}
        </Box>
        {value?.unseenMessage > 0 ? <span style={nofication}>{value?.unseenMessage}</span> : ""}
      </Component>
    </>
  );
};

export default React.memo(Conversation1);
