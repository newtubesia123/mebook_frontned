
import React, { useState } from 'react';
import { Box, Typography, styled } from '@mui/material';
import { Chat as MessageIcon } from '@mui/icons-material';
import { getImgURL } from '../../../../util/getImgUrl';

//components
import HeaderMenu from './HeaderMenu';
import InfoDrawer from '../../drawer/Drawer';
import { useSelector } from 'react-redux';
import { useOwnIdentity } from '../../../../Hooks/useOwnIdentity';

const Component = styled(Box)`
    height: 4rem;
    // background: #ededed;
    background : black;
    display: flex;
    gap: 4rem;
    padding: 0.5rem 1rem;
    align-items: center;
`;

const Wrapper = styled(Box)`
    margin-left: auto;
    & > * {
        margin-left: 2px;
        padding: 8px;
        color: #000;
    };
    & :first-child {
        font-size: 22px;
        margin-right: 8px;
        margin-top: 3px;
    }
`;

const Image = styled('img')({
    width: 40,
    height: 40,
    objectFit: 'cover',
    borderRadius: '50%',
})

const Header = () => {

    const {ownPdf} = useOwnIdentity()
    const [openDrawer, setOpenDrawer] = useState(false);
    const toggleDrawer = () => {
        setOpenDrawer(true);
    }
    return (
        <>
            <Component>
                <Image src={ownPdf?getImgURL(ownPdf):"https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png"} onClick={() => toggleDrawer()} />
                <div>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{
                            flexGrow: 1, display: { xs: 'none', sm: 'none', md: 'block' }, color: 'red', fontSize: "2rem", marginTop: "", fontWeight: "bold", textShadow: '0.2px 0.5px white'
                        }}
                    >


                        {" "}
                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                        <span style={{ color: "#0a0a8a" }}>eBook</span>
                        <span style={{ color: "rgb(187 11 11)" }}>M</span>
                        <span style={{ color: "#0a0a8a" }}>eta</span>

                    </Typography>
                </div>
            </Component>
            <InfoDrawer open={openDrawer} setOpen={setOpenDrawer} profile={true} />
        </>
    )
}

export default React.memo(Header);