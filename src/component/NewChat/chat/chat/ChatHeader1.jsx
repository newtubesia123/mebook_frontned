import { Box, Typography, styled } from "@mui/material";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getImgURL } from "../../../../util/getImgUrl";
import { useEffect, useState } from "react";
import { useRecognization } from "../../../../Hooks/useRecognization";
import axios from "axios";
import { SERVER } from "../../../../server/server";
import BackBtn from "../../../../components/componentsB/btn/backBtn/BackBtn";

const Header = styled(Box)`
  height: 4rem;
  background: #ededed;
  display: flex;
  padding: 8px 16px;
  align-items: center;
  justify-content:space-between;
  background: black;
`;
// const header = {
//     "height": "44px", "background": "#ededed", "display": "flex", "padding": "8px 16px", "alignItems": "center"
// }

const Image = styled("img")({
  width: "3.5rem",
  height: "3.5rem",
  objectFit: "cover",
  borderRadius: "50%",
  border: "2px solid white",
});

const Name = styled(Typography)`
  font-size: 1.3em;
  font-weight: 600;
  color: white;
`;

const RightContainer = styled(Box)`
  margin-left: auto;
  & > svg {
    padding: 8px;
    font-size: 22px;
    color: #000;
  }
`;

const Status = styled(Typography)`
  font-size: 12px !important;
  color: rgb(0, 0, 0, 0.6);
  margin-left: 12px !important;
`;

const Text = styled(Typography)`
  display: block;
  color: rgba(0, 0, 0, 0.6);
  font-size: 14px;
`;

const ChatHeader1 = () => {
  const {messageSection:
    {activeUser:
        {_id,email,name,pdf}
    }} = useSelector((state)=>state)
    const [isOnline , setIsOnline] = useState(false)
    const [loading , setLoading] = useState(false)
    const isOwn = useRecognization(_id)
    async function checkOnline (userId) {
      if(userId){
        setLoading(true)
        let isOnline = await axios.get(`${SERVER}/messages/onlineStatus/${userId}`)
        setLoading(false)
        return setIsOnline(isOnline.data)
      }
      return setIsOnline(false)
    }
    useEffect(()=>{
      checkOnline(_id)
    },[_id])
  return (
    <Header>
      <Box sx={{display:'flex',justifyContent:'center',alignItems:'center'}}>
        {
            !isOwn && <>
             <Image
                src={
                pdf ? getImgURL(pdf) 
                    : "https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png"
                }
                alt="display picture"
            />
            <Box sx={{ marginLeft: "15px" }}>
                <Name>{name}</Name>
                <Text sx={{ fontSize: "1rem", fontWeight: "600", color: " #0dabed" }}>
                {loading && "checking..."}
                {!loading && (isOnline?"Online":"Offline")}
                </Text>
            </Box>
            </>
        }
        </Box>
        <BackBtn style={{ margin: '1rem' }} />

      {/* <RightContainer>
                <Search />
                <MoreVert />
            </RightContainer> */}
    </Header>
  );
};

export default ChatHeader1;
