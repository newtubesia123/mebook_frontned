import { useEffect, useRef, useState } from "react";
import { Box, styled } from "@mui/material";
 

//components
import { useDispatch, useSelector } from "react-redux";
import Message1 from "./Message1";
import { useOwnIdentity } from "../../../../Hooks/useOwnIdentity";
import MeCircularProgress from "../../../../components/componentsC/meCircularProgress/MeCircularProgress";
import { getUserMsg } from "../../../../Action/messages/actions";
import EmptyChat from "./EmptyChat";
import { SOCKET } from "../../../../App";
const Wrapper = styled(Box)`
  //  background-image:url(../chat/.././../global1.jpg);

  background-size: 50%;
  border: 1px solid black;
  border-radius: 1rem 1rem 2px 2px;
  border-bottom: none;
  margin: 0px -2px;
  background: white;
`;

const StyledFooter = styled(Box)`
  height: 55px;
  background: #ededed;
  // position: absolute;
  width: 100%;
  // bottom: 0
`;

const Component = styled(Box)`
  background-image: url(../chat/.././../global1.jpg);
  height: 80vh;
  overflow-y: auto;

  ::-webkit-scrollbar {
    width: 12px;
    border: 2px solid black;
    border-radius: 8px;
  }

  ::-webkit-scrollbar-thumb {
    background: black;
    border-radius: 8px;
  }
`;

const Container = styled(Box)`
  padding: 1rem 1rem;
`;
const EmptyComponent = styled(Box)`
  display:flex;
  justify-content:center;
  height: 81vh;
`;

const Messages1 = () => {

  const scrollRef = useRef(null);
  const dispatch = useDispatch();
  const {ownId} = useOwnIdentity();
  const {messageSection:
    {activeUser:
        {_id}
    }} = useSelector((state)=>state)
const {messageSection:
    {userMessage:
        {loading,data}
    }} = useSelector((state)=>state)
    const [updatedDatas , setUpdatedDatas] = useState([])
    const [seenUserId,setSeenUserId] = useState("")
// getting messages
function O_messageRecieved ({fromSelf,message,recieverId,sender,time}) {
  if(sender!=ownId){
      if(global.ACTIVE_USER==sender){
      dispatch({type:'GET_USER_MSG_BY_SOCKET',payload:{fromSelf,message,recieverId,sender,time}})
      }else{
          let updatadUser = global.USER_LIST?.find((individualUser)=>individualUser.reciever?._id==sender)
          updatadUser.unseenMessage+=1
          let updatedList = global.USER_LIST?.filter((individualUser)=>individualUser.reciever?._id!=sender)
          global.USER_LIST = [updatadUser,...updatedList]
          dispatch({type:'GET_USER_LIST',payload:[updatadUser,...updatedList]})
      }
  }
}
function O_messageSeen ({id}) {
  console.log(("calling..",id))
  setTimeout(()=>{
    dispatch(getUserMsg({from: ownId,to:id},()=>{
      SOCKET.timeout(1000).emit("msg-seen", {sender: global.ACTIVE_USER,recieverId: ownId},()=>{
          let updatedList = global.USER_LIST.map((individualUser)=>{
            let result = individualUser
            if(individualUser.reciever?._id==global.ACTIVE_USER){
                individualUser.unseenMessage=0
            }
            return result
        })
        dispatch({type:'GET_USER_LIST',payload:updatedList})
        });
      }))
  },2000)
}
useEffect(()=>{
  setUpdatedDatas(data)
},[data])
useEffect(()=>{
    SOCKET.on("msg-recieve",O_messageRecieved);
    SOCKET.on("message-seen",O_messageSeen);
  return ()=>{
    SOCKET.off("msg-recieve")
    SOCKET.off("message-seen")
  }
},[global.ACTIVE_USER,SOCKET])

function callGetMessage(){
  if(ownId && global.ACTIVE_USER){
     dispatch(getUserMsg({from: ownId,to:_id},()=>{
      SOCKET.timeout(1000).emit("msg-seen", {sender: global.ACTIVE_USER,recieverId: ownId},()=>{
          console.log("message seen")
          let updatedList = global.USER_LIST.map((individualUser)=>{
            let result = individualUser
            if(individualUser.reciever?._id==global.ACTIVE_USER){
                individualUser.unseenMessage=0
            }
            return result
        })
        dispatch({type:'GET_USER_LIST',payload:updatedList})
        });
      }))
  }
}
useEffect(() => {
  callGetMessage()
    }, [_id,ownId]);

  

  const logoStyle = {
    position: "absolute",
    width: "100%",
    height: "100%",
    display: "flex",
    "justify-content": "center",
    "align-items": "center",
  };
  
  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [data]);

  return (
    <Wrapper sx={{ position: "relative" }}>
      <Component>
        {/* <div style={logoStyle}>
          <img src={globeLogo} alt="" />
        </div> */}
        <Container >
        {
            loading ?
            <EmptyComponent>
                    <MeCircularProgress />
            </EmptyComponent>
            :
            data && data.length?
            <Message1  reference={scrollRef} messages={data} isAllSeen={seenUserId} />
            :
            <EmptyComponent>
                <EmptyChat />
            </EmptyComponent>
            
        }
        </Container>
      </Component>
      {/* <Logo/> */}
    </Wrapper>
  );
};

export default Messages1;
