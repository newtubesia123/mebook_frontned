import { Box } from "@mui/material";
import React, { useRef } from "react";
import sendIcon from "../../../../png/sendIcon.png";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { SERVER } from "../../../../server/server";
import { useOwnIdentity } from "../../../../Hooks/useOwnIdentity";
import { SOCKET } from "../../../../App";

const TypeMessage1 = () => {
  const {ownId,ownName,ownEmail} = useOwnIdentity();
  const inputMsg = useRef()
  const dispatch = useDispatch();
  const {messageSection:
    {userList:
        {loading,list}
    }} = useSelector((state)=>state);
    const {messageSection:
      {activeUser:
          {_id,name,pdf}
      }} = useSelector((state)=>state);
  const handleSendMsg = async (chat) => {
    // for sending message to user
      dispatch({
        type:'GET_USER_MSG_BY_SOCKET',
        payload:{ fromSelf: true, message: chat, time: Date.now(),sender:ownId,recieverId:global.ACTIVE_USER}
    })
    console.log("id",SOCKET.id)
      SOCKET.timeout(500).emit("send-msg",{
        to: global.ACTIVE_USER,
        from: ownId,
        chat,
        date: Date.now(),
      });
      await axios.post(`${SERVER}/messages/sendMsg`, {
        from: ownId,
        to: global.ACTIVE_USER,
        message: chat,
        time: Date.now()
      });
      let isUserInList = list.find((userObj)=>userObj?.reciever?._id == _id )
      if(!isUserInList){
        let newUser = {
          createdAt:Date.now(),
          isOnline:false,
          reciever:{_id: _id, name:name, email: "dummy@gmail.com",pdf:pdf},
          sender:{_id: ownId, name:ownName, email: ownEmail},
          unseenMessage:0,
          updatedAt:Date.now()
        }
        dispatch({
          type:'ADD_IN_USER_LIST',
          payload:newUser
        })
      }else{
        let userPop = list.filter((userObj)=>userObj?.reciever?._id != _id )
        let newUpdatedList = [isUserInList,...userPop]
        dispatch({
          type:'GET_USER_LIST',
          payload:newUpdatedList
        })
      }
  
  };


  function sendTex(e) {
    e.preventDefault();
    if (inputMsg.current.value.length > 0) {
      handleSendMsg(inputMsg.current.value);
    }
    e.target.reset();
  }

  return (
    <Box
      sx={{
        height: "calc(20vh - 4rem)",
        display: "flex",
        justifyContent: "start",
        alignItems: "center",
        background: "white",
        margin: "0px -1px",
        position: "relative",
      }}
    >
      <form onSubmit={sendTex} style={{display:"flex", width:"100%"}}>
        <input
          style={{
            width: "90%",
            padding: "12px 20px",
            borderRadius: "24px",
            margin: "0px 1%",
            fontSize: "1.2rem",
            fontWeight: "500",
          }}
          className="TypeMessage_input"
          type="text"
          placeholder="Type your message..."
          // onChange={(e) => setChat(e.target.value)}
          ref={inputMsg}
          // value={chat}
        />
       
       
          <button type="submit" style={{border:"none", background:"white"}}>
            <img src={sendIcon} alt="" />
          </button>
        
      </form>
    </Box>

    // disabled = {!chat}
  );
};

// width: 98%;
//     /* padding: 0.2rem 1rem 1rem 1rem; */
//     padding: 12px 12px;
//     border-radius: 24px;
//     margin: 0px 1%;

export default TypeMessage1;
