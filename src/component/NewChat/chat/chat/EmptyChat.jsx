
import { Box, styled, Typography, Divider } from '@mui/material';
import img from '../../../../png/Avatar-Profile-PNG-Photos.png'
import globeLogo from "../../global1.jpg";

const Component = styled(Box)`
    background: white;
    padding: 30px 0;
    text-align: center;
    height: 100%;
`;

const Container = styled(Box)`
    padding: 0 0;
`;
    
const Image = styled('img')({
    // marginTop: 100,
    width: 250
})
   
const Title = styled(Typography)`
    font-size: 32px;
    font-family: inherit;
    font-weight: 300;
    color: #41525d;
    margin-top: 25px 0 10px 0;
`;

const SubTitle = styled(Typography)`
    font-size: 14px;
    color: #667781;
    font-weight: 400;
    font-family: inherit;
`;

const StyledDivider = styled(Divider)`
    margin: 40px 0;
    opacity: 0.4;
`;

const EmptyChat = () => {
    
    return (
        <Component>
            <Container>
                <Image src={globeLogo} alt="empty" />
                <Title>MeBookMeta</Title>
                <SubTitle>Start messaging users</SubTitle>
                <StyledDivider />
            </Container>
        </Component>
    )
}

export default EmptyChat;