import { Box, styled, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import { getImgURL } from "../../../../util/getImgUrl";
import { formatTimestamp } from "../../../../assets/dateFun";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

const Wrapper = styled(Box)`
  background: #ffffff;
  padding: 5px;
  max-width: 60%;
  width: fit-content;
  display: flex;
  border-radius: 10px;
  word-break: break-word;
`;

const Own = styled(Box)`
  background: #c00000;
  color: #ffffff;
  padding: 2px 12px;
  max-width: 100%;
  width: fit-content;
  // margin-left: auto;
  display: flex;
  border-radius: 16px;
  word-break: break-word;
  font-size: 0.80rem;
`;

const Text = styled(Typography)`
  font-size: 14px;
  padding: 0 25px 0 5px;
`;

const Time = styled(Typography)`
  font-size: 10px;
  color: #919191;
  margin-top: 6px;
  word-break: keep-all;
  margin-top: auto;
`;

const imgeStyle = {
  width: "3rem",
  height: "3rem",
  border: "1px solid black",
  borderRadius: "50%",
};

const Message1 = ({  messages, reference }) => {
  const STORE = useSelector((state) => state);
  const {messageSection:
    {activeUser:
        {pdf}
    }} = useSelector((state)=>state)

  return (
    <>
      {messages?.length > 0 &&
        messages?.map((value, index) => {
          return (
            <div
              ref={reference}
              key={index}
              className={` d-flex   my-3 ${
                value.fromSelf ? "flex-row-reverse" : "flex-row"
              }`}
            >
              <div style={{padding:"0px"}}
                className={`col-md-1 col-2 d-flex justify-content-center avtar2 ${
                  value?.fromSelf ? "flex-row-reverse" : "flex-row"
                }`}
              >
                
                <img
                  style={imgeStyle}
                  // src={getImgURL(value.message.fromSelf ? STORE.getProfileData?.userData?.pdf : currentUser?.pdf)}
                  src={
                    value?.fromSelf
                      ? STORE.getProfileData?.userData?.pdf?getImgURL(STORE.getProfileData?.userData?.pdf):`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`
                      : pdf?getImgURL(pdf):`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`
                  }
                  alt=""
                />
              </div>
              <div style={{padding:"0px"}}  className="col-md-6 col-9">
                <div className="d-flex flex-column">
                  <div style={value?.fromSelf? {marginLeft:'auto',display:"flex"}: {}}>
                  <Own
                    sx={`${
                      value?.fromSelf
                        ? "background:#002060"
                        : " "
                    }`}
                  >
                    {value?.message}
                  </Own>
                    {value?.fromSelf && !value?.isSeen && <CheckCircleOutlineIcon/>}
                    {value?.fromSelf && value?.isSeen && <CheckCircleIcon/>}
                    
                  </div>
                 
                  <div
                    className={`d-flex ${
                      value?.fromSelf ? "flex-row-reverse" : "flex-row"
                    }`}
                  >
                    <span style={{ color: "black" }}>
                      {formatTimestamp(value?.time)}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
};

// const TextMessage = ({ message }) => {
//   return (
//     <>
//       <Text>{message.text}</Text>
//       {/* <Time>{formatDate(message.createdAt)}</Time> */}
//     </>
//   );
// };

// const ImageMessage = ({ message }) => {
//   return (
//     <div style={{ position: "relative" }}>
//       <Time style={{ position: "absolute", bottom: 0, right: 0 }}>
//         <GetAppIcon
//           fontSize="small"
//           style={{
//             marginRight: 10,
//             border: "1px solid grey",
//             borderRadius: "50%",
//           }}
//         />
//         {/* {formatDate(message.createdAt)} */}
//       </Time>
//     </div>
//   );
// };

export default Message1;
