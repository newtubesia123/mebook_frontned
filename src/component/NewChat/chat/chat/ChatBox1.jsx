
import { Box } from "@mui/material";

//components
import ChatHeader1 from "./ChatHeader1";
import Messages1 from "./Messages1";
import TypeMessage1 from "./TypeMessage1";

const ChatBox1 = () => {

  return (
    <Box style={{ height: "4rem" }}>
      <ChatHeader1 />
      <Messages1/>
      <TypeMessage1/>
    </Box>
  );
};

export default ChatBox1;

// export { MessageContext };
