import { styled, Drawer, Box, Typography } from '@mui/material';
import { ArrowBack } from '@mui/icons-material';

//components
import Profile from './Profile';

const Header = styled(Box)`
  background: black;
  height: 60px;
  color: #FFFFFF;
  display: flex;
  & > svg, & > p {
    margin-top: auto;
    padding: 15px;
    font-weight: 600;
`;

const Component = styled(Box)`
//   background:yellow ;
  height: 100%;
`;

const Text = styled(Typography)`
    font-size: 18px;
`

const drawerStyle = {
    left: 20,
    top: 17,
    height: '95%',
    width: '30%',
    boxShadow: 'none'
}

const InfoDrawer = ({ open, setOpen, profile }) => {

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Drawer
            open={open}
            onClose={handleClose}
            PaperProps={{ sx: drawerStyle }}
            style={{ zIndex: 1500 }}
        >
            <Header>
                <ArrowBack onClick={() => setOpen(false)} />
                <Text>Profile</Text>
            </Header>
            {/* <Component sx={{background: "linear-gradient(329.04deg, #3981ED 1.96%, #000000 95.27%"}}> */}
                <div style={{background: "linear-gradient(329.04deg, #3981ED 1.96%, #000000 95.27%", height:"100%"}}>
                {profile && <Profile />}

                </div>
            {/* </Component> */}
        </Drawer>
    );
}

export default InfoDrawer;