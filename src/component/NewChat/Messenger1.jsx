import { AppBar, styled, Box } from "@mui/material";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getUserList } from "../../Action";
import ChatDialog1 from "./chat/ChatDialog1";

const Messenger1 = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUserList());
  }, []);
  window.onpopstate = () => {
    global.ACTIVE_USER = null
    dispatch({type:"GET_USER_MSG",payload:[]})
}

  return <ChatDialog1 />
};

export default Messenger1;
