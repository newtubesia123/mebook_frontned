import React, { useState, useEffect } from "react";
import axios from "axios";
// import artist from '../artist.webp'
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link, useNavigate } from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MenuItem from '@mui/material/MenuItem';
import ListSubheader from '@mui/material/ListSubheader';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import "./common.css";
import { TreeSelect } from 'antd';
import { Cascader } from 'antd';
import { BackGround, newButton } from "../Component new/background";

const { SHOW_CHILD } = Cascader;
const { SHOW_PARENT } = TreeSelect;


const treeData = [

  {
    label: <b style={{ fontSize: '25px' }}>Music</b>,

    value: 'Music',
    children: [
      {
        label: <b style={{ fontSize: '20px' }}>Alternative</b>,
        value: 'Alternative',

        children: [
          {
            label: 'Art Punk',
            value: 'Art Punk',

          },
          {
            label: 'Alternative Rock',
            value: 'Alternative Rock',

          },
          {
            label: 'Goth',
            value: 'Goth',

          },
          {
            label: 'Grunge',
            value: 'Grunge',
          },
          {
            label: 'Hard Rock',
            value: 'Hard Rock',
          },
          {
            label: 'Indie Rock',
            value: 'Indie Rock',
          },
          {
            label: 'New Wave',
            value: 'New Wave',

          },
          {
            label: 'Progressive Rock',
            value: 'Progressive Rock',
          },
          {
            label: 'Punk',
            value: 'Punk',
          },
          {

            label: 'Suggest Tag ',
            value: "Suggest Tag",
          },
        ],
      },
      {
        label: <b style={{ fontSize: '20px' }}>Blues</b>,
        value: 'Blues',
        key: '0-0-2',
        children: [
          {
            label: 'African Blues',
            value: 'African Blues',
            key: '0-0-2-1',
          },
          {
            label: 'British Blues',
            value: 'British Blues',
            key: '0-0-2-2',
          },
          {
            label: 'Chicago Blues',
            value: 'Chicago Blues',
            key: '0-0-2-3',
          },
          {
            label: 'Classic Blues',
            value: 'Classic Blues',
            key: '0-0-2-4',
          },
          {
            label: 'Delta Blues',
            value: 'Delta Blues',
            key: '0-0-2-5',
          },
          {
            label: 'Folk Blues',
            value: 'Folk Blues',
            key: '0-0-2-6',
          },
          {
            label: 'Jazz Blues',
            value: 'Jazz Blues',
            key: '0-0-2-7',
          },
          {
            label: 'Kansas City Blues',
            value: 'Kansas City Blues',
            key: '0-0-2-8',
          },
          {
            label: 'Ragtime Blues',
            value: 'Ragtime Blues',
            key: '0-0-2-9',
          },
          {
            label: 'Urban Blues',
            value: 'Urban Blues',
            key: '0-0-2-10',
          },
          {
            label: 'West Coast Blues',
            value: 'West Coast Blues',
            key: '0-0-2-11',
          },
          {

            label: 'Suggest Tag ',
            value: "Suggest Tag",
            key: '0-0-2-12',
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Children’s Music</b>,
        value: "0-0-3",
        key: '0-0-3',
        children: [],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Classical</b>,
        value: "Classical",

        children: [
          {

            label: 'Ballet',
            value: "Ballet",

          },
          {

            label: 'Baroque',
            value: "Baroque",


          },
          {

            label: 'Choral',
            value: "Choral",

          },
          {

            label: 'Concerto',
            value: "Concerto",

          },
          {

            label: 'Modern Composition',
            value: "Modern Composition",

          },
          {

            label: 'Opera',
            value: "Opera",

          },
          {

            label: 'Orchestral',
            value: "Orchestral",

          },
          {

            label: 'Symphonic',
            value: "Symphonic",

          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",

          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Comedy</b>,

        value: "Comedy",

        children: [],
      },

      {
        label: <b style={{ fontSize: '20px' }}>Country</b>,

        value: "Country",
        key: '0-0-6',
        children: [
          {

            label: 'Bluegrass',
            value: "Bluegrass",
            key: '0-0-6-1',
          },
          {

            label: 'Christian Country',
            value: "Christian Country",
            key: '0-0-6-2',
          },
          {

            label: 'Contemporary Country',
            value: "Contemporary Country",
            key: '0-0-6-3',
          },
          {

            label: 'Country Gospel',
            value: "Country Gospel",
            key: '0-0-6-4',
          },
          {

            label: 'Country Pop',
            value: "Country Pop",
            key: '0-0-6-5',
          },
          {

            label: 'Country Rap',
            value: "Country Rap",
            key: '0-0-6-6',
          },
          {

            label: 'Country Rock',
            value: "Country Rock",
            key: '0-0-6-7',
          },
          {

            label: 'Country Soul',
            value: "Country Soul",
            key: '0-0-6-8',
          },
          {

            label: 'Cowboy/Western',
            value: "Cowboy/Western",
            key: '0-0-6-9',
          },
          {

            label: 'Honky Tonk',
            value: "Honky Tonk",
            key: '0-0-6-10',
          },
          {

            label: 'Hellbelly',
            value: "Hellbelly",
            key: '0-0-6-11',
          },
          {

            label: 'Outlaw Country',
            value: "Outlaw Country",
            key: '0-0-6-12',
          },
          {

            label: 'Traditional Country',
            value: "Traditional Country",
            key: '0-0-6-13',
          },
          {

            label: 'Western Swing',
            value: "Western Swing",
            key: '0-0-6-14',
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
            key: '0-0-6-15',
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Dance</b>,

        value: "Dance",

        children: [
          {

            label: 'Club/Club/Dance',
            value: "Club/Club/Dance",
            key: '0-0-7-1',
          },
          {

            label: 'Breakbeat/Breakstep',
            value: "Breakbeat/Breakstep",
            key: '0-0-7-2',
          },
          {

            label: 'Electro House',
            value: "Electro House",
            key: '0-0-7-3',
          },
          {

            label: 'Exercise',
            value: "Exercise",
            key: '0-0-7-4',
          },
          {

            label: 'Garage',
            value: "Garage",
            key: '0-0-7-5',
          },
          {

            label: 'Hardcore',
            value: "Hardcore",
            key: '0-0-7-6',
          },
          {

            label: 'House',
            value: "House",
            key: '0-0-7-7',
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
            key: '0-0-7-8',
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Easy Listening</b>,

        value: "Easy Listening",
        key: '0-0-8',
        children: [],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Electronic</b>,
        value: "Electronic",
        key: '0-0-8-1',
        children: [
          {

            label: 'Ambient',
            value: "Ambient",
            key: '0-0-8-2',
          },
          {

            label: 'Crunk',
            value: "Crunk",
            key: '0-0-8-3',
          },
          {

            label: 'Electro',
            value: "Electro",
            key: '0-0-8-4',
          },
          {

            label: 'Electronic Rock',
            value: "Electronic Rock",
            key: '0-0-8-5',
          },
          {

            label: 'Eurodance',
            value: "Eurodance",
            key: '0-0-8-6',
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
            key: '0-0-8-7',
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Fitness & Workout</b>,
        value: "Fitness & Workout",
        key: '0-0-9',
        children: [],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Hip-Hop/Rap</b>,
        value: "Hip-Hop/Rap",
        key: '0-0-10',
        children: [
          {

            label: 'Alternative Rap',
            value: "Alternative Rap",
            key: '0-0-10-1',
          },
          {

            label: 'Avant-Garde',
            value: "Avant-Garde",
            key: '0-0-10-2',
          },
          {

            label: 'Christian Hip Hop',
            value: "Christian Hip Hop",
            key: '0-0-10-3',
          },
          {

            label: 'Conscious Hip Hop ',
            value: "Conscious Hip Hop",
            key: '0-0-10-4',
          },
          {

            label: 'Dirty South',
            value: "Dirty South",
            key: '0-0-10-5',
          },
          {

            label: 'East Coast',
            value: "East Coast",
            key: '0-0-10-6',
          },
          {

            label: 'Freestyle Rap',
            value: "Freestyle Rap",
            key: '0-0-10-7',
          },
          {

            label: 'Gangsta Rap',
            value: "Gangsta Rap",
            key: '0-0-10-8',
          },
          {

            label: 'Hardcore Rap',
            value: "Hardcore Rap",
            key: '0-0-10-9',
          },
          {

            label: 'Industrial Hip Hop',
            value: "Industrial Hip Hop",
            key: '0-0-10-10',
          },
          {

            label: 'Jazz Rap',
            value: "Jazz Rap",
            key: '0-0-10-11',
          },
          {

            label: 'Latin Rap',
            value: "Latin Rap ",
            key: '0-0-10-12',
          },
          {

            label: 'Midwest Hip Hop',
            value: "Midwest Hip Hop",
            key: '0-0-10-13',
          },
          {

            label: 'New Jack Swing',
            value: "New Jack Swing",
            key: '0-0-10-14',
          },
          {

            label: 'Old School Rap',
            value: "Old School Rap",
            key: '0-0-10-15',
          },
          {

            label: 'Underground Rap',
            value: "Underground Rap",
            key: '0-0-10-16',
          },
          {

            label: 'West Coast Rap',
            value: "West Coast Rap",
            key: '0-0-10-17',
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
            key: '0-0-10-18',
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Holiday</b>,
        value: "Holiday",
        key: '0-0-11',
        children: [],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Industrial</b>,
        value: "Industrial",
        key: '0-0-12',
        children: [],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Inspirational<br />Christian & Gospel</b>,
        value: "Inspirational – Christian & Gospel",
        key: '0-0-13',
        children: [],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Instrumental</b>,
        value: "Instrumental",
        children: [],
      },
      {

        label: <b style={{ fontSize: '20px' }}>J-Pop</b>,
        value: "J-Pop",
        children: [
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Jazz</b>,
        value: "Jazz",
        children: [
          {

            label: 'Afro-Cuban Jazz',
            value: "Afro-Cuban Jazz",
          },
          {

            label: 'Avant-Garde Jazz',
            value: "Avant-Garde Jazz",
          },
          {

            label: 'Bebop',
            value: "Bebop",
          },
          {

            label: 'Big Band ',
            value: "Big Band",
          },
          {

            label: 'Contemporary Jazz',
            value: "Contemporary Jazz",
          },
          {

            label: 'Cool Jazz',
            value: "Cool Jazz",
          },
          {

            label: 'Crossover Jazz',
            value: "Crossover Jazz",
          },
          {

            label: 'Dixieland',
            value: "Dixieland ",
          },
          {

            label: 'Fusion',
            value: "Fusion",
          },
          {

            label: 'Jazz-Funk',
            value: "Jazz-Funk",
          },
          {

            label: 'Latin Jazz',
            value: "Latin Jazz",
          },
          {

            label: 'Mainstream Jazz',
            value: "Mainstream Jazz ",
          },
          {

            label: 'Ragtime',
            value: "Ragtime",
          },
          {

            label: 'Smooth Jazz',
            value: "Smooth Jazz",
          },
          {

            label: 'Swing Jazz',
            value: "Swing Jazz",
          },
          {

            label: 'Traditional Jazz',
            value: "Traditional Jazz",
          },
          {

            label: 'West Coast Jazz',
            value: "West Coast Jazz",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Latin</b>,
        value: "Latin",
        children: [
          {

            label: 'Alternativo & Rock Latino',
            value: "Alternativo & Rock Latino",
          },
          {

            label: 'Bolero',
            value: "Bolero",
          },
          {

            label: 'Bossa Nova',
            value: "Bossa Nova",
          },
          {

            label: 'Brazilian ',
            value: "Brazilian",
          },
          {

            label: 'Contemporary Latin',
            value: "Contemporary Latin",
          },
          {

            label: 'Flamenco',
            value: "Flamenco",
          },
          {

            label: 'Mambo',
            value: "Mambo",
          },
          {

            label: 'Mariachi',
            value: "Mariachi ",
          },
          {

            label: 'Punta',
            value: "Punta",
          },
          {

            label: 'Ranchera',
            value: "Ranchera",
          },
          {

            label: 'Raggaeton y Hip-Hop',
            value: "Raggaeton y Hip-Hop",
          },
          {

            label: 'Mexicano',
            value: "Mexicano ",
          },
          {

            label: 'Salsa Tropical',
            value: "Salsa Tropical",
          },
          {

            label: 'Tejano',
            value: "Tejano",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Metal</b>,
        value: "Metal",
        children: [
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>New Age</b>,
        value: "New Age",
        children: [

        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Opera</b>,
        value: "Opera",
        children: [

        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Pop</b>,
        value: "Pop",
        children: [
          {

            label: 'Suggest Tag ',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Progressive</b>,
        value: "Progressive",
        children: [

        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>R&B/Soul</b>,
        value: "R&B/Soul",
        children: [
          {

            label: 'Contemporary R&B',
            value: "Contemporary R&B",
          },
          {

            label: 'Disco',
            value: "Disco",
          },
          {

            label: 'Funk',
            value: "Funk",
          },
          {

            label: 'Modern Soul',
            value: "Modern Soul",
          },
          {

            label: 'Motown',
            value: "Motown",
          },
          {

            label: 'Soul',
            value: "Soul",
          },
          {

            label: 'Soul/Blues',
            value: "Soul/Blues",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Reggae</b>,
        value: "Reggae",
        children: [
          {

            label: '2-Tone',
            value: "2-Tone",
          },
          {

            label: 'Dub',
            value: "Dub",
          },
          {

            label: 'Roots Reggae',
            value: "Roots Reggae",
          },
          {

            label: 'Reggae Fusion',
            value: "Reggae Fusion",
          },
          {

            label: 'Reggae en Español',
            value: "Reggae en Español",
          },
          {

            label: 'Ska',
            value: "Ska",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Rock</b>,
        value: "Rock",
        children: [
          {

            label: 'American Traditional Rock',
            value: "American Traditional Rock",
          },
          {

            label: 'Blues Rock',
            value: "Blues Rock",
          },
          {

            label: 'British Invasion',
            value: "British Invasion",
          },
          {

            label: 'Death Metal',
            value: "Death Metal",
          },
          {

            label: 'Gothic Metal',
            value: "Gothic Metal",
          },
          {

            label: 'Hard Rock',
            value: "Hard Rock",
          },
          {

            label: 'Jam Bands',
            value: "Jam Bands",
          },
          {

            label: 'Progressive Metal',
            value: "Progressive Metal",
          },
          {

            label: 'Rock & Roll',
            value: "Rock & Roll",
          },
          {

            label: 'Southern Rock',
            value: "Southern Rock",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Singer/Songwriter</b>,
        value: "Singer/Songwriter",
        children: [

        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Soundtrack</b>,
        value: "Soundtrack",
        children: [
          {

            label: 'Movie Soundtrack',
            value: "Movie Soundtrack",
          },
          {

            label: 'Musicals',
            value: "Musicals",
          },
          {

            label: 'Original Score',
            value: "Original Score",
          },
          {

            label: 'TV Soundtrack',
            value: "TV Soundtrack",
          },

          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Spoken Word</b>,
        value: "Spoken Word",
        children: [
          {

            label: 'Spoken Word Music ',
            value: "Spoken Word Music",
          },
          {

            label: 'Spoken Word Performances',
            value: "Spoken Word Performances",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Tex-Mex/Tejano</b>,
        value: "Tex-Mex/Tejano",
        children: [
          {

            label: 'Chicano ',
            value: "Chicano",
          },
          {

            label: 'Classic',
            value: "Classic",
          },
          {

            label: 'Conjunto',
            value: "Conjunto",
          },
          {

            label: 'New Mex',
            value: "New Mex",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>Vocal</b>,
        value: "Vocal",
        children: [
          {

            label: 'A capella ',
            value: "A capella",
          },
          {

            label: 'Cantique',
            value: "Cantique",
          },
          {

            label: 'Doo-wop',
            value: "Doo-wop",
          },
          {

            label: 'Gregorian Chant',
            value: "Gregorian Chant",
          },
          {

            label: 'Standard',
            value: "Standard",
          },
          {

            label: 'Yodel',
            value: "Yodel",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
      {

        label: <b style={{ fontSize: '20px' }}>World</b>,
        value: "World",
        children: [
          {

            label: 'Africa ',
            value: "Africa",
          },
          {

            label: 'Asia',
            value: "Asia",
          },
          {

            label: 'Australia',
            value: "Australia",
          },
          {

            label: 'Caribbean',
            value: "Caribbean",
          },
          {

            label: 'Europe',
            value: "Europe",
          },
          {

            label: 'Suggest Tag ➢',
            value: "Suggest Tag",
          },
        ],
      },
    ],
  },
]
const Vocalist = [
  { label: 'Soprano', value: 'Soprano' },
  { label: 'Mezzo-soprano', value: 'Mezzo-soprano' },
  { label: 'Alto', value: 'Alto' },
  { label: 'Contralto', value: 'Contralto' },
  { label: 'Countertenor', value: 'Countertenor' },
  { label: 'Tenor', value: 'Tenor' },
  { label: 'Baritone', value: 'Baritone' },
  { label: 'Bass', value: 'Bass' },
]
const Instrumentalist = [
  {
    label: <b style={{ fontSize: '20px' }}>String Instruments</b>, value: 'String Instruments',
    children: [
      {
        label: <b style={{ fontSize: '18px' }}>Bowed</b>, value: 'Bowed',
        children: [
          { label: 'Bass', value: 'Bass' },
          { label: 'Cello', value: 'Cello' },
          { label: 'Double bass', value: 'Double bass' },
          { label: 'Fiddle', value: 'Fiddle' },
          { label: 'Viola', value: 'Viola' },
          { label: 'Violin', value: 'Violin' }
        ]
      },
      {
        label: <b style={{ fontSize: '18px' }}>Plucked/Strummed</b>, value: 'Plucked/Strummed',
        children: [
          { label: 'Banjo', value: 'Banjo' },
          { label: 'Bass Guitar', value: 'Bass Guitar' },
          { label: 'Guitar', value: 'Guitar' },
          { label: 'Harp', value: 'Harp' },
          { label: 'Lute', value: 'Lute' },
          { label: 'Mandolin', value: 'Mandolin' },
          { label: 'Ukulele', value: 'Ukulele' }
        ]
      },
      {
        label: <b style={{ fontSize: '18px' }}>Tapped/Struck</b>, value: 'Tapped/Struck',
        children: [
          { label: 'Bells,', value: 'Bells,' },
          { label: 'Clavichord', value: 'Clavichord' },
          { label: 'Piano', value: 'Piano' },
        ]
      }
    ],
  },
  {
    label: <b style={{ fontSize: '20px' }}>Woodwind Instruments</b>, value: 'Woodwind Instruments',
    children: [
      {
        label: <b style={{ fontSize: '18px' }}>Flutes </b>, value: 'Flutes',
        children: [
          { label: 'Fife', value: 'Fife' },
          { label: 'Flute', value: 'Flute' },
          { label: 'Alto flute', value: 'Alto flute' },
          { label: 'Bass flute', value: 'Bass flute' },
          { label: 'Piccolo', value: 'Piccolo' },
          {
            label: <b style={{ fontSize: '18px' }}>Clarinet </b>, value: 'Clarinet',
            children: [
              { label: 'Alto', value: 'Alto' },
              { label: 'Bb', value: 'Bb' },
              { label: 'Bass', value: 'Bass' },
              { label: 'Basset horn', value: 'Basset horn' },
            ]
          },
          {
            label: <b style={{ fontSize: '18px' }}>Saxophone </b>, value: 'Saxophone',
            children: [
              { label: 'Alto', value: 'Alto' },
              { label: 'Soprano', value: 'Soprano' },
              { label: 'Tenor', value: 'Tenor' },
              { label: 'Baritone', value: 'Baritone' },
              { label: 'Bass', value: 'Bass' },
            ]
          },
          { label: 'Bagpipes', value: 'Bagpipes' },
          { label: 'Bassoon', value: 'Bassoon' },
          { label: 'Oboe', value: 'Oboe' },


        ]
      },
      {
        label: 'Single Reed', value: 'Single Reed'
      },
      {
        label: 'Double Reed', value: 'Double Reed',
      },
      {
        label: <b style={{ fontSize: '18px' }}>Other </b>, value: 'Other',
        children: [
          { label: 'Harmonica', value: 'Harmonica' },
        ]

      }
    ],
  },
  {
    label: <b style={{ fontSize: '20px' }}>Brass Instruments </b>, value: 'Brass Instruments',
    children: [
      {
        label: <b style={{ fontSize: '18px' }}>Trumpet </b>, value: 'Trumpet',
        children: [
          { label: 'Coronet', value: 'Coronet' },
          { label: 'Fluglehorn', value: 'Fluglehorn' },
          { label: 'Baritone Horn', value: 'Baritone Horn' },
          { label: 'Euphonium', value: 'Euphonium' },
          { label: 'French Horn', value: 'French Horn' },
          { label: 'Trombone', value: 'Trombone' },
          { label: 'Tuba', value: 'Tuba' },
          { label: 'Sousaphone', value: 'Sousaphone' },
        ]
      },
    ],
  },
  {
    label: <b style={{ fontSize: '20px' }}>Percussion Instruments </b>, value: 'Percussion Instruments',
    children: [
      {
        label: 'Bass drum', value: 'Bass drum',
      },
      {
        label: 'Bell', value: 'Bell'
      },
      {
        label: 'Bendir', value: 'Bendir',
      },
      {
        label: 'Bongo drum', value: 'Bongo drum',
      },
      {
        label: 'Chimes', value: 'Chimes',
      },
      {
        label: 'Clapper', value: 'Clapper',
      },
      {
        label: 'Conga', value: 'Conga',
      },
      {
        label: 'Cymbal', value: 'Cymbal',
      },
      {
        label: 'Drums', value: 'Drums',
      },
      {
        label: 'Marimba', value: 'Marimba',
      },
      {
        label: 'Snare', value: 'Snare',
      },
      {
        label: 'Tambourine', value: 'Tambourine',
      },
      {
        label: 'Tap shoe', value: 'Tap shoe',
      },
      {
        label: 'Timpani', value: 'Timpani',
      },
      {
        label: 'Tom-tom', value: 'Tom-tom',
      },
      {
        label: 'Triangle', value: 'Triangle',
      },
      {
        label: 'Vibraphone', value: 'Vibraphone',
      },
      {
        label: 'Washboard', value: 'Washboard',
      },
      {
        label: 'Whistle', value: 'Whistle',
      },
      {
        label: 'Xylophone', value: 'Xylophone',
      },
    ],
  },
]
const Audio = [
  { label: 'Board Operator', value: 'Board Operator' },
  { label: 'Sound Technician', value: 'Sound Technician' },
  { label: 'Production Assistant', value: 'Production Assistant' },
  { label: 'Sound Engineer', value: 'Sound Engineer' },
  { label: 'Radio Operator', value: 'Radio Operator' },
  { label: 'Audiovisual Technician', value: 'Audiovisual Technician' },
  { label: 'Studio Manager', value: 'Studio Manager' },
  { label: 'Session Musician', value: 'Session Musician' },
  { label: 'Editor', value: 'Editor' },
  { label: 'Broadcast Engineer', value: 'Broadcast Engineer' },
  { label: 'Audio Engineer', value: 'Audio Engineer' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Production = [
  { label: 'Audio Director', value: 'Audio Director' },
  { label: 'Boom Operator', value: 'Boom Operator' },
  { label: 'Concert Technician', value: 'Concert Technician' },
  { label: 'Mastering Engineer', value: 'Mastering Engineer' },
  { label: 'Music Editor', value: 'Music Editor' },
  { label: 'Live Sound Engineer', value: 'Live Sound Engineer' },
  { label: 'Mixing Engineer', value: 'Mixing Engineer' },
  { label: 'Production Sound Mixer', value: 'Production Sound Mixer' },
  { label: 'Recording Engineer', value: 'Recording Engineer' },
  { label: 'Sound Editor', value: 'Sound Editor' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Video = [
  { label: 'Video Producer', value: 'Video Producer' },
  { label: 'Video Content Creator', value: 'Video Content Creator' },
  { label: 'Video Editor', value: 'Video Editor' },
  { label: 'Music Supervisor', value: 'Music Supervisor' },
  { label: 'Cinematographer/Videographer', value: 'Cinematographer/Videographer' },
  { label: 'Production Assistant', value: 'Production Assistant' },
  { label: 'Promotions and Productions Coordinator', value: 'Promotions and Productions Coordinator' },
  { label: 'Video Director', value: 'Video Director' },
  { label: 'Line Producer', value: 'Line Producer' },
  { label: 'Crew Supervisor', value: 'Crew Supervisor' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Brands = [
  { label: 'Sync Licensing Contract Administrator', value: 'Sync Licensing Contract Administrator' },
  { label: 'Publishing Administrator/Coordinator', value: 'Publishing Administrator/Coordinator' },
  { label: 'Mechanical Licensing Administrator', value: 'Mechanical Licensing Administrator' },
  { label: 'Royalties Analyst', value: 'Royalties Analyst' },
  { label: 'Brand Licensing Tracking', value: 'Brand Licensing Tracking' },
  { label: 'Income Tracking/Revenue Assurance', value: 'Income Tracking/Revenue Assurance' },
  { label: 'Side-Artist Tracking', value: 'Side-Artist Tracking' },
  { label: 'Contract Administration', value: 'Contract Administration' },
  { label: 'Social Media Management', value: 'Social Media Management' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Management = [
  { label: 'Artist Manager', value: 'Artist Manager' },
  { label: 'Tour Manager', value: 'Tour Manager' },
  { label: 'Booking Agent', value: 'Booking Agent' },
  { label: 'Music Publicist', value: 'Music Publicist' },
  { label: 'Music Agent', value: 'Music Agent' },

  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
export default function CreatorMusic() {
  const navigate = useNavigate();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  let user = sessionStorage.getItem('userName')
  var identity = sessionStorage.getItem('identity Option')
  let workProfile = localStorage.getItem("Work profile")
  // console.log("first",JSON.parse(user).name)
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const [value1, setValue1] = useState([]);
  const onChange1 = (newValue) => {
    setValue1(newValue);
  };
  const [value2, setValue2] = useState([]);
  const onChange2 = (newValue) => {
    setValue2(newValue);
  };
  const [value3, setValue3] = useState([]);
  const onChange3 = (newValue) => {
    setValue3(newValue);
  };
  const [value4, setValue4] = useState([]);
  const onChange4 = (newValue) => {
    console.log('newValue', newValue)
    setValue4(newValue);
  };
  const [value5, setValue5] = useState([]);
  const onChange5 = (newValue) => {
    setValue5(newValue);
  };
  const [value6, setValue6] = useState([]);
  const onChange6 = (newValue) => {
    setValue6(newValue);
  };
  const [value7, setValue7] = useState([]);
  const onChange7 = (newValue) => {
    setValue7(newValue);
  };
  const [value8, setValue8] = useState([]);
  const onChange8 = (newValue) => {
    setValue8(newValue);
  };
  const [value9, setValue9] = useState([]);
  const onChange9 = (newValue) => {
    setValue9(newValue);
  };
  const [value10, setValue10] = useState([]);
  const onChange10 = (newValue) => {
    setValue10(newValue);
  };
  let allChild = []
  let tempParent = []
  let allParent = []
  for (let i = 0; i < value1.length; i++) {
    allChild.push(...value1[i].slice(-1))
    tempParent.push(...value1[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value2.length; i++) {
    allChild.push(...value2[i].slice(-1))
    tempParent.push(...value2[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value3.length; i++) {
    allChild.push(...value3[i].slice(-1))
    tempParent.push(...value3[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value4.length; i++) {
    allChild.push(...value4[i].slice(-1))
    tempParent.push(...value4[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value5.length; i++) {
    allChild.push(...value5[i].slice(-1))
    tempParent.push(...value5[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value6.length; i++) {
    allChild.push(...value6[i].slice(-1))
    tempParent.push(...value6[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value7.length; i++) {
    allChild.push(...value7[i].slice(-1))
    tempParent.push(...value7[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value8.length; i++) {
    allChild.push(...value8[i].slice(-1))
    tempParent.push(...value8[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value9.length; i++) {
    allChild.push(...value9[i].slice(-1))
    tempParent.push(...value9[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  for (let i = 0; i < value10.length; i++) {
    allChild.push(...value10[i].slice(-1))
    tempParent.push(...value10[i].slice(0, -1))
    allParent = new Set(tempParent)
  }
  console.log('allChild', allChild);
  console.log('allParent', new Set(allParent));
  const [category, setCatagory] = useState('')
  const handleArtist = () => {
    setCatagory('Artist/Performer')
  }
  const handleComposer = (e) => {
    setCatagory('Composer')
  }
  const handleGroup = (e) => {
    setCatagory('Group')
  }
  //#endregion
  const handleSubmit = (e) => {
    navigate('/CompletionPhase', { state: { option: 'Music/Audio/Sound' } })
  };
  localStorage.setItem('MultiOptions', JSON.stringify())


  return (
    <div className="container-fluid" style={BackGround}>
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className="col-md-8 offset-md-2 text-center ">

          <CardContent>
            <div>
              <Avatar
                alt="Remy Sharp"
                src={userInfo.filepreview}
                sx={{
                  width: "150px",
                  height: "130px",
                  margin: "auto",
                  color: "white"
                }}
              />
            </div>

            <Typography
              variant="body2"
              sx={{ margin: "15px 0px", fontSize: "25px", color: "white" }}
            >
              {user}, {identity}, {workProfile} <br />
              Music/Audio/Sound
            </Typography>
          </CardContent>

          <div
            style={{
              margin: "15px 0px",
              fontSize: "2.5vw",
              fontWeight: "bold",
              color: "white",
              textAlign: "center",
            }}
          >
            <span style={{ color: '' }}>M</span><span style={{ color: '' }}>ebook</span><span style={{ color: '' }}>M</span><span style={{ color: '' }}>eta</span> Profile: {identity}, {workProfile}, Music/Audio/Sound
          </div>

        </div>
      </div>
      <div className="row">
        <div className="col-md-8 offset-md-2">


          <CardContent>
            <div className="row mx-2">

              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}

                  size="large"
                  options={treeData}
                  onChange={onChange1}
                  onClick={handleArtist}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Artist/Performer</b>}
                // defaultValue={<b style={{ fontSize: '20px' }}>Artist/Performer</b>}
                />

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  id="Composer"
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={treeData}
                  onChange={onChange2}
                  onClick={handleComposer}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}
                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Composer</b>}
                />
              </div>
              <div className=" col-md-6  col-sm-6 col-12 mt-4 ">
                <Cascader
                  id="Group"
                  style={{
                    width: '100%',

                  }}
                  size="large"
                  options={treeData}
                  onChange={onChange3}
                  onClick={handleGroup}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Group</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Group</b>}
                />
                { }

              </div>
              <div className=" col-md-6  col-sm-6 col-12 mt-4 ">
                <Cascader
                  style={{
                    width: '100%',

                  }}
                  size="large"
                  options={Vocalist}
                  onChange={onChange4}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Vocalist</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Vocalist</b>}
                />
                { }

              </div>
              <div className=" col-md-6  col-sm-6 col-12 mt-4 ">
                <Cascader
                  style={{
                    width: '100%',

                  }}
                  size="large"
                  options={Instrumentalist}
                  onChange={onChange5}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Instrumentalist</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Instrumentalist</b>}
                />
                { }

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Audio}
                  onChange={onChange6}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Audio</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Audio</b>}
                />
              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Production}
                  onChange={onChange7}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Production</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Production</b>}
                />
              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Video}
                  onChange={onChange8}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Video</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Video</b>}
                />
              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Brands}
                  onChange={onChange9}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Brands</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Brands</b>}
                />
              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Management}
                  onChange={onChange10}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  placeholder={<b style={{ fontSize: '18px', color: 'black' }}>Management</b>}

                // defaultValue={<b style={{ fontSize: '20px' }}>Management</b>}
                />
              </div>
              <div className=" col-md-4 col-sm-6 col-12 mt-4">


              </div>
            </div>
          </CardContent>

          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              You have Selected  Options
            </DialogTitle>
            <DialogContent>

              {/* {Array.map((i, index) => (
                <p style={{ display: 'inline', fontWeight: 'bold', fontSize: '25px', color: '#100892' }} key={index}>
                  {i},
                </p>
              ))} */}

              <DialogContentText id="alert-dialog-description">
                If want to Continue Please click on Agree otherwise click on Disagree
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Disagree</Button>
              <Button onClick={handleSubmit} autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <Link to={'/CreatorNew1'}><button
              type="button"
              class="btn  btn-lg"
              style={newButton}
            >
              Go Back
            </button></Link>
            <button
              type="button"
              class="btn  btn-lg"
              style={newButton}
              onClick={handleClickOpen}
            >
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
