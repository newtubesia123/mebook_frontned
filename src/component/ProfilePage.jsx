import "./ProfilePageNew.scss";
import SignInModal from '../SignInModal'
import React, { useEffect, useState } from "react";
import './ProfilePage.css'
import Cookies from 'universal-cookie';
import axios from "axios";
import { QRCodeCanvas } from "qrcode.react";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getFollowers,
  getPitch,
  updateUserDetail,
  userProfile,
  userWorkDetail,
  handleFollow,
  callPopUp,

} from "../Action";
import { getAuth } from "../Action";
import { CLIENTSERVER, SERVER } from "../server/server";
import { toast } from "react-toastify";
import ModeOutlinedIcon from "@mui/icons-material/ModeOutlined";
import IconButton from "@mui/material/IconButton";
import FacebookIcon from "@mui/icons-material/Facebook";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import InstagramIcon from "@mui/icons-material/Instagram";
import PinterestIcon from "@mui/icons-material/Pinterest";
import TwitterIcon from "@mui/icons-material/Twitter";
import YouTubeIcon from "@mui/icons-material/YouTube";
import RedditIcon from "@mui/icons-material/Reddit";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import TelegramIcon from "@mui/icons-material/Telegram";
import EmailIcon from "@mui/icons-material/Email";
import Pitch from "../Component new/User Dashboard/Pitch/Pitch";
import Button from "@mui/material/Button";
import pic from "../mepic.png";
import Backdrop from "@mui/material/Backdrop";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import { newButton } from "../Component new/background";
import { Checkbox, CircularProgress } from "@mui/material";
import { circularProgressbarStyle } from "../assets/common/theme";
import { Box } from "@mui/system";
import Typography from '@mui/material/Typography';
import { Cascader } from 'antd';
import { getImgURL } from "../util/getImgUrl";
import Tooltip from '@mui/material/Tooltip';
// for follwers dialog component
import Dialog from '@mui/material/Dialog';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { Avatar } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import DialogContent from '@mui/material/DialogContent';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import ProfileImgView from "./ProfileImgView";
import { onUplProg } from "../assets/common/onUplProg";
import UplProgBar from "../components/componentsC/uplProgBar/UplProgBar";
import MeCircularProgress from "../components/componentsC/meCircularProgress/MeCircularProgress";
import MeLogo from "../assets/MeLogoMain";
import BackBtn from "../components/componentsB/btn/backBtn/BackBtn";
import AvatarImg from '../png/Avatar-Profile-PNG-Photos.png'
// import DefaultImg from '../png/#ff2846#6944ff#6944ff_1920_1080.png'

// for tabs start
function TabPanel(props) {

  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}
// for tabs end

const { SHOW_CHILD } = Cascader;
// unfollowing and follow function
const FollowToggle = ({ followId }) => {
  const [following, setFollowing] = useState(false);
  const STORE = useSelector((state) => state);
  const dispatch = useDispatch()
  const navigate = useNavigate();
  useEffect(() => {
    dispatch(getFollowers(followId))
  }, [])
  return (
    <>
      {
        !following ?
          <Button id="btn1" size="small" sx={{ ...newButton }}
            onClick={() => {
              if (localStorage.getItem('token')) {
                dispatch(handleFollow(followId))
                setFollowing(!following)
              } else {
                navigate('/SignIn')
              }
            }
            }
          >
            unfollow
          </Button>
          :
          <Button id="btn1" size="small" sx={{ ...newButton }}
            onClick={() => {
              if (localStorage.getItem('token')) {
                dispatch(handleFollow(followId))
                setFollowing(!following)
              } else {
                navigate('/SignIn')
              }
            }
            }
          >
            Follow
          </Button>
      }
    </>
    // {
    //   following ? <Button id="btn1" size="small" sx={{ ...newButton }}
    //     onClick={() => { setFollowing(!following) }}
    //   >Unfollow</Button>
    //     : <Button id="btn1" size="small" sx={{ ...newButton }}
    //       onClick={() => { setFollowing(!following) }}
    //     >follow</Button>
    // }
  )
}

const ProfilePage = () => {
  const cookies = new Cookies();
  const [editProfile, setEditProfile] = useState({
    birthPlace: "",
    currentAddress: "",
    homeTown: "",
    college: "",
    schooling: '',
    position: '',
    status: ''
  });
  const [progress, setProgress] = useState(false);
  const [ModalOpen, setModalOpen] = React.useState(false);
  // for followers dialog 
  const [ModalSignin, setModalSignin] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [followersData, setFollowersData] = useState([]);
  const [followingData, setFollowingData] = useState([]);
  const [filterVal, setFilterVal] = useState('')
  const [searchFollowData, setSearchFollowData] = useState([])
  const [searchFollowingData, setSearchFollowingData] = useState([])
  const [following, setFollowing] = useState();
  /////////////////////////////
  const [editMore, setEditMore] = React.useState(false);
  const [checked, setChecked] = useState(false);
  const [type, setType] = useState("");
  // const [callCreator, setCallCreator] = useState(false)
  // const [followState, setFollowState] = useState({
  //   totalFollowers: 100,
  //   isFollow: false
  // })

  // for profileImg View
  // const [viewProfile, setViewProfile] = useState(false);
  //////////////////////

  //   const [buttonValue, setButtonValue] = useState(false);
  const STORE = useSelector((state) => state);
  console.log("userId", STORE.getProfileData?.userData?._id)
  // console.log(STORE, "store data")
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useParams();
  const { username, userId } = params;
  // console.log(userId, "name")
  const [url, setUrl] = useState(
    // `http://54.246.61.54:3000/UserProfile/${name}`
    `${CLIENTSERVER}/Profile/${username}/${userId}`
  );
  //   const hanldeChange = () => {
  //     setButtonValue(true);
  //   };

  const socialHandler = {
    Facebook: <FacebookIcon color="white" className="socialIcon" />,
    Twitter: <TwitterIcon color="white" className="socialIcon" />,
    Instagram: <InstagramIcon color="white" className="socialIcon" />,
    Pinterest: <PinterestIcon color="white" className="socialIcon" />,
    LinkedIn: <LinkedInIcon color="white" className="socialIcon" />,
    Youtube: <YouTubeIcon color="white" className="socialIcon" />,
    Reddit: <RedditIcon color="white" className="socialIcon" />,
    WhatsApp: <WhatsAppIcon color="white" className="socialIcon" />,
    Telegram: <TelegramIcon color="white" className="socialIcon" />,
    0: "link1",
    1: "link2",
    2: "link3",
  };

  const profileView = () => {
    dispatch(callPopUp({ type: "ShowProfile" }))
  }

  const qrcode = (
    <QRCodeCanvas
      id="qrCode"
      value={url}
      size={190}
      bgColor={"#ffff"}
      level={"H"}
    />
  );

  // api call for followerss and following list
  useEffect(() => {
    if (getAuth()) {
      axios.get(`${SERVER}/users/handleFollowers`,
        {
          headers: { authorization: `Bearer ${getAuth()}` }
        }).then((res) => {
          // console.log("res for fol and folw", res?.data)
          // console.log("followers resposne", res?.data?.allFollowers)
          // console.log("followings response", res?.data?.allFollowing)
          /// api res 
          setFollowersData(res?.data?.allFollowers);
          setFollowingData(res?.data?.allFollowing);
          // for search res
          setSearchFollowData(res?.data?.allFollowers);
          setSearchFollowingData(res?.data?.allFollowing);
        })
        .catch((err) =>
          console.log(err)
        )
    }

  }, []);

  // console.log("followersData", followersData)
  // console.log("followingData", followingData)
  /// follower search
  const handleFilter = (e) => {
    if (e.target.value == ' ') {
      setFollowersData(setSearchFollowData)
    } else {
      const filterResult = searchFollowData.filter(item =>
        item?.userId?.name.toLowerCase().includes(e.target.value.toLowerCase())
      )
      if (filterResult.length > 0) {
        setFollowersData(filterResult)
      } else {
        setFollowersData([])
      }

    }
    setFilterVal(e.target.value)
  }

  ///// following search 
  const handleFilter1 = (e) => {
    if (e.target.value == ' ') {
      setFollowingData(setSearchFollowingData)
    } else {
      const filterResult = searchFollowingData.filter(item =>
        item?.follow?.name.toLowerCase().includes(e.target.value.toLowerCase())
      )
      if (filterResult.length > 0) {
        setFollowingData(filterResult)
      } else {
        setFollowingData([])
      }

    }
    setFilterVal(e.target.value)
  }

  /////////////////////////////////////////////////
  // useEffect(() => {
  //   // console.log("creater", callCreator)
  // }, [callCreator])
  if (STORE.errorPage) {
    navigate("/ErrorPage");
  }
  useEffect(() => {
    dispatch(getPitch("get", userId, () => {
      console.log("pitch getting")
    }));
    dispatch(userProfile(userId));
    dispatch(userWorkDetail(userId));
    setUrl(`${CLIENTSERVER}/Profile/${username}/${userId}`);

    dispatch(getFollowers(userId))
    //   {
    //   send: (currentFollower) => {
    //     cookies.set('currentFollowState', currentFollower, { path: "/" })
    //   }
    // }))



  }, [userId, username, dispatch]);

  // console.log('profile',schooling)

  const pdfUpload = async (e) => {
    setProgress(true)
    let token = JSON.parse(localStorage.getItem("token"));
    const formData = new FormData();
    formData.append("profiles", e.target.files[0]);
    await axios
      .post(`${SERVER}/imageupload/?picType=${type}`, formData, {
        headers: { Authorization: `Bearer ${token}` }, onUploadProgress: onUplProg
      })
      .then((response) => {
        setProgress(false)
        if (response.data.isSuccess) {
          dispatch(userProfile(userId));
          dispatch(userWorkDetail(userId));
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "colored",
          });
        }
      })
      .catch((error) => {
        setProgress(false)
        console.log('error', error)
        toast.error('Something Error', { autoClose: 2000, position: 'top-right' })
      })
  };

  const getLink = (a) => {
    let b = {
      0: `https://${STORE.userProfile.userProfile?.meProfile?.userId.link1}`,
      1: `https://${STORE.userProfile.userProfile?.meProfile?.userId.link2}`,
      2: `https://${STORE.userProfile.userProfile?.meProfile?.userId.link3}`,
      3: `https://${STORE.userProfile.userProfile?.meProfile?.userId.link4}`,
      4: `https://${STORE.userProfile.userProfile?.meProfile?.userId.link5}`,
    };
    return b[a];
  };

  const handleModalOpen = () => setModalOpen(true);
  const handleModalClose = () => {
    setModalOpen(false);
  };
  // for follwers  dialog 
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {

    if (userId === STORE?.getProfileData?.userData?._id) {
      setOpen(true);
    }
  }


  const handleClose = () => setOpen(false);

  // for tabs
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  // for tabs
  ////////////////////////////

  useEffect(() => {
    window.scroll(0, 0)
    setEditProfile({
      ...editProfile,
      birthPlace: STORE.getProfileData?.userData?.birthPlace,
      currentAddress: STORE.getProfileData?.userData?.currentAddress,
      homeTown: STORE.getProfileData?.userData?.homeTown,
      college: STORE.getProfileData?.userData?.college,
      schooling: STORE.getProfileData?.userData?.schooling,
      identity: {
        position: STORE.userProfile?.userProfile?.meIdentity?.position,
        status: STORE.userProfile?.userProfile?.meIdentity?.status
      }
    });
  }, []);


  // useEffect(() => {
  //   // console.log("allfollow", followState)
  // }, [followState])
  function handleUpdateProfile(e) {
    e.preventDefault();
    if (checked)
      return dispatch(
        updateUserDetail(editProfile, STORE.getProfileData.userData._id, {
          send: (e) => {
            setModalOpen(false);
            setChecked(false)
          },
        })
      );

    toast.error("Please confirm", {
      autoClose: 1000
    })

  }
  function handleFollowing(request) {
    switch (request) {
      case "add":
        dispatch({ type: 'increamentFollow' })
        break;
      case "remove":
        dispatch({ type: 'decreamentFollow' })
        break;
      default:
        break;
    }
  }
  window.onpopstate = () => {
    cookies.remove("currentFollowState")
  }
  // useEffect(() => {
  //   if (getAuth()) {
  //     const callAfter5Sec = setTimeout(() => {
  //       if (cookies.get('currentFollowState') === STORE.userProfile.userFollower?.allFollower) {
  //         console.log("do nothing", STORE.userProfile.userFollower?.allFollower, cookies.get('currentFollowState'))
  //       } else {// its false means remove and if its true means add in follow api
  //         if (cookies.get('currentFollowState') === undefined) return console.log("not calling cookie undefined")
  //         if (cookies.get('currentFollowState') > STORE.userProfile.userFollower?.allFollower) dispatch(handleFollow(userId, "remove"))
  //         if (cookies.get('currentFollowState') < STORE.userProfile.userFollower?.allFollower) dispatch(handleFollow(userId, "add"))
  //       }
  //     }, 2000)
  //     return () => clearInterval(callAfter5Sec)
  //   }
  // }, [STORE.userProfile.userFollower?.isFollow])

  // function handleActive() {
  //   setCallCreator(false)
  //   setEditMore("")
  // }
  const isBg = STORE.userProfile?.userProfile?.meProfile?.userId?.backGroundImage;
  // const isBgImg = `${getImgURL(isBg)}`;

  //New follow system
  const handleFollowNew = () => {
    dispatch(handleFollow(userId))
  }
  return (
    <div>
      {STORE.userProfile.loading ?
        (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "100%",
              minHeight: "100vh",
            }}
          >
            {/* <CircularProgress sx={circularProgressbarStyle} /> */}
            <MeCircularProgress />
          </Box>
        )
        :
        <div
          className="wrapper"
          style={{
            height: "fitContent",
            backgroundImage: isBg ? `url(${getImgURL(STORE.userProfile?.userProfile?.meProfile?.userId?.backGroundImage)})` :
              `linear-gradient(-20deg, #ff2846 0%, #6944ff 100%)`,
            // backgroundImage: `url(${getImgURL(STORE.userProfile?.userProfile?.meProfile?.userId?.backGroundImage)})`,
            display: "flex",
            flexDirection: "column",
            gap: "2rem",
            alignItems: "center",
            backgroundRepeat: "no-repeat",
            backgroundAttachment: "fixed",
          }}
        >
          {" "}
          {
            progress ?
              <UplProgBar variant="determinate" />
              : ''
          }
          <div style={{ display: "flex", flexDirection: "column", alignItems: "center", alignSelf: "end" }}>
            <BackBtn style={{ margin: '1rem' }} />
            <div >
              {userId === STORE.getProfileData?.userData?._id ? (
                <Tooltip title='Edit Background'>

                  <IconButton
                    onChange={pdfUpload}
                    onClick={() => setType("userBackgroundImage")}
                    color="primary"
                    aria-label="upload picture"
                    component="label"
                    sx={{
                      backgroundColor: "white", width: '2.4rem', height: '2.4rem',
                      background: '#1C6DD0',
                      "&.MuiButtonBase-root:hover": {
                        backgroundColor: "#1C6DD0"
                      }
                    }}
                  >
                    <input hidden accept="image/*" type="file" />
                    <ModeOutlinedIcon sx={{ color: "black", fontSize: "1.2rem" }} />
                  </IconButton>
                </Tooltip>
              ) : (
                ""
              )}
            </div>
          </div>
          {/* )} */}
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              marginBottom: "6rem",
              alignItems: 'center'
            }}
          >
            {/* <img src={pic} style={{ width: '35%', height: '35%' }} alt="" /> */}
            <MeLogo />
          </div>

          {/* <img src="https://wallpaperaccess.com/full/187161.jpg" alt=""
      style="width: 700px; height: 200px; border-radius: 10px;">  */}
          <div
            className="profile-card js-profile-card"
            style={{
              background: "rgba(255,255,255,.7)",
              backgroundSize: "contain",
              backgroundPosition: "center",
            }}
          >
            <div className="profile-card__img" onClick={profileView}>
              {STORE.userProfile?.userProfile?.meProfile?.userId?.pdf ? (
                <img
                  src={getImgURL(STORE.userProfile?.userProfile?.meProfile?.userId?.pdf)}
                  alt="profile card"
                />
              ) : (
                <img
                  src="https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png"
                  alt="profile picture"
                />
              )}
              {userId === STORE.getProfileData?.userData?._id ? (
                <Tooltip title='Edit Profile Picture'>
                  <IconButton
                    color="primary"
                    onChange={pdfUpload}
                    onClick={(e) => {
                      e.stopPropagation()
                      setType("userProfileImage")
                    }}
                    aria-label="upload picture"
                    component="label"
                    style={{ position: "absolute", bottom: "1px", left: "60%", zIndex: "9999" }}
                  >
                    <input hidden accept="image/*" type="file" />
                    <ModeOutlinedIcon
                      sx={{
                        color: "black",
                        backgroundColor: "#1C6DD0",
                        borderRadius: "50%", fontSize: "1.2rem"
                      }}
                    />
                  </IconButton>
                </Tooltip>

              ) : (
                ""
              )}
            </div>
            <div className="profile-card__cnt js-profile-cnt">
              <div
                className="profile-card__name"
                style={{ fontSize: "1.5rem", color: "rgb(0, 0, 0)" }}
              >
                {STORE.userProfile?.userProfile?.meProfile?.userId?.name}

                {/* <span style={{ marginLeft: '1rem',fontWeight:'bold' }}>
                                {
                                    STORE.userProfile.userProfile?.meProfile?.category?
                                    `(
                                        ${STORE.userProfile.userProfile?.meProfile?.category}
                                        )`
                                        :
                                        ""
                                }
                               
                            </span> */}
              </div>
              <div className="profile-card__handleFollow">
                <Box sx={{ cursor: "pointer" }} onClick={handleClickOpen}>
                  <Typography variant="h6">
                    {STORE.userProfile.userFollower?.allFollower}
                  </Typography>
                  <Typography variant="body1" style={{ color: "black" }}>
                    {!STORE.userProfile.userFollower?.allFollower ? "Follower" : STORE.userProfile.userFollower?.allFollower > 1 ? "Followers" : "Follower"}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant="h6">
                    {STORE.userWorkDetail?.userWorkDetail?.message?.length}
                  </Typography>
                  <Typography variant="body1" style={{ color: "black" }}>
                    {STORE.userWorkDetail?.userWorkDetail?.message?.length > 1 ? "Works Samples" : "Work Sample"}
                  </Typography>
                </Box>

              </div>
              <div
                className="profile-card__txt2"
                style={{ color: "rgb(0, 0, 0)", fontSize: "1rem" }}
              >
                <span style={{ fontWeight: "bold" }}>
                  {" "}
                  MEBOOKMETA FINGERPRINT:
                </span>
                {STORE.userProfile?.userProfile?.meIdentity?.position && STORE.userProfile?.userProfile?.meIdentity?.status && STORE.userProfile?.userProfile?.meProfile?.category
                  ?
                  <span
                    style={{
                      color: "black",
                      textTransform: "uppercase",
                      fontSize: "1rem",
                      // marginLeft: "1rem",
                    }}
                  >
                    {/* Author */}
                    {STORE.userProfile?.userProfile?.meIdentity?.position}, {STORE.userProfile?.userProfile?.meIdentity?.status}, {STORE.userProfile?.userProfile?.meProfile?.category}
                  </span>
                  :
                  <span>This Profile has not Completed yet</span>}
              </div>
              <div className="profile-card__txt" style={{ color: "black" }}>
                {" "}
                <strong>
                  {/* Main Stage, Creator */}
                  {/* {STORE.userProfile.userProfile?.meIdentity?.position}, {STORE.userProfile.userProfile?.meIdentity?.status} */}
                </strong>
              </div>

              <div
                className="profile-card__txt2"
                style={{
                  color: "rgb(0, 0, 0)",
                  lineHeight: "1.8",
                  fontWeight: "",
                }}
              >
                <div style={{ fontWeight: "bold" }}>
                  <strong>MEBOOKMETA FINGERPRINT TAGS: </strong>
                </div>
                <span style={{ display: "flex", flexWrap: "wrap" }}>
                  {STORE.userProfile.userProfile?.meProfile?.subCategory &&
                    STORE.userProfile?.userProfile?.meProfile?.subCategory
                      ?.length ? (
                    STORE.userProfile.userProfile.meProfile.subCategory.map(
                      (item, i) => (
                        <span
                          key={"profileSubCat" + i}
                          style={{
                            color: "rgb(252,252,252)",
                            background: "#2B3467",
                            margin: ".2rem .2rem",
                            padding: "1px 8px 4px 8px",
                            borderRadius: "10px",
                            fontSize: "75%",
                          }}
                        >
                          <span>{item.name}</span>
                        </span>
                      )
                    )
                  ) : (
                    <span style={{ display: "" }}>This Profile has not Completed yet</span>
                  )}
                </span>
              </div>

              {STORE.userProfile?.userProfile?.meProfile?.userId?.birthPlace ? (
                <div
                  className="profile-card__txt"
                  style={{ color: "rgb(0, 0, 0)" }}
                >
                  <strong> PLACE OF BIRTH: </strong>
                  <span style={{ color: "black" }}>
                    {/* Author */}
                    {STORE.userProfile?.userProfile?.meProfile?.userId?.birthPlace.toUpperCase()}
                  </span>
                </div>
              ) : (
                ""
              )}

              {STORE.userProfile?.userProfile?.meProfile?.userId
                ?.currentAddress ? (
                <div
                  className="profile-card__txt"
                  style={{ color: "rgb(0, 0, 0)" }}
                >
                  <strong> CURRENT ADDRESS: </strong>
                  <span style={{ color: "black" }}>
                    {/* Author */}
                    {STORE.userProfile?.userProfile?.meProfile?.userId?.currentAddress.toUpperCase()}
                  </span>
                </div>
              ) : (
                ""
              )}

              {STORE.userProfile?.userProfile?.meProfile?.userId?.homeTown ? (
                <div
                  className="profile-card__txt"
                  style={{ color: "rgb(0, 0, 0)" }}
                >
                  <strong> HOME TOWN: </strong>
                  <span style={{ color: "black" }}>
                    {/* Author */}
                    {STORE.userProfile?.userProfile?.meProfile?.userId?.homeTown.toUpperCase()}
                  </span>
                </div>
              ) : (
                ""
              )}

              {STORE.userProfile?.userProfile?.meProfile?.userId?.college ? (
                <div
                  className="profile-card__txt"
                  style={{ color: "rgb(0, 0, 0)" }}
                >
                  <strong> COLLEGE: </strong>
                  <span style={{ color: "black" }}>
                    {/* Author */}
                    {STORE.userProfile?.userProfile?.meProfile?.userId?.college.toUpperCase()}
                  </span>
                </div>
              ) : (
                ""
              )}

              {STORE.userProfile?.userProfile?.meProfile?.userId?.schooling ? (
                <div
                  className="profile-card__txt"
                  style={{ color: "rgb(0, 0, 0)" }}
                >
                  <strong> SCHOOLING: </strong>
                  <span style={{ color: "black" }}>
                    {/* Author */}
                    {STORE.userProfile?.userProfile?.meProfile?.userId?.schooling.toUpperCase()}
                  </span>
                </div>
              ) : (
                ""
              )}

              <div className="profile-card-inf">
                {/* <div className="profile-card-inf__item">
                                <div className="profile-card-inf__title" style={{ color: 'rgb(0, 0, 0)'
                             }}>267</div>
                                <div className="profile-card-inf__txt" style={{ color: 'rgb(113, 5, 125)'
                             }}>Followers</div>
                            </div> */}

                {/* <div className="profile-card-inf__item">
                                <div className="profile-card-inf__title" style={{ color: 'rgb(0, 0, 0)'
                             }}>351</div>
                                <div className="profile-card-inf__txt" style={{ color: 'rgb(113, 5, 125)'
                             }}>Following</div>
                            </div> */}

                {/* <div className="profile-card-inf__item">
                                <div className="profile-card-inf__title" style={{ color: 'rgb(0, 0, 0)' 
                            }}>
                                {STORE.userProfile?.userProfile?.meProfile?.userId?.isVerified?
                               <div className='profile-card-inf__title__verified'><VerifiedIcon/></div> 
                                : <div className="profile-card-inf__title" style={{ color: 'rgb(0, 0, 0)'
                            }}>N0</div>
                            }
                                </div>
                                <div className="profile-card-inf__txt" style={{ color: 'rgb(113, 5, 125)' 
                            }}>Verified</div>
                            </div> */}
              </div>

              <div className="profile-card-social">
                {/* <a
                  // href={"mailto:".concat(
                  //   STORE.userProfile.userProfile?.meProfile?.userId.email
                  // )}
                  target="_blank"
                  className="profile-card-social__item instagram"
                >
                  <EmailIcon />
                </a> */}
                {STORE.userProfile?.userProfile?.meProfile?.userId?.socialMedia?.map(
                  (icon_name, index) => (
                    // <a href="https://www.instagram.com/zeniii.02/" className="profile-card-social__item instagram" target="_blank">

                    // </a> 
                    <a
                      key={"link" + index}
                      href={getLink(index)}
                      target="_blank"
                      className="profile-card-social__item instagram"
                    >
                      {socialHandler[icon_name]}
                    </a>
                  )
                )}
              </div>

              <div className="profile-card-ctr">
                {userId != STORE.getProfileData?.userData?._id ? (
                  <>
                    <button onClick={() => navigate(`/MainDashboard/ChatCombiner/${username}/${userId}`)} className="profile-card__button button--blue js-message-btn">
                      Message
                    </button>
                    {/* <button className="profile-card__button button--orange">
                      Hire Me
                    </button> */}
                    <button className="profile-card__button button--orange"
                      onClick={() => {
                        localStorage.getItem('token') ? navigate(`/MainDashboard/opportunities/${username}/${userId}`) : setModalSignin(true)
                      }}>
                      Opportunities
                    </button>
                    {
                      // STORE.userProfile.userFollower?.isFollow ?
                      //   <button onClick={() =>
                      //     localStorage.getItem('token') ? handleFollowing("remove") : setModalSignin(true)}
                      //     className="profile-card__button button--orange">
                      //     Following
                      //   </button>
                      // :
                      <button onClick={() => localStorage.getItem('token') ? handleFollowNew() : setModalSignin(true)}

                        className="profile-card__button button--orange">
                        {STORE.userProfile.userFollower.isFollow ? 'Following' : 'Follow'}
                      </button>
                    }

                  </>
                ) : (
                  <button
                    className="profile-card__button button--orange"
                    onClick={() => handleModalOpen()}
                  >
                    Edit Details
                  </button>
                )}
              </div>
              <div id="qrVideo">
                {/* 29 june */}
                {/* {userId === STORE.getProfileData?.userData?._id ? (
                  <div style={{ maxWidth: "200", marginTop: "0.5rem" }}>
                    <h6 style={{ fontSize: "80%" }}> <b> Scan QR to Discover Me and My Work</b></h6>

                    {qrcode}
                  </div>
                ) : (
                  ""
                )} */}
                <div style={{ maxWidth: "200", marginTop: "0.5rem" }}>
                    <h6 style={{ fontSize: "80%" }}> <b> Scan QR to Discover Me and My Work</b></h6>

                    {qrcode}
                </div>
                {STORE.getPitch.userPitch?.message?.pitchVdo ? (
                  <div style={{ maxWidth: "200", marginTop: "0.5rem" }}>
                    <h6 style={{ fontSize: "80%" }}> <b> Pitch Video </b></h6>

                    <video
                      // src='http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'
                      src={getImgURL(
                        STORE.getPitch?.userPitch?.message?.pitchVdo
                      )}
                      controlsList="nodownload"
                      autoPlay
                      controls
                      muted
                      loop
                      width="320"
                      height="180"
                      style={{
                        boxShadow: "black 5px 5px 10px 5px",
                        borderRadius: "10px",
                      }}
                    ></video>
                  </div>
                ) : (
                  ""
                )}
              </div>
              {STORE.getPitch.userPitch?.message?.pitch &&
                STORE.getPitch.userPitch?.message?.pitch.length > 1 ? (
                <Pitch id={userId} />
              ) : (
                ""
              )}
            </div>

            <div className="text-center ">
              <button
                onClick={() => {
                  {
                    localStorage.getItem('token') ? userId === STORE.getProfileData?.userData?._id
                      ? navigate(`/MainDashboard/WriterStoryDetails`)
                      : navigate(`/MainDashboard/WriterStoryDetails/${userId}`) : navigate(`/WriterStoryDetails/${userId}`)
                  }
                }}
                className="profile-card__button button--orange "
              >
                {STORE.userWorkDetail?.userWorkDetail?.message?.length} Work{" "}
                {STORE.userWorkDetail?.userWorkDetail?.message?.length > 1
                  ? "Samples"
                  : "Sample"}
              </button>
            </div>
            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              open={ModalOpen}
              onClose={handleModalClose}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
              sx={{ zIndex: 5 }}
            >
              <Fade in={ModalOpen}>
                <div className="row d-flex justify-content-center align-items-center h-100">
                  <div className="col-md-4 col-lg-4 bg-white profile-edit__1stDiv ">
                    <div className="profile-card__img">
                      {STORE.userProfile?.userProfile?.meProfile?.userId?.pdf ? (
                        <img
                          src={getImgURL(STORE.userProfile?.userProfile?.meProfile?.userId?.pdf)}
                          alt="profile card"
                        />
                      ) : (
                        <img
                          src="https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png"
                          alt="profile picture"
                        />
                      )}
                      {userId === STORE.getProfileData?.userData?._id ? (
                        <IconButton
                          color="primary"
                          onChange={pdfUpload}
                          onClick={() => setType("userProfileImage")}
                          aria-label="upload picture"
                          component="label"
                          style={{
                            position: "absolute",
                            bottom: "1px",
                            left: "60%",
                          }}
                        >
                          <input hidden accept="image/*" type="file" />
                          <ModeOutlinedIcon
                            sx={{
                              color: "black",
                              backgroundColor: "#1C6DD0",
                              borderRadius: "50%", fontSize: "1.2rem"
                            }}
                          />
                        </IconButton>
                      ) : (
                        ""
                      )}
                    </div>
                    <div
                      className="profile-card__name"
                      style={{ fontSize: "1.2rem", color: "rgb(0, 0, 0)" }}
                    >
                      {STORE.userProfile?.userProfile?.meProfile?.userId?.name}
                    </div>
                    <div
                      className="profile-card__txt"
                      style={{ fontSize: "1rem", color: "rgb(0, 0, 0)" }}
                    >
                      {STORE.userProfile?.userProfile?.meProfile?.userId?.email}
                    </div>
                    <div
                      className="profile-card__txt"
                      style={{ fontSize: "0.8rem", color: "rgb(0, 0, 0)" }}
                    >
                      {
                        STORE.userProfile.userProfile?.meProfile?.userId?.currentAddress
                      }
                    </div>
                  </div>
                  <div
                    className="col-md-6 col-lg-6 profile-edit__2ndDiv"
                    style={{ backgroundColor: "black", color: "white" }}
                  >
                    <form onSubmit={handleUpdateProfile}>
                      <div className="row">
                        <div className="form-group col-md-12 col-lg-6">
                          <label for="exampleInputEmail1">Birth Place</label>
                          <input
                            type="text" style={{ height: "2rem", fontSize: "75%" }}
                            value={editProfile.birthPlace}
                            onChange={(e) =>
                              setEditProfile({
                                ...editProfile,
                                birthPlace: e.target.value,
                              })
                            }
                            className="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter Birth place"
                          />
                          <small id="emailHelp" className="form-text text-muted">
                            We'll never share your details with anyone else.
                          </small>
                        </div>
                        <div className="form-group col-md-12 col-lg-6">
                          <label for="exampleInputPassword1">Current Add</label>
                          <input
                            type="text" style={{ height: "2rem", fontSize: "75%" }}
                            value={editProfile?.currentAddress}
                            onChange={(e) =>
                              setEditProfile({
                                ...editProfile,
                                currentAddress: e.target.value,
                              })
                            }
                            className="form-control"
                            id="exampleInputPassword1"
                            placeholder="Enter Current address"
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="form-group col-md-12 col-lg-6">
                          <label for="exampleInputEmail1">Home town</label>
                          <input
                            type="text" style={{ height: "2rem", fontSize: "75%" }}
                            value={editProfile.homeTown}
                            onChange={(e) =>
                              setEditProfile({
                                ...editProfile,
                                homeTown: e.target.value,
                              })
                            }
                            className="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter Home Town"
                          />
                          <small id="emailHelp" className="form-text text-muted">
                            We'll never share your details with anyone else.
                          </small>
                        </div>
                        <div className="form-group col-md-12 col-lg-6">
                          <label for="exampleInputPassword1">College</label>
                          <input
                            type="text" style={{ height: "2rem", fontSize: "75%" }}
                            value={editProfile.college}
                            onChange={(e) =>
                              setEditProfile({
                                ...editProfile,
                                college: e.target.value,
                              })
                            }
                            className="form-control"
                            id="exampleInputPassword1"
                            placeholder="Enter College name"
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="form-group col-md-12 col-lg-6">
                          <label for="exampleInputEmail1">School</label>
                          <input
                            type="text" style={{ height: "2rem", fontSize: "75%" }}
                            value={editProfile.schooling}
                            onChange={(e) =>
                              setEditProfile({
                                ...editProfile,
                                schooling: e.target.value,
                              })
                            }
                            className="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter your school"
                          />
                          <small id="emailHelp" className="form-text text-muted">
                            We'll never share your details with anyone else.
                          </small>
                        </div>
                        <div className="form-group col-md-12 col-lg-6"></div>
                      </div>

                      {editMore && (

                        <div className="row">
                          <div className="form-group col-md-12 col-lg-6">
                            <label for="exampleInputEmail1">Identity Profile</label>
                            <select style={{ padding: '8px', width: '100%', borderRadius: '5px', height: "2rem", fontSize: "80%" }} value={editProfile.identity.position}
                              onChange={(e) =>
                                setEditProfile({
                                  ...editProfile,
                                  identity: { ...editProfile.identity, position: e.target.value },
                                })
                              } >
                              <option value="Main Stage">Main Stage</option>
                              <option value=" Bleeding Edge">Bleeding Edge</option>
                              <option value="Specialist">Specialist</option>
                              <option value="Multi-Discipline">Multi-Discipline</option>
                              <option value="Organic">Organic</option>
                              <option value="At-Large">At-Large</option>
                            </select>
                            <small id="emailHelp" className="form-text text-muted">
                              We'll never share your details with anyone else.
                            </small>
                          </div>
                          <div className="form-group col-md-12 col-lg-6">
                            <label for="exampleInputPassword1">Work Profile</label>
                            <select style={{ padding: '8px', width: '100%', borderRadius: '5px', height: "2rem", fontSize: "80%" }} value={editProfile.identity.status}
                              onChange={(e) =>
                                setEditProfile({
                                  ...editProfile,
                                  identity: { ...editProfile.identity, status: e.target.value },
                                })
                              } >
                              <option value="Creator">Creator</option>
                              <option value="Collaborator">Collaborator</option>
                              <option value="Influencer">Influencer</option>
                              <option value="Project Developer">Project Developer</option>
                              <option value="Provider">Provider</option>
                            </select>
                          </div>
                        </div>
                      )}
                      <div className="row d-flex">
                        <div className="form-check col-lg-6">
                          <Checkbox sx={{ color: 'white', ...newButton, }} onChange={() => setChecked(!checked)} checked={checked} color='default' /> Confirm
                        </div>

                        <div className="col-lg-6 text-right">
                          <Button
                            variant="text"
                            onClick={() => {
                              editMore ?
                                navigate(`CreatorNew2/${STORE.userProfile.userProfile?.meProfile?.category}`)
                                : setEditMore(true)
                            }
                            }
                          >
                            {editMore ? " Edit FINGERPRINT TAGS..." : "Edit More..."}
                          </Button>
                        </div>
                      </div>
                      <div style={{ textAlign: "center" }}>
                        <button
                          type="submit"
                          className="profile-card__button button--orange"
                        >
                          Update
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </Fade>
            </Modal>

            {/* new follower modal */}
            <Dialog
              fullScreen={fullScreen}
              open={open}
              onClose={handleClose}
              aria-labelledby="responsive-dialog-title"
              sx={{ height: "30rem", margin: "auto", width: { lg: "33rem", md: "33rem", sm: "33rem", xs: "20rem" }, padding: 0 }} id="dialog"
            >
              <DialogContent id="dialogCont" sx={{ background: "#191c24", height: "29rem", margin: "5px", padding: "1rem" }}>

                <Box id="crossIcon"><CloseIcon onClick={handleClose} sx={{ cursor: "pointer", color: 'white', zIndex: "99", position: "absolute", right: "4%", padding: 0 }} /></Box>
                <Box id="box1" sx={{ background: "#191c24", color: "#fff", padding: 0, }}>
                  <Box id="box2" sx={{ background: "#191c24", borderBottom: 1, borderColor: 'blue', color: "#fff", padding: 0 }}>
                    <Tabs id="tabs" sx={{ margin: 0, padding: 0 }} value={value} onChange={handleChange} aria-label="basic tabs example" centered>
                      <Tab id="tab1" sx={{ color: "#fff" }} label="Followers" {...a11yProps(0)} />
                      <Tab id="tab2" sx={{ color: "#fff" }} label="Followings" {...a11yProps(1)} />
                    </Tabs>
                  </Box>
                  <TabPanel id="tabpanel" value={value} index={0} sx={{ margin: 0, padding: 0 }} >
                    <Box id="serachBox" sx={{ display: "flex", justifyContent: "center", alignItems: "center", marginBottom: "10px", color: "#fff", padding: 0, }}>
                      <input id="searchBar" value={filterVal} onChange={(e) => handleFilter(e)} type="search" placeholder="Search Followers"
                        style={{ textAlign: "center", border: "none", outline: "none", color: "#000000" }}
                      />
                    </Box>
                    {followersData && followersData.length > 0 ? followersData.map((item, index) => (
                      <Box id="box3" key={index} sx={{ display: "flex", padding: 0, width: "100%", background: "#191c24", justifyContent: "flex-start", cursor: "pointer", }}>
                        <div id="div1" style={{ width: "20%" }}>
                          <Avatar id="avatar1" sx={{ width: 40, height: 40, margin: "10px" }}
                            src={getImgURL(item?.userId?.pdf)}>
                          </Avatar>
                        </div>
                        <div id="div2" style={{ justifyContent: 'left', width: "50%", margin: "10px" }}>
                          <Typography varient="h3" align="left">{item?.userId?.name} </Typography>
                        </div>
                        <div id="div3" sx={{ width: "30%" }}>
                          <Button id="btn1" size="small" sx={{ ...newButton }}
                            onClick={() => {
                              navigate(`/MainDashboard/UserProfile/${item?.userId?.name}/${item?.userId?._id}`);
                              handleClose()
                            }}
                          >View</Button>
                        </div>
                        <hr style={{ height: "2px", borderWidth: 0, background: "#fff" }}></hr>
                      </Box>
                    ))
                      : <Box>
                        <Typography varient="h4" align="center">No Followers!</Typography>
                      </Box>
                    }
                  </TabPanel>
                  <TabPanel id="tabpanel" value={value} index={1}>
                    <Box id="serachBox" sx={{ display: "flex", justifyContent: "center", alignItems: "center", marginBottom: "10px", color: "#fff", padding: 0 }}>
                      <input id="searchBar" value={filterVal} onChange={(e) => handleFilter1(e)} type="search" placeholder="Search Followings"
                        style={{ textAlign: "center", border: "none", outline: "none", color: "#000000" }}
                      />
                    </Box>
                    {followingData && followingData.length > 0 ?
                      followingData.map((item, index) => (
                        <Box id="box3" key={"followingData" + index} sx={{ display: "flex", width: "100%", background: "#191c24", justifyContent: "flex-start", padding: 0 }}
                        >
                          <div id="div1" style={{ width: "20%", }}>
                            <Avatar id="avatar" align="left" sx={{ width: 40, height: 40, margin: "10px" }}
                              onClick={() => {
                                navigate(`/MainDashboard/UserProfile/${item?.follow?.name}/${item?.follow?._id}`);
                                handleClose()
                              }}
                              src={getImgURL(item?.follow?.pdf)}>
                            </Avatar>
                          </div>
                          <div id="div2" style={{ width: "50%", margin: "10px" }}>
                            <Typography varient="h3" onClick={() => {
                              navigate(`/MainDashboard/UserProfile/${item?.follow?.name}/${item?.follow?._id}`);
                              handleClose()
                            }}>{item?.follow?.name}</Typography>
                          </div>


                          <div className="btns" style={{ width: "30%" }}>
                            <div id="div3" style={{ width: "15%", margin: "10px" }}>
                              <FollowToggle followId={item?.follow?._id} />
                            </div>
                            <div id="div4" style={{ width: "15%", margin: "10px" }}>
                              <Button size="small" sx={{ ...newButton }}
                                onClick={() => {
                                  navigate(`/MainDashboard/UserProfile/${item?.follow?.name}/${item?.follow?._id}`);
                                  handleClose()
                                }}
                              >View</Button>
                            </div>
                          </div>
                        </Box>
                      )) : <Box>
                        <Typography varient="h4" align="center">No Following!</Typography>
                      </Box>
                    }
                  </TabPanel>
                </Box>
              </DialogContent>
            </Dialog>
            {/* new follower modal */}
            {/* for follwers dialog start */}
            <div>
              {/* <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                handle sx={{ height: "30rem", margin: "auto", width: "33rem", padding: 0 }} 
              >
                
                <Box sx={{ background: "#191c24", width: { sm: 100, md: 500 }, height: "30rem", margin: "auto", padding: "1rem" }}>
                  <Box sx={{ background: "#191c24", borderBottom: 1, borderColor: 'blue', color: "#fff", padding: 0 }}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" centered>
                      <Tab sx={{ color: "#fff" }} label="Followers" {...a11yProps(0)} />
                      <Tab sx={{ color: "#fff" }} label="Followings" {...a11yProps(1)} />
                    </Tabs>
                  </Box>
                  <TabPanel value={value} index={0} >
                  <div sx={{ display: "flex", justifyContent: "center", alignItems: "center", marginBottom: "10px", color: "#fff", padding: 0,margin:0, }}>
                      <input id="searchBar" value={filterVal} onChange={(e) => handleFilter(e)} type="search" placeholder="Search Followers" />
                  </div>
                    {followersData && followersData.length > 0 ? followersData.map((item, index) => (
                      <Box key={index} sx={{ display: "flex", padding: 0, width: "100%", background: "#191c24", justifyContent: "flex-start", cursor: "pointer", }}
                        onClick={() => {
                          navigate(`/MainDashboard/UserProfile/${item?.name}/${item?._id}`);
                          handleClose()
                        }}
                      >
                        <div style={{ width: "20%" }}>
                          <Avatar sx={{ width: 35, height: 35, margin: "10px" }}
                            src={getImgURL(item?.pdf)}>
                          </Avatar>
                        </div>
                        <div style={{ justifyContent: 'left', width: "80%", margin: "10px" }}>
                          <Typography varient="h4" align="left">{item?.name} </Typography>
                        </div>
                      </Box>
                    ))
                      : <Box>
                        <Typography varient="h4" align="center">No Followers!</Typography>
                      </Box>
                    }
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    {followingData && followingData.length > 0 ?
                      followingData.map((item, index) => (
                        <Box key={index} sx={{ display: "flex", width: "100%", background: "#191c24", justifyContent: "flex-start", cursor: "pointer", padding: 0 }}
                        >
                          <div>
                            <Avatar align="left" sx={{ width: 35, height: 35, margin: "10px" }}
                              onClick={() => {
                                navigate(`/MainDashboard/UserProfile/${item?.userId?.name}/${item?._id}`);
                                handleClose()
                              }}
                              src={getImgURL(item?.userId?.pdf)}>
                            </Avatar>
                          </div>
                          <div style={{ width: "50%", margin: "10px" }}>
                            <Typography varient="h4" onClick={() => {
                              navigate(`/MainDashboard/UserProfile/${item?.userId?.name}/${item?._id}`);
                              handleClose()
                            }}>{item?.userId?.name}</Typography>
                          </div>


                          <div style={{ width: "50%", margin: "10px" }}>
                            <Button size="small" sx={{ ...newButton }}
                              onClick={() => {
                                navigate(`/MainDashboard/`)
                              }}
                            >Unfollow</Button>
                          </div>
                        </Box>
                      )) : <Box>
                        <Typography varient="h4" align="center">No Following!</Typography>
                      </Box>
                    }
                  </TabPanel>
                </Box>
              </Dialog> */}
              {ModalSignin ? <SignInModal setModalSignin={setModalSignin} username={username} userId={userId} path={"UserProfile"} /> : ''}
            </div>
            {/* for follwers dialog end */}
          </div>
          {/* View Profile Img */}
          {/* {<ProfileImgView data={STORE.userProfile?.userProfile?.meProfile?.userId?.pdf} />} */}
        </div >
      }
    </div >
  );
};

export default ProfilePage;
