import React, { useState, useEffect } from "react";
import image from "../mepic.png";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import "./common.css";
import MainStagePop from "./Mainstage/MainStagePop";
import { useSelector } from "react-redux";
import { BackGround, newButton } from "../Component new/background";
import { toast } from "react-toastify";

import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Accordion from "@mui/material/Accordion";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';

const BootstrapTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} arrow classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.arrow}`]: {
    color: theme.palette.common.black,
  },
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: theme.palette.common.white,
    maxWidth: 600,
    fontSize: "0.8rem",
    color: "black"
  },
  // [`& .${tooltipClasses.tooltip}`]: {
  //   maxWidth: 600,
  // },
}));

export default function Identity() {
  const [a, setA] = useState(false);
  const [showVal, setShowVal] = useState(false)
  const STORE = useSelector((state) => state)
  console.log("store", STORE)
  const bgBut = {
    ...newButton,
    width: "80%",
    fontSize: "80%",
    color: "white",
    margin: "15px 10%",
    borderRadius: "10px",
    padding: "10px 0px",
    fontWeight: "bold"
  }
  const AccordianBackground = {
    ...BackGround,
    color: "white",
    margin: "10px 0 10px 0",
    border: "1px solid white",
    // width: 900,
  };
  const arry = [
    {
      option: " Main Stage",
      id: "1",
      value:
        "This identity is reserved for creators, collaborators, influencers, providers, project developers and readers/consumers who are willing to pay a small premium to be certain that their profile is continually vetted*, free of errors, and fully functional on an ongoing monthly basis. This identity is purposed to provide and an extra layer of professionalism for profile presenters and profile seekers. *(content, claims and credentials will be verified by an investigative team for accuracy)",
    },
    {
      option: " Bleeding Edge",
      id: "2",
      value:
        "Bleeding Edge describes the very latest and most modern technology available. This profile is reserved for those who work along with content creators, who generate original entertaining or educational material, to be expressed through any medium or channel. In the Global Media Marketplace, collaborators include those who work with presenters of all forms or writing, speech, performance, graphic and visual arts, music, sound and audio, dance and choreography, design, all aspects of media, television, film, and other original material. Those who work with any form of derivative work are also considered as Bleeding Edge.",
    },
    {
      option: "Specialist",
      id: "3",
      value:
        "This identity is reserved for creators, collaborators, influencers, providers, project and reader/consumers who, as a result of lengthy experience, education or specialized training, have attained a level of expertise in a specific area or discipline. A “Specialist-V” designation will indicate that the profile has been vetted by our research team. This identity is purposed to provide experienced individuals and experts an opportunity to share work and work, provide or integrate with other profiles on the platform.",
    },
    {
      option: "Multi-Discipline",
      id: "4",
      value:
        "This identity is reserved for creators,  collaborators, influencers, providers, project developers and reader/consumers whose talents and abilities legitimately expand across two or more levels of expertise — such that a single identity definition does not approximate actual comprehensive identity. This identity is purposed to provide profiles for individuals whose talents, abilities and experience expand across multiple disciplines. Consider carefully: in the Global Media Marketplace, specificity is helpful in defining a unique niche, although some seekers might consider “Multi- Discipline” a required niche for specific intentions and purposes.If more than 2 - 3 substantial identities, consider creating separate profiles.",
    },
    {
      option: "Organic",
      id: "5",
      value:
        "This identity is reserved for creators, collaborators, influencers, providers, project developers and reader/consumers whose focus is “authenticism,” which means that content presents original, unvetted material that might contain errors, coarse language and descriptions, misleading or false claims, profanity, adult or mild sexual content, all within site-defined or should be imposed limitations. This identity is purposed to provide a platform for users whose focus is more message and audience oriented, which often defies mainstream norms, allowing for greater free expression.",
    },
    {
      option: "At-Large",
      id: "6",
      value:
        "This identity is reserved for creators, collaborators, influencers, providers, project developers and reader/consumers who would rather not be defined by labels, and would rather be presented as uncharacterized, non-conformist individuals. Yet this identity values the importance  of the Global Media Marketplace and its necessity for individual success.This self-defined identity is purposed to give expression to the majority of users by providing the greatest exposure and potential for success. At-Large should be considered a 'catch-all' category."
    },
  ];

  const obj = {
    MainStage: "/MainStage",
    CuttingEdge: "/CuttingEdge",
    Specialist: "/Specialist",
    MultiDiscipline: "/MultiDiscipline",
    Organic: "/Organic",
    AtLarge: "/AtLarge",
  };
  const navigate = useNavigate();
  const [checkValue, setCheckValue] = useState("");

  if (STORE.getProfileData?.userData?.identityProfile) {
    navigate('/MainDashboard')
  }

  const handleChange = (val) => {
    console.log("id", val.option);

    setCheckValue(val.option);
  };
  useEffect(() => {
    console.log("checkValue", checkValue);
    sessionStorage.setItem('identity Option', checkValue)
  }, [checkValue]);
  return (
    <div className="container-fluid IdentityM" style={BackGround}>

      {a ? <MainStagePop value={a ? a : "null"} /> : ""}

      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "180px",
            height: "100px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className=" col-md-6 offset-md-3">
          <div className="text-center">
            <Typography
              variant="body2"
              color="black"
              sx={{ margin: "1px auto", fontSize: "150%", color: "white" }}
            >
              {STORE.getProfileData?.userData?.name}
            </Typography>
          </div>

          <div
            style={{
              margin: "15px 0px",
              fontSize: "200%",
              fontWeight: "bold",
              color: "#100892",
              textAlign: "center",
              color: "white",
            }}
          >
            <p>Set up Your Identity Profile</p>

          </div>

        </div>

        <div className=" row d-flex justify-content-center align-items-center ">
          <div className=" col-8 col-sm-10 col-md-7 col-lg-5 ">

            <Accordion sx={AccordianBackground}>
              <AccordionSummary
                sx={{ color: "white", height: "2rem" }}
                expandIcon={<ExpandMoreIcon sx={{ color: "white", }} />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography variant="h6" sx={{ fontSize: "75%" }}>
                  Why an Identity Profile?
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography variant="h6" sx={{ fontSize: "75%" }}>
                  Your Identity Profile will help you define your unique niche in the Marketplace, based on
                  who (persons and groups) you think might be motivated to support what you do. Thus the
                  focus of this profile is to determine the identity of your target audience.<br /><br />
                  For that reason, your choice of an Identity Profile is crucial at this stage of engaging the
                  Global Media Market Place. On MeBookMeta, you’ll be selling your work, services and/or ideas to people who
                  you don’t know, so you’ll want to ask yourself, What audience identities and groups will
                  support my work, and is there a larger, combined category that will translate these into the
                  maximum potential audience for me?<br /><br />
                  For this reason, each of the potential choices for your Identity Profile seeks to help you
                  discover a greater audience, based on the combination of persons and groups in larger
                  categories that our research has determined tend to support what you do.
                </Typography>
              </AccordionDetails>
            </Accordion>
          </div>
        </div>


        <div className="col-md-6 offset-md-3">
          <CardContent>
            {arry.map((val) => {
              return (
                <>
                  <BootstrapTooltip title={val.value} placement="top-end" arrow>
                    <Button
                      className={
                        checkValue === val.option
                          ? "button afterClick"
                          : "button"
                      }
                      sx={bgBut}
                      value={checkValue}
                      // onMouseEnter={() => {
                      //   setA(val);
                      // }}
                      // onMouseLeave={() => {
                      //   setA(false);
                      // }}
                      onClick={(e) => {
                        handleChange(val);
                      }}
                    >
                      {val.option}
                    </Button>
                  </BootstrapTooltip  >
                </>
              );
            })}
          </CardContent>

          <div className="d-flex justify-content-between mt-3 mb-3">
            {/* <Link to={"/DetailSaved"}> */}
            {" "}
            <Button
              sx={newButton} variant="contained" size="small" onClick={() => { navigate('/DetailSaved') }}
            >
              Go Back
            </Button>
            {/* </Link> */}
            {/* <Link to={`/CreatorNew/${checkValue}`}>
              {" "} */}
            <Button
              sx={newButton} variant="contained" size="small"
              onClick={() => {
                checkValue.length ? navigate(`/CreatorNew/${checkValue}`) : toast.info('Please Choose any Options')
              }}
            >
              Continue
            </Button>
            {/* </Link> */}
          </div>
        </div>
      </div>
    </div>
  );
}
