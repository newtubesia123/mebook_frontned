import React, { useEffect, useState } from "react";
import "./ChatSidebar.css";
import { BsSearch } from "react-icons/bs";
import { getAuth, getMessage, getUserList } from "../../../Action";
import axios from "axios";
import { SERVER } from "../../../server/server";
import { getImgURL } from "../../../util/getImgUrl";
import { useDispatch, useSelector } from "react-redux";
import { useSelect } from "@mui/base";
import pic from '../../../mepic.png'
import SearchIcon from '@mui/icons-material/Search';
const ChatSidebar = ({ userList,changeUser }) => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getUserList())
  }, []);

  const getChat = (user) => {
    changeUser(user)
    // dispatch(getMessage(null, id))
  }
console.log('userList',userList)
  return (
    <div
      className="d-flex flex-column "
      id="sideBar"
      style={{height:'100%',}}
    >
      <div
        style={{
          borderBottom: "4px solid black",
          // position: "sticky",
          // top: "20px",
          zIndex: "99",
          backgroundColor: "whiteSmoke",
          height: "30%",
          marginBottom:'1rem'
        }}
        className="d-flex flex-column justify-content-around align-items-center "
      >
        <div className="d-flex align-items-center justify-content-center" style={{height:'50%',width: "100%",}}>
          <img
            src={pic}
            alt="MeBookMeta"
            style={{width:'50%',height:'90%'}}
          />
        </div>
        <div id="searchDiv" style={{ width: "100%",height:'30%' }}>
          <input
            id="search"
            style={{
              width: "90%",
              fontSize: "1rem",
              border:'none',
              background:'none'
            }}
            type="text"
            placeholder="Search..."
          />
          <span id="searchIcon">
            <SearchIcon />
          </span>
        </div>
      </div>

      <div className=" " style={{overflowY:'auto',height:'70%'}}>
        {userList?.length && userList?.map((list,index) => {
          return (
            <>

             
                <div
                  id="listMain"
                  className="d-flex align-items-center "
                  style={{justifyContent:'space-between',width:'100%',height:'5rem'}}
                >
                  
                    <div className="avtar" style={{width:'20%',height:'100%'}}>
                      <img
                        id="avtarImg"
                        src={list?.reciever?.pdf!==null?getImgURL(list?.reciever?.pdf):`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`}
                        alt=""
                      />
                      {/* <span id="dot"></span> */}
                    
                  </div>
                  <div className="chat_list_name" onClick={() => getChat(list?.reciever)} style={{ width:'65%' }}>
                    <h4>{list?.reciever?.name?.split(' ')?.slice?.(0,2)?.join(' ')}{list?.reciever?.name?.split(' ')?.length>2?'...':''}</h4>
                    
                    {/* <p>Kane is online</p> */}
                  </div>
                  <div className="msgNum d-flex align-items-center justify-content-center" style={{width:'15%',height:'100%',}}>
                    <div className="msgNum_Num">4</div>
                  </div>
                  
                </div>
              {index+1!==userList?.length?
              <div>
                <hr
                  id="hr"
                  className="mx-auto"
                />
              </div>
              :""
        }
            </>
          )

        })}


        
      </div>
    </div >
  );
};

export default ChatSidebar;
