import React, { useEffect, useRef, useState } from "react";
import ChatSidebar from "./ChatSidebar";
import ChatMainSection from "./ChatMainSection";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { io } from "socket.io-client";
import { SERVER } from "../../../server/server";
import BackBtn from "../../../components/componentsB/btn/backBtn/BackBtn";


const ChatCombiner = () => {
  const navigate = useNavigate();
  const socket = useRef();
  const params = useParams();
  const STORE = useSelector((state) => state);
  const [currentUser, setCurrentUser] = useState()
  useEffect(() => {
    if (STORE.getProfileData?.userData?._id) {
      socket.current = io(SERVER);
      // socket.current.emit("add-user", STORE.getProfileData?.userData?._id);
    }
  }, [STORE.getProfileData?.userData]);
  const handleUser = (chat) => {
    setCurrentUser(chat)
  }
  return (
    <div className="container-fluid d-flex justify-content-center align-items-center " style={{height:'85vh',marginTop:'5rem',overflow:'hidden'}} >

      <div className="col-md-11" style={{display:'flex',flexDirection:'row',justifyContent:'space-evenly',height:'100%'}}>
        <div className="col-md-4" style={{paddingTop:'',height:'100%'}}>

          <ChatSidebar userList={STORE?.getUserList?.userList?.data} changeUser={handleUser} />

        </div>
        <div className="col-md-9" style={{ background: "black" ,paddingTop:'',height:'100%'}}>
          <ChatMainSection params={params} currentUser={currentUser} socket={socket} />

        </div>
       
      </div>

    </div>
  );
};

export default ChatCombiner;
