import React, { useEffect, useRef, useState } from "react";
import "./ChatMainSection.css";
import { RiSendPlaneLine } from "react-icons/ri";
import MainIcon from "../../../components/componentsC/mainIcon/MainIcon";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { SERVER } from "../../../server/server";
import { v4 as uuidv4 } from "uuid";
import { getAuth, getMessage } from "../../../Action";
import { getImgURL } from "../../../util/getImgUrl";
import SendIcon from '@mui/icons-material/Send';
import { newButton } from "../../../Component new/background";

const ChatMainSection = ({ params, currentUser, socket }) => {
  const [messages, setMessages] = useState([]);
  const [chat, setChat] = useState([]);
  const scrollRef = useRef();
  const [arrivalMessage, setArrivalMessage] = useState(null);

  const STORE = useSelector((state) => state);
  const dispath = useDispatch()
  const getData = async () => {
    try {
      const response = await axios.post(`${SERVER}/messages/getMsg`, {
        from: STORE.getProfileData?.userData?._id,
        to: currentUser?._id || params?.userId,
      });
      console.log("response", response)
      setMessages(response.data);
    } catch (error) {
    }
  }

  useEffect(() => {
    // dispath(getMessage(STORE.getProfileData?.userData?._id, currentUser?._id))
    getData()
  }, [currentUser]);
  const handleSendMsg = async (chat) => {
    socket.current.emit("send-msg", {
      to: currentUser?._id || params?.userId,
      from: STORE.getProfileData?.userData?._id,
      chat,
    });
    await axios.post(`${SERVER}/messages/sendMsg`, {
      from: STORE.getProfileData?.userData?._id,
      to: currentUser?._id || params?.userId,
      message: chat,
    });

    const msgs = [...messages];
    msgs.push({ fromSelf: true, message: chat });
    setMessages(msgs);
    setChat("");
  };
  if (socket.current) {
    socket.current.on("msg-recieve", (e) => {
      setArrivalMessage({ fromSelf: false, message: e });
    });
  }

  useEffect(() => {
    arrivalMessage && setMessages((prev) => [...prev, arrivalMessage]);
  }, [arrivalMessage]);

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  const sendChat = (event) => {
    console.log("send chat")
    event.preventDefault();
    if (chat.length > 0) {
      handleSendMsg(chat);
    }
  };
  //get date
  const getDate = (str) => {
    var date = new Date(str);
    let hours = date.getHours()
    let minute = date.getMinutes()
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear()
    return `${day}/${month + 1}/${year} at ${hours}:${minute}`;

  }
  return (
    <div className="d-flex flex-column alignItems-center " style={{ height:'100%',padding:'0 10px',justifyContent:'',background:'black' }}>
      <div id="listMain1 d-flex flex-column align-items-center " className="py-2" style={{ height:'10%',background:'',justifyContent:'flex-end' }}>
        <div className="d-flex align-items-center" style={{background:'',height:'100%'}}>
          <div className="avtar" style={{border:'2px solid white',}}>
            <img
              id="avtarImg"
              style={{width:'2.3rem',height:'2.3rem'}}
              src={getImgURL(currentUser?.pdf)}
              alt=""
            />
            {/* <span id="dot1"></span> */}
          </div>

          <div className="name1" style={{ marginLeft: "1rem",  }}>
            <h4 style={{ color: "whitesmoke", }}>
              {currentUser?.name || params.username}
            </h4>
          </div>
        </div>
        {/* <hr id="hr1" className="" style={{ zIndex: "",height:'10%' }} /> */}
      </div>

      <div
        id="totalMsg"
        className="row px-3 py-md-5 py-sm-3"
        style={{
          // background:'green',
          height: "80%",
          overflowY: "scroll",
          // position: "relative",
          zIndex: "66",
        }}
      >
        <div className="col-12 ">
          {messages?.length > 0 && messages?.map((message) => {
            return (
              <div ref={scrollRef} key={uuidv4()}>
                <div
                  className={`message ${message.fromSelf ? "sended" : "recieved"}`}
                >
                  <div className={`message ${message.fromSelf ? "flex-row-reverse row  my-2" : "flex-row row  my-2"}`}>
                    <div className="col-md-1 col-2 d-flex justify-content-center avtar2">

                      <img
                        id="avtarImg2"
                        src={getImgURL(message.fromSelf ? STORE.getProfileData?.userData?.pdf : currentUser?.pdf)}
                        alt=""
                      />
                    </div>
                    <div className="col-md-10 col-8">
                      <div className="row " style={{ marginLeft: "0px" }}>
                        <div>
                          <span>
                            <div className="msgText ">
                              <p>{message.message} </p>
                            </div>
                          </span>
                        </div>
                      </div>
                      <div style={{ color: "white" }}>{getDate(message.time)}</div>
                    </div>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>

      <div
        id="typeMsg"
        className="d-flex justify-content-between align-items-center sticky-bottom "
        style={{ background: "" ,height:'10%',width:'90%'}}
      >
        <div className="" style={{width:'90%'}}>
          <input id="writeMsg" type="text" placeholder="Type your message..." value={chat} onChange={(e) => setChat(e.target.value)} />
        </div>
        <div className="d-flex align-items-center justify-content-center" style={{width:'10%',height:'100%',background:'',}}>
          
            {/* <SendIcon onClick={(e) => sendChat(e)}
              style={{ color: "whitesmoke", fontSize: "2rem" }}
            /> */}
            <RiSendPlaneLine className=" d-flex align-items-center"onClick={(e) => sendChat(e)} style={{fontSize:'240%',...newButton,borderRadius:'50%',height:'3rem',width:'3rem'}} />
          
        </div>
      </div>
      {/* <div id="mainIcon" >
        <MainIcon width={"20rem"} />
      </div> */}
    </div>
  );
};

export default ChatMainSection;
