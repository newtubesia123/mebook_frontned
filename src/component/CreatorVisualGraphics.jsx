import React, { useState, useEffect } from "react";
import axios from "axios";

import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link, useNavigate } from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

import "./common.css";
import { Cascader } from 'antd';
const { SHOW_CHILD } = Cascader;
const Animation = [
  { label: '2D Vector', value: '2D Vector' },
  { label: 'Hand-Drawn', value: 'Hand-Drawn' },
  { label: '3D', value: '3D' },
  { label: 'Motion Graphics', value: 'Motion Graphics' },
  { label: 'Rotoscope', value: 'Rotoscope' },
  { label: 'Cut-out', value: 'Cut-out' },
  { label: 'Live Animated', value: 'Live Animated' },
  { label: 'Pinscreen', value: 'Pinscreen' },
  { label: 'Screencast', value: 'Screencast' },
  { label: 'Typography', value: 'Typography' },
  { label: 'Mechanical', value: 'Mechanical' },
  { label: 'Whiteboard', value: 'Whiteboard' },
  { label: 'Stop Motion', value: ' Stop Motion' },
  { label: 'Augmented Reality', value: 'Augmented Reality' },
  { label: 'Retro', value: 'Retro' },
  { label: 'Isometric', value: 'Isometric' },
  { label: 'Flipbook', value: 'Flipbook' },
  { label: 'Claymation', value: 'Claymation' },
  { label: 'Minimalist', value: 'Minimalist' },
  { label: '360', value: '360' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Cover = [
  { label: 'Front', value: 'Front' },
  { label: 'Back', value: 'Back' },
  { label: 'Booklet', value: 'Booklet' },
  { label: 'Medium', value: 'Medium' },
  { label: 'Tray', value: 'Tray' },
  { label: 'Obi', value: 'Obi' },
  { label: 'Spine', value: 'Spine' },
  { label: 'Track', value: 'Track' },
  { label: 'Liner', value: 'Liner' },
  { label: 'Sticker', value: 'Sticker' },
  { label: 'Poster', value: 'Poster' },
  { label: 'Matrix/Runou', value: 'Matrix/Runou' },
  { label: 'Top', value: ' Top' },
  { label: 'Bottom', value: 'Bottom' },
  { label: 'Watermark', value: 'Watermark' },
  { label: 'Raw/Unedited', value: 'Raw/Unedited' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Drawing = [
  { label: 'Caricature', value: 'Caricature' },
  { label: 'Figure', value: 'Figure' },
  { label: 'Gesture', value: 'Gesture' },
  { label: 'Line', value: 'Line' },
  { label: 'Perspective', value: 'Perspective' },
  { label: 'Photorealism', value: 'Photorealism' },
  { label: 'Pointillism', value: 'Pointillism ' },
  { label: 'Scientific Illustrations', value: ' Scientific Illustrations' },
  { label: 'Scratchboard', value: 'Scratchboard' },
  { label: 'Silhouette', value: 'Silhouette' },
  { label: 'Sketch', value: 'Sketch' },
  { label: 'Technical', value: 'Technical' },

  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Graphic = [
  { label: 'Web Design', value: 'Web Design' },
  { label: 'UI and Interactive', value: 'UI and Interactive' },
  { label: 'Advertising and Marketing', value: 'Advertising and Marketing' },
  { label: ' Motion and Animation', value: ' Motion and Animation' },
  { label: 'Packaging', value: 'Packaging' },
  { label: 'Illustration', value: 'Illustration' },
  { label: 'Publication and Typographic', value: 'Publication and Typographic ' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Painting = [
  { label: 'Realism', value: 'Realism' },
  { label: 'UI and Interactive', value: 'Photorealism' },
  { label: 'Expressionism', value: 'Expressionism' },
  { label: ' Impressionism', value: ' Impressionism' },
  { label: 'Abstract', value: 'Abstract' },
  { label: 'Surrealism', value: 'Surrealism' },
  { label: 'Pop Art', value: 'Pop Art ' },
  {
    label: 'Mediums', value: 'Mediums ',
    children: [
      { label: 'Oil', value: 'Oil' },
      { label: 'Watercolor', value: 'Watercolor ' },
      { label: 'Acrylic', value: 'Acrylic ' },
      { label: 'Gouache', value: 'Gouache ' },
      { label: 'Pastel', value: 'Pastel ' },
      { label: 'Encaustic', value: 'Encaustic ' },
      { label: 'Fresco', value: 'Fresco ' },
      { label: 'Spray Paint', value: 'Spray Paint ' },
      { label: 'Digital', value: 'Digital ' },

      { label: 'Suggest Tag', value: 'Suggest Tag' },
    ]
  },


]
const Illustration = [
  { label: 'Woodcutting', value: 'Woodcutting' },
  { label: 'Pencil', value: 'Pencil' },
  { label: 'Charcoal', value: 'Charcoal' },
  { label: 'Lithography', value: ' Lithography' },
  { label: 'Watercolor', value: 'Watercolor' },
  { label: 'Acrylic', value: 'Acrylic' },
  { label: 'Pen and Ink', value: 'Pen and Ink ' },
  { label: 'Freehand Digital', value: 'Freehand Digital' },
  { label: 'Concept Art', value: 'Concept Art ' },
  { label: 'Fashion', value: 'Fashion' },
  { label: 'Architecture', value: 'Architecture ' },
  { label: 'Structural', value: 'Structural ' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Photography = [
  {
    label: 'Photography', value: 'Photography ',
    children: [
      {
        label: 'Nature', value: 'Nature',
        children: [
          { label: 'Lasndscape', value: 'Lasndscape' },
          { label: 'Wildlife', value: 'Wildlife' },
          { label: 'Macro', value: 'Macro' },
          { label: 'Astrophotography', value: 'Astrophotography' },
          { label: 'Aerial', value: 'Aerial' },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ]
      },
      {
        label: 'People', value: 'People',
        children: [
          { label: 'Portraits', value: 'Portraits' },
          { label: 'Wedding', value: 'Wedding' },
          { label: 'Documentary', value: 'Documentary' },
          { label: 'Sports', value: 'Sports' },
          { label: 'Fashion', value: 'Fashion' },
          { label: 'Commercial', value: 'Commercial' },
          { label: 'Street', value: 'Street' },
          { label: 'Event', value: 'Event' },
          { label: 'Travel', value: 'Travel' },
          { label: 'Pet', value: 'Pet' },
          { label: 'Suggest Tag', value: 'Suggest Tag' },

        ]
      },
      {
        label: 'Manmade Objects', value: 'Manmade Objects',
        children: [
          { label: 'Product', value: 'Product' },
          { label: 'Food', value: 'Food' },
          { label: 'Still Life', value: 'Still Life' },
          { label: 'Architecture', value: 'Architecture' },

        ]
      },
    ]
  },


]

export default function CreatorVisualGraphic() {
  const navigate = useNavigate();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  let user = sessionStorage.getItem('userName')
  var identity = sessionStorage.getItem('identity Option')
  let workProfile = localStorage.getItem("Work profile")
  // console.log("first",JSON.parse(user).name)
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const arry = [
    { option1: "Animation ➢", id: '1' },
    { option1: "Cover Art ➢", id: '2' },
    { option1: "Drawing", id: '3' },
    { option1: "Graphic Art➢", id: '4' },
    { option1: "Painting ➢", id: '5' },
    { option1: "Illustration ➢", id: '6' },
    { option1: "PR/Promotion", id: '7' },
    { option1: "Photography ➢", id: '8' },
    { option1: "Rights. Brand, Royalties", id: '9' },
  ];
  const [checkValue, setCheckValue] = useState([])
  const handleChange = (val) => {
    let temp = checkValue;
    if (temp.some((item) => item.id === val.id)) {
      temp = temp.filter((item) => item.id !== val.id);
    } else {
      temp.push(val);
    }
    setCheckValue(temp);
  };
  const handleSubmit = (e) => {
    navigate('/CompletionPhase', { state: { option: 'Visual Graphics' } })
  };
  let Array = ['Visual Graphics'];
  for (let i = 0; i < checkValue.length; i++) {
    Array.push(checkValue[i].option1);
    localStorage.setItem('MultiOptions', JSON.stringify(Array))

  }
  return (
    <div className="container-fluid MebookMetaAuthor">
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className=" col-md-8 offset-md-2 text-center ">
         
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  src={userInfo.filepreview}
                  sx={{
                    width: "150px",
                    height: "130px",
                    margin: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color=""
                sx={{
                  margin: "15px 0px",
                  fontSize: "25px"

                }}
              >
                {user}, {identity}, {workProfile} <br /> Visual Graphics

              </Typography>
            </CardContent>
         
          <div
            className="head"
            style={{
              margin: "15px 0px",
              fontSize: "2.4vw",
              fontWeight: "bold",
              color: "",
              textAlign: "center",
              fontFamily: "Times New Roman"

            }}
          >

            <span style={{ color: '' }}>M</span><span style={{ color: '' }}>ebook</span> <span style={{ color: '' }}>M</span><span style={{ color: '' }}>eta</span> Book Profile:  , {identity}, {workProfile}, Visual Graphics
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-8 offset-md-2">
         
            <CardContent>
              <div className="row mx-2 ">
                <div className=" col-md-6 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Animation}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Animation</b>}
                  />

                </div>
                <div className=" col-md-6 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Cover}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Cover</b>}
                  />

                </div>
                <div className=" col-md-6 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Drawing}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Drawing</b>}
                  />

                </div>
                <div className=" col-md-6 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Graphic}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Graphic</b>}
                  />

                </div>
                <div className=" col-md-6 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Painting}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Painting</b>}
                  />

                </div>
                <div className=" col-md-6 col-sm-6 col-12 mt-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Illustration}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Illustration</b>}
                  />

                </div>
                <div className=" col-md-6 col-sm-6 col-12 my-4">
                  <Cascader
                    style={{
                      width: '100%',
                    }}
                    size="large"
                    options={Photography}
                    // onChange={onChange1}
                    multiple
                    expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                    maxTagCount="responsive"
                    showCheckedStrategy={SHOW_CHILD}
                    defaultValue={<b style={{ fontSize: '20px' }}>Photography</b>}
                  />

                </div>

              </div>
            </CardContent>
        
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              You have Selected {checkValue.length} Options
            </DialogTitle>
            <DialogContent>
              {checkValue !== null ? (
                <>
                  {Array.map((i, index) => (
                    <p style={{ display: 'inline', fontWeight: 'bold', fontSize: '25px', color: '#100892' }} key={index}>
                      {i},
                    </p>
                  ))}
                </>
              ) : null}
              <DialogContentText id="alert-dialog-description">
                If want to Continue Please click on Agree otherwise click on Disagree
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Disagree</Button>
              <Button onClick={handleSubmit} autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <Link to={'/CreatorNew1'}><button
              type="button"
              class="btn  btn-lg"
              style={{ background: "white" }}
            >
              Go Back
            </button></Link>
            <button
              type="button"
              class="btn  btn-lg"
              style={{ background: "white" }}
              onClick={handleClickOpen}
            >
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
