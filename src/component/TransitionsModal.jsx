import * as React from "react";
import './Selectfile.css';

import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import { Cascader } from "antd";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import CircularProgress from "@mui/material/CircularProgress";

import Typography from "@mui/material/Typography";
import { BackGround, newButton } from "../Component new/background";
import { useSelector } from "react-redux";
import { circularProgressbarStyle } from "../assets/common/theme";
import { useNavigate } from "react-router-dom";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function TransitionsModal({
  music,
  ModalOpen,
  handleModalClose,
  dropdown,
  onChangefun,
  SHOW_CHILD,
  cat,
  getData,
  handleSubmit,
  temp,
  active
}) {
  const [localMusic, setLocalMusic] = React.useState(
    JSON.parse(localStorage.getItem("Music"))
  );
  let navigate = useNavigate()
  const STORE = useSelector((state) => state);
  const bgBut = {
    width: "85%",
    fontSize: "80%",
    color: "black",
    margin: "15px 10% 15px ",
    borderRadius: "10px",
    padding: "10px 0px",
    fontFamily: "Times New Roman",

    fontWeight: "bold",
  };
  const bg = {
    ...BackGround,
    height: "100vh",
    paddingTop: 0,
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
    alignItems: "end",
  };
  const dropdownRender = (menus) => (
    <div
      style={{
        zIndex: "19999999999",
        background: "rgb(0,0,0)", color: "rgba(255,0,0,0.85)", fontSize: "1rem", fontWeight: 800

      }}
    >
      {menus}

      <div
        style={{
          color: "black",
          zIndex: 199,
        }}
      >
        The footer is not very short it is use only for fullwidth to view
        subcategory clearly.
      </div>
    </div>
  );

  return (
    <div >
      <Modal
        sx={{ zIndex: 1, }}
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={ModalOpen}
        // onClose={handleModalClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={ModalOpen}>
          {STORE?.getSubCategory?.loading ? (
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                width: "100%",
                height: "18em",
              }}
            >
              <CircularProgress sx={circularProgressbarStyle} />
            </Box>
          ) : (
            <div className="container-fluid MebookMetaCreator d-flex justify-content-center" style={bg}>
              <Button
                style={{ color: "white", fontSize: "125%", marginTop: active ? '50px' : '0px' }}
                onClick={() => {

                  active ? navigate(-1) : setLocalMusic([]);
                  handleModalClose();
                }}
              >
                {" "}
                {" "}
                <i style={{fontSize:'30px'}} class="fa-solid fa-rectangle-xmark"></i>
              </Button>
              
              {dropdown === true ? (
                <>
                  {active && (<div className=" text-center text-light mx-auto"> Your Current META FINGERPRINT TAGS Cannot Be Changed</div>)}
                  <div className="col-10 col-sm-10 col-md-6 mx-auto text-center">
                    <Cascader
                      style={bgBut}
                      dropdownRender={dropdownRender}
                      size="medium"
                      options={
                        !music
                          ? STORE?.getSubCategory?.subCategory
                          : JSON.parse(localStorage.getItem("Music"))
                      }
                      onChange={onChangefun}
                      multiple
                      expandIcon={
                        <b>
                          <ArrowForwardIosIcon
                            sx={{ fontSize: "20px", color: "red" }}
                          />{" "}
                        </b>
                      }
                      open={true}
                      maxTagCount="responsive"
                      showCheckedStrategy={SHOW_CHILD}
                      placeholder={
                        <b style={{ fontSize: "18px", color: "black", zIndex: 2 }}>
                          {cat}
                        </b>
                      }
                    />
                  </div>
                  <div className=" col-md-3 BtnClass">
                <button className=" profile-card__button button--orange "
                  onClick={() => {
                    getData();
                    handleSubmit();
                  }}
                  disabled={temp.length ? false : true}
                >
                  {active ? 'Update' : 'Continue'}
                </button>
                  </div>
                </>
                

              ) : (
                ""
              )}

              
            </div>
            
          )}
        </Fade>
      </Modal>
    </div>
  );
}


