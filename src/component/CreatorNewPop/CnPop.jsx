import { Card, CardContent } from "@mui/material";
import React from "react";
import "./pop.css";

function CnPop({ value }) {
  
  return (
    <div className=" IdentityMain" style={{ width: "450px", borderRadius: "10px" }}>
      <CardContent>
        {/* <div
          style={{
            backgroundColor: "white",
            margin: "5px 100px 5px ",
            borderRadius: "10px",
          }}
        >
          <h3
            style={{
              padding: "8px 0px",
              fontFamily: "Times New Roman",
              textAlign: "center",
            }}
          >
            {value.option}
          </h3>
        </div> */}
        <div className="modalIn"
          style={{
            backgroundColor: "white",
            margin: "15px 40px 0px ",
            borderRadius: "50px",
          }}
        >
           <h3
            style={{
              padding: "8px 0px",
              fontFamily: "Times New Roman",
              textAlign: "center",
            }}
          >
            {value.option}
          </h3>
          <p
            style={{
              padding: "20px 20px",
              textAlign: "justify",
              fontFamily: "Times New Roman",
              
            }}
          >
            <b>{value.value}</b>
          </p>
        </div>
      </CardContent>
    </div>
  );
}

export default CnPop;
