import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { BackGround, newButton } from "../../Component new/background";
import { SERVER } from "../../server/server";
import ProfileCard from "./ProfileCard";
import Button from "@mui/material/Button";
import { CircularProgress } from "@mui/material";
import { Box } from "@mui/system";
import './ProfileCard.css'
import { circularProgressbarStyle } from "../../assets/common/theme";
import BackBtn from "../../components/componentsB/btn/backBtn/BackBtn";
import MeCircularProgress from "../../components/componentsC/meCircularProgress/MeCircularProgress";

const profilesData = [
    {
        name: "Jessica Potter",
        job: "Visual Artist",
        works: 523,
        likes: 1387,
        followers: 146,
        imgUrl: "https://100dayscss.com/codepen/jessica-potter.jpg"
    },
    // add more profiles here
];

const ProfileContainer = () => {
    const params = useParams()
    const [searchData, setSearchData] = useState([]);
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate();
    const Background = {
        display: "flex",
        flexWrap: "wrap",
        // justifyContent: "center"
    }
    const containerStyle = {
        // background: 'black',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',

        minHeight: '100vh',
        margin: 0,
        padding: 0
    }
    function getAllResult() {
        setLoading(true)
        axios.get(`${SERVER}/dashboard/searchUserBymeta?type=${params?.query}`)
            .then((res) => {
                setLoading(false)
                setSearchData(res?.data?.data)
            })
            .catch((err) => setLoading(false))
    }
    useEffect(() => {
        window.scrollTo(0, 0)
        getAllResult()
    }, [params?.query])

    return (
        <>
            <div className="col-md-12" style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', backgroundColor:"black" }}>
                <div className='ml-auto mt-5'>

                    <BackBtn style={{ marginTop: '1rem' }} />
                </div>

                <div className="col-md-10 " style={containerStyle}>

                    <div className="text-center text-light mb-3">
                        <h3>Results In {params?.category}</h3>

                    </div>
                    <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-evenly' }}>
                        {
                            loading ?
                                <Box
                                    sx={{
                                        display: "flex",
                                        justifyContent: "center",
                                        width: "100%",
                                        minHeight: "100vh",
                                    }}
                                >
                                    <MeCircularProgress/>
                                </Box>
                                :
                                searchData?.length > 0 ? searchData?.map((item, index) => (
                                    <ProfileCard key={index + item?.userId + item?._id} profile={item} />
                                ))
                                    :
                                    <h3 style={{ color: 'white' }}>Not Found</h3>
                        }
                    </div>
                </div>
            </div>
        </>
    );
};

export default ProfileContainer;
