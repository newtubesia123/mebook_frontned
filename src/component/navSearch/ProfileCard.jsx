import React from "react";
import { useNavigate } from "react-router-dom";
import { getImgURL } from "../../util/getImgUrl";
import "./ProfileCard.css";
import { useSelector } from "react-redux";
import { getAuth } from "../../Action";

const ProfileCard = ({ profile }) => {
  const navigate = useNavigate();
  const { category, name, pdf, position, status, totalFollowers, totalLike
    , totalWork, userId, _id } = profile;
  const STORE = useSelector((state) => state)

  return (

    <div className="center ">
      <div className="profile-stats">
        <div className="profile">
          <div className="image"
            onClick={() => getAuth() ? navigate(`/MainDashboard/UserProfile/${name}/${userId}`) : navigate(`/Profile/${name}/${userId}`)}
          >
            <div className="circle-1"></div>
            <div className="circle-2"></div>
            {
              pdf !== null ?
                <img src={getImgURL(pdf)} width="70" height="70" alt={name} /> 
                :
                <img src={`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`} width="70" height="70" alt={name} />
            }


          </div>

          <div className="name">{name}</div>
          <div className="job">{category}</div>
          <div className="job"><i>{position}</i></div>
          <div className="job"><i>{status}</i></div>
        </div>

        <div className="stats">
          <div className="box" 
            onClick={() => getAuth() ? navigate(`/MainDashboard/WriterStoryDetails/${userId}`) : navigate(`/WriterStoryDetails/${userId}`)}
          >
            <span className="value">{totalWork}</span>
            <span className="parameter">Works</span>
          </div>
          <div className="box" style={{ cursor: 'default' }}>
            <span className="value">{totalLike}</span>
            <span className="parameter">Likes</span>
          </div>
          <div className="box" style={{ cursor: 'default' }}>
            <span className="value">{totalFollowers}</span>
            <span className="parameter">{totalFollowers > 1 ? 'Followers' : 'Follower'}</span>
          </div>
        </div>
      </div>
    </div>

  );
};

export default ProfileCard;
