import React, { useState, useEffect } from "react";
import axios from "axios";

import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link, useNavigate } from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { BackGround, newButton } from "../Component new/background";
import "./common.css";

export default function CreatorWriter() {
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  let user = sessionStorage.getItem('userName')
  var identity = sessionStorage.getItem('identity Option')
  let workProfile = localStorage.getItem("Work profile")
  const [checkValue, setCheckValue] = useState([])
  const handleChange = (val) => {
    let temp = checkValue;
    if (temp.some((item) => item.id === val.id)) {
      temp = temp.filter((item) => item.id !== val.id);
    } else {
      temp.push(val);
    }
    setCheckValue(temp);
  };
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const arry = [
    { option1: "College/ University", id: '1' },
    { option1: "Copy Writer", id: '2' },
    { option1: "Commentary", id: '3' },
    { option1: "Critic ", id: '4' },
    { option1: "Editor-Print/TV/Film", id: '5' },
    { option1: "Exposé", id: '6' },
    { option1: "Ghost Writer", id: '7' },
    { option1: "Gossip ", id: '8' },
    { option1: "Human Interest", id: '9' },
    { option1: "Instructional /Professor/Lecturer", id: '10' },
    { option1: "Journalist", id: '11' },
    { option1: "Lyricist", id: '12' },
    { option1: "Magazine", id: '13' },
    { option1: "Manual/DIY", id: '14' },
    { option1: "Music", id: '15' },
    { option1: "Newspaper", id: '16' },
    { option1: "Ombudsman", id: '17' },
    { option1: "PLaywright", id: '18' },
    { option1: "Reviewer", id: '19' },
    { option1: "Screenwriter", id: '21' },
    { option1: "Story Developer", id: '22' },
    { option1: "Television", id: '23' },
    { option1: "Thesis", id: '24' },
    { option1: "Writing/Creation Workshop", id: '25' },
  ];
  const handleSubmit = (e) => {
    navigate('/CompletionPhase', { state: { option: 'Writer' } })
  };
  let Array = ['Writer'];
  for (let i = 0; i < checkValue.length; i++) {
    Array.push(checkValue[i].option1);
    localStorage.setItem('MultiOptions', JSON.stringify(Array))
  }
  console.log("Array-->", Array);
  return (
    <div className="container-fluid" style={BackGround}>
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className=" col-md-8 offset-md-2 text-center">

          <CardContent>
            <div>
              <Avatar
                alt="Remy Sharp"
                src={userInfo.filepreview}
                sx={{
                  width: "150px",
                  height: "130px",
                  margin: "auto",
                }}
              />
            </div>

            <Typography
              variant="body2"
              sx={{ margin: "5px 0px", fontSize: "25px", color:"white" }}
            >
              {user},
              Writer

            </Typography>
          </CardContent>

          <div
            style={{
              margin: "15px 0px",
              fontSize: "3vw",
              fontWeight: "bold",
              color:"white",
              textAlign: "center",
              fontFamily: "Times New Roman"
            }}
          >

            <span style={{}}>M</span><span style={{}}>ebook</span><span style={{}}>M</span><span style={{}}>eta</span> Profile:   Writer
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-8 offset-md-2">

          <CardContent>
            <div className="row mx-2">
              {arry.map((val) => {
                return (
                  <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                    <button type="button" class="button" value={checkValue} onClick={() => { handleChange(val) }}
                      style={{ width: '200px', height: '100px', fontSize: '20px', borderRadius: "20px", margin: '10px auto' }}
                    >
                      {val.option1}
                      <img src={image} width='100px' height='50px' alt="" />
                    </button>
                  </div>
                );
              })}
            </div>
          </CardContent>

          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              You have Selected {checkValue.length} Options
            </DialogTitle>
            <DialogContent>
              {checkValue !== null ? (
                <>
                  {Array.map((i, index) => (
                    <p style={{ display: 'inline', fontWeight: 'bold', fontSize: '25px', color: '#100892' }} key={index}>
                      {i},
                    </p>
                  ))}
                </>
              ) : null}
              <DialogContentText id="alert-dialog-description">
                If want to Continue Please click on Agree otherwise click on Disagree
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Disagree</Button>
              <Button onClick={handleSubmit} autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <Link to={'/CreatorNew1'}><Button
              sx={newButton} variant="contained" size="large"
            >
              Go Back
            </Button></Link>
            <Button
              sx={newButton} variant="contained" size="large"
              onClick={handleClickOpen}
            >
              Continue
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
