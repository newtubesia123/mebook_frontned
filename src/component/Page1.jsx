import React, { useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import image from "../mepic.png";
import EastIcon from '@mui/icons-material/East';
import useSound from "use-sound";
import Button from "@mui/material/Button";
import './Page2.css';
import boopSfx from "../lisaFisher.mp3";
import '../styles/page1.css'
import { BackGround, newButton } from "../Component new/background";
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import LoginIcon from '@mui/icons-material/Login';



export default function Page1() {
  // const navigate = useNavigate();
  const [play, { stop }] = useSound(boopSfx);
  const navigate = useNavigate();
  return (
    <>
      <div style={{
        background: 'linear-gradient(245deg, rgba(0,0,0,1) 69%, rgba(255,0,0,1) 100%)', display: "flex", alignItems: 'center', flexDirection: 'column'
      }}>
        <div
          style={{ position: "absolute", top: "25%", right: "45%" }}
          className="text-center"
        >
          <img
            class="blink"
            src={image}
            alt=""
            style={{
              width: "220px",
              height: "125px",
            }}
          />
        </div>
        <div
          className=" container-fluid "
          style={{
            height: "100vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            // background: 'rgb(0,0,0)',
            // background: 'linear-gradient(245deg, rgba(0,0,0,1) 69%, rgba(255,0,0,1) 100%)'
          }}
        >
          <div
            className="tagname mt-2"
            id="move"
            style={{ fontFamily: "Times New Roman", fontSize: "50px" }}
          >
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eBook</span>
            <span style={{ color: "rgb(187 11 11)" }}>M</span>
            <span style={{ color: "#0a0a8a" }}>eta</span>
          </div>

        </div>
        {/* <Link to="/New">
        <Button
        id="contBtn"
          variant="contained"
          endIcon={<EastIcon />}
          onClick={() => play()}
          sx={{
            position: "absolute",
            right: 50,
            bottom: 50,
            fontSize: "20px",
            backgroundColor: "#100892", color: 'white',
            padding: '.5rem 2rem', background: 'rgb(14,0,255)',
            background: 'linear-gradient(90deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)', color: 'whitesmoke', animation: 'ease .1s',
            '&:hover': {
              background: 'rgb(14,0,255)',
              background: 'linear-gradient(245deg, rgba(14,0,255,1) 0%, rgba(255,0,0,1) 100%)', animation: 'ease .1s'
            }
          }}
        >
          Continue
        </Button>
      </Link> */}

        <div style={{ display: 'flex', flexWrap: 'wrap', top: '70%', position: 'absolute' }}>
          <Button
            variant="contained"
            size="large"
            sx={{ ...newButton, py: "15px", width: "10rem", margin: "5px" }}

            onClick={() => {
              play()
              navigate('/New')
            }
            }
          >
            SIGN UP<AssignmentIndIcon sx={{ marginLeft: '5px' }} />
          </Button>
          <Button
            variant="contained"
            size="large"
            sx={{ ...newButton, py: "15px", width: "10rem", margin: "5px" }}

            onClick={() => navigate('/SignIn')}
          >
            SIGN IN<LoginIcon sx={{ marginLeft: '5px' }} />
          </Button>
          {/* <Button
             variant="contained"
             size="large"
             sx={{ ...newButton, py: "15px", width: "10rem", margin: "5px" }}
              
              onClick={()=>navigate('/New')}
            >
              CONTINUE<EastIcon sx={{marginLeft:'5px'}}/>
            </Button> */}
        </div>
      </div>
    </>
  );
}