import React, { useState, useEffect } from "react";
import image from "../mepic.png";
import './CreatorNew1.css'
import CardContent from "@mui/material/CardContent";
import { useParams, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getCreatorData,
  getSubCategory,
  getUserData,
  mebookProfile,
} from "../Action/index";

import { BackGround, newButton } from "../Component new/background";
import { Cascader } from "antd";
import Button from "@mui/material/Button";
import TransitionsModal from "./TransitionsModal";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { toast } from "react-toastify";

import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import BackBtn from "../components/componentsB/btn/backBtn/BackBtn";

const { SHOW_CHILD } = Cascader;




export default function CreatorNew1() {
  const [music, setMusic] = useState(false);
  const [subCategory, setCatagory] = useState([]);
  const [userSubCategory, setUserSubCatagory] = useState([]);
  const [toggle, setToggle] = useState(true);
  const [slice, setSlice] = useState({ starting: 0, ending: 0 });
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const STORE = useSelector((state) => state);
  const params = useParams();
  let { active } = params
  const [ModalOpen, setModalOpen] = React.useState(false);
  const handleModalOpen = () => setModalOpen(true);
  const handleModalClose = () => {
    setCatagory([]);
    setModalOpen(false);
    setTimeout(() => {
      dispatch({ type: "GET_SUBCATEGORY", payload: false });
      setMusic(false);
    }, 300);
  };
  let user = sessionStorage.getItem("userName");
  var identity = sessionStorage.getItem("identity Option");

  const bg = { ...BackGround, minHeight: "100vh" };
  const bgBut = {
    ...newButton,
    width: "80%",
    fontSize: "90%",
    color: "black",
    margin: "15px 10% 15px ",
    borderRadius: "10px",
    padding: "10px 0px",
    fontFamily: "Times New Roman",
    fontWeight: "bold",
    color: "white",
  };

  let workProfile = localStorage.getItem("Work profile");

  const [cat, setCat] = useState("");
  const handleSubmit = (e) => {
    dispatch(
      mebookProfile({
        subCategory: active ? newArr : subCategory,
        userId: STORE.getProfileData?.userData?._id,
        category: cat,
        identityId: STORE.userIdentity?.userIdentity?.id,
        type: active ? 'Edit' : 'Add'
      })
    );
    setTimeout(() => {
      if (active) {
        navigate(-1)
      } else {
        navigate("/CompletionPhase");
      }
    }, 2000);
  };

  useEffect(() => {
    if (!active) {
      dispatch(getCreatorData());
      dispatch(getUserData());
      if (
        STORE?.getProfileData?.userData?.homeTown &&
        STORE?.getProfileData?.userData?.metaProfile
      ) {
        localStorage.removeItem("token"); // access from dashboard
        toast.error("access denied", {
          position: "top-center",
          autoClose: 1000,
        });
        navigate("/SignIn");
      }
      if (!STORE?.getProfileData?.userData?.identityProfile) {
        // access after logIn
        localStorage.removeItem("token");
        toast.error("Validation failed", {
          position: "top-center",
          autoClose: 1500,
        });
        navigate("/SignIn");
      }
    } else {
      setUserSubCatagory(STORE.userProfile.userProfile.meProfile.subCategory.map((e) => e._id))

      setModalOpen(true);
      active === "Music" ? setMusic(true) : setMusic(false)
      dispatch(getSubCategory(active));
      setCat(active);
      setDropdown(true);
    }
  }, [active]);



  if (STORE.errorPage) {
    navigate("/ErrorPage");
  }
  const [dropdown, setDropdown] = useState(false);
  let [temp, setTemp] = useState([]);
  const onChangefun = (val) => {
    // console.log(val.map((e)=>e[e.length-1]))
    setCatagory(val.map((e) => e[e.length - 1]));
  }
  let tempArr = [...userSubCategory, ...subCategory]
  let newArr = tempArr.filter((value, index, self) =>
    index === self.findIndex((t) => (
      t === value
    )))



  const getData = () => {
    // for (let i = 0; i < temp.length; i++) {
    //   subCategory.push(...temp[e.length-1])

    // }
    // setOpen(true);
  };
  const dropdownRender = (menus) => (
    <div
      style={{
        zIndex: "19999999999",
        backgroundColor: "black",
      }}
    >
      {menus}

      <div
        style={{
          color: "white",
          zIndex: 199,
        }}
      >
        The footer is not very short it is use only for width.
      </div>
    </div>
  );

  // for dialog
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  function handleSelector(starting, ending) {
    setToggle(!toggle);
    setSlice({
      ...slice,
      starting: starting,
      ending: ending,
    });
  }
  useEffect(() => {
    // console.log("music=>", music);
  }, [music]);

  // console.log(STORE.getCreatorData.userCreatorData, "writer author data")

  // Description Aquordian style
  const AccordianBackground = {
    ...BackGround,
    width: 1000,
    color: "white",
    margin: "10px 0 10px 0",
    border: "1px solid white",
  };
  return (
    <>
      {
        active ?
          <>
            <TransitionsModal
              active={active ?? false}
              music={music}
              ModalOpen={ModalOpen}
              handleModalClose={handleModalClose}
              handleSubmit={handleSubmit}
              temp={subCategory}
              getData={getData}
              cat={cat}
              SHOW_CHILD={SHOW_CHILD}
              onChangefun={onChangefun}
              STORE={STORE}
              dropdownRender={dropdownRender}
              dropdown={dropdown}
              image={image}
              user={user}
            />
          </>
          :
          <div className="container-fluid MebookMetaCreator" style={bg}>
            {/* <WriterNav /> */}

            <div className="img text-center">

              <img
                src={image}
                alt=""
                style={{
                  width: "150px",
                  height: "80px",
                  margin: "5px 0px 0px",
                }}
              />
            </div>
            {!toggle ?
              <div className='ml-auto mt-5'>
                <Button sx={{ ...newButton, fontSize: "70%",float:'right' }} onClick={() => setToggle(!toggle)} variant="contained">Go Back</Button>
              </div>
              : ''
            }
            <div className="row ">

              <div className=" col-md-8 offset-md-2 text-center ">

                <div
                  id="font"
                  style={{
                    margin: "5px 0px",
                    fontSize: "200%",
                    fontWeight: "bold",
                    textAlign: "center",
                    color: "white",
                  }}
                >
                  {/* <span></span>  Welcome, {JSON.parse(user).name} <br /> To the MebookMeta  Universe! */}
                  Hi, {user} <br />
                  Welcome to the MebookMeta Universe! <br />
                  Please Set Up Your <span style={{}}>M</span>
                  <span style={{}}>eBook</span>
                  <span style={{}}>M</span>
                  <span style={{}}>eta</span> Work Profile

                </div>
              </div>
            </div>
            <div className="row d-flex ">
              <div className="col-md-3">

              </div>
              <div className=" col-md-6 ">
                {toggle ? (
                  <CardContent>
                    <Button sx={bgBut} onClick={() => handleSelector(0, 2)}>
                      Author / Writer
                    </Button>
                    <Button sx={bgBut} onClick={() => handleSelector(2, 6)}>
                      Music / Audio / Sound
                    </Button>
                    <Button sx={bgBut} onClick={() => handleSelector(6, 9)}>
                      Film / Television / Media
                    </Button>
                    <Button sx={bgBut} onClick={() => handleSelector(9, 10)}>
                      Visual Arts
                    </Button>
                    <Button sx={bgBut} onClick={() => handleSelector(10, 11)}>
                    Performing Arts
                    </Button>
                  </CardContent>
                ) : (
                  <CardContent>
                    {STORE.getCreatorData.userCreatorData &&
                      STORE.getCreatorData.userCreatorData.length
                      ? STORE.getCreatorData.userCreatorData
                        .slice(slice.starting, slice.ending)
                        .map((item) => (
                          <>
                            <Button
                              key={item._id}
                              onClick={() => {
                                if (item.name === "Music") {
                                  setMusic(true)
                                  setCat(item.name);
                                  handleModalOpen(true);
                                  setDropdown(true);
                                } else {
                                  setMusic(false);
                                  dispatch(getSubCategory(item.name));
                                  setCat(item.name);
                                  handleModalOpen(true);
                                  setDropdown(true);
                                  // navigate(`subcategory/${item.name}`)
                                }
                              }}
                              sx={bgBut}
                            >
                              {item.name}

                            </Button>
                          </>
                        ))
                      : "null"}
                  </CardContent>
                )}
                {/* <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title" sx={{ fontFamily: "Times New Roman" }}>
            You have Selected {subCategory.length} Options
          </DialogTitle>
          <DialogContent>

            <DialogContentText id="alert-dialog-description">
              If these are your final choices for your {cat} profile, please click to confirm
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Go Back</Button>
            <Button onClick='' autoFocus>
              Confirm
            </Button>
          </DialogActions>
        </Dialog> */}
              </div>
              <div className="col-md-6 offset-3 m-auto">
                {
                  slice.starting === 0 && slice.ending === 2 ? <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                    <Accordion sx={AccordianBackground}>
                      <AccordionSummary
                        sx={{ color: "white", height: "2rem" }}
                        expandIcon={<ExpandMoreIcon sx={{ color: "white" }} />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                      >
                        <h6 className="fontReSize">
                          Are You a Writer or Are You an Author? – The MeBookMeta Distinction
                        </h6>
                      </AccordionSummary>
                      <AccordionDetails>
                        <h6>
                          An Author is a person who is the originator of a written work like an article, or a book. Many authors
                          specialize in specific genres, so if you choose “Author” on this site, you will find a list of genres
                          (categories) in which you can include your specialization(s). Authors tend to be associated with books,
                          which involve plot development and realistic characters, who help to tell an engaging story with an
                          overall theme and a message.
                          Writers are persons who write pieces of literature, articles, blogs, novels or short stories not necessarily
                          on their own ideas. “Writer” is all encompassing term, which can include authors, while “Writer” also
                          comprises persons who create social media posts, commentary, website content, periodicals, essays,
                          reviews, screenplays, advertising and other forms of written content.
                        </h6>
                        <h5>
                          Still Unsure on Which Category to Choose?
                        </h5>
                        <h6>
                          The good news is that, for the MeBookMeta Universe, this specific choice is less relevant than the work
                          samples that you will choose to share. To help make up your mind, take a look at the categories listed
                          under “Author” and “Writer” and determine which group or groups of listings best define your work, as
                          they will be listed as your fingerprint tags to help better define you to the greater audience.
                        </h6>
                        <h5>
                          Can You Choose Both?
                        </h5>
                        <h6>
                          Because we want to help you expand your market reach and connectivity, we limit each profile user to
                          choose one or the other so that we can focus on a specific audience and specific collaborators, influencers,
                          providers, project developers and readers. However, persons who would like to create two separate
                          MeBookMeta profiles can do so by setting up a second profile using new credentials.
                        </h6>
                      </AccordionDetails>
                    </Accordion>
                  </div> : ""
                }

              </div>

              {/* {dropdown === true ? (
        <div className="col-md-3 text-center mt-4">

          <Cascader
            style={{
              width: '100%',
            }}
            dropdownRender={dropdownRender}
            size="large"
            options={STORE?.getSubCategory?.subCategory}
            onChange={onChangefun}
            multiple
            expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}
            open={true}
            maxTagCount="responsive"
            showCheckedStrategy={SHOW_CHILD}
            placeholder={<b style={{ fontSize: '18px', color: 'black' }}>{cat}</b>}
          />

        </div>
      ) : ""} */}
            </div>

            <TransitionsModal
              music={music}
              ModalOpen={ModalOpen}
              handleModalClose={handleModalClose}
              handleSubmit={handleSubmit}
              temp={subCategory}
              getData={getData}
              cat={`${cat} (check all boxes that apply)`}
              SHOW_CHILD={SHOW_CHILD}
              onChangefun={onChangefun}
              STORE={STORE}
              dropdownRender={dropdownRender}
              dropdown={dropdown}
              image={image}
              user={user}
            />
          </div>
      }
    </>
  );
}
