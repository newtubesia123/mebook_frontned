import React, { useEffect, useState } from "react";
import "./GlobalPop.css";
import Modal from "@mui/material/Modal";
import CloseIcon from '@mui/icons-material/Close';
import payment from '../../voiceMessage/HomePageVoice.mp3'
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { motion } from "framer-motion";
import { Link, useNavigate } from "react-router-dom";
import { newButton } from "../../../Component new/background";
import axios from "axios";
import { toast } from "react-toastify";
import { SERVER } from "../../../server/server";
import { getAuth, handleFollow, userDashboardDetail } from "../../../Action";
import PaymentSuccess from "../../../Component new/PaymentSuccess/PaymentSuccess";
import useSound from "use-sound";
import { DeleteWork } from "../../../Component new/userWork/WorkCard";
import { DeletePitch } from "../../../Component new/WritePitch";
import ProfileImgView from "../../../component/ProfileImgView";
import { getImgURL } from "../../../util/getImgUrl";
import AvatarImg from '../../../png/Avatar-Profile-PNG-Photos.png';
import MeCircularProgress from "../../componentsC/meCircularProgress/MeCircularProgress";
import Pitch from "../../../Component new/User Dashboard/Pitch/Pitch";
import MuiAccordian from "./MuiAccordian";
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';

const styles = {
  position: "absolute",
  top: "10%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  color: "white",
  boxShadow: 24,
  borderRadius: "10px",
  padding: "22px 22px",
  fontSize: "2vmin",
};
export default function GlobalPop({ children }) {
  const [stage, setStage] = useState("login");
  const STORE = useSelector((state) => state);
  const dispatch = useDispatch();
  const [popUp, setPopUp] = useState(false);
  const [play, { stop }] = useSound(payment);
  const popType = useSelector((state) => state.callPopUp.PopType);
  

  useEffect(() => {
    setPopUp(STORE?.callPopUp.showPop);
    setStage(popType);
  }, [STORE, popType]);

  //  useEffect(()=>{
  //     setStage(STORE?.callPopUp.PopType)
  //  },[STORE?.callPopUp.PopType])

  function handleModalClose(type) {
    if (type == "payment") {
      dispatch({ type: "REMOVE_POPUP" });
      return play()
    }
    dispatch({ type: "REMOVE_POPUP" });
  }

  var obj = {
    login: <SignUp handleModalClose={handleModalClose} />,
    signin: <SignIn handleModalClose={handleModalClose} />,
    SuccessPayment: <SuccessPayment handleModalClose={handleModalClose} />,
    makeSubscription: <MakeSubscription handleModalClose={handleModalClose} />,
    CouponCheck: <CouponCheck handleModalClose={handleModalClose} />,
    workDelete: <WorkDelete handleModalClose={handleModalClose} />,
    pitchDelete: <PitchDelete handleModalClose={handleModalClose} />,
    CancelSubscription: <CancelSubscription handleModalClose={handleModalClose} />,
    ShowProfile: <ShowProfile handleModalClose={handleModalClose} />,
    ShowPitch: <ShowPitch handleModalClose={handleModalClose} />,
    ShowFollowers: <ShowFollowers handleModalClose={handleModalClose} />,
    ShowFPTags: <ShowFPTags handleModalClose={handleModalClose} />,
    // askDelete: <AskDelete handleModalClose={handleModalClose} handleDelte={STORE?.callPopUp.handleDelete} askToDelete={STORE?.callPopUp.askToDelete} />,
  };

  return (
    <>
      <div>
        {children && children}
        <Modal
          open={popUp}
          // onClose={handleModalClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <motion.div
            initial={{ scale: 1, opacity: 0 }} // Set initial scale to 1 and opacity to 0 for a fade-down effect
            animate={{ scale: 1, opacity: 1 }} // Animate scale to 1 and opacity to 1 for normal visibility
            exit={{ scale: 1, opacity: 0 }} // Animate scale to 1 and opacity to 0 for a fade-down effect when closing
            transition={{ duration: 0.6 }}

          >
            {obj[stage]}
          </motion.div>
        </Modal>
      </div>
    </>
  );
}

// sign Up popUp Box

function SignUp({ handleModalClose }) {
  const dispatch = useDispatch();
  function handleModalClose() {
    dispatch({ type: "REMOVE_POPUP" });
  }

  return (
    <Box sx={{ ...styles, background: "#2f2841" }}>
      <div className='text-desc mx-auto my-auto' style={{ position: "absolute", right: 0, top: 0, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      <div>
        <Typography id="modal-modal-title">
          <p>Please Sign Up to Enjoy The Uninterrupted Services..</p>
          <Link to="/Signup" style={{ textDecoration: "none" }}>
            {" "}
            <Button
              variant="contained"
              size="large"
              sx={{
                ...newButton,
                py: "6px",
                width: "6rem",
                fontSize: "75%",
              }}
              onClick={handleModalClose}
            >
              Sign Up
            </Button>
          </Link>
        </Typography>
      </div>
    </Box>
  );
}

function SignIn({ handleModalClose }) {
  const dispatch = useDispatch();
  function handleModalClose() {
    dispatch({ type: "REMOVE_POPUP" });
  }

  return (
    <Box sx={{ ...styles, background: "#2f2841" }}>
      <div className='text-desc mx-auto my-auto' style={{ position: "absolute", right: 0, top: 0, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      <div></div>
      <div>
        <Typography id="modal-modal-title">
          <p>Please SignIn To Enjoy The Uninterrupted survices</p>
          <Link to="/SignIn" style={{ textDecoration: "none" }}>
            {" "}
            <Button
              variant="contained"
              size="large"
              sx={{
                ...newButton,
                py: "6px",
                width: "6rem",
                fontSize: "75%",
              }}
              onClick={handleModalClose}
            >
              Sign In
            </Button>
          </Link>
        </Typography>
      </div>
    </Box>
  );
}

function SuccessPayment({ handleModalClose }) {
  return (
    <PaymentSuccess handleModalClose={handleModalClose} />
  );
}

function WorkDelete({ handleModalClose }) {
  return (
    <DeleteWork handleModalClose={handleModalClose} />
  );
}

function PitchDelete({ handleModalClose }) {
  return (
    <DeletePitch handleModalClose={handleModalClose} />
  );
}

function MakeSubscription({ handleModalClose }) {
  return (
    <Box sx={{ ...styles, background: "#2f2841", padding: "20px" }}>
      <div className='text-desc mx-auto my-auto' style={{ position: "absolute", right: 0, top: 0, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      <div></div>
      <div>
        <Typography id="modal-modal-title">
          <p>Please Make A Subscription To Enjoy Our Services...</p>
        </Typography>
      </div>
    </Box>
  );
}
function CouponCheck({ handleModalClose }) {
  const [code, setCode] = useState({ couponCode: '' })
  const [backCoupon, setBackCoupon] = useState('')
  // console.log('backCoupon',backCoupon)
  const handleChange = (e) => {
    setCode({
      ...code,
      [e.target.name]: e.target.value,
    });
  }
  function handlePayment(type, couponObj) {
    // console.log('chal raha')
    const paymentApi = `${SERVER}/payment/checkout`
    const method = {
      1: "standard",
      2: "enhanced"
    }
    // console.log('test',{ product: method[type],couponObj:backCoupon.find((item)=>item?.code==code?.couponCode)?.coupon })
    axios.post(paymentApi, { product: method[type], couponObj: backCoupon.find((item) => item?.code == code?.couponCode)?.coupon },
      {
        headers: { Authorization: `Bearer ${getAuth()}` }
      }).then((response) => {
        if (response.data.errorCode == 200) {
          // console.log("response", response?.data, response?.data?.sessionurl)
          window.location.href = response?.data?.sessionurl
        } else {
          toast.error(response.data?.message)
          // console.log("response", response)
        }
      }).catch((error) => {
        console.log(error)
      })
  }

  useEffect(() => {
    axios.get(`https://api.stripe.com/v1/promotion_codes`,
      {
        headers: { Authorization: `Bearer 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` }
      })
      .then((res) => {
        // console.log(res.data)
        setBackCoupon(res?.data?.data)
      })
  }, [])
  const checkCoupon = () => {
    // console.log('backCoupon?.map((item)=>item?.code)', backCoupon.find((item) => item?.code == code?.couponCode && item?.active))


    if (code?.couponCode == '' || code?.couponCode === backCoupon.find((item) => item?.code == code?.couponCode && item?.active)?.code && backCoupon.find((item) => item?.code == code?.couponCode && item?.active).active === true) {
      handlePayment(1, backCoupon.find((item) => item?.code == code?.couponCode && item?.active))

    } else if (code?.couponCode !== backCoupon.find((item) => item?.code == code?.couponCode && item?.active)?.code) {
      toast.error("Invalid Coupon Code")
    } else if (!backCoupon.find((item) => item?.code == code?.couponCode && item?.active).active) {
      toast.error("Coupon Code Expires")

    }

  }
  return (
    <Box sx={{ ...styles, background: "#2f2841", top: '50%', width: "23rem", height: "20rem", display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', }}>
      <div className='text-desc mx-auto my-auto' style={{ position: "absolute", right: 0, top: 0, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      <div></div>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', gap: '12px', width: "350px", height: "250px" }}>
        <Typography id="modal-modal-title" align="center">
          Do You Have a Coupon Code?
        </Typography>
        <input name='couponCode' value={code?.couponCode} type="text" placeholder="Enter Coupon Code" onChange={handleChange} style={{ width: '15rem', height: '2rem', textAlign: 'center', margin: '15px auto' }} />
        <Typography id="modal-modal-title" align="center">
          No Coupon Code? <br /> Tap the box below to begin your free trial
        </Typography>
        <Button
          variant="contained"
          size="medium"
          // sx={{
          //   ...newButton,
          //   py: "6px",
          //   width: "100%",
          //   fontSize: "75%",
          //   my:2
          // }}
          className="mx-auto profile-card__button button--orange"
          onClick={checkCoupon}
        >START YOUR FREE TWO-WEEK TRIAL</Button>
      </div>
    </Box>
  )
}



function CancelSubscription({ handleModalClose }) {
  const STORE = useSelector((state) => state);
  const cancelApi = `${SERVER}/payment/stripe/cancelSubscription`
  const dispatch = useDispatch()
  const cancelSubs = async () => {
    // alert('meraj')
    axios.post(cancelApi, {}, {
      headers: { Authorization: `Bearer ${await getAuth()}` }
    })
      .then((response) => {
        console.log('response', response)
        if (response?.data?.errorCode == 404) {
          toast.error(response?.data?.message, { position: 'top-center', autoClose: 2000, pauseOnHover: false })
        } else if (response?.data?.errorCode == 200) {
          toast.success(response?.data?.message, { position: 'top-center', autoClose: 2000, pauseOnHover: false })
          dispatch(userDashboardDetail());
        }

      })
      .catch((error) => console.log('error in cancel subscription', error))
  }
  // otp verification

  const [openInput, setOpenInput] = useState(false)
  const [otp, setOtp] = useState("")
  const [flag, setFlag] = useState(false)
  const [CircularProgress, setCircularProgress] = useState(false)
  const handleSendOtp = () => {
    axios.post(`${SERVER}/SendOtp`, { email: STORE.getData.userData.data.email })
      .then((res) => {
        if (res.data.isSuccess) {
          toast.success("Otp has been sent to your email id");

        } else {
          toast.error(res.data.Error);
        }
      })
      .catch((error) => {
        console.log(error)
      })
  }
  const handleVarifyOtp = () => {
    setCircularProgress(true)
    axios.post(`${SERVER}/checkOtp`, { otp: otp })
      .then((res) => {
        setCircularProgress(false)
        if (res.data.success) {
          setFlag(true)
          toast.success("Otp matched successfully");
        } else {
          toast.error("Session Expired try again");
        }
      })
      .catch((err) => console.log(err));
  }
  return (

    <Box sx={{ ...styles, background: "#2f2841", top: '50%', width: "25rem", height: "20rem", display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
      <div className='text-desc mx-auto my-auto' style={{ position: "absolute", right: 0, top: 0, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      <div> {CircularProgress ? <MeCircularProgress /> : ''}</div>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', gap: '12px', width: "350px", height: "20px" }}>
        <Typography id="modal-modal-title" align="center">
          Do You Want To Cancel Your Subscription?
        </Typography>
        <div className="d-flex justify-content-around">
          {!openInput ? <>
            <Button
              onClick={() => handleModalClose()}
              variant="contained"
              size="large"
              sx={{
                ...newButton,
                py: "6px",
                width: "6rem",
                fontSize: "75%",
                mx: '2rem',
                my: '2rem'
              }}
            >
              No
            </Button>
            <Button
              onClick={() => {
                setOpenInput(true)
                handleSendOtp()
              }}
              variant="contained"
              size="large"
              sx={{
                ...newButton,
                py: "6px",
                width: "6rem",
                fontSize: "75%",
                mx: '2rem',
                my: '2rem'
              }}
            >
              Yes
            </Button>
          </> : ""}
        </div>
        {openInput && !flag ?
          <>
            <input type="text" class="form-control" value={otp} onChange={(e) => setOtp(e.target.value)} placeholder="Enter your OTP here " />
            <small>Otp will expire in 5 minutes</small>
            <Button
              variant="contained"
              size="medium"
              className="mx-auto profile-card__button button--orange"
              onClick={handleVarifyOtp}
            >Varify</Button>
          </>
          : ''}
        <Button
          variant="contained"
          size="medium"
          disabled={flag ? false : true}
          className="mx-auto profile-card__button button--orange"
          onClick={cancelSubs}
        >Cancel Subscription</Button>
      </div>
    </Box>
  )
}


function ShowProfile({ handleModalClose }) {
  const STORE = useSelector((state) => state);

  return (
    <Box sx={{ ...styles, top: '46%' }}>
      <div className='text-desc mx-auto my-auto' style={{ position: "absolute", right: 0, top: 0, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      <div></div>
      {STORE.userProfile?.userProfile?.meProfile?.userId?.pdf ? (
        <img style={{ height: 300, width: 300, borderRadius: "50%" }}
          src={getImgURL(STORE.userProfile?.userProfile?.meProfile?.userId?.pdf)}
          alt="User-profile"
        />
      ) : (
        <img style={{ height: 300, width: 300, borderRadius: "50%" }}
          src="https://icon-library.com/images/default-user-icon/default-user-icon-13.jpg"
          alt="profile picture"
        />
      )}
    </Box>
  )
}

// function AskDelete({ handleModalClose, handleDelte, askToDelete }) {
//   const dispatch = useDispatch();
//   function handleModalClose() {
//     dispatch({ type: "REMOVE_POPUP" });
//   }

//   return (
//     <Box sx={{ ...styles, background: "#2f2841", top: '50%' }}>
//       <div className='text-desc mx-auto my-auto' style={{ position: "absolute", right: 0, top: 0, cursor: "pointer" }}>
//         <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
//       </div>
//       <h6
//         id="transition-modal-description"
//         style={{ color: "#fff", textAlign: "center", marginTop: '7px' }}
//       >
//         Do you really want to Delete the Ask Permannetly?
//       </h6>
//       <Box
//         style={{
//           display: "flex",
//           flexDirection: "row",
//           alignItems: "center",
//           justifyContent: "space-evenly",
//           padding: "1rem",
//         }}
//       >
//         <Button
//           onClick={handleModalClose}
//           variant="contained"
//           size="large"
//           sx={{
//             ...newButton,
//             py: "6px",
//             width: "6rem",
//             fontSize: "75%",
//           }}
//         >
//           Cancel
//         </Button>

//         <Button
//           varient="contained"
//           onClick={() => { handleModalClose(); handleDelte(askToDelete); }}
//           variant="contained"
//           size="large"
//           sx={{
//             ...newButton,
//             py: "6px",
//             width: "6rem",
//             fontSize: "75%",
//           }}
//         >
//           Delete
//         </Button>
//       </Box>
//     </Box>
//   );
// }

function ShowPitch({ handleModalClose }) {
  const STORE = useSelector((state) => state);
  const AccordianBackground = {

    color: "black",
    margin: "10px 0 10px 0",
    border: "1px solid white",
    width: 'auto',
    marginBottom: '1rem'
  };
  return (
    <Box className='col-lg-8 col-md-8 col-sm-11' sx={{ ...styles, top: '50%', left: '50%', minHeight: '20rem', height: 'auto', boxShadow: "black 5px 5px 10px 5px" }}>
      <div className='text-desc mx-auto my-2' style={{ position: "absolute", right: 0, top: 5, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      {(STORE?.getPitch?.userPitch?.message?.pitchVdo !== "" || STORE?.getPitch?.userPitch?.message?.pitch?.lemgth > 0) ?

        <div className='col-12' style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
          <h3 style={{ color: '#fff', textAlign: 'center' }}>My Pitch</h3>

          {STORE?.getPitch?.userPitch?.message?.pitchVdo ?
            <video className="col-12" style={{
              boxShadow: "black 5px 5px 10px 5px",
              borderRadius: "10px",
            }}

              src={getImgURL(
                STORE?.getPitch?.userPitch?.message?.pitchVdo
              )}
              controlsList="nodownload"
              autoPlay
              controls
              muted
              loop
            ></video>
            : ""
          }
          {/* <MuiAccordian accBg={{width:'100%'}}/> */}
          {STORE?.getPitch?.userPitch?.message?.pitch ?
            <div className="col-12">
              <MuiAccordian accBg={AccordianBackground} iconColor={{ color: 'black' }} accSummary={'Click here to see my personal pitch to you and global media market place'} accDetails={STORE?.getPitch?.userPitch?.message?.pitch?.join(' ')} accSumTypo={{ color: 'black', textAlign: 'center' }} accDetailTypo={{ color: 'black' }} />
            </div>
            : ""
          }
        </div>
        : <div className='col-12' style={{ minWidth: '100%', minHeight: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
          <h3 style={{ color: '#fff', textAlign: 'center', textTransform: 'uppercase' }}> user has not uploaded any pitch</h3>
        </div>
      }
    </Box>
  )
}




function ShowFollowers({ handleModalClose }) {
 
  const [value, setValue] = useState('1');
  const [users, setUsers] = useState({})
  const STORE = useSelector((state) => state)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  useEffect(() => {
    // below line is commented because we set these data using api here name getFolData(). if we uncomment below lines then we will get data from profile page through colpopup dispatch.


    // setUsers({ allFollowers: STORE?.callPopUp?.data?.allFollowers, allFollowings: STORE?.callPopUp?.data?.allFollowings })
    getFolData()
    setValue(`${STORE?.callPopUp?.data?.query}`)
  }, [STORE?.userProfile?.userFollower?.allFollowing])

  function getFolData() {
    if (getAuth()) {
      axios.get(`${SERVER}/users/handleFollowers`,
        {
          headers: { authorization: `Bearer ${getAuth()}` }
        }).then((res) => {
          setUsers({ allFollowers: res?.data?.allFollowers, allFollowings: res?.data?.allFollowing })
        })
        .catch((err) =>
          console.log(err)
        )
    }
  }
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function TableList({ data }) {
    const [isFollow, setIsFollow] = useState(true);
    const handleFollowings = () => {
      dispatch(handleFollow(data?.id, STORE?.getProfileData?.userData?._id))
      setIsFollow(false)
    }
    return (
      <div className="col-12 p-0 d-flex align-items-center" style={{ minHeight: '3rem', justifyContent: 'space-between' }}>
        <div className="col-md-1 p-0 d-flex align-items-center justify-content-center" style={{ height: '100%', cursor: 'pointer' }}>
          <img style={{ width: '2rem', height: '2rem', borderRadius: '50%' }} src={data?.profile ? `${data?.profile}` : `https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`} />
        </div>
        <div className="col-md-6 p-0 d-flex align-items-center " style={{ height: '100%', textTransform: 'capitalize', cursor: 'pointer' }}
          onClick={() => {
            navigate(`/MainDashboard/UserProfile/${data?.name}/${data?.id}`)
            handleModalClose()
          }
          }
        >
          <h6>{data?.name}</h6>
        </div>
        <div className="col-md-2 p-0 d-flex align-items-center justify-content-center" style={{ height: '100%', }}>
          <button className="new_prof_btn" style={{ background: 'white', color: 'black', borderRadius: '20px', padding: '5px 20px', outline: 'none', border: 'none', fontSize: '.8rem', letterSpacing: '1px' }}
            onClick={() => {
              navigate(`/MainDashboard/UserProfile/${data?.name}/${data?.id}`)
              handleModalClose()
            }
            }
          >
            View
          </button>
        </div>
        {!data?.followersView ?
          <div className="col-md-3 p-0 d-flex align-items-center justify-content-center" style={{ height: '100%', }}>
            <button className="new_prof_btn" style={{ background: 'black', color: 'white', borderRadius: '20px', padding: '5px 20px', outline: 'none', border: 'none', fontSize: '.8rem', letterSpacing: '1px' }}
              onClick={() => handleFollowings()}
            >
              {isFollow ? `Following` : `Follow`}
            </button>
          </div>

          : ''
        }
      </div>
    )
  }
  return (
    <Box className='col-lg-8 col-md-8 col-sm-12 bg-dark' sx={{ ...styles, top: '50%', left: '50%', height: '20rem', boxShadow: "black 5px 5px 10px 5px", overflowY: 'auto' }}>
      <div className='text-desc mx-auto my-1' style={{ position: "absolute", right: 0, top: 5, cursor: "pointer" }}>
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} onClick={() => handleModalClose()} />
      </div>
      <Box className='col-12 my-4 p-0' sx={{ typography: 'body1' }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider', bgcolor: 'background.paper' }}>
            <TabList onChange={handleChange} aria-label="lab API tabs example" centered textColor="secondary">
              <Tab label="FOLLOWINGS" value="1" />
              <Tab label="FOLLOWERS" value="2" />

            </TabList>
          </Box>
          <TabPanel className='col-12 p-0' value="1">

            {
              users?.allFollowings && users?.allFollowings?.length > 0 ? (
                users?.allFollowings?.map((item, index) => (
                  <TableList key={index} data={{ name: item?.follow?.name, profile: item?.follow?.pdf, id: item?.follow?._id, followersView: false }} />
                ))
              )
                :
                <div className="col-12 d-flex align-items-center justify-content-center" style={{ height: '25vh' }}>
                  <MeCircularProgress />
                </div>

            }
          </TabPanel>
          <TabPanel className='col-12 p-0' value="2">
            {
              users?.allFollowers && users?.allFollowers?.length > 0 ? (
                users?.allFollowers?.map((item, index) => (
                  <TableList key={index} data={{ name: item?.userId?.name, profile: item?.userId?.pdf, id: item?.userId?._id, followersView: true }} />
                ))
              )
                :
                <div className="col-12 d-flex align-items-center justify-content-center" style={{ height: '25vh' }}>
                  <MeCircularProgress />
                </div>
            }

          </TabPanel>


        </TabContext>
      </Box>

    </Box>
  )
}

function ShowFPTags({ handleModalClose }) {
  const STORE = useSelector((state) => state)

  return (
    <Box className='col-md-6 bg-dark' sx={{ ...styles, top: '50%', left: '50%', height: '20rem', boxShadow: "black 5px 5px 10px 5px", overflowY: 'auto' }}>
      <div className='text-desc mx-auto my-2' style={{ position: "absolute", right: 0, top: 5, cursor: "pointer", }}
        onClick={() => handleModalClose()}
      >
        <CloseIcon sx={{ ...newButton, fontSize: '2rem', borderRadius: '50%' }} />
      </div>
      <div style={{ width: '100%', minHeight: '50%', padding: '1rem' }}>
        <h6 className='profLable'>MEBOOKMETA FINGERPRINT TAGS:</h6>
        <span style={{ display: "flex", flexWrap: "wrap" }}>
          {STORE?.callPopUp?.data?.fptags && STORE?.callPopUp?.data?.fptags?.length ? (
            STORE?.callPopUp?.data?.fptags?.map((item, index) => (
              <span
                key={index}
                style={{
                  color: "rgb(252,252,252)",
                  background: "#2B3467",
                  margin: ".2rem .2rem",
                  padding: "4px 8px 4px 8px",
                  borderRadius: "10px",
                  fontSize: "100%",
                }}
              >
                <span>{item?.name}</span>
              </span>
            ))

          ) : <span style={{ display: "" }}>This Profile has not Completed yet</span>
          }

        </span>
      </div>
    </Box>
  )

}