import React from 'react'
import './MeCircularProgress.css'
import pic from '../../../mepic.png'
const MeCircularProgress = () => {
    return (
      <div className='loader_main'>
        <div className="loader_img_container">
<img className='loader_img' src={pic} />
      </div>
</div>
    )
}

export default MeCircularProgress