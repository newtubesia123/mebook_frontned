import Avatar from "@mui/material/Avatar";
import React from 'react'

function MebookMetaAvtar({sx}) {
  return (
    <Avatar
    sx={sx}
    src={`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`}
  ></Avatar>
  )
}

export default MebookMetaAvtar
