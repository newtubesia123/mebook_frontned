import React from 'react'
import './ForwardBtn.css'
const ForwardBtn = ({text,style,onClick}) => {
  return (
    <div>
        <button className='glowing-btn' style={style} onClick={onClick}><span className='glowing-txt'>{text}</span></button>
    </div>
  )
}

export default ForwardBtn