import { SERVER } from "../server/server";

export const getImgURL = (name) => name?.startsWith('https')?name:`${SERVER}/uploads/${name}`;