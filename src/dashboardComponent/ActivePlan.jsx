import React, { useEffect } from 'react'
import CheckIcon from '@mui/icons-material/Check';
import './ActivePlan.css';
import { EplanData } from '../Component new/User Dashboard/sideBarItem/SideBar';
import { useDispatch, useSelector } from "react-redux";
import { callPopUp, userDashboardDetail } from '../Action';

const ActivePlan = () => {

  const STORE = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(userDashboardDetail());
  }, []);

  const paymentDetails = STORE?.userDashboardDetail?.userWorkDetail?.subscriptionType?.isSubscribed;
  // console.log("isSubscribed",paymentDetails)
  const cancelSub=()=>{
    dispatch(callPopUp({type:"CancelSubscription"}))

  }
  return (
    <>
      <section>
        {/* <div class="u-center-text u-margin-bottom-big">
          <h2 class="heading-secondary">
            Premium Plan
          </h2>
        </div> */}
        <div class="pay_row">
          <div class="col-1-of-3">
            <div class="card">
              <div class="card__side card__side--front-2">
                <div class="card__title card__title--2">
                  <h4 class="card__heading">Standard</h4>
                  <h4 class="card__heading__dol"><span>$</span>35/month</h4>
                </div>
                <div class="card__details">
                  <div className='card__details__inner' >
                    {
                      EplanData.map((item, index) => (index <= 10 ?
                        <div key={index} style={{ display: 'flex', margin: '3px 0' }}>
                          <div style={{}}><CheckIcon sx={{ color: 'white', background: 'green', marginRight: '5px', borderRadius: '50%', fontSize: '1.5rem' }} /></div>
                          <h6>{item.title}</h6>
                        </div> : ''
                      ))
                    }
                  </div>
                  <div className='card__details__inner' >
                    {
                      EplanData.map((item, index) => (index > 10 ?
                        <div key={index} style={{ display: 'flex', margin: '3px 0' }}>
                          <div style={{}}><CheckIcon sx={{ color: 'white', background: 'green', marginRight: '5px', borderRadius: '50%', fontSize: '1.5rem' }} /></div>
                          <h6>{item.title}</h6>
                        </div> : ''
                      ))
                    }
                  </div>
                </div>
              </div>
              <div class="card__side card__side--back card__side--back-2">
                <div class="card__cta">
                  <div class="card__price-box">
                    <p class="card__price-only">MeBookMeta launch offer</p>
                    <p class="card__price-only">Enjoy your free trail</p>
                    <p class="card__price-only">{paymentDetails ? "Plan type: Standard" : ""}</p>
                  </div>
                  <div class="u-center-text u-margin-top-huge">
                      <button onClick={cancelSub} class="butn butn--white">Cancel Subscription</button>
                    </div>
                  {/* <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                    <div style={{ marginBottom: '1rem', maxWidth: '', maxHeight: '2rem', display: 'flex', justifyContent: 'space-evenly', flexDirection: 'row', alignItems: 'center' }}>
                    </div>
                  </div> */}
                </div>
              </div>
            </div>
          </div>

        </div>


      </section>


    </>
  )





























  // return (
  //   <div style={{ minHeight: "100vh", minWidth: "100wh" }}>

  //     <Box
  //       sx={{
  //         display: "flex",
  //         alignItems: "center",
  //         justifyContent: "center",
  //         // width: {lg:"30rem", md:"30rem", xs:"20rem"},
  //         // height: {lg:"30rem", md:"30rem"},

  //       }}
  //     >
  //       <div class="ag-courses_box" style={{
  //         width: {
  //           xl: "30rem", lg: "30rem", md: "30rem", sm: "30rem", sm: "25rem"
  //         }
  //       }}>
  //         <div class="ag-courses_item">
  //           <span class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg"></div>

  //             <div class="ag-courses-item_title">
  //               Your MeBookMeta Subscription
  //             </div>
  //             <div class="ag-courses-item_title">
  //               Standard plan <Badge color="secondary" badgeContent={"Active"} sx={{ marginLeft: '1.5rem', }} />
  //             </div>

  //             <div class="ag-courses-item_date-box">
  //               Ending by:
  //               <span class="ag-courses-item_date">
  //                 04.15.2023
  //               </span>
  //             </div>
  //           </span>
  //         </div>

  //         {/* <div class="ag-courses_item">
  //           <a href="#" class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg"></div>

  //             <div class="ag-courses-item_title">
  //               UX/UI Web-Design&#160;+ Mobile Design
  //             </div>

  //             <div class="ag-courses-item_date-box">
  //               Start:
  //               <span class="ag-courses-item_date">
  //                 04.11.2022
  //               </span>
  //             </div>
  //           </a>
  //         </div> */}

  //         {/* <div class="ag-courses_item">
  //           <a href="#" class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg"></div>

  //             <div class="ag-courses-item_title">
  //               Annual package "Product+UX/UI+Graph designer&#160;2022"
  //             </div>

  //             <div class="ag-courses-item_date-box">
  //               Start:
  //               <span class="ag-courses-item_date">
  //                 04.11.2022
  //               </span>
  //             </div>
  //           </a>
  //         </div> */}

  //         {/* <div class="ag-courses_item">
  //           <a href="#" class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg"></div>

  //             <div class="ag-courses-item_title">
  //               Graphic Design
  //             </div>

  //             <div class="ag-courses-item_date-box">
  //               Start:
  //               <span class="ag-courses-item_date">
  //                 04.11.2022
  //               </span>
  //             </div>
  //           </a>
  //         </div> */}

  //         {/* <div class="ag-courses_item">
  //           <a href="#" class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg"></div>

  //             <div class="ag-courses-item_title">
  //               Motion Design
  //             </div>

  //             <div class="ag-courses-item_date-box">
  //               Start:
  //               <span class="ag-courses-item_date">
  //                 30.11.2022
  //               </span>
  //             </div>
  //           </a>
  //         </div> */}

  //         {/* <div class="ag-courses_item">
  //           <a href="#" class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg"></div>

  //             <div class="ag-courses-item_title">
  //               Front-end development&#160;+ jQuery&#160;+ CMS
  //             </div>
  //           </a>
  //         </div> */}

  //         {/* <div class="ag-courses_item">
  //           <a href="#" class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg">
  //             </div>
  //             <div class="ag-courses-item_title">
  //               Digital Marketing
  //             </div>
  //           </a>
  //         </div> */}

  //         {/* <div class="ag-courses_item">
  //           <a href="#" class="ag-courses-item_link">
  //             <div class="ag-courses-item_bg"></div>

  //             <div class="ag-courses-item_title">
  //               Interior Design
  //             </div>

  //             <div class="ag-courses-item_date-box">
  //               Start:
  //               <span class="ag-courses-item_date">
  //                 31.10.2022
  //               </span>
  //             </div>
  //           </a>
  //         </div> */}
  //       </div>
  //     </Box>

  //   </div>
  // )
}

// const NoPlan = () => {
//   return (
//     <div style={{ minHeight: "100vh", minWidth: "100wh" }}>

//       <Box
//         sx={{
//           display: "flex",
//           alignItems: "center",
//           justifyContent: "center",
//           // width: {lg:"30rem", md:"30rem", xs:"20rem"},
//           // height: {lg:"30rem", md:"30rem"},

//         }}
//       >
//         <div class="ag-courses_box" style={{
//           width: {
//             xl: "30rem", lg: "30rem", md: "30rem", sm: "30rem", sm: "25rem"
//           }
//         }}>
//           <div class="ag-courses_item">
//             <span class="ag-courses-item_link">
//               <div class="ag-courses-item_bg"></div>

//               <div class="ag-courses-item_title">
//                 Your MeBookMeta Subscription
//               </div>
//               <div class="ag-courses-item_title">
//                 No Active plan
//               </div>
//             </span>
//           </div>

//           {/* <div class="ag-courses_item">
//             <a href="#" class="ag-courses-item_link">
//               <div class="ag-courses-item_bg"></div>

//               <div class="ag-courses-item_title">
//                 UX/UI Web-Design&#160;+ Mobile Design
//               </div>

//               <div class="ag-courses-item_date-box">
//                 Start:
//                 <span class="ag-courses-item_date">
//                   04.11.2022
//                 </span>
//               </div>
//             </a>
//           </div> */}

//           {/* <div class="ag-courses_item">
//             <a href="#" class="ag-courses-item_link">
//               <div class="ag-courses-item_bg"></div>

//               <div class="ag-courses-item_title">
//                 Annual package "Product+UX/UI+Graph designer&#160;2022"
//               </div>

//               <div class="ag-courses-item_date-box">
//                 Start:
//                 <span class="ag-courses-item_date">
//                   04.11.2022
//                 </span>
//               </div>
//             </a>
//           </div> */}

//           {/* <div class="ag-courses_item">
//             <a href="#" class="ag-courses-item_link">
//               <div class="ag-courses-item_bg"></div>

//               <div class="ag-courses-item_title">
//                 Graphic Design
//               </div>

//               <div class="ag-courses-item_date-box">
//                 Start:
//                 <span class="ag-courses-item_date">
//                   04.11.2022
//                 </span>
//               </div>
//             </a>
//           </div> */}

//           {/* <div class="ag-courses_item">
//             <a href="#" class="ag-courses-item_link">
//               <div class="ag-courses-item_bg"></div>

//               <div class="ag-courses-item_title">
//                 Motion Design
//               </div>

//               <div class="ag-courses-item_date-box">
//                 Start:
//                 <span class="ag-courses-item_date">
//                   30.11.2022
//                 </span>
//               </div>
//             </a>
//           </div> */}

//           {/* <div class="ag-courses_item">
//             <a href="#" class="ag-courses-item_link">
//               <div class="ag-courses-item_bg"></div>

//               <div class="ag-courses-item_title">
//                 Front-end development&#160;+ jQuery&#160;+ CMS
//               </div>
//             </a>
//           </div> */}

//           {/* <div class="ag-courses_item">
//             <a href="#" class="ag-courses-item_link">
//               <div class="ag-courses-item_bg">
//               </div>
//               <div class="ag-courses-item_title">
//                 Digital Marketing
//               </div>
//             </a>
//           </div> */}

//           {/* <div class="ag-courses_item">
//             <a href="#" class="ag-courses-item_link">
//               <div class="ag-courses-item_bg"></div>

//               <div class="ag-courses-item_title">
//                 Interior Design
//               </div>

//               <div class="ag-courses-item_date-box">
//                 Start:
//                 <span class="ag-courses-item_date">
//                   31.10.2022
//                 </span>
//               </div>
//             </a>
//           </div> */}
//         </div>
//       </Box>

//     </div>
//   )
// }

export { ActivePlan }