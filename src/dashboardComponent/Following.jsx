import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Avatar from '@mui/material/Avatar';
import { getImgURL } from '../util/getImgUrl';
import { newButton } from '../Component new/background';
import Button from '@mui/material/Button';
import Cookies from 'universal-cookie';
import { useDispatch, useSelector } from 'react-redux';
import { getFollowers, handleFollow } from '../Action';
import { useNavigate } from 'react-router-dom';
import DefaultImg from '../png/Avatar-Profile-PNG-Photos.png';

function Row(props) {
  const { data } = props;
  const STORE = useSelector((state) => state);
  const [open, setOpen] = React.useState(false);
  const [following, setFollowing] = useState(false)
  const navigate = useNavigate();
  const saveBtn = { ...newButton, width: "8rem", letterSpacing: "1px", fontSize: "80%", height: "2rem" };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getFollowers(data?.follow?._id))
  }, [])
  return (
    <React.Fragment>
      <TableRow sx={{
        '& > *': { borderBottom: 'unset' }, display: "flex", alignItems: "center", justifyContent: "space-around", width: "100%",
        flexDirection: { xs: "column", sm: "row", md: "row", lg: "row" }, gap: { lg: "5rem", md: "5rem", }
      }}>
        <TableCell align="center" sx={{ width: { lg: "10%", md: "10%" }, borderBottom: 'none' }} >
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => navigate(`/MainDashboard/UserProfile/${data?.follow?.name}/${data?.follow?._id}`)}
          >
            {/* {
              data?.follow?.pdf ? <Avatar alt={data?.follow?.name} src={getImgURL(data?.follow?.pdf)} /> : <Avatar alt={data?.follow?.name} src="https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png" />
            } */}

            <Avatar alt={data?.follow?.name} src={data?.follow?.pdf === null ? DefaultImg : getImgURL(data?.follow?.pdf)} />

          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row" sx={{ width: { lg: "80%", md: "80%" }, color: 'white', cursor: 'pointer', display: 'flex', justifyContent: 'center', borderBottom: 'none' }}
          onClick={() => navigate(`/MainDashboard/UserProfile/${data?.follow?.name}/${data?.follow?._id}`)}
        >
          {data?.follow?.name.toUpperCase()}
        </TableCell>
        {/* <TableCell align="center" sx={{ color: 'white', borderBottom: 'none' }}></TableCell> */}



        <TableCell align="center" sx={{ width: { lg: "40%", md: "40%", display: "flex", aligndatas: "center" }, borderBottom: 'none' }}>
          {/* {following ? */}

          <Button sx={saveBtn} size="small"
            onClick={
              () => {
                dispatch(handleFollow(data?.follow?._id))
                setFollowing(!following)
              }
            }
          >
            {!following ? 'unfollow' : 'follow'}

          </Button>
          {/* :
            <Button sx={saveBtn} size="small" onClick={
              () => {
                dispatch(handleFollow(data?.follow?._id, "add"))
                setFollowing(!following)
              }
            }
            >
              Follow
            </Button>

          } */}
        </TableCell>

        <TableCell align="center" sx={{ width: { lg: "40%", md: "40%", display: "flex", aligndatas: "center" }, borderBottom: 'none' }}>


          <Button sx={saveBtn} size="small"
            onClick={() => navigate(`/MainDashboard/UserProfile/${data?.follow?.name}/${data?.follow?._id}`)}
          >
            SEE PROFILE
          </Button>




        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                History
              </Typography>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

const Following = ({ data }) => {
  console.log("Following data", data)
  return (
    <Box sx={{
      color: 'white', background: '#181823', marginTop: '5px', padding: '2rem', display: 'flex', flexDirection: 'column', aligndatas: 'center', height: 450, minWidth: "65vw"
      // Width: {
      //   xs: "100px", sm: "200px", md: "800px", lg: "800px"
      // }
    }}>
      <TableContainer component={Paper} sx={{ background: '#181823', width: '100%' }}>
        <Table aria-label="collapsible table" >
          <TableBody>
            {data?.length > 0 ?
              data?.map((data, index) => (
                <Row key={index} data={data} />
              ))
              :

              <Typography
                variant="h4"
                sx={{
                  textAlign: "center",
                  color: "white",
                  textTransform: "uppercase",
                  marginBottom: "1.5rem",
                }}
              >
                <b>
                  No Followings
                </b>
              </Typography>
            }
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  )
}

export default Following