let Data = [
    {
        question:'What is a chipset?',
        answer:'A chipset is a collection of circuit boards and other components that are used to connect a computer to the rest of the world. Chipsets are typically used to connect computers to the internet, printers, scanners, and other peripherals.',
        img:'https://assets.thehansindia.com/h-upload/2019/11/24/238826-beautiful-scene.jpg'
    },

    {
        question:'What is an operating system?',
        answer:'An operating system is a collection of software that runs on a computer and provides the basic functions of storing and managing information, such as memory, storage devices, and network access. ',
        img:'https://assets.thehansindia.com/h-upload/2019/11/24/238826-beautiful-scene.jpg'
    },
    {
        question:' How many popular operating systems are in use today?',
        answer:'Operating systems are the software that controls the way a computer works. There are many different operating systems out there, and they all have different features and benefits.',
        img:'https://assets.thehansindia.com/h-upload/2019/11/24/238826-beautiful-scene.jpg'
    },
]
export default Data;