import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Avatar from '@mui/material/Avatar';
import { useDispatch, useSelector } from 'react-redux';
import { getDate } from '../assets/dateFun';
import { userWorkDetail } from '../Action';
import { CircularProgress } from '@mui/material';
import { circularProgressbarStyle } from '../assets/common/theme';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import { newButton } from '../Component new/background';
import { getImgURL } from '../assets/getImage';
import axios from 'axios';
import './WorkLikes.css';
import { SERVER } from '../server/server';
import DefaultImg from '../png/Avatar-Profile-PNG-Photos.png';


const saveBtn = { ...newButton, width: "8rem", letterSpacing: "1px" };

function createData(name, calories, fat, carbs, protein, price) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    price,
    history: [
      {
        date: '2020-01-05',
        customerId: '11091700',
        amount: 3,
      },
      {
        date: '2020-01-02',
        customerId: 'Anonymous',
        amount: 1,
      },
    ],
  };
}
export const LikeRow = ({ props }) => {
  const [profile, setProfile] = useState([])
  const navigate = useNavigate();

  useEffect(() => {

    axios.get(`${SERVER}/users/getProfileDetails/${props}`)
      .then((res) => {
        // console.log('liked user listing', res);
        setProfile(res?.data?.meProfile)
      })
      .catch((err) => console.log('error', err));

  }, [])
  console.log('everyprofilelllelle', profile)


  return (

    <>
      {
        profile?.userId ?



          <TableRow>
            <TableCell component="th" scope="row" align='left' sx={{ borderBottom: 'none' }}>
              <IconButton
                aria-label="expand row"
                size="small"
                onClick={() => navigate(`/MainDashboard/UserProfile/${profile?.userId?.name}/${profile?.userId?._id}`)}
              >
                {/* {
                  profile?.userId?.pdf ?
                    <Avatar alt={profile?.userId?.name} src={getImgURL(profile?.userId?.pdf)} />
                    :
                    <Avatar alt={profile?.userId?.name} src="https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png" />
                } */}

                <Avatar alt={profile?.userId?.name} src={profile?.userId?.pdf === null ? DefaultImg : profile?.userId?.pdf} />

              </IconButton>
            </TableCell>
            <TableCell align='left' sx={{ color: 'white', borderBottom: 'none',cursor:'pointer' }}
             onClick={() => navigate(`/MainDashboard/UserProfile/${profile?.userId?.name}/${profile?.userId?._id}`)}
            >{profile?.userId?.name}</TableCell>
            <TableCell align="left" sx={{ color: 'white', borderBottom: 'none' }}>{profile?.category}</TableCell>

          </TableRow>
          :
          <CircularProgress sx={circularProgressbarStyle} />

      }
    </>
  )
}



function Row(props) {
  const { row } = props;
  console.log("row", row);
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();
  const saveBtn = { ...newButton, width: "8rem", letterSpacing: "1px" };


  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell align="left" sx={{ width: '15%', borderBottom: 'none' }}>
          <IconButton
            aria-label="expand row"
            size="large"
            sx={{ width: '100%' }}
          >
            <img src={row?.coverPicture} style={{ width: '100%' }} />
          </IconButton>
        </TableCell>
        <TableCell align="center" component="th" scope="row" sx={{ color: 'white', borderBottom: 'none' }}>
          {row?.title}
        </TableCell>
        <TableCell align="center" sx={{ color: 'white', borderBottom: 'none' }}>{row?.liked}</TableCell>
        <TableCell align="center" sx={{ color: 'white', borderBottom: 'none' }}>{getDate(row?.createdAt)}</TableCell>
        {/* <TableCell align="right">{row.carbs}</TableCell>
          <TableCell align="right">{row.protein}</TableCell> */}
        <TableCell align="center" sx={{ borderBottom: 'none' }}>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon sx={{ color: 'white' }} /> : <KeyboardArrowDownIcon sx={{ color: 'white' }} />}
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>

              <Button sx={saveBtn}
                onClick={() => {
                  navigate('/MainDashboard/createStory/Edit', { state: { workId: row?._id } })
                  // , { state: { workId: work._id } }

                }}
              >
                Edit Work
              </Button>

              <Button sx={saveBtn}
                onClick={() => {
                  navigate(`/MainDashboard/WorkView/${row?.userId?._id}/${row?._id}`)


                }}
              >
                View Work
              </Button>



            </Box>
            <Table size="small" aria-label="purchases">

              {/* <TableHead>
                <TableRow>
                  <TableCell>Date</TableCell>
                  <TableCell>Customer</TableCell>
                  <TableCell align="right">Amount</TableCell>
                  <TableCell align="right">Total price ($)</TableCell>
                </TableRow>
              </TableHead> */}
              <TableBody>
                {/* {row.history.map((historyRow) => (
                      <TableRow key={historyRow.date}>
                        <TableCell component="th" scope="row">
                          {historyRow.date}
                        </TableCell>
                        <TableCell>{historyRow.customerId}</TableCell>
                        <TableCell align="right">{historyRow.amount}</TableCell>
                        <TableCell align="right">
                          {Math.round(historyRow.amount * row.price * 100) / 100}
                        </TableCell>
                      </TableRow>
                    ))} */}


                {
                  row?.likedBy?.length > 0 ?
                    row?.likedBy?.map((item, index) => (
                      <LikeRow key={index} props={item} />
                    ))
                    :

                    <TableRow >
                      <TableCell component="" scope="row" sx={{ color: 'white', borderBottom: 'none' }}>
                        <h6>None Of The Users Liked Your Work Till Now...</h6>
                      </TableCell>

                    </TableRow>



                }

              </TableBody>
            </Table>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

// Row.propTypes = {
//   row: PropTypes.shape({
//     calories: PropTypes.number.isRequired,
//     carbs: PropTypes.number.isRequired,
//     fat: PropTypes.number.isRequired,
//     history: PropTypes.arrayOf(
//       PropTypes.shape({
//         amount: PropTypes.number.isRequired,
//         customerId: PropTypes.string.isRequired,
//         date: PropTypes.string.isRequired,
//       }),
//     ).isRequired,
//     name: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired,
//     protein: PropTypes.number.isRequired,
//   }).isRequired,
// };

//   const rows = [
//     createData('Don Quixote',21, 6.0, 24, 4.0, 3.99),
//     createData('Lord of the Rings',32, 9.0, 37, 4.3, 4.99),
//     createData('Lord of the Rings', 12, 16.0, 24, 6.0, 3.79),
//     createData('Lord of the Rings', 23, 3.7, 67, 4.3, 2.5),
//     createData('Lord of the Rings', 1, 16.0, 49, 3.9, 1.5),
//     createData('Lord of the Rings', 35, 16.0, 49, 3.9, 1.5),

//   ];




const WorkLikes = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    dispatch(userWorkDetail());
  }, []);

  const STORE = useSelector((state) => state);
  // console.log("for work", STORE)
  return (
    <Box sx={{ color: 'white', background: '#181823', marginTop: '5px', padding: '2px', display: 'flex', flexDirection: 'column', alignItems: 'center', height: 450, minWidth: "65vw", }}>
      {
        !STORE.userWorkDetail?.userWorkDetail ?
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "100%",
              height: "18em",
            }}
          >
            <CircularProgress sx={circularProgressbarStyle} />
          </Box>

          :


          <TableContainer component={Paper} sx={{ background: '#181823' }}>
            <Table aria-label="collapsible table">
              <TableHead>
                <TableRow>
                  <TableCell align="center" sx={{ color: 'white' }}>Cover</TableCell>

                  <TableCell align="center" sx={{ color: 'white' }}>Title</TableCell>
                  <TableCell align="center" sx={{ color: 'white' }}>Likes</TableCell>
                  <TableCell align="center" sx={{ color: 'white' }}>Date</TableCell>
                  <TableCell align="center" sx={{ color: 'white' }}>Edit / View</TableCell>
                  {/* <TableCell align="right">Protein&nbsp;(g)</TableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {STORE.userWorkDetail?.userWorkDetail?.message.map((item, index) => (
                  <Row key={index} row={item} />
                ))}
              </TableBody>
            </Table>
          </TableContainer>

      }
    </Box>
  )
}


export default WorkLikes