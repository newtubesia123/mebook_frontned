import { Accordion, AccordionDetails, AccordionSummary, Box, CircularProgress, Grid, Typography } from '@mui/material';
import React from 'react'
import { useSelector } from 'react-redux'
import './Fingerprint.css'
import pic from '../mepic.png'
import { circularProgressbarStyle } from '../assets/common/theme';
import { BackGround, newButton } from '../Component new/background';
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import MeLogo from '../assets/MeLogoMain';

const Fingerprint = () => {

  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };
  const STORE = useSelector((state) => state);
  const saveBtn = { ...newButton, width: "auto", letterSpacing: "1px" };
  const navigate = useNavigate();
  const AccordianBackground = {
    ...BackGround,
    color: "white",
    margin: "10px 0 10px 0",
    border: "1px solid white",
  };
  return (
    <div className='FingerprintMain' >
    <Box sx={{ color: 'white', background: '#181823',padding:'2rem', display: 'flex', flexDirection: 'column', alignItems: 'center', minHeight: "100vh", minWidth: "65vw" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          width: '50%'

        }}
      >
        {/* <img src={pic} alt="mePic" style={{ width: '20%' }} /> */}
        <MeLogo/>

      </div>
      <Box >

        <Grid sx={{}} container spacing={1}>
          {STORE.userProfile.loading ? (
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                width: "100%",
                height: "18em",
              }}
            >
              <CircularProgress sx={circularProgressbarStyle} />
            </Box>
          ) : (
            <Grid item md={12}>
              <Typography
                variant="h4"
                sx={{
                  textAlign: "center",
                  color: "white",
                  textTransform: "uppercase",
                  marginBottom: "1.5rem", fontSize:"150%"
                }}
              >
                <b>
                  Hey, {" "}

                  {STORE.getProfileData?.userData?.name}
                </b>
              </Typography>

              <Typography
                sx={{
                  textAlign: "start",
                  color: "whitesmoke",
                  margin: "10px 0 10px 0",
                  fontWeight: "bold",
                  fontSize: "100%",
                }}
                variant="span"
              >
                YOUR MEBOOKMETA FINGERPRINT:&nbsp;
              </Typography>
              <span style={{ color: "whitesmoke", fontSize: "100%" }}>
                {STORE.userProfile.userProfile?.meIdentity?.position},{" "}
                {STORE.userProfile.userProfile?.meIdentity?.status},{" "}
                {STORE.userProfile.userProfile?.meProfile?.category}
              </span>
              <Typography
                sx={{
                  textAlign: "start",
                  color: "white",
                  fontSize: "0.9rem",
                  margin: "10px 0 10px 0",
                }}
                variant="h6"
              >
                <span style={{ fontWeight: "bold" }}>
                  {" "}
                  YOUR MEBOOKMETA FINGERPRINT TAGS: &nbsp;
                </span>
                <span>
                  {STORE.userProfile.userProfile?.meProfile?.subCategory &&
                    STORE.userProfile.userProfile?.meProfile?.subCategory
                      .length ? (
                    STORE.userProfile.userProfile.meProfile.subCategory.map(
                      (item, i) => (
                        <span key={i} style={{ marginRight: "0.1rem" }}>
                          {item.name}
                          {STORE.userProfile.userProfile.meProfile.subCategory
                            .length -
                            1 ===
                            i
                            ? ""
                            : ", "}
                        </span>
                      )
                    )
                  ) : (
                    <span style={{ display: "none" }}></span>
                  )}
                </span>
              </Typography>
              <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button sx={{...saveBtn, fontSize:"60%"}}
                  onClick={() => {
                    // navigate(`CreatorNew2/${STORE.userProfile?.userProfile?.meProfile?.category}`)
                    navigate(`${STORE.getProfileData?.userData?.name}/${STORE.getProfileData?.userData?._id}/CreatorNew2/${STORE.userProfile?.userProfile?.meProfile?.category}`)


                  }}
                >
                  ADD FINGERPRINT TAGS
                </Button>
              </Box>

              {/* <List>
                <ListItemButton onClick={handleClick}>
                  <ListItemText primary="Your Dashboard Page provides a visual display of your
                  complete profile and data at-a-glance. It will help you
                  to..." />
                  {open ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={open} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    <ListItemButton >

                      <ListItemText primary="1) Identify historical trends, establish targets,
                        predict outcomes, and/or discover insights.
                        
                        2) Monitor, measure and manage processes in real time.
                     
                        3) Track key performance indicators (KPIs) and progress
                        toward established targets." />
                    </ListItemButton>
                  </List>
                </Collapse>
              </List> */}
              
              <Accordion  sx={AccordianBackground}>
                <AccordionSummary
                  sx={{ color: "white", fontSize:"75%" }}
                  expandIcon={<ExpandMoreIcon sx={{ color: "white" }} />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography className='FingerprintAccordion'>
                    Your Dashboard Page provides a visual display of your
                    complete profile and data at-a-glance. It will help you
                    to: click here to see more
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography variant="h6" className='FingerprintAccordion_des'>
                    1) Identify historical trends, establish targets,
                    predict outcomes, and/or discover insights.
                    <br />
                    2) Monitor, measure and manage processes in real time.
                    <br />
                    3) Track key performance indicators (KPIs) and progress
                    toward established targets.
                  </Typography>
                </AccordionDetails>
              </Accordion>
              



            </Grid>
          )}
        </Grid>
      </Box>
    </Box>
    </div>
  )
}

export default Fingerprint