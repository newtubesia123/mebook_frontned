import { Box, Tooltip } from '@mui/material'
import React, { useRef } from 'react'
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

const ShareId = () => {
    const STORE = useSelector((state) => state)
    const affiliateKey = STORE?.userDashboardDetail?.userWorkDetail?.affiliateCode
    const textInput = useRef(null);

    const copyToClipboard = async () => {
        const text = textInput.current.value;
        try {
            await navigator.clipboard.writeText(text);
            toast.success('Copied To Clipboard', { position: 'top-right', autoClose: 2000, pauseOnHover: false })
            //   console.log('Text copied to clipboard:', text);
        } catch (error) {
            toast.error('Failed to copy text', { position: 'top-right', autoClose: 2000, pauseOnHover: false })
            //   console.error('Failed to copy text:', error);
        }
    };
    return (
        <div className='FingerprintMain' >
            <Box sx={{ color: 'white', background: '#181823', padding: '2rem', display: 'flex', flexDirection: 'column', alignItems: 'center', minHeight: "90vh", minWidth: "65vw" }}>
                <h4>Your MeBookMeta Unique Id</h4>
                <input type="text" ref={textInput} defaultValue={affiliateKey} readOnly
                    style={{
                        background: 'none', outline: 'none', border: 'none', textAlign: 'center', fontSize: '3rem', color: 'green',
                    }} />
                                    <Tooltip title='Copy To Clipboard'>

                <button onClick={copyToClipboard} className="profile-card__button button--blue js-message-btn">
                    copy
                    <ContentCopyIcon sx={{ marginLeft: '1rem' }} />
                </button>
                </Tooltip>
            </Box>
        </div>
    )
}

export default ShareId