import React from 'react'
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Avatar from '@mui/material/Avatar';
import { getImgURL } from '../util/getImgUrl';
import { useNavigate } from 'react-router-dom';
import Button from '@mui/material/Button';
import { newButton } from '../Component new/background';
import DefaultImg from '../png/Avatar-Profile-PNG-Photos.png';

function Row(props) {
  const { data } = props;
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();
  const saveBtn = { ...newButton, width: "8rem", letterSpacing: "1px", fontSize: "80%", height: '2rem' };

  // const profileImageSrc = data?.userId?.pdf ? getImgURL(data?.userId?.pdf) : "https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png";

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' }, display: "flex", alignItems: "center", flexDirection: "row", gap: { lg: "10rem", md: "5rem" }, width: "100%" }}>
        <TableCell align="left" sx={{ width: { sm: '35%', lg: "25%", md: "25%" }, borderBottom: 'none' }}>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => navigate(`/MainDashboard/UserProfile/${data?.userId?.name}/${data?.userId?._id}`)}
          >
            <Avatar alt={data?.userId?.name} src={data?.userId?.pdf === null ? DefaultImg : getImgURL(data?.userId?.pdf)} />
          </IconButton>
        </TableCell>
        <TableCell align="center" sx={{ width: { lg: "50%", md: "50%" }, color: 'white', cursor: 'pointer', borderBottom: 'none' }}
          onClick={() => navigate(`/MainDashboard/UserProfile/${data?.userId?.name}/${data?.userId?._id}`)}
        >
          {data?.userId?.name.toUpperCase()}
        </TableCell>
        <TableCell align="center" sx={{ borderBottom: 'none' }}></TableCell>

        <TableCell align="center" sx={{ width: { lg: "40%", md: "40%", display: "flex", aligndatas: "center", borderBottom: 'none' } }}>


          <Button sx={saveBtn} size="small"
            onClick={() => navigate(`/MainDashboard/UserProfile/${data?.userId?.name}/${data?.userId?._id}`)}
          >
            SEE PROFILE
          </Button>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                History
              </Typography>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}







const Followers = ({ data }) => {
  console.log("Followers data", data);
  return (
    <Box sx={{
      color: 'white', background: '#181823', marginTop: '5px', padding: '2rem', display: 'flex', flexDirection: 'column', aligndatas: 'center', height: 450, minWidth: "100%",
      // Width: {
      //   xs: "80px", sm: "200px", md: 800, lg: 800
      // }
    }}>


      <TableContainer component={Paper} sx={{ background: '#181823' }}>
        <Table aria-label="collapsible table" >
          <TableBody>
            {data?.length > 0 ?
              data?.map((data, index) => (
                <Row key={index} data={data} />
              ))
              :
              <Typography
                variant="h4"
                sx={{
                  textAlign: "center",
                  color: "white",
                  textTransform: "uppercase",
                  marginBottom: "1.5rem",
                }}
              >
                <b>
                  No Followers
                </b>
              </Typography>
            }
          </TableBody>
        </Table>
      </TableContainer>


    </Box>
  )
}

export default Followers