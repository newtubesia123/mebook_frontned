import React, { useState } from 'react'
import imgs from "../mepic.png";
import Data from './FaqsMap'
import './Faqs.css'

import Typography from '@mui/material/Typography';
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import { BackGround } from "../Component new/background";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MeLogo from '../assets/MeLogoMain';


export default function Faqs() {
  const [search,setSearch]=useState('')
  const AccordianBackground = {
    ...BackGround,
    color: "white",
    margin: "10px 0 10px 0",
    border: "1px solid white",
  };
  return (
    <div className='text-white FaqsCss'>
       <div className='container'>
           <span className='d-flex justify-content-center'>
              {/* <img className='my-2' src={imgs} alt="/"  style={{width:'130px'}}/>   */}
              <MeLogo/>
           </span>

           <h3 className='text-center'><b>Frequently Asked Questions</b></h3>
           <hr style={{backgroundColor:'white'}} width='30%'  className='mx-auto'/>

           <div className='row d-flex justify-content-center my-3 '>
           <div className="input-group mb-3 col-12 col-sm-8 px-3 bg-white col-md-6 col-lg-4" style={{border:'1px solid ',borderRadius:'20px'}}>
             <input type="text" style={{border:'0px'}} onChange={(e)=>setSearch(e.target.value)} className="form-control shadow-none" placeholder="Ask Questions" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
             <div className="input-group-append Search-Group">
                 <span className=" text-dark shadow-none mx-auto my-auto" style={{border:'0px'}}>
                 <svg xmlns="http://www.w3.org/2000/svg" width="40" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                   <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                 </svg>
                 </span>
             </div>
             </div>
           </div>


           <div className='row d-flex justify-content-center'>
           <div className='col-12 col-sm-12 col-md-10 col-lg-10'>
            
            
             {Data.filter((user)=>user.question.toLowerCase().includes(search)).map((e,i)=>{
                return(
                  <div key={i}>
                  <Accordion sx={AccordianBackground} >
                  <AccordionSummary className='bgchange'
                    sx={{ color: "white" }}
                    expandIcon={<ExpandMoreIcon sx={{ color: "white" }} />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography className='FingerprintAccordion' sx={{fontSize:"90%"}}>
                      {e.question}
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails className='bgchange'>
                  <Typography className='FingerprintAccordion' sx={{fontSize:"90%"}}>
                      {e.answer}
                      <img className='mt-2' src={e.img} alt="/" width='100%'/>
                    </Typography>
                  </AccordionDetails>
                </Accordion>
                  </div>
                )
              })
            }
           </div>
           </div>

           
           
           

           



    </div>
    </div>
  )
}
