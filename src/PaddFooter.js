import React from 'react'
import { NavLink, useNavigate } from 'react-router-dom'

export default function PaddFooter() {
  const navigate = useNavigate();
  const onClickHandle = (num) => {
    const category = { 7: 'Author', 8: "Writer", 9: 'Music', 10: 'Audio', 11: 'Music Video', 12: 'Film', 13: 'Television' }
    navigate(`Search/${num}/${category[num]}`)
  }
  const itemStyle = { cursor: 'pointer' }
  return (
    <div>
      <div className='mx-auto pt-3' style={{marginTop:"21px"}} >
        <span className='row' style={{ fontSize: '13px' }}>
          {/* <div className="col-6 col-sm-3 col-md-3">
          <NavLink><p className='text-light'>Paid Stories</p></NavLink>
        </div> */}

          {/* <div className="col-6 col-sm-3 col-md-3 ">
          <NavLink><p className='text-light'>Get the App</p></NavLink>
        </div> */}
          {/* <div className="col-6 col-sm-3 col-md-3 ">
          <NavLink><p className='text-light'>Language</p></NavLink>
        </div> */}
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} onClick={(e) => onClickHandle(7)}>
            <p className='text-light'>Author</p>
          </div>
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} onClick={() => onClickHandle(8)}>
            <NavLink><p className='text-light'>Writer</p></NavLink>
          </div>
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} onClick={() => onClickHandle(9)}>
            <NavLink><p className='text-light'>Music</p></NavLink>
          </div>
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} onClick={() => onClickHandle(10)}>
            <NavLink><p className='text-light'>Audio</p></NavLink>
          </div>
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} onClick={() => onClickHandle(11)}>
            <NavLink><p className='text-light'>Music Video</p></NavLink>
          </div>
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} onClick={() => onClickHandle(12)}>
            <NavLink><p className='text-light'>Film</p></NavLink>
          </div>
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} onClick={() => onClickHandle(13)}>
            <NavLink><p className='text-light'>Television</p></NavLink>
          </div>
          <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} >
            <p className='text-light'> <a style={{ color: "inherit" }} href="/privacypolicy">Terms & Conditions</a></p>
          </div>
          {/* <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} >
            <NavLink><p className='text-light'>Try Premium</p></NavLink>
          </div> */}
        </span>

        <span className='row' style={{ fontSize: '13px' }}>
          {/* <div className="col-6 col-sm-6 col-md-12 " style={itemStyle} >
            <p className='text-light'> <a style={{ color:"inherit" }} href="/privacypolicy">Terms & Conditions</a></p>
          </div> */}
          {/* <div className="col-6 col-sm-3 col-md-3 " style={itemStyle}>
            <p className='text-light'>Privacy</p>
          </div> */}
          {/* <div className="col-6 col-sm-3 col-md-3 " style={itemStyle} >
            <NavLink><p className='text-light'>Accessibility</p></NavLink>
          </div> */}

        </span>

      </div >
      <p style={{ fontSize: '15px' }} className='text-center me-5 my-2'>© 2023 MeBookMeta</p>
      <p style={{ fontSize: '15px' }} className='text-center'>Maintained by Belforbes Communication</p>
    </div>
  )
}
