import React, { useState, createContext, useEffect } from "react";
// import { useNavigate } from "react-router-dom";

import "./App.css";
import { Routes, Route } from "react-router-dom";
import Page1 from "./component/Page1";
import ProfileDetails from "./component/ProfileDetails";
import ConfirmMail from "./component/ConfirmMail";
import AuthorProfile from "./component/ProfileAuthor";
import TelevisionProfile from "./component/ProfileTelevision";
import CreatorWriter from "./component/CreatorWriter";
import CreatorMusic from "./component/CreatorMusic";
import CreatorVisualGraphic from "./component/CreatorVisualGraphics";
import MainStage from "./UnusedFile/MainStage.jsx";
import CuttingEdge from "./UnusedFile/CuttingEdge";
import AtLarge from "./UnusedFile/AtLarge";
import Identity from "./component/Identity";
import Private from "./component/Private";
import CreatorNew from "./component/CreatorNew";
import CreatorNew1 from "./component/CreatorNew1";
import New from "./Component new/New";
import Signup from "./Component new/Signup";
import UserInfo from "./Component new/User Dashboard/UserInfo";
import SignIn from "./Component new/SignIn";
import NewSignInPage from "./Component new/NewSigninPage/NewSignInPage";
import UserDashboard from "./Component new/User Dashboard/UserDashboard";
import DetailSaved from "./Component new/DetailSaved";
import CompletionPhase from "./Component new/CompletionPhase";
import CompletionIdentity from "./Component new/CompletionIdentity";
import ProfileSubscription from "./Component new/ProfileSubscription";
import PaymentPage from "./Component new/PaymentPage";
import UserProfile from "./Component new/User Dashboard/UserProfile";
import Crousel from "./Component new/CrouselPage";
// import MyPitch from "./Component new/User Dashboard/MyPitch";
import UserChoice from "./Component new/UserChoice";
import MultipleOptions from "./UnusedFile/MultipleOptions";
import WritePitch from "./Component new/WritePitch";
import WriterStoryDetails from "./Component new/WriterStoryDetails";
import WriterPublishPage from "./Component new/WriterPublishPage";
import WriterNav from "./Component new/WriterNav";
import WriterStory from "./Component new/WriterStory";
import BenefitsOfMebook from "./Component new/BenefitsOfMebook";
import Educator from "./Component new/Reader/Educator";
import ProjectDeveloper from "./Component new/Reader/ProjectDeverloper";
import ReaderReviewer from "./Component new/Reader/ReaderReviewer";
import RetailStore from "./Component new/Reader/RetailStore";
import Buyer from "./Component new/Reader/Buyer";
import ViewPublishedStory from "./Component new/ViewPublishedStory";
import UpdatePublishedStory from "./Component new/UpdatePublishedStory";
import ForgotPassword from "./Component new/ForgotPassword";
import CheckOtp from "./Component new/CheckOtp";
import ResetPassword from "./Component new/ResetPassword";
import PaymentHtml from "./Component new/PaymentHtml";
import CreatorTheatrical from "./component/CreatorTheatrical";
import PaymentDetails from "./Component new/paymentPage/PaymentDetails";
import BankSelect from "./Component new/paymentPage/BankSelect";
import UserProfileEdit from "./Component new/User Dashboard/UserProfileEdit";
import EmailConfirm from "./EmaiConfirm/EmailConfirm";
import MainDashboard from "./userDashboard/MainDashboard";
import DashboardMain from "./Component new/User Dashboard/DashboardMain/DashboardMain";
import ProfilePage from "./component/ProfilePage";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ErrorPage from "./component/ErrorPage";
// import WorkCard from "./Component new/userWork/WorkCard";
import MyWork from "./Component new/userWork/MyWork";
import Faqs from "./dashboardComponent/Faqs";
import WorkView from "./Component new/userWork/WorkView";
import DashboardCard from "./Component new/User Dashboard/userDashboardItem/DashboardCard";
import WorkViewTop5 from "./Component new/userWork/WorkViewTop5";
import Feedback from "./Component new/feedBack/Feedback";
import AllWork from "./Component new/userWork/AllWork";
// import AllWorkCard from "./Component new/userWork/AllWorkCard";
// import Dropdown from "./Component new/staticDropdown";
// import { Stream } from "@mui/icons-material";
import "../node_modules/slick-carousel/slick/slick.css";
import "../node_modules/slick-carousel/slick/slick-theme.css";
import Copyrights from "./Component new/Copyrights";
import GlobalProgress from "./assets/GlobalProgress";
import Support from "./Component new/Support Engine/Support";
import PitchInfo from "./Component new/User Dashboard/Pitch/PitchInfo";
import PrivateCross from "./component/PrivateCross";
import AboutMe from "./pages/aboutMe/AboutMe";
import NewPayPage from "./Component new/paymentPage/NewPayPage";
import ProfileContainer from "./component/navSearch/ProfileContainer";
import ChatBox from "./Component new/ChatBox/ChatBox";
import Padd from "../src/Padd";
import MyAsk from "./pages/myAsk/MyAsk";
import Opportunities from "./pages/Opportunities/Opportunities";
import NewSigUpPage from "./Component new/NewSignUpPage/NewSignUpPage";
import PrivacyPolicy from "./pages/PrivacyPolicy/PrivacyPolicy";
import VideoCarousel from "./Component new/VideoCrousel/VideoCrousel";
import PaymentSuccess from "./Component new/PaymentSuccess/PaymentSuccess";
import MeProfilePage from "./pages/ProfilePageUi/MeProfilePage";
import Messenger1 from "./component/NewChat/Messenger1";
import { io } from "socket.io-client";
import { SERVER } from "./server/server";

export const SOCKET = io(SERVER);

function App() {
  return (
      <div className="App">
        <ChatBox />
        {/* <Support /> */}
        <ToastContainer limit={1} />
        <GlobalProgress />
        <Routes>
          <Route element={<PrivateCross />}>
            <Route path="/" element={<Padd />} />
            <Route path="New" element={<New />} />
            <Route path="BenefitsOfMebook" element={<BenefitsOfMebook />} />
            <Route path="NewSignUpPage" element={<Signup />} />
            <Route path="NewSigninPage" element={<SignIn />} />

            {/* <Route path="/" element={<Page1 />} /> */}
            {/* <Route path="MyAskList" element={<MyAskList/>} /> */}
            <Route path="SignIn" element={<NewSignInPage />} />
            <Route path="Signup" element={<NewSigUpPage />} />
          </Route>

          {/* changes on 15-02-23 */}
          {/* <Route path="CreatorNew/:position" element={<CreatorNew />} />
        <Route path="Identity" element={<Identity />} />
        <Route path="CreatorNew1/subcategory/:category" element={<AuthorProfile />} />
        <Route path="CreatorNew1" element={<CreatorNew1 />} /> */}

          <Route element={<Private />}>
            <Route path="PaymentSuccess" element={<PaymentSuccess />} />
            <Route path="CreatorWriter" element={<CreatorWriter />} />
            <Route path="CreatorMusic" element={<CreatorMusic />} />
            <Route path="CreatorVisualGraphic" element={<CreatorVisualGraphic />}/>
            <Route path="CuttingEdge" element={<CuttingEdge />} />
            <Route path="AtLarge" element={<AtLarge />} />
            <Route path="MainStage" element={<MainStage />} />
            <Route path="Identity" element={<Identity />}/>
            <Route path="ConfirmMail" element={<ConfirmMail />} />
            <Route path="CreatorNew1/subcategory/:category" element={<AuthorProfile />}/>
            <Route path="TelevisionProfile" element={<TelevisionProfile />} />
            {/* New Sequence */}
            <Route path="CreatorNew/:position" element={<CreatorNew />} />
            <Route path="CreatorNew1" element={<CreatorNew1 />} />
            <Route path="UserInfo" element={<UserInfo />} />
            <Route path="ProfileDetails" element={<ProfileDetails />} />
            <Route path="UserDashboard/:username" element={<UserDashboard />} />
            <Route path="DetailSaved" element={<DetailSaved />} />
            <Route path="CompletionPhase" element={<CompletionPhase />} />
            <Route path="NewPayPage" element={<NewPayPage />} />
            {/* Testing for video crousel  */}
            <Route path="VideoCarousel" element={<VideoCarousel />} />

            <Route path="CompletionIdentity" element={<CompletionIdentity />} />
            <Route path="ProfileSubscription" element={<ProfileSubscription />}/>
            <Route path="PaymentPage" element={<PaymentPage />} />
            <Route path="PaymentPage/BankSelect" element={<BankSelect />} />
            <Route path="PaymentPage/PaymentDetails" element={<PaymentDetails />}/>
            <Route path="UserProfile/:username" element={<UserProfile />} />
            <Route path="Crousel" element={<Crousel />} />
            {/* <Route path="MyPitch" element={<MyPitch />} /> */}
            <Route path="UserChoice" element={<UserChoice />} />
            <Route path="MultipleOptions" element={<MultipleOptions />} />
            <Route path="WritePitch" element={<WritePitch />} />
            <Route path="WriterPublishPage" element={<WriterPublishPage />} />
            <Route path="WriterNav" element={<WriterNav />} />
            <Route path="WriterStory" element={<WriterStory />} />

            <Route path="Educator" element={<Educator />} />
            <Route path="ProjectDeveloper" element={<ProjectDeveloper />} />
            <Route path="ReaderReviewer" element={<ReaderReviewer />} />
            <Route path="RetailStore" element={<RetailStore />} />
            <Route path="Buyer" element={<Buyer />} />
            <Route path="Private" element={<Private />} />
            <Route path="ViewPublishedStory" element={<ViewPublishedStory />} />
            <Route path="UpdatePublishedStory" element={<UpdatePublishedStory />}/>
            <Route path="PaymentHtml" element={<PaymentHtml />} />
            <Route path="CreatorTheatrical" element={<CreatorTheatrical />} />
            <Route path="UserProfileEdit" element={<UserProfileEdit />} />

            <Route path="MainDashboard" element={<MainDashboard />}>
              <Route index element={<DashboardMain />} />
              <Route path="NewPayPage" element={<NewPayPage />} />
              {/* <Route path="NewPayPage" element={<Homepage />} /> */}

              <Route path="MyPitch" element={<WritePitch />} />
              <Route path="MyPitch/PitchInfo" element={<PitchInfo />} />
              <Route path="MyAsk" element={<MyAsk />} />
              <Route path="opportunities/:name/:userId" element={<Opportunities />}/>
              <Route path="WriterStoryDetails" element={<MyWork />} />
              <Route path="createStory/:process" element={<WriterStoryDetails />}/>
              <Route path="Copyrights" element={<Copyrights />} />
              {/* <Route
                path="ChatCombiner/:username/:userId"
                element={<ChatCombiner />}
              /> */}
              {/* <Route path="UserProfile/:username/:userId" element={<ProfilePage />} /> */}
              <Route path="Chat" element={<Messenger1 />}/>
              <Route path="UserProfile/:username/:userId" element={<MeProfilePage />}/>
              {/* <Route path="WorkView/:id/:name" element={<WorkView />} /> */}
              <Route path="WriterStoryDetails/:userId" element={<MyWork />} />
              <Route path="WorkView/:userId/:id/" element={<WorkView />} />

              <Route path="WorkViewTop5/:id" element={<WorkViewTop5 />} />
              <Route path="feedback" element={<Feedback />} />
              <Route path="AllWork/:userId" element={<AllWork />} />
              <Route path="AboutMe" element={<AboutMe />} />
              <Route path="Faqs" element={<Faqs />} />
              <Route path="AboutMe/:username/:userId/CreatorNew2/:active" element={<CreatorNew1 />}/>
              <Route path="UserProfile/:username/:userId/CreatorNew2/:active" element={<CreatorNew1 />}/>
              {/* <Route path="MyWork" element={<MyWork />} /> */}
              <Route path="Search/:query/:category" element={<ProfileContainer />}/>
            </Route>
            <Route path="WorkView/:id/:name" element={<WorkView />} />
          </Route>
          <Route path="ForgotPassword" element={<ForgotPassword />} />
          <Route path="CheckOtp" element={<CheckOtp />} />
          <Route path="ResetPassword" element={<ResetPassword />} />
          <Route path="ConfirmMail/:email" element={<ConfirmMail />} />
          <Route path="EmailConfirm/:id" element={<EmailConfirm />} />
          {/* <Route path="Profile/:username/:userId" element={<ProfilePage />} /> */}
          <Route path="Profile/:username/:userId" element={<MeProfilePage />} />

          <Route path="*" element={<New />} />
          <Route path="ErrorPage" element={<ErrorPage />} />
          <Route path="DashboardCard" element={<DashboardCard />} />
          {/* <Route path="WorkView/:id/:name" element={<WorkView />} /> */}
          <Route path="WorkView/:userId/:id/" element={<WorkView />} />
          <Route path="WriterStoryDetails/:userId" element={<MyWork />} />
          <Route path="WriterStoryDetails" element={<MyWork />} />
          <Route
            path="Search/:query/:category"
            element={<ProfileContainer />}
          />
          <Route path="privacypolicy" element={<PrivacyPolicy />} />
        </Routes>
      </div>
  );
}
export default App;

