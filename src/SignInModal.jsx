import React, { useState } from 'react'
import axios from "axios";
import './SignInModal.css'
import { SERVER } from "../src/server/server";
import Button from "@mui/material/Button";
import { newButton } from "./Component new/background";
import { useNavigate, } from 'react-router-dom';
import { toast } from "react-toastify";

export default function SignInModal({ setModalSignin, userId, username, path }) {

  const [signInSuccessMssge, setsignInSuccessMssge] = useState();
  const [showErrMsgePass, setShowErrMsgePass] = useState();
  const [showErrMsgeEmail, setShowErrMsgeEmail] = useState();
  const [input, setInput] = useState({ email: '', password: '' })

  // let { username ,userId } = useParams();
  const navigate = useNavigate();

  let handleChange = async (e) => {
    let { name, value } = e.target;
    setInput({ ...input, [name]: value })
  }

  let handleSubmit = async (e) => {
    const LoginData = {
      email: input.email,
      password: input.password,
    };
    e.preventDefault();
    await axios.post(`${SERVER}/login`, { LoginData: LoginData })
      .then((res) => {
        if (res.data.success) {
          localStorage.setItem('token', JSON.stringify(res.data.token))
          toast.success(res.data.success, { autoClose: 2000, position: 'top-right' })
          setsignInSuccessMssge(res.data.success)

          setTimeout(() => {
            setModalSignin(false)
          }, 1000);
          navigate(`/MainDashboard/${path}/${username}/${userId}`)

        } else {
          setShowErrMsgePass(res.data.error)
          setShowErrMsgeEmail(res.data.Error)
          setTimeout(() => {
            setShowErrMsgePass('')
            setShowErrMsgeEmail('')
          }, 5000);

        }
      })
      .catch((err) => { console.log(err); })
  }


  return (
    <div>
      <div className="header">
        <div className="child">

          <div style={{ display: 'flex', justifyContent: 'end' }}>
            <i onClick={() => setModalSignin(false)} style={{ fontSize: '22px' }} class=" mb-2 fa-solid fa-square-xmark"></i>
          </div>

          <h5 className='modaltext text-center'>SignIn</h5>

          <div className='d-flex justify-content-center mt-3'>
            <form onSubmit={handleSubmit}>
              <input type="email"
                name='email'
                class="form-control"
                placeholder="Enter your Email"
                value={input.email}
                onChange={handleChange}
              /> <br />

              <input
                type="password"
                class="form-control"
                placeholder="Enter your Password "
                name="password"
                value={input.password}
                onChange={handleChange}
              />
              <h6 className='mt-2 text-center text-danger'><b>{showErrMsgePass}</b></h6>
              <h6 className='mt-2 text-center text-danger'><b>{showErrMsgeEmail}</b></h6>
              <h6 className='mt-2 text-center text-danger'><b>{signInSuccessMssge}</b></h6>
              <span className='d-flex justify-content-center'>
                <Button
                  type="submit" variant="contained"
                  sx={{ ...newButton, mt: 2, fontSize: "0.8rem", width: "60%" }}>
                  Sign In
                </Button>
              </span>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}
