import React, { useState,useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link } from "react-router-dom";
import img from "../Screenshot_2.png";
// import "./common.css";

export default function ReaderPara() {
  let user=sessionStorage.getItem('userName')
  let workProfile= localStorage.getItem("Work profile")
  var identity=sessionStorage.getItem('identity Option')

  // console.log("first",JSON.parse(user).name)
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {         
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="container-fluid MebookMetaimage">
      {/* rowSpacing={1}
      // columnSpacing={{ xs: 1, sm: 2, md: 2 }}
      sx={{ margin: "25px 0px" }}
    > */}
      <div className="row">
        <div className="col-md-3"></div>
        <div
          className="col-md-6"
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />
          <Card
            id="card"
            sx={{
              width: "80%",
              height: "200px",
              backgroundColor: "#100892",
              borderRadius: "30px",
              m: "auto",
            }}
          >
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  src={userInfo.filepreview}
                  sx={{
                    width: "150px",
                    height: "130px",

                    m: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "15px 0px",  fontFamily: "Times New Roman"}}
                
              >
                    ({user}, {workProfile}, {identity})
              </Typography>
            </CardContent>
          </Card>
          <Typography className="head"
            gutterBottom
            component="div"
            sx={{
              margin: "auto",
              fontSize: "2.1vw",
              fontWeight: "bold",
              color: "#100892",
              width:'80%'
            
            }}
          >
             Set Up Your <span style={{ color: 'rgb(187 11 11)' }}>M</span><span style={{ color: '#0a0a8a' }}>eBook</span><span style={{ color: '#d40000',  }}>M</span><span style={{ color: '#0a0a8a' }}>eta</span> Profile: Reader/Consumer
          </Typography>
          <Card
            sx={{
              width: "80%",
              minHeight: "420px",
              maxHeight: "2500px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              <div
                style={{
                  backgroundColor: "white",
                  margin: "25px 100px 20px ",
                  borderRadius: "10px",
                }}
              >
                <h3
                  style={{
                    padding: "8px 0px",
                    fontFamily: "Times New Roman"
                  }}
                >
                  Reader/Consumer
                </h3>
              </div>
              <div
                style={{
                  backgroundColor: "white",
                  margin: "25px 30px 20px ",
                  borderRadius: "50px",
                }}
              >
                <p
                  style={{
                    padding: "20px 20px",
                    textAlign: "justify",
                    fontFamily: "Times New Roman"
                  }}
                >
                  <b>This profile is reserved for those who are the
                    most valued group in the Global Media
                    Marketplace for creators, collaborators,
                    influencers, project managers and providers.
                    Reader/Consumers are the are the reason for
                    content and the key to the success of the other
                    “Work” profiles. Reader/Consumers will receive
                    incentives, rewards and discounts for providing
                    reviews and rating work presented on the
                    MeBook app. Various Reader/Consumers will
                    be featured in the site’s online interviews and
                    podcasts. “Cutting Edge” Reader/Consumers
                    will be considered as “Influencers.</b>
                </p>
              </div>
            </CardContent>
          </Card>

          <div className="d-flex justify-content-between">
            <div>
              <Link to={'/CreatorNew'}><button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
              >
                Go Back
              </button></Link>
            </div>
            <div>
              <Link to={'/CreatorNew1'}><button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
              >
                Choose
              </button></Link>
            </div>
          </div>
        </div>
        <div className="col-md-3"></div>
      </div>
    </div>
  );
}



