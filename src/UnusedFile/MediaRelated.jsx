import React from "react";
import Common from "./Common";

export default function MediaRelated() {
  return (
    <div style={{color:"white"}}>
      <Common
        title="Are You a Media Related Creator?"
        para="The Global Media Marketplace is immense… and still growing. This diverse group includes individuals and companies involved in film, television, radio and social media categories. If you are among the visionaries, engineers and builders of most of the audio/visual content that people experience, ranging from all forms of print to watch screens to movie screens, then your work will fall into the Media Related category on this site.
"
        one="Accountant"
        two="Actor"
        three="Lawyer"
        four="Investor"
        five="Event Planner"
      />
    </div>
  );
}
