import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link } from "react-router-dom";
import img from "../Screenshot_2.png";
import WriterNav from "../Component new/WriterNav";
// import "./common.css";

export default function CommonIdentity(props) {
  sessionStorage.setItem("identity Option", props.title);
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://34.244.150.71:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://34.244.150.71:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://34.244.150.71:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="container-fluid MebookMetaimage">
      <WriterNav />
      <div className="row">
        <div className="col-md-3"></div>
        <div
          className="col-md-6"
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />
          <div>
            <Avatar
              alt="Remy Sharp"
              src={userInfo.filepreview}
              sx={{
                width: "150px",
                height: "130px",

                m: "auto",
              }}
            />
          </div>
          {/* <Card
            id="card"
            sx={{
              width: "80%",
              height: "200px",
              backgroundColor: "white",
              borderRadius: "30px",
              m: "auto",
            }}
          >
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  src={userInfo.filepreview}
                  sx={{
                    width: "150px",
                    height: "130px",

                    m: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "15px 0px" }}
              >
                {user}, Identity, {props.title}
              </Typography>
            </CardContent>
          </Card> */}
          <Typography
            className="head"
            gutterBottom
            component="div"
            sx={{
              margin: "10px auto 20px ",
              fontSize: "2.5vw",
              fontWeight: "bold",
              color: "",
              width: "70%",
            }}
          >
            Set Up Your{" "}
            <span
              style={{  }}
            >
              M
            </span>
            <span style={{ }}>eBook</span>
            <span style={{  }}>M</span>
            <span style={{  }}>eta</span> Identity Profile:{" "}
            {props.heading}
          </Typography>
          <div
            style={{
              backgroundColor: "white",
              margin: "25px 100px 20px ",
              borderRadius: "10px",
            }}
          >
            <h3
              style={{
                padding: "8px 0px",
                fontFamily: "Times New Roman",
              }}
            >
              {props.heading}
            </h3>
          </div>
          <CardContent>
            <div
              style={{
                backgroundColor: "white",
                margin: "25px 30px 20px ",
                borderRadius: "50px",
              }}
            >
              <p
                style={{
                  padding: "20px 20px",
                  textAlign: "justify",
                  fontFamily: "Times New Roman",
                }}
              >
                <b> {props.paragraph}</b>
              </p>
            </div>
          </CardContent>

          <div className="d-flex justify-content-between">
            <div>
              <Link to={"/Identity"}>
                <button
                  type="button"
                  class="btn  btn-lg"
                  style={{ background: "white" }}
                >
                  Go Back
                </button>
              </Link>
            </div>
            <div>
              <Link to={"/UserChoice"}>
                {" "}
                <button
                  type="button"
                  class="btn  btn-lg"
                  style={{ background: "white" }}
                >
                  Continue
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-3"></div>
      </div>
    </div>
  );
}
