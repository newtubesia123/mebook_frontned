import React from "react";
import CommonIdentity from "./CommonIdentity";

export default function Organic() {
  return (
    <div>
      <CommonIdentity
        heading="Organic"
        title="Organic"
        paragraph="This identity is reserved for creators, collaborators,
        influencers, providers, project developers and
        reader/consumers whose focus is “authenticism,”
        
        which means that content presents original, un-
        vetted material that might contain errors, coarse
        
        language and descriptions, misleading or false
        claims, profanity, adult or mild sexual content, all
        within site-defined or imposed limitations.
        This identity is purposed to provide a platform for
        
        users whose focus is more message and audience-
        oriented, which often defies mainstream norms,
        
        allowing for greater free expression."
      />
    </div>
  );
}
