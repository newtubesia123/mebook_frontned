import React, { useState, useEffect } from "react";
import axios from "axios";
import image from '../mepic.png'
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link, useNavigate } from "react-router-dom";
import pic from "../Screenshot_2.png";
import CommonAppbar from "../Component new/CommonAppbar";
import { BackGround, newButton } from "../Component new/background";

export default function ReaderProfile() {
  let user = sessionStorage.getItem('userName')
  // const location = useLocation();
  const navigate = useNavigate();

  let workProfile = localStorage.getItem("Work profile")

  const bgBut = {
    ...newButton, width: '80%',
    fontSize: '20px',
    color: 'black',
    margin: "15px 10% 15px ",
    borderRadius: "10px",
    padding: '10px 0px',
    fontFamily: "Times New Roman",
    fontWeight: 'bold'
  }

  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const arry = [
    { option: "Author" },
    { option: "Writer" },
    { option: "Film/Media" },
    { option: "Music/Audio/Sound" },
    { option: "Visual/Graphics" },
    { option: "Performer" },
  ];
  const [btn1, setBtn1] = useState('')
  const handleAuthor = (e) => {
    setBtn1(e.target.value)
    navigate('/ReaderReviewer')
  }
  const [btn2, setBtn2] = useState('')
  const handleWriter = (e) => {
    setBtn2(e.target.value)
    navigate('/RetailStore')
  }
  const [btn3, setBtn3] = useState('')
  const handleFilmMedia = (e) => {
    setBtn3(e.target.value)
    navigate('/Buyer')
  }
  const [btn4, setBtn4] = useState('')
  const handleMusic = (e) => {
    setBtn4(e.target.value)
    navigate('/Educator')

  }
  const [btn5, setBtn5] = useState('')
  const handleVisualGraphics = (e) => {
    setBtn5(e.target.value)
    navigate('/ProjectDeveloper')

  }
  const [btn6, setBtn6] = useState('')
  const handlePerformer = (e) => {
    setBtn6(e.target.value)
    navigate('')

  }
  return (
    <div className="container-fluid" style={BackGround}>
      <CommonAppbar />
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className=" col-md-6 offset-md-3 ">
          <Card
            sx={{
              borderRadius: "30px",
              textAlign: "center",
              width: '80%',
              m: '10px auto'
            }}
          >
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  src={userInfo.filepreview}
                  sx={{
                    width: "150px",
                    height: "130px",
                    margin: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "15px 0px", fontFamily: "Tekton Pro" }}
              >
                {user}, Reader, Consumer
              </Typography>
            </CardContent>
          </Card>
          <div
            style={bgBut}
          >
            <span style={{ color: 'rgb(187 11 11)', fontFamily: "Times New Roman", }}>M</span><span style={{ color: '#0a0a8a', }}>eBook</span><span style={{ color: 'rgb(187 11 11)', }}>M</span><span style={{ color: '#0a0a8a', }}>eta</span> Profile:Reader,Consumer
          </div>
        </div>
      </div>
      <div className="row">
        <div className=" col-md-6 offset-md-3">
          <Card
          >
            <CardContent >
              <button type="button" class="button"
                style={bgBut}
                onClick={handleAuthor}
                value="Author"
              >
                Reader/Reviewer
              </button>
              <button type="button" class="button"
                style={bgBut}
                onClick={handleWriter}
                value="Writer"
              >
                Retail Store/Site
              </button>
              <button type="button" class="button"
                style={bgBut}
                onClick={handleFilmMedia}
                value=" Film/Media"
              >
                Wholesale Buyer
              </button>
              <button type="button" class="button"
                style={bgBut}
                onClick={handleMusic}
                value=" Music/Audio/Sound"
              >
                Educator
              </button>
              <button type="button" class="button"
                style={bgBut}
                onClick={handleVisualGraphics}
                value=" Visual/Graphics"
              >
                Project Developer
              </button>
              <button type="button" class="button"
                style={bgBut}
                onClick={handlePerformer}
                value=" Performer"
              >
                Reader-General
              </button>
            </CardContent>
          </Card>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <Link to={'/UserChoice'}><button
              type="button"

              class="btn btn-primary btn-lg"
              style={newButton}
            >
              Go Back
            </button></Link>
            <Link to={""}>
              {" "}
              <button
                // onClick={handleSubmit}
                type="button"
                class="btn btn-primary btn-lg"
                style={newButton}
              >
                Continue
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
