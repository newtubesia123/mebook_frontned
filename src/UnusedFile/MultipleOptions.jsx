import { color } from '@mui/system';
import React from 'react'
import pic from '../mepic.png';
import { generatePath, Link, useNavigate } from "react-router-dom";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import ListSubheader from '@mui/material/ListSubheader';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import { BackGround, newButton } from '../Component new/background'

export default function MultipleOptions() {
    let option = sessionStorage.getItem('options')
    let userName = JSON.parse(localStorage.getItem('userData'))
    console.log("val", option)
    const styleSheet = {
        fontSize: '25px',
        fontWeight: "bold",
        color:"white"
    }
    const styleSelect = {
        fontSize: '20px',
        fontWeight: "bold"
    }
    const styleMenu = {
        marginLeft: '15px'
    }
    const [select1, setSelect1] = React.useState('');

    const handleChange1 = (event) => {
        setSelect1(event.target.value);
    };
    const [select2, setSelect2] = React.useState('');

    const handleChange2 = (event) => {
        setSelect2(event.target.value);
    };
    const [select3, setSelect3] = React.useState('');

    const handleChange3 = (event) => {
        setSelect3(event.target.value);
    };
    const [select4, setSelect4] = React.useState('');

    const handleChange4 = (event) => {
        setSelect4(event.target.value);
    };
    const [select5, setSelect5] = React.useState('');

    const handleChange5 = (event) => {
        setSelect5(event.target.value);
    };
    const [select6, setSelect6] = React.useState('');

    const handleChange6 = (event) => {
        setSelect6(event.target.value);
    };
    const [select7, setSelect7] = React.useState('');

    const handleChange7 = (event) => {
        setSelect7(event.target.value);
    };
    const [select8, setSelect8] = React.useState('');

    const handleChange8 = (event) => {
        setSelect8(event.target.value);
    };
    return (
        <div className='container-fluid' style={BackGround} >
            <div className="row mx-5" style={styleSheet} >
                <div className="col-md-12 text-center">
                    <img src={pic} width={250} height={150} alt="" />
                </div>
                <div className="col-md-4 "> <p style={{color:"white"}}> Print/Television/Radio/Video </p>
                    <FormControl className='' fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel> */}
                        <Select
                            value={select1}
                            onChange={handleChange1}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },
                                        "& .MuiMenuItem-root.Mui-selected": {
                                            backgroundColor: ""
                                        }
                                    }
                                }
                            }}

                        >
                            <MenuItem value="">
                                <em>Select Options</em>
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢ Ad Agency/Ad Writer</MenuItem>
                            <MenuItem sx={styleSelect} value={2}>➢ Blogger</MenuItem>
                            <MenuItem sx={styleSelect} value={3}>➢ Casting</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢ Copy Writer</MenuItem>
                            <MenuItem sx={styleSelect} value={5}>➢ Commentary</MenuItem>
                            <MenuItem sx={styleSelect} value={6}>➢ Critic</MenuItem>
                            {/* <ListSubheader sx={styleSelect}>➢ Critic</ListSubheader> */}
                            <MenuItem sx={styleMenu} value={7}>➢ Newspaper</MenuItem>
                            <MenuItem sx={styleMenu} value={8}>➢ Magazine</MenuItem>
                            <MenuItem sx={styleMenu} value={9}>➢ Television</MenuItem>
                            <MenuItem sx={styleSelect} value={10}>➢ Director</MenuItem>
                            <MenuItem sx={styleMenu} value={11}>➢ Magaziner</MenuItem>
                            <MenuItem sx={styleMenu} value={12}>➢ Television</MenuItem>
                            <MenuItem sx={styleMenu} value={13}>➢ Video</MenuItem>
                            <MenuItem sx={styleSelect} value={14}>➢ Editor</MenuItem>
                            <MenuItem sx={styleMenu} value={15}>➢ Newspaper</MenuItem>
                            <MenuItem sx={styleMenu} value={16}>➢ Magazine</MenuItem>
                            <MenuItem sx={styleMenu} value={17}>➢ Television</MenuItem>
                            <MenuItem sx={styleMenu} value={18}>➢ Video</MenuItem>
                            <MenuItem sx={styleSelect} value={19}>➢ Expose</MenuItem>
                            <MenuItem sx={styleSelect} value={20}>➢ Ghost Writer</MenuItem>
                            <MenuItem sx={styleSelect} value={21}>➢ Gossip</MenuItem>
                            <MenuItem sx={styleSelect} value={22}>➢ Human Interest/Community</MenuItem>
                            <MenuItem sx={styleSelect} value={23}>➢ Influencer (On-line)</MenuItem>
                            <MenuItem sx={styleSelect} value={24}>➢ Journalist</MenuItem>
                            <MenuItem sx={styleMenu} value={25}>➢ Newspaper</MenuItem>
                            <MenuItem sx={styleMenu} value={26}>➢ Magaziner</MenuItem>
                            <MenuItem sx={styleMenu} value={27}>➢ Television</MenuItem>
                            <MenuItem sx={styleSelect} value={28}>➢ Manual (Instructional)/DIY</MenuItem>
                            <MenuItem sx={styleSelect} value={29}>➢Ombudsman</MenuItem>
                            <MenuItem sx={styleSelect} value={30}>➢ Podcast</MenuItem>
                            <MenuItem sx={styleSelect} value={31}>➢ Public Relations/Publicity</MenuItem>
                            <MenuItem sx={styleSelect} value={32}>➢ Publisher/Producer</MenuItem>
                            <MenuItem sx={styleMenu} value={33}>➢ Newspaper</MenuItem>
                            <MenuItem sx={styleMenu} value={34}>➢ Magazine</MenuItem>
                            <MenuItem sx={styleMenu} value={35}>➢ Radio</MenuItem>
                            <MenuItem sx={styleMenu} value={36}>➢ Television</MenuItem>
                            <MenuItem sx={styleSelect} value={37}>➢ Radio Show</MenuItem>
                            <MenuItem sx={styleSelect} value={38}>➢ Reviewer</MenuItem>
                            <MenuItem sx={styleMenu} value={39}>➢ Newspaper</MenuItem>
                            <MenuItem sx={styleMenu} value={40}>➢ Magazine</MenuItem>
                            <MenuItem sx={styleMenu} value={41}>➢ Television</MenuItem>
                            <MenuItem sx={styleSelect} value={42}>➢ Reporter</MenuItem>
                            <MenuItem sx={styleMenu} value={43}>➢ Newspaper</MenuItem>
                            <MenuItem sx={styleMenu} value={44}>➢ Magazine</MenuItem>
                            <MenuItem sx={styleMenu} value={45}>➢ Radio</MenuItem>
                            <MenuItem sx={styleMenu} value={46}>➢ Television</MenuItem>
                            <MenuItem sx={styleSelect} value={47}>➢ Story Developer</MenuItem>
                            <MenuItem sx={styleSelect} value={48}>➢ Television</MenuItem>
                            <MenuItem sx={styleMenu} value={49}>➢ Actor</MenuItem>
                            <MenuItem sx={styleMenu} value={50}>➢ Adaptation</MenuItem>
                            <MenuItem sx={styleMenu} value={51}>➢ Agent</MenuItem>
                            <MenuItem sx={styleMenu} value={52}>➢ Agent</MenuItem>
                            <MenuItem sx={styleMenu} value={53}>➢ Sound</MenuItem>
                            <MenuItem sx={styleMenu} value={54}>➢ Lighting</MenuItem>
                            <MenuItem sx={styleSelect} value={55}>➢ Television News</MenuItem>
                            <MenuItem sx={styleMenu} value={56}>➢ Anchor</MenuItem>
                            <MenuItem sx={styleMenu} value={57}>➢ Special Feature</MenuItem>
                            <MenuItem sx={styleMenu} value={58}>➢ Sports</MenuItem>
                            <MenuItem sx={styleMenu} value={59}>➢ Weather</MenuItem>
                            <MenuItem sx={styleSelect} value={60}>➢ Voice/Voice Over</MenuItem>


                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-4">  <p> Stage</p>
                    <FormControl fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel> */}

                        <Select
                            value={select2}
                            onChange={handleChange2}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },


                                    }
                                }
                            }}
                        >
                            <MenuItem value="">
                                <em>Select Options</em>
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢ Actor</MenuItem>
                            <MenuItem sx={styleMenu} value={2}>➢ Talent</MenuItem>
                            <MenuItem sx={styleMenu} value={3}>➢ Extra</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢ Agents/Agency</MenuItem>
                            <MenuItem sx={styleSelect} value={5}>➢ Awards/Reviews</MenuItem>
                            <MenuItem sx={styleMenu} value={6}>➢ List Awards</MenuItem>
                            <MenuItem sx={styleMenu} value={7}>➢ List Reviews</MenuItem>

                            <MenuItem sx={styleSelect} value={8}>➢ Casting</MenuItem>
                            <MenuItem sx={styleMenu} value={9}>➢ Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={10}>➢ Director</MenuItem>
                            <MenuItem sx={styleMenu} value={11}>➢ Stage Director</MenuItem>
                            <MenuItem sx={styleMenu} value={12}>➢ Assistant to Director </MenuItem>
                            <MenuItem sx={styleMenu} value={13}>➢ Stage Manager</MenuItem>
                            <MenuItem sx={styleSelect} value={14}>➢ Stage Play Writer</MenuItem>
                            <MenuItem sx={styleMenu} value={15}>➢ Adaptation</MenuItem>
                            <MenuItem sx={styleMenu} value={16}>➢ Original</MenuItem>
                            <MenuItem sx={styleSelect} value={17}>➢ Electric-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={18}>➢ Gaffer</MenuItem>
                            <MenuItem sx={styleMenu} value={19}>➢ Lighting Crew </MenuItem>
                            <MenuItem sx={styleMenu} value={20}>➢ Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={21}>➢  Grip-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={22}>➢ Key Grip</MenuItem>
                            <MenuItem sx={styleMenu} value={23}>➢ Grip Crew</MenuItem>
                            <MenuItem sx={styleMenu} value={24}>➢ Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={25}>➢ Hair and Make-Up</MenuItem>
                            <MenuItem sx={styleMenu} value={26}>➢  Hair Stylist</MenuItem>
                            <MenuItem sx={styleMenu} value={26}>➢ Make-Up Artist</MenuItem>
                            <MenuItem sx={styleSelect} value={27}>➢  Music-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={28}>➢ Artist</MenuItem>
                            <MenuItem sx={styleMenu} value={29}>➢ Composer</MenuItem>
                            <MenuItem sx={styleMenu} value={30}>➢ Group</MenuItem>
                            <MenuItem sx={styleMenu} value={31}>➢ Performer</MenuItem>
                            <MenuItem sx={styleSelect} value={32}>➢ Producer</MenuItem>
                            <MenuItem sx={styleMenu} value={33}>➢ First Assistant Director</MenuItem>
                            <MenuItem sx={styleMenu} value={34}>➢ First Assistant Director</MenuItem>
                            <MenuItem sx={styleMenu} value={35}>➢  Third Assistant Director</MenuItem>
                            <MenuItem sx={styleSelect} value={36}>➢  Producer-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={37}>➢ Investor</MenuItem>
                            <MenuItem sx={styleMenu} value={38}>➢  Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={39}>➢ Production-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={40}>➢ Accounting</MenuItem>
                            <MenuItem sx={styleMenu} value={41}>➢  Executive Producer</MenuItem>
                            <MenuItem sx={styleMenu} value={42}>➢ Finance</MenuItem>
                            <MenuItem sx={styleMenu} value={43}>➢ Legal</MenuItem>
                            <MenuItem sx={styleMenu} value={44}>➢  Line Producer</MenuItem>
                            <MenuItem sx={styleMenu} value={45}>➢ Location</MenuItem>
                            <MenuItem sx={styleMenu} value={46}>➢ Production Assistant</MenuItem>
                            <MenuItem sx={styleSelect} value={47}>➢ Props</MenuItem>
                            <MenuItem sx={styleMenu} value={48}>➢  Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={49}>➢ Public Relations/Publicity</MenuItem>
                            <MenuItem sx={styleSelect} value={50}>➢  Set Design</MenuItem>
                            <MenuItem sx={styleMenu} value={51}>➢  Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={52}>➢ Sound-Related</MenuItem>

                            <MenuItem sx={styleMenu} value={53}>➢ Boom Operator</MenuItem>
                            <MenuItem sx={styleMenu} value={54}>➢  First Assistant</MenuItem>
                            <MenuItem sx={styleMenu} value={55}>➢  Sound Engineer</MenuItem>
                            <MenuItem sx={styleMenu} value={56}>➢  Sound Engineer</MenuItem>
                            <MenuItem sx={styleMenu} value={57}>➢ Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={58}>➢  Special FX</MenuItem>
                            <MenuItem sx={styleMenu} value={59}>➢ Engineered</MenuItem>
                            <MenuItem sx={styleMenu} value={60}>➢ Virtual</MenuItem>
                            <MenuItem sx={styleMenu} value={61}>➢  Stunt-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={62}>➢  Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={63}>➢ Spoken Word or Poetry</MenuItem>
                            <MenuItem sx={styleMenu} value={64}>➢ Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={65}>➢ Wardrobe Department</MenuItem>
                            <MenuItem sx={styleMenu} value={66}>➢ Wardrobe Designer</MenuItem>
                            <MenuItem sx={styleMenu} value={67}>➢ Wardrobe Supervisor</MenuItem>
                            <MenuItem sx={styleMenu} value={68}>➢ Wardrobe </MenuItem>
                            <MenuItem sx={styleMenu} value={69}>➢ Dresser/Costumer</MenuItem>



                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-4"> <p> Film</p>
                    <FormControl fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel> */}

                        <Select
                            value={select3}
                            onChange={handleChange3}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },


                                    }
                                }
                            }}
                        >
                            <MenuItem value="">
                                <em>Select Options</em>
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢ Actor</MenuItem>
                            <MenuItem sx={styleMenu} value={2}>➢ Talent</MenuItem>
                            <MenuItem sx={styleMenu} value={3}>➢ Extra</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢ Agents/Agency</MenuItem>
                            <MenuItem sx={styleSelect} value={5}>➢ Animation</MenuItem>
                            <MenuItem sx={styleMenu} value={6}>➢ Full-Feature</MenuItem>
                            <MenuItem sx={styleMenu} value={7}>➢ Scene</MenuItem>

                            <MenuItem sx={styleSelect} value={8}>➢ Titles</MenuItem>
                            <MenuItem sx={styleSelect} value={9}>➢ Awards/Reviews</MenuItem>
                            <MenuItem sx={styleMenu} value={10}>➢ List Awards</MenuItem>
                            <MenuItem sx={styleMenu} value={11}>➢  List Reviews</MenuItem>

                            <MenuItem sx={styleSelect} value={12}>➢  Camera-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={13}>➢Operatorr</MenuItem>
                            <MenuItem sx={styleMenu} value={14}>➢ Technical </MenuItem>
                            <MenuItem sx={styleMenu} value={15}>➢ Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={16}>➢Casting</MenuItem>
                            <MenuItem sx={styleMenu} value={17}>➢ Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={18}>➢ Director</MenuItem>
                            <MenuItem sx={styleMenu} value={19}>➢  Film Director</MenuItem>
                            <MenuItem sx={styleMenu} value={20}>➢  Director of Photography </MenuItem>
                            <MenuItem sx={styleSelect} value={21}>➢ Distribution</MenuItem>
                            <MenuItem sx={styleMenu} value={22}>➢Local</MenuItem>
                            <MenuItem sx={styleMenu} value={23}>➢Global</MenuItem>

                            <MenuItem sx={styleSelect} value={24}>➢  Editor</MenuItem>
                            <MenuItem sx={styleMenu} value={25}>➢ Select Tags</MenuItem>


                            <MenuItem sx={styleSelect} value={26}>➢Screenwriter</MenuItem>
                            <MenuItem sx={styleMenu} value={27}>➢ Adaptation</MenuItem>
                            <MenuItem sx={styleMenu} value={28}>➢ Original</MenuItem>

                            <MenuItem sx={styleSelect} value={29}>➢ Commentary</MenuItem>
                            <MenuItem sx={styleSelect} value={30}>➢ Critic</MenuItem>
                            <MenuItem sx={styleSelect} value={31}>➢ Documentary</MenuItem>
                            <MenuItem sx={styleSelect} value={32}>➢ Electric-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={33}>➢ Gaffer</MenuItem>
                            <MenuItem sx={styleMenu} value={34}>➢ Lighting Crew</MenuItem>
                            <MenuItem sx={styleMenu} value={35}>➢ Gaffer</MenuItem>
                            <MenuItem sx={styleMenu} value={36}>➢  Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={37}>➢ Foreign Language</MenuItem>
                            <MenuItem sx={styleMenu} value={38}>➢  Select Country</MenuItem>
                            <MenuItem sx={styleMenu} value={39}>➢  Select Language</MenuItem>

                            <MenuItem sx={styleSelect} value={40}>➢  Grip-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={41}>➢ Key Grip</MenuItem>
                            <MenuItem sx={styleMenu} value={42}>➢ Grip Crew</MenuItem>
                            <MenuItem sx={styleMenu} value={43}>➢Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={44}>➢Hair and Make-Up</MenuItem>

                            <MenuItem sx={styleMenu} value={45}>➢  Hair Stylist</MenuItem>
                            <MenuItem sx={styleMenu} value={46}>➢  Make-Up Artist</MenuItem>
                            <MenuItem sx={styleSelect} value={47}>➢ Music-Related</MenuItem>

                            <MenuItem sx={styleMenu} value={48}>➢ Artist</MenuItem>
                            <MenuItem sx={styleMenu} value={49}>➢ Composer</MenuItem>
                            <MenuItem sx={styleMenu} value={50}>➢ Group</MenuItem>
                            <MenuItem sx={styleMenu} value={51}>➢ Performer</MenuItem>
                            <MenuItem sx={styleSelect} value={52}>➢ Performer</MenuItem>
                            <MenuItem sx={styleMenu} value={53}>➢  First Assistant Director</MenuItem>
                            <MenuItem sx={styleMenu} value={54}>➢  Second Assistant Director</MenuItem>
                            <MenuItem sx={styleMenu} value={55}>➢  Second Assistant Director</MenuItem>

                            <MenuItem sx={styleSelect} value={56}>➢Second</MenuItem>
                            <MenuItem sx={styleMenu} value={57}>➢ Investor</MenuItem>
                            <MenuItem sx={styleMenu} value={58}>➢Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={60}>➢ Production-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={61}>➢ Accounting</MenuItem>
                            <MenuItem sx={styleMenu} value={62}> Executive Producer</MenuItem>
                            <MenuItem sx={styleMenu} value={63}>➢ Finance </MenuItem>
                            <MenuItem sx={styleMenu} value={64}>➢Legal</MenuItem>
                            <MenuItem sx={styleMenu} value={65}> Line Producer</MenuItem>
                            <MenuItem sx={styleMenu} value={66}>Location</MenuItem>
                            <MenuItem sx={styleMenu} value={67}> Production Assistant</MenuItem>

                            <MenuItem sx={styleSelect} value={68}>➢ Props</MenuItem>
                            <MenuItem sx={styleMenu} value={69}>➢  Select Tags</MenuItem>

                            <MenuItem sx={styleSelect} value={70}>➢ Public Relations/Publicity</MenuItem>
                            <MenuItem sx={styleSelect} value={71}>➢  Set Design</MenuItem>
                            <MenuItem sx={styleMenu} value={72}>➢  Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={73}>➢ Sound-Related</MenuItem>

                            <MenuItem sx={styleMenu} value={74}>➢ Boom Operator</MenuItem>
                            <MenuItem sx={styleMenu} value={75}>➢  First Assistant</MenuItem>
                            <MenuItem sx={styleMenu} value={76}>➢  Sound Engineer</MenuItem>
                            <MenuItem sx={styleMenu} value={77}>➢  Sound Technician</MenuItem>
                            <MenuItem sx={styleMenu} value={78}>➢ Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={79}>➢  Special FX</MenuItem>
                            <MenuItem sx={styleMenu} value={80}>➢ Engineered</MenuItem>
                            <MenuItem sx={styleMenu} value={81}>➢ Virtual</MenuItem>
                            <MenuItem sx={styleMenu} value={82}>➢  Stunt-Related</MenuItem>
                            <MenuItem sx={styleMenu} value={83}>➢  Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={84}>➢ Voice/Voice Over</MenuItem>
                            <MenuItem sx={styleMenu} value={85}>➢ Child</MenuItem>
                            <MenuItem sx={styleMenu} value={86}>➢ Female</MenuItem>
                            <MenuItem sx={styleMenu} value={87}>➢ Male</MenuItem>
                            <MenuItem sx={styleMenu} value={88}>➢  Non-Binary</MenuItem>
                            <MenuItem sx={styleSelect} value={89}>➢ Wardrobe Department</MenuItem>
                            <MenuItem sx={styleMenu} value={90}>➢ Wardrobe Designer</MenuItem>
                            <MenuItem sx={styleMenu} value={91}>➢ Wardrobe Supervisor</MenuItem>
                            <MenuItem sx={styleMenu} value={92}>➢ Dresser/Costumer</MenuItem>



                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-4 mt-3"> <p> Visual Art</p>
                    <FormControl fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel> */}

                        <Select
                            value={select4}
                            onChange={handleChange4}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },


                                    }
                                }
                            }}
                        >
                            <MenuItem value="">
                                <em>Select Options</em>
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢ Animation</MenuItem>
                            <MenuItem sx={styleSelect} value={2}>➢  Cover Art</MenuItem>
                            <MenuItem sx={styleSelect} value={3}>➢ Drawing</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢  Graphic Art</MenuItem>
                            <MenuItem sx={styleSelect} value={5}>➢ Painting</MenuItem>
                            <MenuItem sx={styleSelect} value={6}>➢ Illustration</MenuItem>
                            <MenuItem sx={styleSelect} value={7}>➢ Photography</MenuItem>



                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-4 mt-3"> <p> Dance  </p>
                    <FormControl fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel > */}

                        <Select
                            value={select5}
                            onChange={handleChange5}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },
                                    }
                                }
                            }}
                        >
                            <MenuItem value="">
                                <em>Select Options</em>
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢ Choreography</MenuItem>
                            <MenuItem sx={styleSelect} value={2}>➢ Solo</MenuItem>
                            <MenuItem sx={styleMenu} value={3}>➢  Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢ Troupe</MenuItem>
                            <MenuItem sx={styleMenu} value={5}>➢  Select Tags</MenuItem>
                            <MenuItem sx={styleSelect} value={6}>➢ Group</MenuItem>
                            <MenuItem sx={styleMenu} value={7}>➢  Select Tags</MenuItem>

                        </Select>
                    </FormControl> </div>
                <div className="col-md-4 mt-3"> <p> Academia </p>
                    <FormControl fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel > */}

                        <Select
                            value={select6}
                            onChange={handleChange6}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },

                                    }
                                }
                            }}
                        >
                            <MenuItem value="">
                                <em>Select Options</em>
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢ Author</MenuItem>
                            <MenuItem sx={styleSelect} value={2}>➢  Coach</MenuItem>
                            <MenuItem sx={styleSelect} value={3}>➢ Critic</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢ Dissertation</MenuItem>
                            <MenuItem sx={styleSelect} value={5}>➢ Educator</MenuItem>
                            <MenuItem sx={styleSelect} value={6}>➢ Instructor</MenuItem>
                            <MenuItem sx={styleSelect} value={7}>➢ Lecturer</MenuItem>
                            <MenuItem sx={styleSelect} value={8}>➢ Professor</MenuItem>
                            <MenuItem sx={styleSelect} value={9}>➢ Publisher</MenuItem>
                            <MenuItem sx={styleSelect} value={10}>➢ Researcher</MenuItem>
                            <MenuItem sx={styleSelect} value={11}>➢ Thesis</MenuItem>
                            <MenuItem sx={styleSelect} value={12}>➢ Workshops</MenuItem>
                            <MenuItem sx={styleSelect} value={13}>➢  Writing Programs</MenuItem>



                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-4 mt-3"> <p> Market Partners </p>
                    <FormControl fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel > */}

                        <Select
                            value={select7}
                            onChange={handleChange7}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },


                                    }
                                }
                            }}
                        >
                            <MenuItem value="">
                                <em>Select Options</em>
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢ Accountant</MenuItem>
                            <MenuItem sx={styleSelect} value={2}>➢ Beta Tester</MenuItem>
                            <MenuItem sx={styleSelect} value={3}>➢ Book Club</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢ Book Store</MenuItem>
                            <MenuItem sx={styleSelect} value={5}>➢  Computer Code Creator</MenuItem>
                            <MenuItem sx={styleSelect} value={6}>➢  Copy Editor</MenuItem>
                            <MenuItem sx={styleSelect} value={7}>➢ Distributor</MenuItem>
                            <MenuItem sx={styleSelect} value={8}>➢  Event Planner</MenuItem>
                            <MenuItem sx={styleSelect} value={9}>➢  Financial Planner</MenuItem>
                            <MenuItem sx={styleSelect} value={10}>➢ Graphic Artist</MenuItem>
                            <MenuItem sx={styleSelect} value={11}>➢ Illustrator</MenuItem>
                            <MenuItem sx={styleSelect} value={12}>➢ Investor</MenuItem>
                            <MenuItem sx={styleSelect} value={13}>➢ Lawyer</MenuItem>
                            <MenuItem sx={styleSelect} value={14}>➢  Layout/design</MenuItem>
                            <MenuItem sx={styleSelect} value={15}>➢  Media Reviewer</MenuItem>
                            <MenuItem sx={styleSelect} value={16}>➢ Photographer</MenuItem>
                            <MenuItem sx={styleSelect} value={17}>➢ Proofreader</MenuItem>
                            <MenuItem sx={styleSelect} value={18}>➢ Promoter</MenuItem>
                            <MenuItem sx={styleSelect} value={19}>➢  Public Relations</MenuItem>
                            <MenuItem sx={styleSelect} value={20}>➢  Publisher</MenuItem>
                            <MenuItem sx={styleSelect} value={21}>➢ Reader</MenuItem>



                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-4 mt-3"> <p> Technology Partners </p>
                    <FormControl fullWidth sx={{ background: 'white' }}>
                        {/* <InputLabel htmlFor="grouped-select" sx={{}}>Select Options</InputLabel > */}

                        <Select
                            value={select8}
                            onChange={handleChange8}
                            displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            MenuProps={{
                                PaperProps: {
                                    sx: {

                                        "& .MuiMenuItem-root:hover": {
                                            backgroundColor: "#03a9f4"
                                        },


                                    }
                                }
                            }}
                        >
                            <MenuItem value="">
                                Select Options
                            </MenuItem>
                            <MenuItem sx={styleSelect} value={1}>➢  Ad Creator</MenuItem>
                            <MenuItem sx={styleSelect} value={2}>➢  Actor – Voice/Ads</MenuItem>
                            <MenuItem sx={styleSelect} value={3}>➢ Aspiring Content Creator</MenuItem>
                            <MenuItem sx={styleSelect} value={4}>➢ Audio Book Creator</MenuItem>
                            <MenuItem sx={styleSelect} value={5}>➢ Blogger </MenuItem>
                            <MenuItem sx={styleSelect} value={6}>➢Blogger</MenuItem>
                            <MenuItem sx={styleSelect} value={7}>➢  College/University</MenuItem>
                            <MenuItem sx={styleSelect} value={8}>➢  Computer Code Writer</MenuItem>
                            <MenuItem sx={styleSelect} value={9}>➢  Computer Software</MenuItem>
                            <MenuItem sx={styleSelect} value={10}>➢  Costume Designer</MenuItem>
                            <MenuItem sx={styleSelect} value={11}>➢ eBook Creator</MenuItem>
                            <MenuItem sx={styleSelect} value={12}>➢ Filmmaker</MenuItem>
                            <MenuItem sx={styleSelect} value={13}>➢ Filmmaker</MenuItem>
                            <MenuItem sx={styleSelect} value={14}>➢ Inventor</MenuItem>
                            <MenuItem sx={styleSelect} value={15}>➢Library</MenuItem>
                            <MenuItem sx={styleSelect} value={16}>➢  Make-up Artist</MenuItem>
                            <MenuItem sx={styleSelect} value={17}>➢ Model</MenuItem>
                            <MenuItem sx={styleSelect} value={18}>➢  Movie Agent</MenuItem>
                            <MenuItem sx={styleSelect} value={19}>➢  Online Magazine</MenuItem>
                            <MenuItem sx={styleSelect} value={20}>➢   Set Designer</MenuItem>
                            <MenuItem sx={styleSelect} value={21}>➢  Sound Engineer</MenuItem>
                            <MenuItem sx={styleSelect} value={22}>➢  Student</MenuItem>
                            <MenuItem sx={styleSelect} value={23}>➢  Teacher/Educator/Coach</MenuItem>
                            <MenuItem sx={styleSelect} value={24}>➢Translator</MenuItem>
                            <MenuItem sx={styleSelect} value={25}> Travel Planner</MenuItem>
                            <MenuItem sx={styleSelect} value={26}>Video Creator</MenuItem>



                        </Select>
                    </FormControl>
                </div>
                <div className="col-md-4"></div>
                <div className=" d-flex col-md-12 justify-content-between mt-5" style={{ marginBottom: "170px" }}>
                    <div>

                        <Link to={'/WriterPublishPage'}><button variant="contained" type="button" style={newButton} class="btn btn-primary btn-lg">
                            Go Back
                        </button></Link>
                    </div>
                    <div>
                    <Link to={generatePath("/UserDashboard/:username", {username:userName.name.toLowerCase()})}>
                        <button
                            variant="contained"
                            type="button" class="btn btn-primary btn-lg"
                            style={newButton}
                        >
                            Continue
                        </button></Link>
                    </div>
                </div>

            </div>
        </div>
    )
}
