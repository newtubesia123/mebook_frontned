import React from "react";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link } from "react-router-dom";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
// import "./common.css";
import img from "../Screenshot_2.png";

export default function ProfileWork() {
  const arry = [
    { option1: "Creator" },
    { option1: "Collaborator" },
    { option1: "Influencer" },
    { option1: "Project Developer" },
    { option1: "Provider" },
    { option1: "Reader/Consumer" },
  ];
  return (
    <div className="container">
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              backgroundColor: "#100892",
              borderRadius: "30px",
              textAlign: "center",
            }}
          >
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  // src={picture}
                  sx={{
                    width: "100px",
                    height: "100px",
                    margin: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "25px 0px" }}
              >
                Welcome, J.K. Rowling
              </Typography>
            </CardContent>
          </Card>
          <div
            id="font"
            style={{
              margin: "15px 0px",
              fontSize: "2vw",
              fontWeight: "bold",
              color: "#100892",
              textAlign: "center",
            }}
          >
            <img src={img} alt="" style={{ height: "30px", width: "50px" }} />{" "}
             Profile: Influencer
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              minHeight: "480px",
              maxHeight: "800px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              {arry.map((val) => {
                return (
                  <>
                    {/* <FormControlLabel
                      sx={{ paddingLeft: "10px" }}
                      control={<Checkbox sx={{ color: "white" }} />}
                      // label={val.option1}
                    /> */}
                    <div
                      style={{
                        backgroundColor: "white",
                        margin: "25px 60px 20px ",
                        borderRadius: "10px",
                      }}
                    >
                      <h3
                        id="author"
                        style={{
                          padding: "8px 0px",
                          textAlign: "center",
                        }}
                      >
                        {val.option1}
                      </h3>
                    </div>
                  </>
                );
              })}
            </CardContent>
          </Card>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <button
              type="button"
              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Go Back
            </button>
            <button
              type="button"
              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
