import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";

import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link } from "react-router-dom";
import img from "../mepic.png";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";

export default function SetupProfile(props) {
  const [userTypeArray, setUserTypeArray] = useState([]);
  const { check } = props.handleCheck;
  useEffect(() => {
    // let token = JSON.parse(localStorage.getItem("token"));
    // let id = sessionStorage.getItem("id");

    axios
      .get("http://34.244.150.71:3002/get-User")

      .then((res) => {
        console.log("res.data---> ", res.data.data);

        if (res.data) {
          setUserTypeArray(res.data.data);
        }
      })
      .catch((err) => console.log(err));
  }, []);
  const handleUserType = useCallback(
    (id) => () => {
      console.log("ID: ", id);
      console.log(props.handleCheck(id));
    },
    []
  );
  // const arry = [
  //   { option1: "Creator" },
  //   { option1: "Collaborator" },
  //   { option1: "Influencer" },
  //   { option1: "Project Developer" },
  //   { option1: "Provider" },
  //   { option1: "Reader/Consumer" },
  // ];

  return (
    <div className="container">
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              backgroundColor: "#100892",
              borderRadius: "30px",
              textAlign: "center",
            }}
          >
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  // src={picture}
                  sx={{
                    width: "100px",
                    height: "100px",
                    margin: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "25px 0px" }}
              >
                Welcome, J.K. Rowling
              </Typography>
            </CardContent>
          </Card>
          <div
            id="font"
            style={{
              margin: "15px 0px",
              fontSize: "3vw",
              fontWeight: "bold",
              color: "#100892",
              textAlign: "center",
            }}
          >
            Set Up Your
            <img
              src={img}
              alt=""
              style={{ height: "30px", width: "50px" }}
            />{" "}
            Book Profile
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              minHeight: "480px",
              maxHeight: "800px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              {userTypeArray.map((val) => {
                return (
                  <>
                    <div
                      style={{
                        backgroundColor: "white",
                        margin: "25px 60px 20px ",
                        borderRadius: "10px",
                      }}
                      value={val._id}
                      onClick={handleUserType(val._id)}
                    >
                      <h3
                        id="author"
                        style={{
                          padding: "8px 0px",
                          textAlign: "center",
                        }}
                      >
                        {val.usertype}
                      </h3>
                    </div>
                  </>
                );
              })}
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
}
