import React from "react";
import CommonIdentity from "./CommonIdentity";

export default function MultiDiscipline() {
  return (
    <div>
      <CommonIdentity
        heading="Multi-Discipline"
        title="Multi-Discipline"
        paragraph="This identity is reserved for creators,
        collaborators, influencers, providers, project
        developers and reader/consumers whose
        talents and abilities legitimately expand across
        two or more levels of expertise — such that a
        single identity definition does not approximate
        actual comprehensive identity.
        This identity is purposed to provide profiles for
        individuals whose talents, abilities and
        experience expand across multiple disciplines.
        Consider carefully: in the Global Media
        Marketplace, specificity is helpful in defining a
        unique niche, although some seekers might
        consider “Multi-Discipline” a required niche
        for specific intentions and purposes. If more
        than 2-3 substantial identities, consider
        creating separate profiles."
      />
    </div>
  );
}
