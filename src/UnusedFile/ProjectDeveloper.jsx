import React from "react";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link } from "react-router-dom";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { CardActionArea } from "@mui/material";

// import "./common.css";
import img from "../Screenshot_2.png";

export default function ProjectDeveloperProfile() {
  const arry = [
    { option1: "Biography" },
    { option1: "Children’s" },
    { option1: "Comics/Graphic Novels" },
    { option1: "Composer" },
    { option1: "Crime/Mystery/ Thriller" },
    { option1: "Essay" },
    { option1: "Fantasy" },
    { option1: "Horror" },
    { option1: "Humor" },
    { option1: "Mainstream Fiction" },
    { option1: "History" },
    { option1: "Literary" },
    { option1: "Memoir" },
    { option1: "Mystery" },
    { option1: "Non-Fiction" },
    { option1: "Poet" },
    { option1: "Political" },
    { option1: "Romance" },
    { option1: "Science Fiction" },
    { option1: "Short Story" },
    { option1: "Spiritual" },
    { option1: "Spoken Word Artist" },
    { option1: "Suspense/ Thriller" },
    { option1: "Urban" },
    { option1: "Young Adult" },
  ];
  return (
    <div className="container">
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              backgroundColor: "#100892",
              borderRadius: "30px",
              textAlign: "center",
            }}
          >
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  src={""}
                  sx={{
                    width: "100px",
                    height: "100px",
                    margin: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "25px 0px" }}
              >
                Welcome, J.K. Rowling
              </Typography>
            </CardContent>
          </Card>
          <div
            id="font"
            style={{
              margin: "15px 0px",
              fontSize: "3vw",
              fontWeight: "bold",
              color: "#100892",
              textAlign: "center",
            }}
          >
            <img
              src={img}
              alt=""
              style={{ height: "50px", width: "100px", fontSize: "3vw" }}
            />{" "}
             Profile: Project Developer
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              minHeight: "480px",
              maxHeight: "3000px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              <div className="row mx-2">
                {arry.map((item) => {
                  return (
                    <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                      <Card
                        elevation={10}
                        fullwidth
                        sx={{
                          marginTop: "15px",

                          height: "100px",
                        }}
                      >
                        {/* <input type="checkbox" checked={checked} /> */}

                        <CardActionArea>
                          <CardContent>
                            <Typography
                              gutterBottom
                              variant="h5"
                              component="div"
                            >
                              {/* <button
                                type="button"
                                class="btn"
                                onClick={handleCheck}
                              > */}{" "}
                              {item.option1}
                              {/* </button> */}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </div>
                  );
                })}
              </div>
            </CardContent>
          </Card>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <button
              type="button"
              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Go Back
            </button>
            <button
              type="button"
              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
