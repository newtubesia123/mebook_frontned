import React from "react";
import Grid from "@mui/material/Grid";
import image from "../mepic.png";
import Button from "@mui/material/Button";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { BackGround, newButton } from "../Component new/background";

export default function Common(props) {
  const mystyle = {
    "@media (max-width: 500px)": {
      width: "100%",
      padding: "15px 0px",
    },
  };
  // @media screen and (max-width: "570px") {

  //     width: 100%,
  //     padding: "15px 0px",
  //   }
  // }

  return (
    <div style={BackGround}>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
        sx={{ padding: "50px 5px" }}
      >
        <Grid item xs={1} md={1} sm={1}></Grid>
        <Grid
          item
          xs={10}
          md={10}
          sm={10}
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />
          <b>
            {" "}
            <p style={{ fontSize: "20px", margin: "10px 0px" }}>
              {props.title}
            </p>
          </b>
          <p>{props.para}</p>
          <div style={{ backgroundColor: "#FFFFFF", marginTop: "70px" }}>
            <FormGroup style={{ paddingLeft: "15px" }}>
              <FormControlLabel control={<Checkbox />} label={props.one} />
              <FormControlLabel control={<Checkbox />} label={props.two} />
              <FormControlLabel control={<Checkbox />} label={props.three} />
              <FormControlLabel control={<Checkbox />} label={props.four} />
              <FormControlLabel control={<Checkbox />} label={props.five} />
            </FormGroup>
          </div>
        </Grid>
        <Grid item xs={1} md={1} sm={1}></Grid>
        <div className="btn m-auto " style={mystyle}>
          <Button
            variant="contained"
            style={{margin: "50px 0px"}}
            sx={newButton}
          >
            Go Back
          </Button>
          <Button
            variant="contained"
            sx={newButton}
            style={{ marginLeft: "20px" }}
          >
            Continue
          </Button>
        </div>
      </Grid>
    </div>
  );
}
