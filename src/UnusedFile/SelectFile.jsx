import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import "./Selectfile.css";
export default function SelectFile() {
  const [fileArray, setFileArray] = useState([]);
  console.log("fileArray", fileArray);
  let Array = [];
  for (let i = 0; i < fileArray.length; i++) {
    Array.push(fileArray[i].fileName);
    console.log("Array-->", Array);
  }
  const [userInfo, setuserInfo] = useState({
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));

    axios
      .get("http://34.245.73.139:3002/getFilesById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        // console.log("token--->", id);
        console.log("res.data---> ", res.data.userData.filePath[0].fileName);
        setFileArray(res.data.userData.filePath);

        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://34.245.73.139:3002/uploads/${res.data.userData.filePath}`,

            // file: `localhost:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://34.245.73.139:3002/uploads/${res.data.userData.filePath}`,
          });
          // setFileArray(userInfo.filepreview);
          console.log("file---->", userInfo.filepreview);
          console.log("fileArray---->", fileArray);
        }
      })
      .catch((err) => console.log(err));
  }, []);

  // logout privatisation
  // const navigate = useNavigate();
  // const auth = localStorage.getItem("token");
  // const logout = () => {
  //   localStorage.clear();
  //   navigate("/Page2");
  // };

  return (
    <Grid className="MebookMetaimage"
      container
      rowSpacing={1}
      columnSpacing={{ xs: 1, sm: 2, md: 2 }}
      sx={{ margin: "25px 0px" }}
    >
      <Grid item xs={1} md={2}></Grid>
      <Grid
        style={{
          textAlign: "center",
          display: "flex",
          justifyContent: "center",
          display: "block",
        }}
        item
        xs={10}
        md={8}
      >
        <img
          src={image}
          alt=""
          style={{
            width: "180px",
            height: "106px",
          }}
        />
        <Card
          sx={{
            width: "323px",
            height: "440px",
            backgroundColor: "#100892",
            borderRadius: "24px",
            m: " 25px auto 40px",
          }}
        >
          <CardContent>
            {userInfo.filepreview !== null ? (
              <>
                <p id="text">My Books</p>
                {Array.map((i, index) => (
                  <p>
                    <p id="text1" key={index}>
                      {i}
                    </p>
                    {/* <h1> console.log("i--",i)</h1> */}
                    <hr style={{ backgroundColor: "white" }} />
                  </p>
                ))}
              </>
            ) : null}
          </CardContent>
        </Card>
    <Link to={'/UserDashboard'}><Button
          variant="contained"
          style={{ background: "#100892", margin: "25px 0px" }}
         
        >
         Go Back
        </Button></Link> 
      </Grid>
      <Grid item xs={1} md={2}>
        {" "}
        {/* <Button
          variant="contained"
          style={{ background: "#100892", marginTop: "25px" }}
          onClick={logout}
        >
          Log out({JSON.parse(auth).name})
        </Button> */}
      </Grid>
    </Grid>
  );
}
