import React from "react";
import Common from "./Common";

export default function Writer() {
  return (
    <div style={{color:"white"}}>
      <Common
        title="Are You a Writer?"
        para="Most professional writers write something other than books. So whether you write gags for stand-up comedians, ads, magazine or newspaper articles, theatrical reviews, social commentary or copy for DIY manuals or websites, your book will fall into the “Writer” category on this site. 
        You will be matched with readers, other writers, collaborators and producers, so specificity is crucial in the creation of your profile. Without being too broad, please check all boxes that apply to define your Writer content. 
        
"
        one="Critic"
        two="Expose"
        three="Editor"
        four="Magazine"
        five="Newspaper"
      />
    </div>
  );
}
