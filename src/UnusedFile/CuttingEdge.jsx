import React from "react";
import CommonIdentity from "./CommonIdentity";

export default function CuttingEdge() {
  return (
    <div>
      <CommonIdentity
        heading="Cutting Edge"
        title="Cutting Edge"
        paragraph="This identity is reserved for creators,
        collaborators, influencers, providers, project
        developers and reader/consumers who have
        established 250+ followers, who demonstrate
        understanding and innovation in profile
        choice areas, and who exert a relative
        influence on the global media marketplace.
        This identity is purposed to provide engaged,
        charismatic trendsetters an opportunity to
        create and advance the beginning of a unique
        
        brand and to sell work and services to a pre-
        identified and pre-defined niche."
      />
    </div>
  );
}
