import React from "react";
import Common from "./Common";

export default function ContentType12() {
  return (
    <div style={{color:"white"}}>
      <Common
        title="Are you a Visual Related Creator?"
        para="In 1911, newspaper editor Arthur Brisbane suggested that a picture is “worth a thousand words,” yet that was long before the Canon EOS 90D Digital, micro photography and even moving pictures (movies). If your work includes painting, graphic design, illustration, sculpture or art film, then your work will fall into the Visual Related category on this site.
        You will be matched with other visual related creators, consumers, collaborators and producers, so specificity is crucial in the creation of your profile. Without being too broad, please check all boxes that apply to define your Visual Related content and work."
        one="Biography"
        two="Fantasy"
        three="Horror"
        four="Non-Fiction"
        five="Romance"
      />
    </div>
  );
}
