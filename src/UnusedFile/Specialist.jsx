import React from "react";
import CommonIdentity from "./CommonIdentity";

export default function Specialist() {
  return (
    <div>
      <CommonIdentity
        heading="Specialist"
        title="Specialist"
        paragraph="This identity is reserved for creators,
        collaborators, influencers, providers, project
        developers and reader/consumers who, as a
        result of lengthy experience, education or
        specialized training, have attained a level of
        expertise in a specific area or discipline. A
        “Specialist-V” designation will indicate that the
        profile has been vetted by our research team.
        This identity is purposed to provide experienced
        individuals and experts an opportunity to share
        work and work, provide or integrate with other
        profiles on the platform."
      />
    </div>
  );
}
