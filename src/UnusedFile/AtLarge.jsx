import React from "react";
import CommonIdentity from "./CommonIdentity";

export default function AtLarge() {
  return (
    <div>
      <CommonIdentity
        heading="At-Large"
        title="At-Large"
        paragraph="This identity is reserved for creators,
        collaborators, influencers, providers, project
        developers and reader/consumers who would
        rather not be defined by labels, and would rather
        be presented as uncharacterized, non-conformist
        individuals. Yet this identity values the importance
        of the Global Media Marketplace and its necessity
        for individual success.
        This identity is purposed to give expression to the
        majority of users by providing the greatest
        exposure and potential for success. At-Large
        should be considered a “catch-all” category."
      />
    </div>
  );
}
