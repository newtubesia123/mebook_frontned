import React, { useState, useEffect } from "react";
import image from "../mepic.png";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";
import { BackGround, newButton } from "../Component new/background";

import "./Page2.css";
export default function CreateProfile() {
  const [success, setSuccess] = React.useState(false);
  const bg ={...BackGround, minHeight:"100vh"}
  useEffect(() => {
    setSuccess(true);
    setTimeout(() => {
      setSuccess(false);
    }, 5000);
  }, []);
  return (
    <div>
      <Collapse in={success}>
        <Alert
          severity="success"
          variant="filled"
          onClose={() => setSuccess(false)}
        >
          Mail has been sent to your email id
        </Alert>
      </Collapse>
      <div
        className="container-fluid d-flex align-items-center justify-content-center flex-column text-center "
        style={bg}
      >
        <div className="row ">
          <div className="col-10 mx-auto">
            <div className="">
              <img
                src={image}
                alt=""
                style={{
                  width: "300px",
                  height: "153.1px",
                  marginTop: "153px",
                }}
              />
            </div>

            <div className="" style={{ margin: "21px 0px", fontSize: "17px", color:"white" }}>
              <p>
                Thank you for sharing information about the content you create.
                A unique MeBook algorithm will use your input to provide us with
                useful information to help you promote your work, and create
                connections with other content creators, collaborators and key
                contacts!
              </p>
            </div>
            <div className="">
              <Link className="nav-link m-auto" to={"/Login"}>
                {" "}
                <Button
                  variant="contained"
                  className="btn-get"
                  style={newButton}
                >
                  Continue
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
