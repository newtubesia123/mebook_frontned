import React from "react";
import Common from "./Common";

export default function Author() {
  return (
    <div style={{color:"white"}}>
      <Common 
        title="Are You an Author?"
        para="Most traditional book writers fit into this category on our site. Whether your work fits in the “Thriller,” “Biography,” “Children’s,” “Non-Fiction,” “Poetry” or “Romance” genre, your book will fall into the “Author” category on this site. Please take the time to define your work.
You will be matched with readers, other content creators and collaborators, so specificity is crucial in the creation of your profile. Without being too broad, please check all boxes that apply to define your Author content.
"
        one="Biography"
        two="Fantasy"
        three="Horror"
        four="Non-Fiction"
        five="Romance"
      />
    </div>
  );
}
