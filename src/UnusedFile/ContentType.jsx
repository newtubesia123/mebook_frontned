import React from "react";
import Grid from "@mui/material/Grid";
import image from "../mepic.png";
import Button from "@mui/material/Button";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { BackGround, newButton } from "../Component new/background";

export default function ContentType(props) {
  const mystyle = {
    "@media (max-width: 500px)": {
      width: "100%",
      padding: "15px 0px",
    },
  };
  // @media screen and (max-width: "570px") {

  //     width: 100%,
  //     padding: "15px 0px",
  //   }
  // }

  return (
    <div style={BackGround}>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
        sx={{ padding: "50px 5px" }}
      >
        <Grid item xs={1} md={1} sm={1}></Grid>
        <Grid
          item
          xs={10}
          md={10}
          sm={10}
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />
          <b>
            {" "}
            <p style={{ fontSize: "20px", color:"white" }}>
              Are you a Music or Audio Related Creator?
            </p>
          </b>
          <p style={{color:"white"}}>
            It is impossible to overestimate the importance of music and sound
            in our lives. Music allows us to experience every possible emotion,
            sometimes providing instant recall of our most powerful memories.
            Music and sound can inspire courage or dread, pride or humility,
            love or war. If you write or create music or sound, whether you are
            a violinist, a rap group, a composer, an effects specialist or a
            music/sound supervisor or engineer, then your work will fall into
            the Music and Audio category on this site.You will be matched with
            other music and sound related creators, consumers, collaborators and
            producers, so specificity is crucial in the creation of your
            profile. Without being too broad, please check all boxes that apply
            to define your Music and Audio content and work.{" "}
          </p>
          <div style={{ backgroundColor: "#FFFFFF" }}>
            <FormGroup style={{ paddingLeft: "15px" }}>
              <FormControlLabel control={<Checkbox />} label="Biography" />
              <FormControlLabel control={<Checkbox />} label="Fantasy" />
              <FormControlLabel control={<Checkbox />} label="Horror" />
              <FormControlLabel control={<Checkbox />} label="Non-Fiction" />
              <FormControlLabel control={<Checkbox />} label="Romance" />
            </FormGroup>
          </div>
        </Grid>
        <Grid item xs={1} md={1} sm={1}></Grid>
        <div className="btn m-auto " style={mystyle}>
          <Button
            variant="contained"
            sx={newButton}
            style={{  marginTop: "25px" }}
          >
            Continue
          </Button>
        </div>
      </Grid>
    </div>
  );
}
