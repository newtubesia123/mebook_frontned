import React, { useState,useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link } from "react-router-dom";
import img from "../Screenshot_2.png";
// import "./common.css";

export default function ProjectDeveloperPara() {
  let user=sessionStorage.getItem('userName')
  let workProfile= localStorage.getItem("Work profile")
  var identity=sessionStorage.getItem('identity Option')
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {         
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="container-fluid MebookMetaimage">
      {/* rowSpacing={1}
      // columnSpacing={{ xs: 1, sm: 2, md: 2 }}
      sx={{ margin: "25px 0px" }}
    > */}
      <div className="row">
        <div className="col-md-3"></div>
        <div
          className="col-md-6"
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />
        
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  src={userInfo.filepreview}
                  sx={{
                    width: "150px",
                    height: "130px",

                    m: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color=""
                sx={{ margin: "5px 0px",fontSize: "25px" }}
              >
                 {user}, {identity}, {workProfile}
              </Typography>
            </CardContent>
          <Typography className="head"
            gutterBottom
            component="div"
            sx={{
              margin: "auto",
              fontSize: "2.1vw",
              fontWeight: "bold",
              color: "",
              width:'70%'
            }}
          >
             Set Up Your <span style={{ }}>M</span><span style={{  }}>eBook</span><span style={{  }}>M</span><span style={{}}>eta</span> Profile : {identity}, {workProfile}
          </Typography>
        
            <CardContent>
              <div
                style={{
                  backgroundColor: "white",
                  margin: "5px 100px 5px ",
                  borderRadius: "10px",
                }}
              >
                <h3
                  style={{
                    padding: "8px 0px",
                    fontFamily: "Times New Roman"
                  }}
                >
                  Project Developer
                </h3>
              </div>
              <div
                style={{
                  backgroundColor: "white",
                  margin: "5px 30px 5px ",
                  borderRadius: "50px",
                }}
              >
                <p
                  style={{
                    padding: "20px 20px",
                    textAlign: "justify",
                    fontFamily: "Times New Roman"
                  }}
                >
                  <b>This profile is reserved for those whose work
                    involves the discovery and acquisition of
                    original or existing work and developing that
                    work to achieve a pre-determined goal.
                    Project developers are often producers or
                    entrepreneurs who are required to expand
                    ideas to create overall visions, locate talented
                    players for teams over multiple projects,
                    develop budgets, find investors, manage, and
                    complete projects on-time and within budget.
                    Project Developers work with creators,
                    collaborators, influencers and providers to
                    bring work to the Global Media Marketplace.</b>
                </p>
              </div>
            </CardContent>
        

          <div className="d-flex justify-content-between">
            <div>
              <Link to={'/CreatorNew'}><button
                type="button"
                class="btn  btn-lg"
                style={{ background: "white" }}
              >
                Go Back
              </button></Link>
            </div>
            <div>
              <Link to={'/CreatorNew1'}><button
                type="button"
                class="btn  btn-lg"
                style={{ background: "white" }}
              >
                Choose
              </button></Link>
            </div>
          </div>
        </div>
        <div className="col-md-3"></div>
      </div>
    </div>
  );
}


