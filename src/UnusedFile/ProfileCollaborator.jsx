import React from "react";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
// import picture from "../Ellipse 11.png";
import { Link } from "react-router-dom";
import img from "../mepic.png";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { CardActionArea } from "@mui/material";
import CardMedia from "@mui/material/CardMedia";

// import "./common.css";

export default function CollaboratorProfile() {
  const arry = [
    { option1: "Accountant" },
    { option1: "Actor-Stage/TV/Films" },
    { option1: "Agent" },
    { option1: "Book Club" },
    { option1: "Book Store" },
    { option1: "Computer Code Creator" },
    { option1: "Copy Editor" },
    { option1: "Cover Art " },
    { option1: "Director-Stage/TV/Films" },
    { option1: "Distributor" },
    { option1: "Event Planner" },
    { option1: "Financial Planner" },
    { option1: "Graphic Artist" },
    { option1: "Illustrator" },
    { option1: "Investor" },
    { option1: "Lawyer" },
    { option1: "Layout/design" },
    { option1: "Media Reviewer" },
    { option1: "Photographer" },
    { option1: "Proofreader" },
  ];
  return (
    // <Grid
    //
    //   container
    //   rowSpacing={1}
    //   columnSpacing={{ xs: 1, sm: 2, md: 2 }}
    //   sx={{ margin: "25px 0px" }}
    // >
    //   <Grid item xs={2} sm={2} md={3}></Grid>
    //   <Grid
    //     style={{
    //       textAlign: "center",
    //       display: "flex",
    //       justifyContent: "center",
    //       display: "block",
    //     }}
    //     item
    //     xs={8}
    //     md={6}
    //   >
    //     <img
    //       src={image}
    //       alt=""
    //       style={{
    //         width: "120px",
    //         height: "61.24px",
    //       }}
    //     />
    //     <Card
    //       sx={{
    //         width: "500px",
    //         height: "200px",
    //         backgroundColor: "#100892",
    //         borderRadius: "30px",
    //         m: "auto",
    //       }}
    //     >
    //       <CardContent>
    //         <div>
    //           <Avatar
    //             alt="Remy Sharp"
    //             src={picture}
    //             sx={{
    //               width: "100px",
    //               height: "100px",

    //               m: "auto",
    //             }}
    //           />
    //         </div>

    //         <Typography
    //           variant="body2"
    //           color="#FFFFFF"
    //           sx={{ margin: "25px 0px" }}
    //         >
    //           Welcome, J.K. Rowling
    //         </Typography>
    //       </CardContent>
    //     </Card>
    //     <Typography
    //       id="font"
    //       gutterBottom
    //       component="div"
    //       sx={{
    //         margin: "10px 5px 20px ",
    //         fontSize: "30px",
    //         fontWeight: "bold",
    //         color: "#100892",
    //       }}
    //     >
    //       <img src={img} alt="" style={{ height: "30px", width: "50px" }} />{" "}
    //       Book Profile: Collaborator
    //     </Typography>
    //     <Card
    //       sx={{
    //         width: "500px",
    //         minHeight: "480px",
    //         maxHeight: "800px",
    //         backgroundColor: "#100892",
    //         borderRadius: "50px",
    //         m: " 25px auto 40px",
    //       }}
    //     >
    //       <CardContent>
    //         {arry.map((val) => {
    //           return (
    //             <div
    //               style={{
    //                 backgroundColor: "white",
    //                 margin: "25px 60px 20px ",
    //                 borderRadius: "10px",
    //               }}
    //             >
    //               <h3
    //                 style={{
    //                   padding: "8px 0px",
    //                 }}
    //               >
    //                 {val.option1}
    //               </h3>
    //             </div>
    //           );
    //         })}
    //       </CardContent>
    //     </Card>

    //     <div className="d-flex justify-content-between">
    //       <div>
    //         <button
    //           type="button"
    //           class="btn btn-primary btn-lg"
    //           style={{ background: "#100892" }}
    //         >
    //           Go Back
    //         </button>
    //       </div>
    //       <div>
    //         <button
    //           type="button"
    //           class="btn btn-primary btn-lg"
    //           style={{ background: "#100892" }}
    //         >
    //           Continue
    //         </button>
    //       </div>
    //     </div>
    //   </Grid>
    //   <Grid item xs={2} sm={2} md={3}></Grid>
    // </Grid>
    <div className="container">
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              backgroundColor: "#100892",
              borderRadius: "30px",
              textAlign: "center",
            }}
          >
            <CardContent>
              <div>
                <Avatar
                  alt="Remy Sharp"
                  // src={picture}
                  sx={{
                    width: "100px",
                    height: "100px",
                    margin: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "25px 0px" }}
              >
                Welcome, J.K. Rowling
              </Typography>
            </CardContent>
          </Card>
          <div
            id="font"
            style={{
              margin: "15px 0px",
              fontSize: "3vw",
              fontWeight: "bold",
              color: "#100892",
              textAlign: "center",
            }}
          >
            <img
              src={img}
              alt=""
              style={{ height: "50px", width: "100px", fontSize: "3vw" }}
            />{" "}
            Book Profile: Collaborator
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              minHeight: "500px",
              maxHeight: "3000px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              <div className="row mx-2">
                {arry.map((item) => {
                  return (
                    <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                      <Card
                        elevation={10}
                        fullwidth
                        sx={{
                          marginTop: "15px",

                          height: "100px",
                        }}
                      >
                        {/* <input type="checkbox" checked={checked} /> */}

                        <CardActionArea>
                          <CardContent>
                            <Typography
                              gutterBottom
                              variant="h5"
                              component="div"
                            >
                              {/* <button
                                type="button"
                                class="btn"
                                onClick={handleCheck}
                              > */}{" "}
                              {item.option1}
                              {/* </button> */}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </div>
                  );
                })}
              </div>
            </CardContent>
          </Card>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <button
              type="button"
              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Go Back
            </button>
            <button
              type="button"
              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
