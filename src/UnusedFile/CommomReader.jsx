import React from "react";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import SearchIcon from "@mui/icons-material/Search";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Switch from "@mui/material/Switch";
import { BackGround } from "../Component new/background";

const label = { inputProps: { "aria-label": "Switch demo" } };

export default function CommonReader() {
  return (
    <div >
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 2, md: 2 }}
        style={BackGround}
        sx={{ margin: "25px 0px" }}
      >
        <Grid item xs={1} md={2}></Grid>
        <Grid
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
          item
          xs={10}
          md={8}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />
          <Card
            sx={{
              width: "320px",
              height: "200px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: "10px auto",
            }}
          >
            <CardContent>
              <div style={{}}>
                <Avatar
                  alt="Remy Sharp"
                  // src={picture}
                  sx={{
                    width: "100px",
                    height: "100px",

                    m: "auto",
                  }}
                />
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "25px 0px" }}
              >
                Welcome, J.K. Rowling
              </Typography>
            </CardContent>
          </Card>
          <div className="d-flex justify-content-between mt-3 ">
            <div>
              <h5
                style={{
                  color: "#000000",
                  fontFamily: "Poppins",

                  fontSize: "18px",
                }}
              >
                Reader Mode
              </h5>
            </div>
            <div>
              {" "}
              <Switch {...label} disabled />
            </div>
          </div>
          <Box
            sx={{
              backgroundColor: "#100892",
              // paddingRight: "60px",
              borderRadius: "10px",
            }}
          >
            <IconButton type="submit" aria-label="search">
              {" "}
              <SearchIcon sx={{ color: "#FFFFFF" }} />
            </IconButton>
            <InputBase placeholder="Search" sx={{ color: "#FFFFFF" }} />
          </Box>
          <Card
            sx={{
              width: "323px",
              height: "440px",
              backgroundColor: "#100892",
              borderRadius: "24px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              <p id="text">My Books</p>

              <p id="text1">the_kite_runner.pdf</p>
              <hr style={{ backgroundColor: "white" }} />
              <p id="text1">the_sea_of_innocence.pdf</p>
              <hr style={{ backgroundColor: "white" }} />
              <p id="text1">paper_towns.pdf</p>
              <hr style={{ backgroundColor: "white" }} />
              <p id="text1">witness_the_night.pdf</p>
              <hr style={{ backgroundColor: "white" }} />
              <p id="text1">the_secret.pdf</p>
              <hr style={{ backgroundColor: "white" }} />
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={1} md={2}></Grid>
      </Grid>
    </div>
  );
}
