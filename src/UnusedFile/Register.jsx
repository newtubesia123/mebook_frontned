import React, { useState, useEffect } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import pic from "../textpic.png";
import { Link } from "react-router-dom";
import { useNavigate, useParams } from "react-router-dom";
import Alert from "@mui/material/Alert";
import IconButton from "@mui/material/IconButton";
import Collapse from "@mui/material/Collapse";
import CloseIcon from "@mui/icons-material/Close";

// import "./Reg.css";
export default function Register(props) {
  const navigate = useNavigate();
  const { id } = useParams();

  // const [alert, setAlert] = useState(false);
  const [showAlert, setShowAlert] = useState(null);
  const [alertContent, setAlertContent] = useState("");
  const [userTypeId, setUserTypeId] = useState(null);

  const [error, setError] = useState(false);
  const [confirmpassword, setConfirmpassword] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordValue, setPasswordValue] = useState("");
  const [submitted, setSubmitted] = useState(false);
  useEffect(() => {
    console.log(props);
    setUserTypeId(props.currId);
  }, []);
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
  });
  const handleEmailChange = (e) => {
    let item = e.target.value;
    if (item.includes("@")) {
      setEmail(false);
    } else {
      setEmail(true);
    }
    setSubmitted(true);
    const { name, value } = e.target;
    setValues((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handlePasswordChange = (e) => {
    let item = e.target.value;
    setPasswordValue(item);

    if (item.length < 4) {
      setPassword(true);
    } else {
      setPassword(false);
    }
    setSubmitted(true);
    const { name, value } = e.target;
    setValues((prevState) => ({
      ...prevState,
      [name]: value,
    }));
    // setPassword(e.target.value);
    // setSubmitted(false);
  };
  const handleChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
    setUsername(false);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();

    const registerData = {
      name: values.name,
      email: values.email,
      password: values.password,
      confirmpassword: values.confirmpassword,
      userTypeId: userTypeId,
    };
    if (email === "" && password === "" && username === "") {
      setEmail(true);
      setError(true);
      setPassword(true);
      setUsername(true);
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
      }, 5000);
      return false;
    } else if (email === true || email === "") {
      setEmail(true);
      setMail(true);
      return false;
    } else if (error === true || password === "" || password === true) {
      setPwd(true);
      setError(true);
      setPassword(true);
      return false;
    } else if (username === "") {
      setUser(true);
      setUsername(true);
      return false;
    }
    await axios
      .post("http://13.57.31.200:3002/signup", { registerData: registerData })
      .then((res) => {
        if (res.data.success) {
          navigate("/EditDetail");
          // localStorage.setItem("token", JSON.stringify(res.data.token));

          sessionStorage.setItem("id", res.data.data._id);
          sessionStorage.setItem("key", id);
          // navigate("/EditDetail");
        } else {
          setUserExist(true);
          return;
        }
      })
      .catch((err) => console.log(err));
  };

  const checkValidation = (e) => {
    setConfirmpassword(e.target.value);
    if (passwordValue !== e.target.value) {
      setError(true);
    } else {
      setError(false);
    }
    setSubmitted(true);
    const { name, value } = e.target;
    setValues((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  // Alert hooks
  const [open, setOpen] = React.useState(false);

  const [userExist, setUserExist] = React.useState(false);
  const [mail, setMail] = React.useState(false);
  const [pwd, setPwd] = React.useState(false);
  const [user, setUser] = React.useState(false);

  return (
    <div className="header">
      <Collapse in={open}>
        <Alert severity="error" variant="filled" onClose={() => setOpen(false)}>
          Must enter your details
        </Alert>
      </Collapse>

      <Collapse in={mail}>
        <Alert variant="filled" severity="error" onClose={() => setMail(false)}>
          Invalid email id
        </Alert>
      </Collapse>
      <Collapse in={pwd}>
        <Alert variant="filled" severity="error" onClose={() => setPwd(false)}>
          Invalid Password
        </Alert>
      </Collapse>
      <Collapse in={user}>
        <Alert
          variant="filled"
          severity="warning"
          onClose={() => setUser(false)}
        >
          Invalid Name
        </Alert>
      </Collapse>
      <Collapse in={userExist}>
        <Alert
          variant="filled"
          severity="info"
          onClose={() => setUserExist(false)}
        >
          User already exist
        </Alert>
      </Collapse>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 2 }}>
        <Grid item xs={1} md={2}></Grid>

        <Grid item xs={10} md={8}>
          <div className="mx-auto">
            <img
              src={pic}
              alt=""
              style={{ width: "277px", height: "42px", margin: "50px 0px" }}
            />
          </div>
          <h5>Welcome onboard</h5>
          <p style={{ margin: "10px 0px" }}>
            Let’s help connect you to readers!
          </p>
          <form style={{ margin: "50px 0px" }}>
            <div class="form-group">
              <input
                type="text"
                class="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                placeholder="Enter your Full Name"
                name="name"
                value={values.name}
                onChange={handleChange}
              />
              {username ? (
                <p style={{ color: "red", float: "left" }}>
                  Please Enter a valid Name
                </p>
              ) : (
                ""
              )}
            </div>

            <div class="form-group">
              <input
                type="email"
                class="form-control"
                id="exampleInputPassword1"
                placeholder="Enter your Email "
                name="email"
                value={values.email}
                onChange={handleEmailChange}
              />
              {email ? (
                <p style={{ color: "red", float: "left" }}>
                  Please Enter a valid Email address
                </p>
              ) : (
                ""
              )}
            </div>
            <div class="form-group">
              <input
                type="password"
                class="form-control"
                id="exampleInputPassword1"
                placeholder="Enter your Password"
                name="password"
                value={values.password}
                onChange={handlePasswordChange}
              />
              {password ? (
                <p style={{ color: "red", float: "left" }}>
                  password must be 8 character{" "}
                </p>
              ) : (
                ""
              )}
            </div>
            <div class="form-group">
              <input
                type="password"
                class="form-control"
                id="exampleInputPassword1"
                placeholder="Confirm Password "
                name="confirmpassword"
                onChange={(e) => checkValidation(e)}
                value={confirmpassword}
              />
              {error ? (
                <p style={{ color: "red", float: "left" }}>
                  Password does not match
                </p>
              ) : (
                ""
              )}
            </div>
          </form>
          <Button
            className="btn-get"
            variant="contained"
            style={{ background: "#100892", marginBottom: "10px" }}
            onClick={handleSubmit}
            value={id}
          >
            Register
          </Button>
          <p>
            Already have an account ?
            <Link
              className="nav-link"
              to={"/Login"}
              style={{ display: "inline-block" }}
            >
              Sign in
            </Link>
          </p>
        </Grid>
        <Grid item xs={1} md={2}></Grid>
      </Grid>
    </div>
  );
}
