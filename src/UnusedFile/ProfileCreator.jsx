import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link, useParams } from "react-router-dom";
import { CardActionArea } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import Collapse from "@mui/material/Collapse";
import Alert from "@mui/material/Alert";

import img from "../mepic.png";

export default function ProfileCreator() {
  const [showData, setShowData] = useState("");
  const [userName, setUserName] = useState("");
  const [success, setSuccess] = React.useState(false);

  console.log("showData---->", showData);

  const [userInfo, setuserInfo] = useState({});

  useEffect(() => {
    setSuccess(true);
    setTimeout(() => {
      setSuccess(false);
    }, 5000);
    let token = JSON.parse(localStorage.getItem("token"));
    console.log("token", token);
    // let id = sessionStorage.getItem("id");

    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        // console.log("token--->", id);
        console.log("res.data---> ", res.data);
        if (res.data.success) {
          console.log("res.datauserData---> ", res.data.userData);
          setUserName(res.data.userData.name);
          setShowData(res.data.userData.userTypeId.usertype);
          console.log("showData", showData);

          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            // file: `localhost:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });

          console.log("data---> ", res.data);
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const { id } = useParams();

  const Creator = [
    { option1: "Biography" },
    { option1: "Children’s" },
    { option1: "Comics/Graphic Novels" },
    { option1: "Composer" },
    { option1: "Crime/Mystery/ Thriller" },
    { option1: "Essay" },
    { option1: "Fantasy" },
    { option1: "Horror" },
    { option1: "Humor" },
    { option1: "Mainstream Fiction" },
    { option1: "History" },
    { option1: "Literary" },
    { option1: "Memoir" },
    { option1: "Mystery" },
    { option1: "Non-Fiction" },
    { option1: "Poet" },
    { option1: "Political" },
    { option1: "Romance" },
    { option1: "Science Fiction" },
    { option1: "Short Story" },
    { option1: "Spiritual" },
    { option1: "Spoken Word Artist" },
    { option1: "Suspense/ Thriller" },
    { option1: "Urban" },
    { option1: "Young Adult" },
  ];
  const Collaborator = [
    { option1: "Accountant" },
    { option1: "Actor-Stage/TV/Films" },
    { option1: "Agent" },
    { option1: "Book Club" },
    { option1: "Book Store" },
    { option1: "Computer Code Creator" },
    { option1: "Copy Editor" },
    { option1: "Cover Art " },
    { option1: "Director-Stage/TV/Films" },
    { option1: "Distributor" },
    { option1: "Event Planner" },
    { option1: "Financial Planner" },
    { option1: "Graphic Artist" },
    { option1: "Illustrator" },
    { option1: "Investor" },
    { option1: "Lawyer" },
    { option1: "Layout/design" },
    { option1: "Media Reviewer" },
    { option1: "Photographer" },
    { option1: "Proofreader" },
  ];
  const Influencer = [
    { option1: "Blogger" },
    { option1: "Library" },
    { option1: "Promoter" },
    { option1: "Publicist" },
    { option1: "Publisher" },
    { option1: "Reviewer" },
  ];
  const Provider = [
    { option1: "Agent" },
    { option1: "Attorney/Lawyer" },
    { option1: "Distributor" },
    { option1: "Editor/Consultant" },
    { option1: "School/Education" },
    { option1: "Specialized Services" },
    { option1: "Technology Related" },
  ];

  const [option, setOption] = React.useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setOption(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  // check box
  const [checked, setChecked] = React.useState({});
  const onValueChange = (e) => {
    setChecked(e.target.value);
  };

  return (
    <div className="container">
      <Collapse in={success}>
        <Alert
          severity="success"
          variant="filled"
          onClose={() => setSuccess(false)}
        >
          welcome to {showData} page
        </Alert>
      </Collapse>
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              backgroundColor: "#100892",
              borderRadius: "30px",
              textAlign: "center",
            }}
          >
            <CardContent>
              <div>
                {userInfo.filepreview !== null ? (
                  <Avatar
                    alt="Remy Sharp"
                    src={userInfo.filepreview}
                    sx={{
                      width: "150px",
                      height: "150px",
                      margin: "auto",
                    }}
                  />
                ) : null}
              </div>

              <Typography
                variant="body2"
                color="#FFFFFF"
                sx={{ margin: "25px 0px" }}
              >
                Welcome, {userName}
              </Typography>
            </CardContent>
          </Card>
          <div
            id="font"
            style={{
              margin: "15px 0px",
              fontSize: "3vw",
              fontWeight: "bold",
              color: "#100892",
              textAlign: "center",
            }}
          >
            <img
              src={img}
              alt=""
              style={{ height: "50px", width: "100px", fontSize: "3vw" }}
            />{" "}
            Book Profile: {showData}
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
          <Card
            sx={{
              minHeight: "500px",
              maxHeight: "3000px",
              backgroundColor: "#100892",
              borderRadius: "50px",
              m: " 25px auto 40px",
            }}
          >
            <CardContent>
              <div className="row mx-2">
                {showData === "Creator" ||
                showData === "Project Developer" ||
                showData === "Reader/Consumer" ? (
                  <>
                    {Creator.map((item) => {
                      return (
                        <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                          <Card
                            elevation={10}
                            fullwidth
                            sx={{
                              marginTop: "15px",
                              height: "120px",
                            }}
                          >
                            <Checkbox
                              disabled={false}
                              value={checked[item.id]}
                              onValueChange={(newValue) => {
                                setChecked({ ...checked, [item.id]: newValue });
                              }}
                            />

                            <CardActionArea>
                              <CardContent>
                                <Typography
                                  gutterBottom
                                  variant="h5"
                                  component="div"
                                >
                                  {item.option1}
                                </Typography>
                              </CardContent>
                            </CardActionArea>
                          </Card>
                        </div>
                      );
                    })}
                  </>
                ) : showData === "Collaborator" ? (
                  <>
                    {Collaborator.map((item) => {
                      return (
                        <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                          <Card
                            elevation={10}
                            fullwidth
                            sx={{
                              marginTop: "15px",
                              height: "120px",
                            }}
                          >
                            <Checkbox
                              disabled={false}
                              value={checked[item.id]}
                              onValueChange={(newValue) => {
                                setChecked({ ...checked, [item.id]: newValue });
                              }}
                            />

                            <CardActionArea>
                              <CardContent>
                                <Typography
                                  gutterBottom
                                  variant="h5"
                                  component="div"
                                >
                                  {item.option1}
                                </Typography>
                              </CardContent>
                            </CardActionArea>
                          </Card>
                        </div>
                      );
                    })}
                  </>
                ) : showData === "Influencer" ? (
                  <>
                    {Influencer.map((item) => {
                      return (
                        <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                          <Card
                            elevation={10}
                            fullwidth
                            sx={{
                              marginTop: "15px",
                              height: "120px",
                            }}
                          >
                            <Checkbox
                              disabled={false}
                              value={checked[item.id]}
                              onValueChange={(newValue) => {
                                setChecked({ ...checked, [item.id]: newValue });
                              }}
                            />

                            <CardActionArea>
                              <CardContent>
                                <Typography
                                  gutterBottom
                                  variant="h5"
                                  component="div"
                                >
                                  {item.option1}
                                </Typography>
                              </CardContent>
                            </CardActionArea>
                          </Card>
                        </div>
                      );
                    })}
                  </>
                ) : showData === "Provider" ? (
                  <>
                    {Provider.map((item, b) => {
                      return (
                        <div className="col-lg-3 col-md-6 col-sm-6 col-12">
                          <Card
                            elevation={10}
                            fullwidth
                            sx={{
                              marginTop: "15px",
                              height: "120px",
                            }}
                          >
                            <Checkbox
                              disabled={false}
                              value={checked[item.id]}
                              onValueChange={(newValue) => {
                                setChecked({ ...checked, [item.id]: newValue });
                              }}
                            />

                            <CardActionArea>
                              <CardContent>
                                <Typography
                                  gutterBottom
                                  variant="h5"
                                  component="div"
                                >
                                  {item.option1}
                                </Typography>
                              </CardContent>
                            </CardActionArea>
                          </Card>
                        </div>
                      );
                    })}
                  </>
                ) : (
                  ""
                )}
              </div>
            </CardContent>
          </Card>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <button
              type="button"
              class="btn btn-primary btn-lg"
              style={{ background: "#100892" }}
            >
              Go Back
            </button>
            <Link to={"/BookUpload"}>
              {" "}
              <button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
              >
                Continue
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
