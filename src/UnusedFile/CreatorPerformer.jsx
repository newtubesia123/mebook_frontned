import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import { Link, useNavigate } from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
// import "./common.css";
import { Cascader } from 'antd';
const { SHOW_CHILD } = Cascader;


const Dance = [
  { label: 'Acro', value: 'Acro' },
  { label: 'Argentine tango', value: 'Argentine tango' },
  { label: 'Ballet', value: 'Ballet' },
  { label: 'Ballroom', value: 'Ballroom' },
  { label: 'Bolero', value: 'Bolero' },
  { label: 'Breakdancing', value: 'Breakdancing' },
  { label: 'Cha-cha-cha', value: 'Cha-cha-cha' },
  { label: 'Contemporary', value: 'Contemporary' },
  { label: 'East Coast Swing', value: 'East Coast Swing' },
  { label: 'Folk', value: 'Folk' },
  { label: 'Foxtrot', value: 'Foxtrot' },
  { label: 'Hip Hop', value: 'Hip Hop' },
  { label: 'Jazz', value: 'Jazz' },
  { label: 'Lyrical', value: 'Lyrical' },
  { label: 'Mambo', value: 'Mambo' },
  { label: 'Modern', value: 'Modern' },
  { label: 'Pasodoble', value: 'Pasodoble' },
  { label: 'Popping', value: 'Popping' },
  { label: 'Rumba', value: 'Rumba' },
  { label: 'Samba', value: 'Samba' },
  { label: 'Swing', value: 'Swing' },
  { label: 'Tap', value: 'Tap' },
  { label: 'Viennese waltz', value: 'Viennese waltz' },
  { label: ' Waltz', value: 'Waltz' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Comedy = [
  { label: 'Dark', value: 'Dark' },
  { label: 'Deadpan', value: 'Deadpan' },
  { label: 'Farce', value: 'Farce' },
  { label: 'High comedy', value: 'High comedy' },
  { label: 'Observational', value: 'Observational' },
  { label: 'Parody', value: 'Parody' },
  { label: 'Romantic', value: 'Romantic' },
  { label: 'Selfdeprecating humor', value: 'Selfdeprecating humor' },
  { label: 'Slapstick', value: 'Slapstick' },
  { label: 'Surreal', value: 'Surreal' },
  { label: 'Tragicomedy', value: 'Tragicomedy' },
  { label: 'Wordplay', value: 'Wordplay' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
const Lecture = [
  {
    label: <b style={{ fontSize: '20px' }}>Category</b>, value: 'Category',
    children: [
      { label: 'Formal', value: 'Formal' },
      { label: 'Interactive', value: 'Interactive' },
      { label: 'Lecture-discussion', value: 'Lecture-discussion' },
      { label: 'Semi-formal', value: 'Semi-formal' },
      { label: 'Socratic', value: 'Socratic' },
      { label: 'Suggest Tag', value: 'Suggest Tag' },
    ]
  },
  {
    label: <b style={{ fontSize: '20px' }}>Content</b>, value: 'Content',
    children: [
      { label: 'Expository essay', value: 'Expository essay' },
      { label: 'Storytelling', value: 'Storytelling' },
      { label: 'Demonstration', value: 'Demonstration' },
      { label: 'Problem-solving', value: 'Problem-solving' },
      { label: 'Suggest Tag', value: 'Suggest Tag' },
    ]
  },
  {
    label: <b style={{ fontSize: '20px' }}>Medium</b>, value: 'Medium',
    children: [
      { label: 'Naked', value: 'Naked' },
      { label: 'Chalk and Talk', value: 'Chalk and Talk' },
      { label: 'Multimedia', value: 'Multimedia' },
      { label: 'Video', value: 'Video' },
      { label: 'Suggest Tag', value: 'Suggest Tag' },
    ]
  }
]
const Recitals = [
  {
    label: <b style={{ fontSize: '18px' }}>Music</b>, value: 'Music',
    children: [
      { label: 'Solo', value: 'Solo' },
    ]
  },
  { label: 'Duet', value: 'Duet' },
  { label: 'Trio', value: ' Trio' },
  { label: 'Quartet', value: 'Quartet' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
  {
    label: <b style={{ fontSize: '18px' }}>Dance</b>, value: 'Dance',
    children: [
      { label: 'Solo', value: 'Solo' },
    ]
  },
  { label: 'Duet', value: 'Duet' },
  { label: 'Troupe', value: 'Troupe' },
  { label: 'Company', value: 'Company' },

]
const Poetry = [
  { label: 'Acrostic', value: 'Acrostic' },
  { label: 'Ballad', value: 'Ballad' },
  { label: 'Cinquain', value: 'Cinquain' },
  { label: 'Concrete', value: 'Concrete' },
  { label: 'Couplet', value: 'Couplet' },
  { label: 'Elegy', value: 'Elegy' },
  { label: 'Epic', value: 'Epic' },
  { label: 'Epigram', value: 'Epigram' },
  { label: 'Epitaph', value: 'Epitaph' },
  { label: 'Free verse', value: 'Free verse' },
  { label: 'Haiku', value: 'Haiku' },
  { label: 'Limerick', value: 'Limerick' },
  { label: 'Lyric', value: 'Lyric' },
  { label: 'Narrative', value: 'Narrative' },
  { label: 'Ode', value: 'Ode' },
  { label: 'Petrarchan sonnet', value: 'Petrarchan sonnet' },
  { label: 'Prose', value: 'Prose' },
  { label: 'Quatrain', value: 'Quatrain' },
  { label: 'Shakespeare sonnets', value: 'Shakespeare sonnets' },
  { label: 'Sonnet', value: 'Sonnet' },
  { label: 'Villanelle', value: 'Villanelle' },
  { label: 'Suggest Tag', value: 'Suggest Tag' },
]
export default function CreatorPerformer() {
  const navigate = useNavigate();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const user = sessionStorage.getItem('userName')
  var identity = sessionStorage.getItem('identity Option')
  let workProfile = localStorage.getItem("Work profile")
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });
  useEffect(() => {
    let token = JSON.parse(localStorage.getItem("token"));
    axios
      .get("http://54.246.61.54:3002/getuserById", {
        headers: { Authorization: `Bearer ${token}` },
      })

      .then((res) => {
        console.log("res.data---> ", res.data);
        if (res.data) {
          setuserInfo({
            ...userInfo,
            file: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
            filepreview: `http://54.246.61.54:3002/uploads/${res.data.userData.pdf}`,
          });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const arry = [
    { option1: "Dance ➢", id: '1' },
    { option1: "Humor/Stand-up ➢", id: '2' },
    { option1: "Lecture ➢", id: '3' },
    { option1: "Music➢", id: '4' },
    { option1: "Pitch ➢", id: '5' },
    { option1: "Poetry➢", id: '6' },
    { option1: "Spoken Word ➢", id: '7' },
    { option1: "PR/Promotion", id: '8' },
    { option1: "Recital ➢", id: '9' },
    { option1: "Rights, Brand, Royalties", id: '10' },
  ];
  const [checkValue, setCheckValue] = useState([])
  const handleChange = (val) => {
    let temp = checkValue;
    if (temp.some((item) => item.id === val.id)) {
      temp = temp.filter((item) => item.id !== val.id);
    } else {
      temp.push(val);
    }
    setCheckValue(temp);
  };
  const handleSubmit = (e) => {
    navigate('/CompletionPhase', { state: { option: 'Performer' } })
  };
  let Array = ['Performer'];
  for (let i = 0; i < checkValue.length; i++) {
    Array.push(checkValue[i].option1);
    localStorage.setItem('MultiOptions', JSON.stringify(Array))

  }
  return (
    <div className="container-fluid MebookMetaAuthor">
      <div className="img text-center">
        <img
          src={image}
          alt=""
          style={{
            width: "232.5px",
            height: "131px",
            margin: "20px 0px",
          }}
        />
      </div>
      <div className="row">
        <div className="col-md-8 offset-md-2 text-center">

          <CardContent>
            <div>
              <Avatar
                alt="Remy Sharp"
                src={userInfo.filepreview}
                sx={{
                  width: "150px",
                  height: "130px",
                  margin: "auto",
                }}
              />
            </div>

            <Typography
              variant="body2"
              color=""
              sx={{
                margin: "15px 0px",
                fontSize: "25px"
              }}
            >
              {user},  Performer
            </Typography>
          </CardContent>

          <div
            className="head"
            style={{
              margin: "15px 0px",
              fontSize: "2.5vw",
              fontWeight: "bold",
              color: "",
              textAlign: "center",
              fontFamily: "Times New Roman"

            }}
          >
            <span style={{ color: '' }}>M</span><span style={{ color: '' }}>ebook</span> <span style={{ color: '' }}>M</span><span style={{ color: '' }}>eta</span> Book Profile: Performer
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-8 offset-md-2">

          <CardContent>
            <div className="row mx-2">

              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Dance}
                  // onChange={onChange1}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  defaultValue={<b style={{ fontSize: '20px' }}>Dance</b>}
                />

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Comedy}
                  // onChange={onChange1}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  defaultValue={<b style={{ fontSize: '20px' }}>Comedy/Humor/Stand-up</b>}
                />

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Lecture}
                  // onChange={onChange1}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  defaultValue={<b style={{ fontSize: '20px' }}>Lecture</b>}
                />

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Recitals}
                  // onChange={onChange1}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  defaultValue={<b style={{ fontSize: '20px' }}>Recitals/Concerts</b>}
                />

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  options={Poetry}
                  // onChange={onChange1}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  defaultValue={<b style={{ fontSize: '20px' }}>Poetry</b>}
                />

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  // options={Poetry}
                  // onChange={onChange1}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  defaultValue={<b style={{ fontSize: '20px' }}>Producer</b>}
                />

              </div>
              <div className=" col-md-6 col-sm-6 col-12 mt-4">
                <Cascader
                  style={{
                    width: '100%',
                  }}
                  size="large"
                  // options={Poetry}
                  // onChange={onChange1}
                  multiple
                  expandIcon={<b><ArrowForwardIosIcon sx={{ fontSize: '20px', color: 'black' }} /> </b>}

                  maxTagCount="responsive"
                  showCheckedStrategy={SHOW_CHILD}
                  defaultValue={<b style={{ fontSize: '20px' }}>Promotion</b>}
                />

              </div>

            </div>
          </CardContent>

          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              You have Selected {checkValue.length} Options
            </DialogTitle>
            <DialogContent>
              {checkValue !== null ? (
                <>
                  {Array.map((i, index) => (
                    <p style={{ display: 'inline', fontWeight: 'bold', fontSize: '25px', color: '#100892' }} key={index}>
                      {i},
                    </p>
                  ))}
                </>
              ) : null}
              <DialogContentText id="alert-dialog-description">
                If want to Continue Please click on Agree otherwise click on Disagree
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Disagree</Button>
              <Button onClick={handleSubmit} autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>
          <div className="d-flex justify-content-between mt-3 mb-3">
            <Link to={'/CreatorNew1'}><button
              type="button"
              class="btn  btn-lg"
              style={{ background: "white" }}
            >
              Go Back
            </button></Link>
            <button
              type="button"
              class="btn  btn-lg"
              style={{ background: "white" }}
              onClick={handleClickOpen}
            >
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
