import React, { useState, useEffect } from "react";
import axios from "axios";
import image from "../mepic.png";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";
import picture from "../Screenshot_2.png";
import { Link } from "react-router-dom";
import img from "../mepic.png";
// import "./common.css";
import CommonAppbar from "../Component new/CommonAppbar";
import WriterNav from "../Component new/WriterNav";
export default function MainStage() {
  sessionStorage.setItem("identity Option", "Main Stage");
  // console.log("first",JSON.parse(user).name)
  const [userInfo, setuserInfo] = useState({
    pdf: "",
    file: "",
    filepreview: null,
  });

  return (
    <div className="container-fluid MebookMetaimage">
      {/* <WriterNav /> */}
      <div className="row">
        <div className="col-md-3"></div>
        <div
          className="col-md-6"
          style={{
            textAlign: "center",
            display: "flex",
            justifyContent: "center",
            display: "block",
          }}
        >
          <img
            src={image}
            alt=""
            style={{
              width: "120px",
              height: "61.24px",
            }}
          />

          <div>
            <img
              alt="Remy Sharp"
              src={userInfo.filepreview}
              style={{
                width: "150px",
                height: "130px",
                margin: "auto",
                borderRadius: "50%",
              }}
            />
          </div>

          <Typography
            variant="body2"
            sx={{ margin: "15px 0px", fontSize: "30px" }}
          >
           Identity, Main Stage
          </Typography>

          <Typography
            className="head"
            gutterBottom
            component="div"
            sx={{
              margin: "10px auto 20px ",
              fontSize: "2.3vw",
              fontWeight: "bold",

              width: "70%",
            }}
          >
            Set Up Your <span style={{}}>M</span>
            <span style={{}}>eBook</span>
            <span style={{}}>M</span>
            <span style={{}}>eta</span> Identity Profile: Main Stage
          </Typography>

          <div
            style={{
              backgroundColor: "white",
              margin: "25px 100px 20px ",
              borderRadius: "10px",
              color: "black",
            }}
          >
            <h3
              style={{
                padding: "8px 0px",
                fontFamily: "Times New Roman",
              }}
            >
              Main Stage
            </h3>
          </div>
          <div
            style={{
              backgroundColor: "white",
              margin: "25px 30px 20px ",
              borderRadius: "50px",
              color: "black",
            }}
          >
            <p
              style={{
                padding: "20px 20px",
                textAlign: "justify",
                fontFamily: "Times New Roman",
              }}
            >
              <b>
                {" "}
                This identity is reserved for creators, collaborators,
                influencers, providers, project developers and reader/consumers
                who are willing to pay a small premium to be certain that their
                profile is continually vetted*, free of errors, and fully
                functional on an ongoing monthly basis. This identity is
                purposed to provide and an extra layer of professionalism for
                profile presenters and profile seekers. *(content, claims and
                credentials will be verified by an investigative team for
                accuracy)
              </b>
            </p>
          </div>

          <div className="d-flex justify-content-between mb-5">
            <div>
              <Link to={"/Identity"}>
                {" "}
                <button
                  type="button"
                  class="btn  btn-lg"
                  style={{ background: "white" }}
                >
                  Go Back
                </button>
              </Link>
            </div>
            {/* <div>
              <button
                type="button"
                class="btn btn-primary btn-lg"
                style={{ background: "#100892" }}
              >
                Choose
              </button>
            </div> */}
            <div>
              {" "}
              <Link to={"/UserChoice"}>
                <button
                  type="button"
                  class="btn btn-lg"
                  style={{ background: "white" }}
                >
                  Continue
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-3"></div>
      </div>
    </div>
  );
}
