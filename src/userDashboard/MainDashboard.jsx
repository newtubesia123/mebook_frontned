import React, { useCallback, useEffect, useState } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import Sidebar from './sidebar/Sidebar'
import './MainDashboard.css'
import { useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import Reminder from '../Component new/Reminder/Reminder'
import ScreenBlock from '../component/ScreenBlock/ScreenBlock'
import { Button } from '@mui/material';

export default function MainDashboard() {
  const STORE = useSelector((state) => state)
  const  navigate =useNavigate()
  // !STORE?.getProfileData?.userData?.identityProfile || 
  // useEffect(()=>{
  //   if (!STORE?.getProfileData?.userData?.identityProfile || !STORE?.getProfileData?.userData?.metaProfile) {
  //     localStorage.removeItem("token")  // access from dashboard
  //     toast.error("access denied", {
  //       position: 'top-center',
  //       autoClose: 1000,
  
  //     })
  //     navigate('/SignIn')
  //   }
  // },[STORE?.getProfileData?.userData])
  const [openModal, setOpenModal] = useState(false);
const [getNow,setGetNow]=useState('')
  return (
    <div className='AppGlass'>
      <Sidebar interchage={(name)=>setGetNow(name)}/>
      <Outlet/>
      {/* <Reminder getNow={getNow}/>       */}
      {/* {openModal?
        <ScreenBlock setOpenModal={setOpenModal} openModal={openModal}/>
        :''
      } */}
      
    </div>
  )
}
