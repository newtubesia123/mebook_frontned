import React, { useContext, useEffect, useRef, useState } from "react";
import "./sidebar.css";
import {
  sidebarItem,
  searchDataItem,
  suggestionDataItem,
  suggestionCategoryItem,
  suggestionSubcategoryItem,
  suggestionStatusItem,
} from "../../Component new/User Dashboard/sideBarItem/SideBar";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getAuth, getUserData, notificationRequest, userDashboardDetail } from "../../Action";

// mui app bar with responsive sidebar
import { io } from "socket.io-client";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import ChatIcon from "@mui/icons-material/Chat";

import YoutubeSearchedForIcon from "@mui/icons-material/YoutubeSearchedFor";
import SearchIcon from "@mui/icons-material/Search";

import Avatar from "@mui/material/Avatar";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { SERVER } from "../../server/server";
import Slide from "@mui/material/Slide";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import NotificationIcon from "./NotificationIcon/NotificationIcon";
import SearchBar from "./SearchBar/SearchBar";

import LogoutModal from "./LogoutModal";
import ScreenBlock from "../../component/ScreenBlock/ScreenBlock";
import { getImgURL } from "../../util/getImgUrl";
import MeCircularProgress from "../../components/componentsC/meCircularProgress/MeCircularProgress";
import { Badge } from "@mui/material";
import { SOCKET } from "../../App";
import { getRecognization } from "../../Action/messages/actions";
import { useOwnIdentity } from "../../Hooks/useOwnIdentity";

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const drawerWidth = 240;
// const navItems = ['Home', 'About', 'Contact'];

export default function Sidebar(props) {
  const [cnt, setCnt] = useState("");
  const [isBlock, setIsBlock] = useState(false);
  const [CircularProgress, setCircularProgress] = useState(false);
  const [countExp, setCountExp] = useState("");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [select, setSelect] = useState(0);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  // search bar
  const [filteredData, setFilteredData] = useState([]);
  const [categoryData, setCategoryData] = useState([]);
  const [statusData, setStatusData] = useState([]);
  const [dataResult, setDataResult] = useState(false);
  const [searchLoading, setSearchLoading] = useState(false);
  const [wordEntered, setWordEntered] = useState("");
  const [searchedData, setSearchedData] = useState({ user: [], work: [] });
  // search bar end
  const [subs, setSubs] = useState(null);
  const STORE = useSelector((state) => state);
  let {ownId} = useOwnIdentity();
  const STORE_UNSEEN_MESSAGE = useSelector(
    (state) => state.notificationRequest.unSeenMessage
  );
  const inPayPage = window?.location?.href?.includes(
    "/maindashboard/NewPayPage"
  );

// connecting user with socket-conn
// useEffect(()=>{
//   console.log("use-effect is calling")
//   // if(global.SOCKET.active){
//   //     console.log("socket-active",global.SOCKET.active,"ownId",ownId)
//   //     if(ownId){
//   //         console.log("connecting calling with",ownId)
//   //         global.SOCKET.emit("add-user", ownId);
//   //       }else{
//   //         console.log("not connecting as Ownid is",ownId)
//   //       }
//   // }
//   global.SOCKET.on('connect',()=>{
//     console.log("globalCon",global.SOCKET.connected,"ownId",ownId)
//     if(ownId){
//       console.log("connecting calling with",ownId)
//       global.SOCKET.emit("add-user", ownId);
//     }
//   })
// },[ownId,global.SOCKET])


  useEffect(() => {
    dispatch(getUserData());
    dispatch(userDashboardDetail());
  }, [dispatch]);

  //Below useEffect will redirect to payment page if not done till. But developer can comment this during working

  useEffect(() => {
    setSubs(
      STORE?.userDashboardDetail?.userWorkDetail?.subscriptionType?.isSubscribed
    );
  }, [STORE]);
  useEffect(() => {
    if(subs===false){
            navigate('NewPayPage')
    }
    if(getAuth()){
      dispatch(notificationRequest(0, { send: (e) => null}))
    }
  }, [window?.location?.href,subs])

  //Above useEffect will redirect to payment page if not done till. But developer can comment this during working
  const logOut = () => {
    setCircularProgress(true);
    localStorage.removeItem("token");
    SOCKET.emit("remove-user", ownId);
    // global.SOCKET.disconnect();
    ownId = null
    dispatch({ type: "USER_LOGOUT" });
    dispatch({ type: "CALLING__PROGRESS", payload: 10 });
    setTimeout(() => {
      dispatch({ type: "COMPLETE__PROGRESS", payload: 100 });
      navigate("/");
    }, 1500);
  };

  const [show, setShow] = useState(true);
  const [lastScrollY, setLastScrollY] = useState(0);

  const controlNavbar = () => {
    if (typeof window !== "undefined") {
      if (window.scrollY > lastScrollY) {
        // if scroll down hide the navbar
        setShow(false);
      } else {
        // if scroll up show the navbar
        setShow(true);
      }

      // remember current page location to use in the next move
      setLastScrollY(window.scrollY);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", controlNavbar);

      // cleanup function
      return () => {
        window.removeEventListener("scroll", controlNavbar);
      };
    }
  }, [lastScrollY]);

  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box
      onClick={handleDrawerToggle}
      sx={{
        textAlign: "center",
        background: "#181823",
        minHeight: "100vh",
        color: "#fff",
      }}
    >
      <Typography
        variant="h5"
        sx={{
          my: 2,
          fontSize: "1rem",
          fontWeight: "bold",
          background: "#fff",
          textAlign: "center",
          color: "black",
        }}
      >
        <span style={{ color: "rgb(187 11 11)" }}>M</span>
        <span style={{ color: "#0a0a8a" }}>eBook</span>
        <span style={{ color: "rgb(187 11 11)" }}>M</span>
        <span style={{ color: "#0a0a8a" }}>eta</span>
        <br />
        <span style={{ fontSize: "1rem" }}>
          {STORE?.getProfileData?.userData?.name?.toUpperCase()}
        </span>
      </Typography>
      <Divider />
      <List>
        {/* {navItems.map((item) => (
          
        ))} */}

        {sidebarItem.map((item, index) => (
          <ListItem
            key={"listItem" + index}
            disablePadding
            sx={{ textAlign: "justify" }}
          >
            <ListItemButton
              sx={{ textAlign: "" }}
              onClick={() => {
                // getUserId().then((e)=>console.log(e))
                setSelect(index);
                // navigate(item.link);
                navigate(
                  item.link === "UserProfile"
                    ? `${item.link}/${STORE.getProfileData?.userData?.name}/${STORE.getProfileData?.userData?._id}`
                    : item.link === "LogOut"
                    ? logOut()
                    : item.link
                );
                setAnchorEl(null);
              }}
            >
              {index == 3 ? (
                STORE?.getProfileData?.userData?.pdf === null ? (
                  <Avatar
                    sx={{ width: 37, height: 37, border: "2px solid #1C6DD0" }}
                    src={`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`}
                  ></Avatar>
                ) : (
                  <Avatar
                    sx={{ mr: 1, width: 30, height: 30, color: "#1C6DD0" }}
                    src={getImgURL(STORE?.getProfileData?.userData?.pdf)}
                  ></Avatar>
                )
              ) : (
                item.icon
              )}
              <ListItemText>{item.listItem}</ListItemText>
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      {/* {openModal && <LogoutModal setOpenModal={setOpenModal} />} */}
    </Box>
  );

  const container =
    window !== undefined ? () => window.document.body : undefined;

  // search bar call
  function handleFilteredCall(id) {
    // console.log(id)
  }
  const handleFilter = (event) => {
    const searchWord = event.target.value;
    setWordEntered(searchWord);
    const newFilter = suggestionDataItem.filter((value) => {
      return value.title.toLowerCase().includes(searchWord.toLowerCase());
    });
    const filterCategoryItem = suggestionCategoryItem.filter((value) => {
      return value.title.toLowerCase().includes(searchWord.toLowerCase());
    });
    const filterStatusItem = suggestionStatusItem.filter((value) => {
      return value.title.toLowerCase().includes(searchWord.toLowerCase());
    });
    if (searchWord === "") {
      setFilteredData([]);
      setCategoryData([]);
      setStatusData([]);
    } else {
      setFilteredData(newFilter);
      setCategoryData(filterCategoryItem);
      setStatusData(filterStatusItem);
    }
  };

  // search bar call end

  // useEffect(()=>{
  //   console.log(filteredData)
  // },[filteredData])

  // to activate expiry timer starts

  useEffect(() => {
    var x = setInterval(function () {
      // Get today's date and time
      var now = new Date().getTime();
      // Find the distance between now and the count down date
      var distance =
        new Date(
          STORE.userDashboardDetail?.userWorkDetail?.subscriptionType?.trialExpiry
        ).getTime() - now;
      // below distance is for testing

      // var distance = new Date('2023-04-07T17:15:25.286Z').getTime() - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Output the result in an element with id="demo"
      // console.log(days + "d " + hours + "h "
      // + minutes + "m " + seconds + "s ")
      setCountExp(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");
      props.interchage(
        days + "d " + hours + "h " + minutes + "m " + seconds + "s "
      );
      // If the count down is over, write some text
      if (distance <= 0) {
        clearInterval(x);
        setIsBlock(true);
        console.log("EXPIRED");
      }
    }, 1000);
    return () => clearInterval(x);
  }, [
    STORE.userDashboardDetail?.userWorkDetail?.subscriptionType?.trialExpiry,
  ]);

  // Update the count down every 1 second

  // to activate expiry timer ends

  return (
    <>
      {CircularProgress && <MeCircularProgress />}

      <Box
        sx={{ display: "flex", marginBottom: { sm: 0, md: 4 } }}
        className={`active ${show && "hidden"}`}
      >
        <CssBaseline />
        <HideOnScroll {...props}>
          <AppBar
            component="nav"
            sx={{
              opacity: "",
              background: { xs: "none", sm: "none", md: "#000000" },
              border: "none",
              boxShadow: "none",
            }}
          >
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                edge="start"
                onClick={handleDrawerToggle}
                sx={{
                  mr: 2,
                  display: { xs: "block", sm: "block", md: "none" },
                }}
              >
                <MenuIcon sx={{ width: 35, height: 35, color: "#1C6DD0" }} />
              </IconButton>
              <Typography
                variant="h6"
                component="div"
                sx={{
                  flexGrow: 1,
                  display: { xs: "none", sm: "none", md: "block" },
                  color: "red",
                  fontSize: "2rem",
                  marginTop: "",
                  fontWeight: "bold",
                  textShadow: "0.2px 0.5px white",
                }}
              >
                {" "}
                <span style={{ color: "rgb(187 11 11)" }}>M</span>
                <span style={{ color: "#0a0a8a" }}>eBook</span>
                <span style={{ color: "rgb(187 11 11)" }}>M</span>
                <span style={{ color: "#0a0a8a" }}>eta</span>
              </Typography>

              <SearchBar
                placeholder="Search the Universe..."
                setSearchedData={setSearchedData}
                handleFilter={handleFilter}
                wordEntered={wordEntered}
                setWordEntered={setWordEntered}
                data={searchDataItem}
                filteredData={filteredData}
                setDataResult={setDataResult}
                setFilteredData={setFilteredData}
                setCategoryData={setCategoryData}
                searchLoading={searchLoading}
                setSearchLoading={setSearchLoading}
                statusData={statusData}
                setStatusData={setStatusData}
              />

              <div className="dataResultParent">
                {dataResult || filteredData.length ? (
                  <div className="dataResult">
                    <div className="dataResultchild">
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          height: "250px",
                        }}
                      >
                        <h6>Quick Search In Identity/Identity Status</h6>
                        <div
                          style={{
                            padding: "10px",
                            overflowY: "scroll",
                            overflowX: "hidden",
                          }}
                        >
                          {filteredData.length ||
                          // categoryData.length ||
                          statusData.length ? (
                            <>
                              {filteredData.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      // handleFilteredCall(value.id);
                                      navigate(
                                        `/MainDashboard/Search/${value?.query}/${value?.title}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon /> {value.title}{" "}
                                    </div>
                                  </div>
                                );
                              })}
                              {/* {
                                      categoryData.map((value, key) => {
                                        return (
                                          <div className="dataItem" onClick={() => {
                                            // handleFilteredCall(value.id);
                                            navigate(`/MainDashboard/Search/${value?.query}/${value?.title}`)
                                          }}>
                                            <div><SearchIcon /> {value.title} </div>
                                          </div>
                                        );
                                      })
                                    } */}
                              {statusData.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      handleFilteredCall(value.id);
                                      navigate(
                                        `/MainDashboard/Search/${value?.query}/${value?.title}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon />{" "}
                                      <span style={{ color: "blue" }}>
                                        {value.title}
                                      </span>{" "}
                                    </div>
                                  </div>
                                );
                              })}
                            </>
                          ) : (
                            <>
                              {suggestionDataItem.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      // handleFilteredCall(value.id);
                                      navigate(
                                        `/MainDashboard/Search/${value?.query}/${value?.title}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon /> {value.title}{" "}
                                    </div>
                                  </div>
                                );
                              })}
                              {/* {
                                     suggestionCategoryItem.map((value, key) => {
                                      return (
                                        <div className="dataItem" onClick={() => {
                                          // handleFilteredCall(value.id)
                                          navigate(`/MainDashboard/Search/${value?.query}/${value?.title}`)
                                        }}>
                                          <div><SearchIcon /> {value.title} </div>
                                        </div>
                                      );
                                    })
                                  } */}
                              {suggestionStatusItem.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      handleFilteredCall(value.id);
                                      navigate(
                                        `/MainDashboard/Search/${value?.query}/${value?.title}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon />{" "}
                                      <span style={{ color: "blue" }}>
                                        {value.title}
                                      </span>{" "}
                                    </div>
                                  </div>
                                );
                              })}
                            </>
                          )}
                          {/* {
                                filteredData.length ?
                                  filteredData.map((value, key) => {
                                    return (
                                      <div className="dataItem" onClick={() => {
                                        handleFilteredCall(value.id);
                                        navigate(`/MainDashboard/Search/${value?.query}`)
                                      }}>
                                        <div><SearchIcon /> {value.title} </div>
                                      </div>
                                    );
                                  })
                                  :
                                  suggestionDataItem.map((value, key) => {
                                    return (
                                      <div className="dataItem" onClick={() => {
                                        handleFilteredCall(value.id);
                                        navigate(`/MainDashboard/Search/${value?.query}`)
                                      }}>
                                        <div><SearchIcon /> {value.title} </div>
                                      </div>
                                    );
                                  })
                              }
                              {
                                categoryData.length ?
                                  categoryData.map((value, key) => {
                                    return (
                                      <div className="dataItem" onClick={() => {
                                        handleFilteredCall(value.id);
                                        navigate(`/MainDashboard/Search/${value?.query}`)
                                      }}>
                                        <div><SearchIcon /> {value.title} </div>
                                      </div>
                                    );
                                  })
                                  :
                                  suggestionCategoryItem.map((value, key) => {
                                    return (
                                      <div className="dataItem" onClick={() => {
                                        handleFilteredCall(value.id)
                                        navigate(`/MainDashboard/Search/${value?.query}`)
                                      }}>
                                        <div><SearchIcon /> {value.title} </div>
                                      </div>
                                    );
                                  })
                              } */}
                        </div>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          height: "250px",
                        }}
                      >
                        <h6>Quick Search In Category</h6>
                        <div
                          style={{
                            padding: "10px",
                            overflowY: "scroll",
                            overflowX: "hidden",
                          }}
                        >
                          {/* {
                                searchedData?.subCategory?.length ?
                                  searchedData.subCategory.map((value, key) => {
                                    return (
                                      <div className="dataItem"
                                        onClick={() => {
                                          // handleFilteredCall(value.id);
                                          navigate(`/MainDashboard/Search/${value?._id}/${value?.name}`)
                                        }}
                                      >
                                        <div><SearchIcon /> {value.name} </div>
                                      </div>
                                    );
                                  })
                                  :
                                  suggestionSubcategoryItem.map((value, key) => {
                                    return (
                                      <div className="dataItem" onClick={() => {
                                        // handleFilteredCall(value.id)
                                        navigate(`/MainDashboard/Search/${value?.query}/${value?.title}`)
                                      }}>
                                        <div><SearchIcon /> {value.title} </div>
                                      </div>
                                    );
                                  })
                              } */}

                          {categoryData.length ? (
                            <>
                              {categoryData.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      // handleFilteredCall(value.id);
                                      navigate(
                                        `/MainDashboard/Search/${value?.query}/${value?.title}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon /> {value.title}{" "}
                                    </div>
                                  </div>
                                );
                              })}
                            </>
                          ) : (
                            <>
                              {suggestionCategoryItem.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      // handleFilteredCall(value.id)
                                      navigate(
                                        `/MainDashboard/Search/${value?.query}/${value?.title}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon /> {value.title}{" "}
                                    </div>
                                  </div>
                                );
                              })}
                            </>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="dataResultchild">
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          height: "250px",
                        }}
                      >
                        <h6>Results In Subcategory</h6>
                        <div
                          style={{
                            padding: "10px",
                            overflowY: "scroll",
                            overflowX: "hidden",
                          }}
                        >
                          {searchedData?.subCategory?.length
                            ? searchedData.subCategory.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      // handleFilteredCall(value.id);
                                      navigate(
                                        `/MainDashboard/Search/${value?._id}/${value?.name}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon /> {value.name}{" "}
                                    </div>
                                  </div>
                                );
                              })
                            : suggestionSubcategoryItem.map((value, key) => {
                                return (
                                  <div
                                    className="dataItem"
                                    onClick={() => {
                                      // handleFilteredCall(value.id)
                                      navigate(
                                        `/MainDashboard/Search/${value?.query}/${value?.title}`
                                      );
                                    }}
                                  >
                                    <div>
                                      <SearchIcon /> {value.title}{" "}
                                    </div>
                                  </div>
                                );
                              })}
                        </div>
                      </div>

                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          height: "250px",
                        }}
                      >
                        <h6>Results In User Profile</h6>
                        <div
                          style={{
                            padding: "10px",
                            overflowY: "scroll",
                            overflowX: "hidden",
                          }}
                        >
                          {searchedData.user?.length ? (
                            searchedData.user.map((value, key) => {
                              return (
                                <div
                                  className="dataItem"
                                  style={{ justifyContent: "space-between" }}
                                  key={value._id}
                                  onClick={() =>
                                    navigate(
                                      `/MainDashboard/UserProfile/${value.name}/${value._id}`
                                    )
                                  }
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                  >
                                    <IconButton
                                      size="small"
                                      sx={{
                                        display: {
                                          xs: "none",
                                          sm: "none",
                                          md: "block",
                                        },
                                      }}
                                      aria-controls={
                                        open ? "account-menu" : undefined
                                      }
                                      aria-haspopup="true"
                                      aria-expanded={open ? "true" : undefined}
                                    >
                                      <Avatar
                                        sx={{
                                          width: 27,
                                          height: 27,
                                          border: "2px solid #1C6DD0",
                                        }}
                                        src={getImgURL(value.pdf)}
                                      ></Avatar>
                                    </IconButton>
                                    <div>{value.name} </div>
                                  </div>
                                  <div
                                    style={{
                                      padding: ".3rem",
                                      color: "blue",
                                      background: "lightgray",
                                      borderRadius: "5px",
                                    }}
                                  >
                                    view profile
                                  </div>
                                </div>
                              );
                            })
                          ) : (
                            <div className="dataItem">
                              <p style={{ textAlign: "center" }}>
                                Oops no user found
                              </p>
                            </div>
                          )}
                        </div>
                      </div>
                      {/* <div style={{ display: 'flex', flexDirection: 'column', height: '250px', }}>
                            <h6>Results In User Work</h6>
                            <div style={{ padding: '10px', overflowY: 'scroll', overflowX: 'hidden' }}>
                              {searchedData.work?.length ?
                                searchedData.work?.map((value, key) => {
                                  return (
                                    <div className="dataItem" style={{ justifyContent: "space-between" }}
                                      onClick={() => navigate(`/MainDashboard/WorkView/${value.userId}/${value._id}`)}
                                    >
                                      <div style={{ display: 'flex', alignItems: 'center' }}>
                                        <img src={getImgURL(value.coverPicture)} width="35px" alt="work img" />
                                        <div style={{ paddingLeft: '1rem' }}>{value.title?.slice(0, 30)}... </div>
                                      </div>
                                      <div style={{ paddingRight: '1rem' }}>
                                        <Link to={`WorkView/${value.userId}/${value._id}`}>
                                          view work
                                        </Link>
                                      </div>

                                    </div>
                                  );
                                })
                                :
                                <div className="dataItem" style={{ textAlign: "center" }}>
                                  <p className="text-center">Oops no work found</p>
                                </div>
                              }
                            </div>
                          </div> */}
                    </div>
                  </div>
                ) : (
                  ""
                )}
              </div>

              <Box sx={{ display: { xs: "none", sm: "none", md: "block" } }}>
                {sidebarItem.map((item, index) =>
                  index === 0 ? (
                    <Button
                      key={index}
                      sx={{ color: "#1C6DD0" }}
                      onClick={() => {
                        setSelect(index);
                        navigate(
                          item.link === "UserProfile"
                            ? `${item.link}/${STORE.getProfileData?.userData?.name}/${STORE.getProfileData?.userData?._id}`
                            : item.link === "LogOut"
                            ? logOut()
                            : item.link === "WriterStoryDetails"
                            ? `${item.link}/${STORE.getProfileData?.userData?._id}`
                            : item.link
                        );
                      }}
                    >
                      {item.listItem}
                      {/* {item.icon} */}
                    </Button>
                  ) : (
                    ""
                  )
                )}
                {/* {openModal && <LogoutModal setOpenModal={setOpenModal} />} */}
              </Box>
              <Button
                sx={{
                  color: "#1C6DD0",
                  display: { xs: "none", sm: "none", md: "block" },
                }}
                onClick={handleClick}
                variant="outlined"
              >
                {STORE?.getProfileData?.userData?.name?.split(" ")[0]}
              </Button>
              {/* <Box
                sx={{
                  gap: "1rem",
                  display: { xs: "none", sm: "none", md: "flex" },
                }}
              >
                <Badge badgeContent={STORE_UNSEEN_MESSAGE} color="error">
                  <ChatIcon
                    sx={{
                      ml: 2,
                      display: { xs: "none", sm: "none", md: "block" },
                      color: "blue",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      dispatch(getRecognization(
                        {
                          _id:STORE?.getProfileData?.userData?._id,
                          name:STORE?.getProfileData?.userData?.name,
                          email:STORE?.getProfileData?.userData?.email,
                          pdf:STORE?.getProfileData?.userData?.pdf
                        }))
                      navigate(`chat`);
                    }}
                  />
                </Badge>
              </Box> */}

              <Box
                sx={{
                  gap: "1rem",
                  display: { xs: "none", sm: "none", md: "flex" },
                }}
              >
                <NotificationIcon
                  sx={{
                    mr: 2,
                    display: { xs: "none", sm: "none", md: "block" },
                  }}
                />

                <IconButton
                  // onClick={handleClick}
                  size="small"
                  sx={{ display: { xs: "none", sm: "none", md: "block" } }}
                  aria-controls={open ? "account-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? "true" : undefined}
                >
                  {STORE?.getProfileData?.userData?.pdf === null ? (
                    <Avatar
                      sx={{
                        width: 37,
                        height: 37,
                        border: "2px solid #1C6DD0",
                      }}
                      src={`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`}
                    ></Avatar>
                  ) : (
                    <div
                      onClick={() =>
                        navigate(
                          `UserProfile/${STORE.getProfileData?.userData?.name}/${STORE.getProfileData?.userData?._id}`
                        )
                      }
                    >
                      <Avatar
                        sx={{
                          width: 37,
                          height: 37,
                          border: "2px solid #1C6DD0",
                        }}
                        src={getImgURL(STORE?.getProfileData?.userData?.pdf)}
                      ></Avatar>
                    </div>
                  )}
                </IconButton>
              </Box>
            </Toolbar>
          </AppBar>
        </HideOnScroll>
        <Box component="nav">
          <Drawer
            container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
            sx={{
              display: { xs: "block", sm: "block", md: "none" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
                width: drawerWidth,
              },
            }}
          >
            {drawer}
          </Drawer>
        </Box>
        {/* <Box component="main" sx={{ p: 3 }}>
        <Toolbar />
        
      </Box> */}
      </Box>

      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            bgcolor: "#181823",
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
              border: "2px solid #1C6DD0",
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "#181823",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        {sidebarItem.map((item, index) =>
          index != 0 ? (
            <MenuItem
              key={index}
              sx={{ color: "white" }}
              // onClick={handleClose}
              onClick={() => {
                setSelect(index);
                // navigate(item.link);
                navigate(
                  item.link === "UserProfile"
                    ? `${item.link}/${STORE.getProfileData?.userData?.name}/${STORE.getProfileData?.userData?._id}`
                    : item.link === "LogOut"
                    ? logOut()
                    : item.link === "WriterStoryDetails"
                    ? `${item.link}/${STORE.getProfileData?.userData?._id}`
                    : item.link
                );
                setAnchorEl(null);
              }}
            >
              {index == 3 ? (
                STORE?.getProfileData?.userData?.pdf === null ? (
                  <Avatar
                    sx={{ width: 37, height: 37, border: "2px solid #1C6DD0" }}
                    src={`https://www.pngall.com/wp-content/uploads/12/Avatar-Profile-PNG-Photos.png`}
                  ></Avatar>
                ) : (
                  <Avatar
                    src={getImgURL(STORE?.getProfileData?.userData?.pdf)}
                  ></Avatar>
                )
              ) : (
                // <Avatar
                //   src={getImage(STORE?.getProfileData?.userData?.pdf)}
                // />
                item.icon
              )}

              {item.listItem}
            </MenuItem>
          ) : (
            ""
          )
        )}
        {/* {openModal && <LogoutModal setOpenModal={setOpenModal} />} */}
      </Menu>
      {/* {openModal && <LogoutModal setOpenModal={setOpenModal} />} */}
    </>
  );
}
// navigate(item.link==="UserProfile"?`${item.link}/${STORE.getProfileData?.userData?.name}`:item.link)

// onClick=(() => {setOpenModal(false)})
