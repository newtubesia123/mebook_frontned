import React, { useEffect } from 'react'
import './logoutmodal.scss'
import { Button } from '@mui/material';
import { useDispatch } from "react-redux";
import { getUserData, } from "../../Action";
import { toast } from 'react-toastify';

const LogoutModal = ({ setOpenModal }) => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUserData());
    }, [dispatch]);
    const logOut = () => {
        localStorage.removeItem("token");
        dispatch({ type: "CALLING__PROGRESS", payload: 10 })
        setTimeout(() => {
            dispatch({ type: "COMPLETE__PROGRESS", payload: 100 })
            window.location.reload()
            // toast.success("Logout succesfully", { pauseOnHover: false, position: "top-center", autoClose: 2000, })
        }, 500)
        // toast.success("Logout succesfully", { pauseOnHover: false, position: "top-center", autoClose: 2000, })
    };

    return (
        <>
            <div id="wrap">
                <div id="box">
                    <h5 style={{color:"#eee"}}>Are you sure want to logout ?</h5>
                    <div className='btns'>
                        <Button variant="contained" size="large" color="success" onClick={() => { logOut() }}>Logout</Button>
                        <Button variant="outlined" size="large" color="error" onClick={() => { setOpenModal(false) }}>Cancel</Button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default LogoutModal