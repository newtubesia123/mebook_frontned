import React, { useState } from "react";
import "./SearchBar.css";
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
// import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import YoutubeSearchedForIcon from '@mui/icons-material/YoutubeSearchedFor';
import { CircularProgress } from "@mui/material";
import axios from "axios";
import { SERVER } from "../../../server/server";
import { toast } from "react-toastify";

function SearchBar({ placeholder,setSearchedData,wordEntered,setWordEntered, handleFilter,filteredData,setFilteredData,setCategoryData,setDataResult,setSearchLoading,searchLoading,statusData,setStatusData }) {

   
  
    const [loading , setLoading] = useState(false)
    const controller = new AbortController();




    const clearInput = () => {
        setFilteredData([]);
        setWordEntered("");
        setCategoryData([])
        setStatusData([])
    };

    function handleFocus () {
        setDataResult(true)
    }
    function handleBlus () {
        setTimeout(()=>{
            setDataResult(false)
            setFilteredData([]);
            setCategoryData([])
            setStatusData([])
        },300)
    }



React.useEffect(() => {
    
        setSearchLoading(true)
        let token = localStorage.getItem('token')
        setTimeout(()=>{
        axios.get(`${SERVER}/dashboard/searchUser?name=${wordEntered}`,
        {
            headers: { Authorization: `bearer ${token}` },
            signal: controller.signal
        }).then((response)=>{
           if(response.data.errorCode===200){
            // console.log('response.data',response.data)
            setSearchedData(response.data)
            setSearchLoading(false)
           }

        }).catch(()=>{
            setSearchLoading(false)
        })
        },150)

    return () => {
        controller.abort()
        setSearchLoading(false)
    };
    }, [wordEntered]);




    return (
         <div className="search">
            <div className="searchInputs" >
                <input
                    type="text"
                    placeholder={placeholder}
                    value={wordEntered}
                    onChange={(e)=>handleFilter(e)}
                    onFocus={handleFocus}
                    onBlur={handleBlus}
                />
                <div className="searchIcon" >
                    {
                    searchLoading?(
                        <CircularProgress size={25} />
                    )
                    :
                    !wordEntered ? 
                    (
                        <SearchIcon />
                    ) 
                    :
                    (
                    <CloseIcon id="clearBtn" onClick={clearInput} />

                    )
                }
                </div>
            </div>
          
        </div>
        
       
    );
}

export default SearchBar;







// {filteredData.length != 0 && (
//     <div className="dataResult">
//         {filteredData.slice(0, 15).map((value, key) => {
//             return (
//                 <a className="dataItem" href={value.link} target="_blank">
//                     <p>{value.title} </p>
//                 </a>
//             );
//         })}
//     </div>
// )}
