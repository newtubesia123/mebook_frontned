import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import MenuItem from '@mui/material/MenuItem';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import NotificationsIcon from '@mui/icons-material/Notifications';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Button } from '@mui/material';
import { Box } from '@mui/system';
import { useDispatch, useSelector } from 'react-redux';
import { handleNotification, notificationRequest } from '../../../Action';
import { getImgURL } from '../../../util/getImgUrl';
import ThemeDialog from '../../../assets/ThemeDialog';
import { useNavigate } from 'react-router-dom';


const unSeenStyle = { display: "flex", gap: "1rem", backgroundColor: "rgba(128, 128, 128, 0.607)", color: "black", "&:hover": { backgroundColor: "rgba(128, 128, 128, 0.307)" } }

const seenStyle = { display: "flex", gap: "1rem" }


export default function NotificationIcon() {
    const [open, setOpen] = React.useState(false)
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
    const [dialogData, setDialogData] = React.useState(false)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const STORE_NOTIFICATION = useSelector((state) => state.notificationRequest.data)
    const STORE_UNSEEN_NOTIFICATION = useSelector((state) => state.notificationRequest.unseenNotification)
    const isMenuOpen = Boolean(anchorEl);
    const [debounceStage, setDebounceStage] = React.useState(0)
    const [seeMore, setSeeMore] = React.useState(true)
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
    const [seen, setSeen] = React.useState([]);
    const [noData, setNodata] = React.useState(false)
    const menuId = 'primary-notification-menu';
    const mobileMenuId = 'primary-search-account-menu-mobile';

    const newNotficationArr = [...STORE_NOTIFICATION]
    // console.log(newNotficationArr, "newNotficationArr")
    // console.log(STORE_NOTIFICATION, "STORE_NOTIFICATION")


    const handleProfileMenuOpen = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
        handleMobileMenuClose();
        setSeeMore(true)
    };

    const handleMobileMenuOpen = (event) => {
        setMobileMoreAnchorEl(event.currentTarget);
    };
    function handleDialogData(data, notificationId) {
        setDialogData(data)
        handleOpen()
        dispatch(handleNotification(notificationId))
    }
    function handleOpen() {
        setOpen(true)
    }
    function handleClose() {
        setOpen(false)
        handleMenuClose()
        handleMobileMenuClose()
        setDialogData(false)
        setSeeMore(true)

    }
    function handleUserFollow(name, id, notificationId) {
        navigate(`/MainDashboard/UserProfile/${name}/${id}`)
        handleMenuClose()
        handleMobileMenuClose()
        dispatch(handleNotification(notificationId))
        setSeen([...seen, notificationId])
    }



    const renderMenu = (
        <Menu sx={{ marginTop: "2rem" }}
            anchorEl={anchorEl}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            id={menuId}
            keepMounted
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            {seeMore ?
                newNotficationArr && newNotficationArr.length ?
                    newNotficationArr.splice(0, 3).map((data, idx) => (
                        data.workId ?  // ye section work like dikhane wala hai
                            !data.haveSeen ?  // agar seen nhi hua 
                                <MenuItem key={idx} onClick={() => handleDialogData(data.workId, data._id)} sx={{ display: "flex", gap: "1rem", backgroundColor: "rgba(128, 128, 128, 0.607)", color: "black", "&:hover": { backgroundColor: "rgba(128, 128, 128, 0.307)" } }}>
                                    <div onClick={(e) => {
                                        e.stopPropagation()
                                        navigate(`UserProfile/${data?.otherUserId?.name}/${data.otherUserId?._id}`)
                                    }}>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.otherUserId?.pdf)}>
                                        </Avatar>
                                    </div>
                                    {data.otherUserId?.name} Liked Your Work
                                    <div>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.workId.coverPicture)}>
                                        </Avatar>
                                    </div>
                                </MenuItem>
                                :    // agar seen ho gaya
                                <MenuItem key={idx} onClick={() => handleDialogData(data.workId, data._id)} sx={{ display: "flex", gap: "1rem" }}>
                                    <div onClick={(e) => {
                                        e.stopPropagation()
                                        navigate(`UserProfile/${data.otherUserId?.name}/${data.otherUserId?._id}`)
                                    }}>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.otherUserId?.pdf)}>
                                        </Avatar>
                                    </div>
                                    {data.otherUserId?.name} Liked Your Work
                                    <div>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.workId.coverPicture)}>
                                        </Avatar>
                                    </div>
                                </MenuItem>
                            :  // ye section follower dikahane wala hai
                            !data.haveSeen ?  // agar seen nahi hua
                                <MenuItem key={idx}
                                    onClick={() => handleUserFollow(data.otherUserId?.name, data.otherUserId?._id, data._id)}
                                    sx={seen.includes(data._id) ? seenStyle : unSeenStyle}
                                >
                                    <div>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.otherUserId?.pdf)}>
                                        </Avatar>
                                    </div>
                                    {data.otherUserId?.name} started following you<CheckCircleIcon style={{ color: "#2F58CD" }} />

                                </MenuItem>
                                :   // agar seen ho gaya
                                <MenuItem
                                    key={idx}
                                    onClick={() => handleUserFollow(data.otherUserId?.name, data.otherUserId?._id, data._id)}
                                    sx={{ display: "flex", gap: "1rem" }}>
                                    <div>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.otherUserId?.pdf)}>
                                        </Avatar>
                                    </div>
                                    {data.otherUserId?.name} started following you<CheckCircleIcon style={{ color: "#2F58CD" }} />
                                </MenuItem>
                    ))
                    :
                    <MenuItem onClick={handleMenuClose} sx={{ display: "flex", gap: "1rem" }}>
                        No Notification
                    </MenuItem>
                :
                STORE_NOTIFICATION && STORE_NOTIFICATION.length ?
                    STORE_NOTIFICATION.map((data, idx) => (
                        data.workId ?  // ye section work like dikhane wala hai
                            !data.haveSeen ?  // agar seen nhi hua 
                                <MenuItem key={idx} onClick={() => handleDialogData(data.workId, data._id)} sx={{ display: "flex", gap: "1rem", backgroundColor: "rgba(128, 128, 128, 0.607)", color: "black", "&:hover": { backgroundColor: "rgba(128, 128, 128, 0.307)" } }}>
                                    <div onClick={(e) => {
                                        e.stopPropagation()
                                        navigate(`UserProfile/${data.otherUserId?.name}/${data.otherUserId?._id}`)
                                    }}>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.otherUserId?.pdf)}>
                                        </Avatar>
                                    </div>
                                    {data.otherUserId?.name} Liked Your Work
                                    <Avatar sx={{ width: 35, height: 35 }}
                                        src={getImgURL(data.workId.coverPicture)}>
                                    </Avatar>
                                </MenuItem>
                                :    // agar seen ho gaya
                                <MenuItem key={idx} onClick={() => handleDialogData(data.workId, data._id)} sx={{ display: "flex", gap: "1rem" }}>
                                    <div onClick={(e) => {
                                        e.stopPropagation()
                                        navigate(`UserProfile/${data.otherUserId?.name}/${data.otherUserId?._id}`)
                                    }}>
                                        <Avatar sx={{ width: 35, height: 35 }}
                                            src={getImgURL(data.otherUserId?.pdf)}>
                                        </Avatar>
                                    </div>
                                    {data.otherUserId?.name} Liked Your Work
                                    <Avatar sx={{ width: 35, height: 35 }}
                                        src={getImgURL(data.workId.coverPicture)}>
                                    </Avatar>
                                </MenuItem>
                            :  // ye section follower dikahane wala hai
                            !data.haveSeen ?  // agar seen nahi hua
                                <MenuItem key={idx}
                                    onClick={() => handleUserFollow(data.otherUserId?.name, data.otherUserId?._id, data._id)}
                                    sx={seen.includes(data._id) ? seenStyle : unSeenStyle}
                                >

                                    <Avatar sx={{ width: 35, height: 35 }}
                                        src={getImgURL(data.otherUserId?.pdf)}>
                                    </Avatar>

                                    {data.otherUserId?.name} started following you<CheckCircleIcon style={{ color: "#2F58CD" }} />

                                </MenuItem>
                                :   // agar seen ho gaya
                                <MenuItem
                                    key={idx}
                                    onClick={() => handleUserFollow(data.otherUserId?.name, data.otherUserId?._id, data._id)}
                                    sx={{ display: "flex", gap: "1rem" }}>
                                    <Avatar sx={{ width: 35, height: 35 }}
                                        src={getImgURL(data.otherUserId?.pdf)}>
                                    </Avatar>
                                    {data.otherUserId?.name} started following you<CheckCircleIcon style={{ color: "#2F58CD" }} />
                                </MenuItem>
                    ))
                    :
                    <MenuItem onClick={handleMenuClose} sx={{ display: "flex", gap: "1rem" }}>
                        No Notification
                    </MenuItem>
            }
            {
                !noData && seeMore && newNotficationArr.length > 3 &&
                <Box style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                    <Button varient="text" onClick={() => setSeeMore(false)}>See more</Button>
                </Box>
            }

        </Menu>
    );


    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
            <MenuItem onClick={handleProfileMenuOpen}>
                <IconButton
                    size="large"
                    aria-label="show new notifications"
                    aria-controls="show new notifications"
                    aria-haspopup="true"
                    color="black"
                >
                    <AccountCircle />
                </IconButton>
                <p>Profile</p>
            </MenuItem>
        </Menu>
    );

    React.useEffect(() => {
        dispatch(notificationRequest(debounceStage, { send: (e) => setNodata(true) }))
    }, [debounceStage])


    // React.useEffect(()=>{
    //     const timeLapse = setInterval(()=>{
    //         dispatch(notificationRequest(0,{send:()=>console.log("calling 0")}))
    //     },30000)
    //     return ()=>clearInterval(timeLapse)
    // },[])

    //     React.useEffect(()=>{
    //         console.log("dialogData",dialogData)
    //     },[dialogData])
    //     React.useEffect(()=>{
    // console.log("STORE_NOTIFICATION",STORE_NOTIFICATION)
    //     },[STORE_NOTIFICATION])

    return (
        <>
            {
                open ?
                    <ThemeDialog
                        open={open}
                        forOpen={handleOpen}
                        handleClose={handleClose}
                        work={dialogData} />
                    : ""
            }

            <IconButton
                size="large"
                edge="end"
                aria-label="show new notifications"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="black"
            >
                <Badge badgeContent={STORE_UNSEEN_NOTIFICATION} color="error">
                    <NotificationsIcon sx={{ color: "#1C6DD0", }} />
                </Badge>
            </IconButton>
            {renderMobileMenu}
            {renderMenu}
        </>
    );
}