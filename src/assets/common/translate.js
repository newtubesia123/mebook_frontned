import axios from "axios";

export const translate = (value) => {

  const params = new URLSearchParams();
  params.append('q', value);
  params.append('source', 'en');
  params.append('target', 'hi');
  params.append('api_key', 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx');

  axios.post('https://libretranslate.de/translate',params, {
    headers: {
      'accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  }).then(res=>{
    console.log(res.data)
    // document.write(res?.data?.translatedText)
  })
};
