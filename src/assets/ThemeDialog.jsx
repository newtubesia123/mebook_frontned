import React, { useState } from 'react'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { useTheme } from '@mui/material/styles';
import { newButton } from '../Component new/background';
import { getImgURL } from '../util/getImgUrl';
import useMediaQuery from '@mui/material/useMediaQuery';
import CardMedia from '@mui/material/CardMedia';
import CloseIcon from '@mui/icons-material/Close';
import { useNavigate } from "react-router-dom";

function ThemeDialog({
    open,
    work,
    forOpen,
    handleClose
}) {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    const navigate = useNavigate();

  return (
    <Dialog
    fullScreen={fullScreen}
    open={open}
    onClose={handleClose}
    aria-labelledby="responsive-dialog-title"
>
    <DialogContent sx={{ display: 'flex', flexDirection: 'row', gap: "1rem",position:'relative' ,backgroundColor:"black",border:'1px dotted white',height:{md:"350px",sm:"100%"},width:{md:"600px",sm:"100%"}}}>
                {/* close button */}
                <CloseIcon onClick={handleClose} sx={{ cursor: "pointer", color:'white', position: "absolute", right: "1%", }} />
            <CardMedia
                component="img"
                sx={{ width: "250px", height: "300px", borderRadius: "3px" }}
                image={getImgURL(work?.coverPicture)}
            />
            <DialogContentText sx={{ color:'white',marginTop:'2rem' }}>
                {/* card text */}
                <Typography gutterBottom variant="h6" component="div" sx={{ width: "100%", }}>
                    <span><b>Tittle&nbsp;:  </b></span>  {work?.title}
                </Typography>
                <div style={{ height: "60%" }}>
                    <p>
                        <b>Description &nbsp;:</b> {work?.description !== "" ? work?.description?.slice(0, 30).join(" ") : ""}...
                    </p>
                </div>
                <div style={{ height: "12%", position:'absolute',bottom:'5%',right:'2%', marginTop:"10px" }}>
                    <Button sx={{...newButton }}
                        onClick={() => { 
                            handleClose()
                            navigate(`WorkView/${work?.userId !== "" ? work?.userId : ""}/${work?._id !== "" ? work?._id : ""}`) 
                        }}
                    >See More</Button>
                </div>
            </DialogContentText>

    </DialogContent>
</Dialog>
  )
}

export default ThemeDialog
